dnl @synopsis AC_COND_WITH(PACKAGE [,DEFAULT])
dnl
dnl Actually used after an AC_ARG_WITH(PKG,...) option-directive,
dnl where AC_ARG_WITH is a part of the standard autoconf to define a
dnl `configure` --with-PKG option.
dnl
dnl The AC_COND_WITH(PKG) will use the $with_PKG var to define WITH_PKG and
dnl WITHOUT_PKG substitutions (AC_SUBST), that are either '' or '#' -
dnl depending whether the var was "no" or not (probably 'yes', or a value);
dnl it will also declare WITHVAL_PKG for use when someone wanted to set a val
dnl other than just "yes". And there is a WITHDEF_PKG that expands to a
dnl C-precompiler definition of the form -DWITH_PKG or  -DWITH_PKG=\"value\"
dnl (n.b.: the PKG *is*  uppercased if in lowercase and "-" translit to "_")
dnl
dnl This macro is most handily in making Makefile.in/Makefile.am that have
dnl a set of if-with declarations that can be defined as follows:
dnl
dnl  CFLAGS = -Wall @WITHOUT_FLOAT@ -msoft-float # --without-float
dnl  @WITH_FLOAT@ LIBS += -lm              # --with-float
dnl  DEFS += -DNDEBUG @WITHDEF_MY_PKG@     # --with-my-pkg="/usr/lib"
dnl  DEFS += @WITHVAL_DEFS@                # --with-defs="-DLOGLEVEL=6"
dnl
dnl Example configure.in:
dnl
dnl  AC_ARG_WITH(float,
dnl  [ --with-float,       with float words support])
dnl  AC_COND_WITH(float,no)
dnl
dnl Extened notes:
dnl
dnl 1. The idea comes from AM_CONDITIONAL but it is much easier to use,
dnl and unlike automake's ifcond, the Makefile.am will work as a
dnl normal $(MAKE) -f Makefile.am makefile.
dnl
dnl 2. The @VALS@ are parsed over by automake so automake will see all
dnl the filenames and definitions that follow @WITH_FLOAT@, so that
dnl the AC_COND_WITH user can see additional message if they apply.
dnl
dnl 3. In this m4-part, there's a AC_ARG_COND_WITH with the synopsis of
dnl AC_ARG_WITH and an implicit following AC_COND_WITH   =:-)
dnl
dnl 4. There is an AC_ARG_COND_WITH_DEFINE that will emit an
dnl implicit AC_DEFINE that is actually seen by autoheader, even
dnl generated with the correct name and comment, for config.h.in
dnl
dnl Some non-autoconf coders tend to create "editable" Makefile where
dnl they have out-commented lines with an example (additional) definition.
dnl Each of these can be replaced with a three-liner in configure.in as
dnl shown above. Starting to use AC_COND_WITH will soon lead you to
dnl provide a dozen --with-option rules for the `configure` user. Do it!
dnl
dnl @version $Id: ac_cond_with.m4 8638 2002-03-04 17:00:45Z rmd $
dnl @author Guido Draheim <guidod@gmx.de>
dnl
AC_DEFUN([AC_COND_WITH],
[changequote(<<, >>)dnl
dnl the names to be defined...
define(<<WITH_VAR>>,       patsubst(with_$1, -, _))dnl
define(<<AC_VAR_WITH>>,    patsubst(translit(with_$1, [a-z], [A-Z]), -, _))dnl
define(<<AC_VAR_WITHOUT>>, patsubst(translit(without_$1, [a-z], [A-Z]), -, _))dnl
define(<<AC_VAR_WITHVAL>>, patsubst(translit(withval_$1, [a-z], [A-Z]), -, _))dnl
define(<<AC_VAR_WITHDEF>>, patsubst(translit(withdef_$1, [a-z], [A-Z]), -, _))dnl
changequote([, ])dnl
AC_SUBST(AC_VAR_WITH)
AC_SUBST(AC_VAR_WITHOUT)
AC_SUBST(AC_VAR_WITHVAL)
AC_SUBST(AC_VAR_WITHDEF)
if test -z "$WITH_VAR" ; then WITH_VAR=`echo ifelse([$2], , no, [$2])` ; fi
if test "$WITH_VAR" != "no"; then
  AC_VAR_WITH=    ; AC_VAR_WITHOUT='#'
  case "$WITH_VAR" in
    yes)      AC_VAR_WITHVAL= ;          AC_VAR_WITHDEF="-D""AC_VAR_WITH" ;;
      *)      AC_VAR_WITHVAL="$WITH_VAR" ;  AC_VAR_WITHDEF="-D""AC_VAR_WITH="'"'$WITH_VAR'"' ;;
  esac
else
  AC_VAR_WITH='#' ;  AC_VAR_WITHOUT=
  AC_VAR_WITHVAL= ;  AC_VAR_WITHDEF=
fi
undefine([AC_VAR_WITH])dnl
undefine([AC_VAR_WITHOUT])dnl
undefine([AC_VAR_WITHVAL])dnl
undefine([AC_VAR_WITHDEF])dnl
undefine([WITH_VAR])dnl
])

AC_DEFUN([AC_ARG_COND_WITH],
[dnl
AC_ARG_WITH([$1],[$2],[$3],[$4],[$5])
# done with AC_ARG_WITH, now do AC_COND_WITH (rather than AM_CONDITIONAL)
AC_COND_WITH([$1])
])

dnl and the same version as AC_COND_WITH but including the
dnl AC_DEFINE for WITH_PACKAGE
AC_DEFUN([AC_COND_WITH_DEFINE],
[changequote(<<, >>)dnl
dnl the names to be defined...
define(<<WITH_VAR>>,       patsubst(with_$1, -, _))dnl
define(<<AC_VAR_WITH>>,    patsubst(translit(with_$1, [a-z], [A-Z]), -, _))dnl
define(<<AC_VAR_WITHOUT>>, patsubst(translit(without_$1, [a-z], [A-Z]), -, _))dnl
define(<<AC_VAR_WITHVAL>>, patsubst(translit(withval_$1, [a-z], [A-Z]), -, _))dnl
define(<<AC_VAR_WITHDEF>>, patsubst(translit(withdef_$1, [a-z], [A-Z]), -, _))dnl
changequote([, ])dnl
AC_SUBST(AC_VAR_WITH)
AC_SUBST(AC_VAR_WITHOUT)
AC_SUBST(AC_VAR_WITHVAL)
AC_SUBST(AC_VAR_WITHDEF)
if test -z "$WITH_VAR" ; then WITH_VAR=`echo ifelse([$2], , no, [$2])` ; fi
if test "$WITH_VAR" != "no"; then
  AC_VAR_WITH=    ; AC_VAR_WITHOUT='#'
  case "$WITH_VAR" in
    yes)      AC_VAR_WITHVAL= ;          AC_VAR_WITHDEF="-D""AC_VAR_WITH" ;;
      *)      AC_VAR_WITHVAL="$WITH_VAR" ;  AC_VAR_WITHDEF="-D""AC_VAR_WITH="'"'$WITH_VAR'"' ;;
  esac
else
  AC_VAR_WITH='#' ;  AC_VAR_WITHOUT=
  AC_VAR_WITHVAL= ;  AC_VAR_WITHDEF=
fi
dnl -- the additional line is here --
if test "u$WITH_VAR" != "uno" ; then
        AC_DEFINE_UNQUOTED(AC_VAR_WITH, $AC_VAR_WITHVAL, "--with-$1")
fi dnl
undefine([AC_VAR_WITH])dnl
undefine([AC_VAR_WITHOUT])dnl
undefine([AC_VAR_WITHVAL])dnl
undefine([AC_VAR_WITHDEF])dnl
undefine([WITH_VAR])dnl
])
