/*  File: acedb.h
 *  Author: Jean Thierry-Mieg (mieg@mrc-lmb.cam.ac.uk)
 *  Copyright (C) J Thierry-Mieg and R Durbin, 1991
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (MRC LMB, UK) rd@mrc-lmb.cam.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.cnrs-mop.fr
 *
 * Description:
 * Exported functions:
 * HISTORY:
 * Last edited: Mar 14 14:35 1995 (srk)
 * * Oct 21 14:01 1991 (mieg): added overflow  protection in KEYMAKE 
 * Created: Mon Oct 21 14:01:19 1991 (mieg)
 *-------------------------------------------------------------------
 */

/* @(#)acedb.h	1.4    3/14/95 */
 
             /*********************************************/
             /* ACeDB.h                                   */
             /* type definitions and size limits          */
             /*********************************************/
 
#ifndef DEF_ACeDB_h
#define DEF_ACeDB_h
 
#include "regular.h"           /*contains KEY*/
#include "keyset.h"            /*contains KEYSET, Array, Stack */

void help(void) ;
void helpOn(char *text) ; 
void mainActivity(char * text) ;

#define KEYMAKE(t,i)  ((KEY)( (((KEY) (t))<<24) | ( ((KEY) (i)) & 0xffffffL) ))
#define KEYKEY(kk)  ((KEY)( ((KEY) (kk)) & ((KEY) 0xffffffL) ))
#define class(kk)  ((int)( ((KEY) (kk))>>24 ))
 
char* name(KEY k);     /*returns the name or the word "NULL" in case of a wrong key */
char* className(KEY k) ; /* returns the name of the class of key */

typedef BOOL (*DisplayFunc)(KEY key, KEY from, BOOL isOld) ;
typedef BOOL (*ParseFunc)(int level, KEY key) ;
typedef BOOL (*DumpFunc)(FILE *f, Stack s, KEY k) ;
typedef void (*BlockFunc)(KEY) ;
#endif
 

