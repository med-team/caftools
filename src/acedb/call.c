/*  File: call.c
 *  Author: Richard Durbin (rd@sanger.ac.uk)
 *  Copyright (C) J Thierry-Mieg and R Durbin, 1994
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (MRC LMB, UK) rd@mrc-lmb.cam.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.cnrs-mop.fr
 *
 * Description: provides hooks to optional code, basically a call by
 			name dispatcher
	        plus wscripts/ interface
 * Exported functions:
 * HISTORY:
 * Last edited: Jun 13 12:01 2001 (rmd)
 * * Mar  3 15:51 1996 (rd)
 * * Oct  6 20:52 1994 (rd): callMosaic
 * Created: Mon Oct  3 14:05:37 1994 (rd)
 *-------------------------------------------------------------------
 */

/* @(#)call.c	1.16 10/12/96 */

#include <stdarg.h>   /* for va_start */
#include <acedb.h>
#include <call.h>
#include <mytime.h>

/************************* call by name package *********************/

typedef struct
{ char *name ;
  CallFunc func ;
} CALL ;

static Array calls ;	/* array of CALL to store registered routines */

/************************************/

static int callOrder (void *a, void *b) 
{ return strcmp (((CALL*)a)->name, ((CALL*)b)->name) ; }

void callRegister (char *name, CallFunc func)
{
  CALL c ;

  if (!calls)
    calls = arrayCreate (16, CALL) ;
  c.name = name ; c.func = func ;
  if (!arrayInsert (calls, &c, callOrder))
    messcrash ("Duplicate callRegister with name %s", name) ;
}

BOOL callExists (char *name)
{
  CALL c ;
  int i;

  c.name = name ;
  return (calls && arrayFind (calls, &c, &i, callOrder)) ;
}


BOOL call (char *name, ...)
{
  va_list args ;
  CALL c ;
  int i ;

  c.name = name ;
  if (calls && arrayFind (calls, &c, &i, callOrder))
    { va_start(args, name) ;
      (*(arr(calls,i,CALL).func))(args) ;
      va_end(args) ;
      return TRUE ;
    }

  return FALSE ;
}

/***************** routines to run external programs *******************/

/* ALL calls to system() and popen() should be through these routines
** First, this makes it easier for the Macintosh to handle them.
** Second, by using wscripts as an intermediate one can remove system
**   dependency in the name, and even output style, of commands.
** Third, if not running in ACEDB it does not look for wscripts...
*/

static char *buildCommand (char *dir, char *script, char *args)
{
  static Stack command = 0 ;
#ifdef ACEDB
  char *cp ;
  static Stack s = 0 ;		/* don't use messprintf() - often used to make args */
  s = stackReCreate (s, 32) ; 
  catText (s, "wscripts/") ; 
  catText (s, script) ;
  if (cp = filName (stackText (s, 0), 0, "x"))
    script = cp ;  /* mieg else fall back on direct unix call */
#endif

  command = stackReCreate (command, 128) ;
  if (dir)
    { catText (command, "cd ") ;
      catText (command, dir) ;
      catText (command, "; ") ;
    }
  catText (command, script) ;
  if (args)
    { catText (command, " ") ;
      catText (command, args) ;
    }
  return stackText (command, 0) ;
}

int callCdScript (char *dir, char *script, char *args)
{
#if !defined(MACINTOSH)
  return system (buildCommand (dir, script, args)) ;
#else
  return -1 ;
#endif
}

int callScript (char *script, char *args)
{
  return callCdScript (0, script, args) ;
}

FILE* callCdScriptPipe (char *dir, char *script, char *args)
{
#if !(defined(MACINTOSH) || defined(WIN32))
  return popen (buildCommand (dir, script, args), "r" ) ;
#elif defined(WIN32)
  return _popen (buildCommand (dir, script, args), "rt")  ;
#else	/* defined(MACINTOSH) */
  return 0 ;
#endif	/* defined(MACINTOSH) */
}

FILE* callScriptPipe (char *script, char *args)
{
  return callCdScriptPipe (0, script, args) ;
}

/****************** Mosaic handler ***********************/
/** start a single mosaic process, then send it signals **/

	/* Priluski, Kocab, mieg, ace94 workshop 
	   transferred to call.c from help.c by rd
	 */

#if !(defined(MACINTOSH) || defined(WIN32))

#include <signal.h>
#include <sys/wait.h>
#include <time.h>

static int  M_pid         = 0; /* filled after fork Mosaic */
static int  M_status      = 0; /* status of Mosaic process */
static int  M_code        = 0;
#define SLEEP_MOSAIC        15  /* time, which Mosaic needs for init */

#define M_STOPPED           1
#define M_EXITED            2
#define M_TERMINATED        3

#ifndef WNOHANG 
  /* should be in sys/wait.h but it does not work on IBM */
#define WNOHANG		0x1
#define WUNTRACED       0x2
#endif

void SIGCHLD_handler (int sig) 
{
  int wstatus, xstatus ;
  int pid;
  
  while ((pid = waitpid (-1, &wstatus, WNOHANG | WUNTRACED)) > 0) 
    { xstatus = wstatus & 0177 ;
      if (pid == M_pid) 
	{ if (xstatus == 0177) 
	    { M_status = M_STOPPED; 
	      M_code = (wstatus >> 8 ) & 0377;
	      if (M_code > 0177) 
		M_code = ( -1 ^ 0377 ) | M_code;
/* M_code contains the number of the signal that caused the process to stop */
	    } 
	  else if (!xstatus)
	    { M_status = M_EXITED;
	      M_code   = ( wstatus >> 8 ) & 0377;
	      if (M_code > 0177) 
		M_code = ( -1 ^ 0377 ) | M_code;
	      if (M_code == 1)
		messout ("Could not start mosaic; please check wscripts/Mosaic") ;
/* M_code contains the exit status of child process */
	    } 
	  else
	    { M_status = M_TERMINATED;
	      M_code   = ( wstatus & 0177 );
/* M_code contains the number of the signal that caused the termination */
	    }
	}
    }
  signal (sig, &SIGCHLD_handler) ;
}

BOOL callMosaic (char *url)
{
  static time_t M_start_time = 0;
  static char MosaicPID[32] = {0}; /* string form of M_pid */
  time_t t = time(0) ;
  FILE *fil ;
  static BOOL execMosaicFails = FALSE ;

  if (execMosaicFails)
    return FALSE ;
 
  if (t - M_start_time < SLEEP_MOSAIC)
    return FALSE ;		/* mosaic is a bit slow during startup */

  if (!*MosaicPID || M_status)	/* start Mosaic if it is not running */
    {
#ifdef ACEDB
      mainActivity ("Looking for Mosaic") ;
#endif
      signal (SIGCHLD, &SIGCHLD_handler) ; /* could be called only once */
      M_status = 0 ;
      switch (M_pid = fork())
	{
	case -1 : 
	  messcrash ("fork failed in callMosaic()") ; 
	case 0 :		/* forked child, substitute mosaic now */
	  execlp (buildCommand (0, "Mosaic", 0),
		  "ACEDB_MOSAIC", url, NULL) ;
	  exit (-17) ;	/* exec should never return */
	default:		/* parent, proceed */
	  callScript ("sleep", "15") ;
	  if (M_status)		/* exec failed or Mosaic died quickly */
	    { if (M_code == -17)
		execMosaicFails = TRUE ;
	      printf ("status %d, code %d\n", M_status, M_code) ;
	      return 0 ;
	    }
	  sprintf (MosaicPID, "%d", M_pid) ;
	  M_start_time = time (0) ;
	}
      return TRUE ;
    }
  else if ((fil = filopen ("/tmp/Mosaic", MosaicPID, "w")) != 0)
    { fprintf (fil,"goto\n") ;  /* deliver URL to existing Mosaic */
      fprintf (fil,"%s\n", url);
      filclose (fil) ;
      kill (M_pid, SIGUSR1) ;	/* notify Mosaic to pick up message */
      return TRUE ;
    }
  else
    return FALSE ;
}

#elif !defined(WIN32) /* MACINTOSH ONLY, WIN32 callMosaic() implemented elsewhere */

BOOL callMosaic (char *url)
{
  return FALSE ;
}

#endif /* MACINTOSH ONLY */

/********************** End of file **********************/
