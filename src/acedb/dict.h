/*  File: dict.h
 *  Author: Richard Durbin (rd@sanger.ac.uk)
 *  Copyright (C) J Thierry-Mieg and R Durbin, 1995
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (MRC LMB, UK) rd@mrc-lmb.cam.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.cnrs-mop.fr
 *
 * Description: public header for cut-out lex package in dict.c
 * Exported functions:
 * HISTORY:
 * Last edited: Nov 23 11:58 1995 (srk)
 * Created: Tue Jan 17 17:34:44 1995 (rd)
 *-------------------------------------------------------------------
 */

/* @(#)dict.h	1.2 11/23/95 */
#ifndef DICT_H
#define DICT_H

#include "array.h"

	/* The DICT structure is private to lexhash.c
	   DO NOT LOOK AT OR TOUCH IT IN CLIENT CODE!!
	   Only use it via the subroutine interface.
	*/

typedef struct {
  int dim ;
  int max ;
  Array table ;			/* hash table */
  Array names ;			/* mark in text Stack per name */
  Stack nameText ;		/* holds names themselves */
} DICT ;

DICT *dictCreate (int size) ;
void dictDestroy (DICT *dict) ;
BOOL dictFind (DICT *dict, char *s, int *ip) ;
BOOL dictAdd (DICT *dict, char *s, int *ip) ;
char *dictName (DICT *dict, int i) ;
int dictMax (DICT *dict) ;		/* 1 + highest index = number of names */

#endif /* ndef DICT_H */
/******* end of file ********/
