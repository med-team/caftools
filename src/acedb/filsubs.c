/*  File: filsubs.c
 *  Author: Jean Thierry-Mieg (mieg@mrc-lmb.cam.ac.uk)
 *  Copyright (C) J Thierry-Mieg and R Durbin, 1991
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (MRC LMB, UK) rd@mrc-lmb.cam.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.cnrs-mop.fr
 *
 * Description:
   file opening routines (File Chooser) - Unix version
 * Exported functions:
 * HISTORY:
 * Last edited: Jun 12 18:49 2001 (rmd)
 *	filDirectory() returns a sorted Array of character strings of the names
	of files, with specified ending and spec's, listed in a given directory "dirName";
	If !dirName or directory is inaccessible, the function returns 0
 * * Jun  6 17:58 1996 (rd)
 * * Mar 24 02:42 1995 (mieg)
 * * Feb 13 16:11 1993 (rd): allow "" endName, and call getwd if !*dname
 * * Sep 14 15:57 1992 (mieg): sorted alphabetically
 * * Sep  4 13:10 1992 (mieg): fixed NULL used improperly when 0 is meant
 * * Jul 20 09:35 1992 (aochi): Added directory names to query file chooser
 * * Jan 11 01:59 1992 (mieg): If file has no ending i suppress the point
 * * Nov 29 19:15 1991 (mieg): If file had no ending, we were losing the
                               last character in dirDraw()
 * Created: Fri Nov 29 19:15:34 1991 (mieg)
 *-------------------------------------------------------------------
 */

/* @(#)filsubs.c	1.29    10/24/96 */

#include <regular.h>
#include <array.h>
#include <call.h>

#include <sys/types.h>
#include <mydirent.h>		/* drawDir directory operations */
#include <sys/stat.h>		/* gha.  for filqueryopen dir stuff */

#if !defined(WIN32)

#include <sys/file.h>
#define HOME_DIR_ENVP "HOME"

#define ABSOLUTE_PATH(path) *path == SUBDIR_DELIMITER

#else  /* Utility macros for WIN32 only */

#include <tchar.h> 
#include <direct.h>	   /* for getwcd() and _getdrive() */

/* simple, single letter logical drives assumed here */
static const char *DRIVES = "abcdefghijklmnopqrstuvwxyz";
#define DRIVE_NO(drv) ((drv)-'a'+1)
#define GET_CURRENT_DRIVE *( DRIVES + _getdrive() - 1 )

#define HOME_DIR_ENVP "HOMEPATH"

#include <ctype.h> /* for isalpha() */
#define ABSOLUTE_PATH(path) \
           ( isalpha( (int)*path ) ) && \
           (*(path+1) == DRIVE_DELIMITER) && \
           (*(path+2) == SUBDIR_DELIMITER)
#endif

/******************************/

static Stack dirPath = 0 ;

void filAddDir (char *s)	/* add to dirPath */
{
  char *home ;

  if (!dirPath)
    dirPath = stackCreate (128) ;

  /* if the user directory is specified */
  if (*s == '~' &&
		(home = getenv (HOME_DIR_ENVP))) /* substitute */
    {
#if defined(WIN32) /* in WIN32, need to prefix homepath with home drive */
      char *drive;
      drive = getenv ("HOMEDRIVE") ;
	  pushText(dirPath, drive) ;
	  catText(dirPath, home) ;
#else
      pushText (dirPath, home) ;
#endif
      catText (dirPath, ++s) ;
    }
  else
    pushText (dirPath, s) ;
  catText ( dirPath,
			SUBDIR_DELIMITER_STR) ;
}

void filAddPath (char *cp)
{
  char *cq = cp ;

  while (TRUE)
    { while (*cq && 
			 *cq != PATH_DELIMITER) ++cq ;
      if (*cq == PATH_DELIMITER )
	{ *cq = 0 ;
	  filAddDir (cp) ;
	  cp = ++cq ;
	}
      else
	{ filAddDir (cp) ;
	  break ;
	}
    }
}

/* This function takes a directory name and does the following:
   1. Returns the name if it is "complete" (an absolute path on a given platform)
   2. On WIN32 platforms, for onto rooted paths	lacking a drive specification,
      returns the directory name prefixed with the default drive letter 
   3. Otherwise, assumes that the directory name resides within the current working
      directory and thus, returns it prefixes the directory name with the working directory path
*/
char *filGetFullPath(char *dir)
{
  char *fullpath, *pwd ;
  char dirbuf[DIR_BUFFER_SIZE] ;

  /* Return dir if absolute path already */
  if (ABSOLUTE_PATH(dir))
    { fullpath = (char*) messalloc (strlen(dir) + 1) ;
      strcpy (fullpath, dir) ;
      return fullpath ;
    }

#if defined(WIN32)
  /* else if dir is a Win32 rooted path, then add current drive to rooted paths */
  else if ( *dir == SUBDIR_DELIMITER )
    { char drive[3] = { GET_CURRENT_DRIVE, DRIVE_DELIMITER, '\0' } ;
      fullpath = (char*) messalloc (strlen(dir) + strlen(drive) + 1) ;
      strcpy (fullpath, drive) ;
      strcat (fullpath, dir) ;
      return fullpath ;
    }
#endif

  /* else if I can, then prefix "dir" with working directory path... */
  else if ((pwd = getcwd (dirbuf, sizeof(dirbuf))))
    { fullpath = (char*) messalloc (strlen(pwd) + strlen(dir) + 2) ;
      strcpy (fullpath, pwd) ;
      strcat (fullpath, SUBDIR_DELIMITER_STR) ;
      strcat (fullpath, dir) ;
      return fullpath ;
    }
  else
    return 0 ;  /* signals error that the path was not found */
}

/*******************************/

static BOOL filCheck (char *name, char *spec)
	/* allow 'd' as second value of spec for a directory */
{
  char *cp ;
  BOOL result ;
  struct stat status ;

  if (!spec) /* so filName returns full file name (for error messages) */
    return TRUE ;
				/* directory check */
  if (spec[1] == 'd'  &&
      (stat (name, &status) || !(status.st_mode & S_IFDIR)))
    return 0 ;

  switch (*spec)
    {
    case 'r':
      return !(access (name, R_OK)) ;
    case 'w':
    case 'a':
      if (!access (name, W_OK))	/* requires file exists */
	return TRUE ;
				/* test directory writable */
      cp = name + strlen (name) ;
      while (cp > name)
	if (*--cp == SUBDIR_DELIMITER) break ;
      if (cp == name)
	return !(access (".", W_OK)) ;
      else
	{ *cp = 0 ;
	  result = !(access (name, W_OK)) ;
	  *cp = SUBDIR_DELIMITER ;
	  return result ;
	}
    case 'x':
      return !(access (name, X_OK)) ;
    default:
      messcrash ("Unknown spec %s passed to filName", spec) ;
    }
  return FALSE ;
}

/************************************************/

#if defined(WIN32)
/*******************************************************
 *	Converts '/' to '\' in strings
 * 	Note: this function is non-reentrant;
 * 	however, see comments below, in filName().
 * 	It also uses array indices instead of pointers,
 * 	for array bounds checking, thus, long pathnames
 * 	with lots of slashes may be inadvertently truncated
 ********************************************************/
static char *filPathFix(char *s)
{
	static char buf[DIR_BUFFER_SIZE] ;
	int i,j ; char *p = buf ;

	for(j = i = 0; j < DIR_BUFFER_SIZE-3 && s[i]; i++)
		if(s[i] == '/')
		{  /* add "\\" to target string */
			buf[j++] = '\\' ;
		}
		else
			buf[j++] = s[i] ;
	buf[j] = '\0' ;
	return p ;
}
#endif /* defined(WIN32) */

/************************************************/

char *filName (char *name, char *ending, char *spec)
{
  static Stack part = 0, full = 0 ;
  char *dir, *result ;

  if (!name)
    messcrash ("filName received a null name") ;

  if (!part)
    { part = stackCreate (128) ;
      full = stackCreate (128) ;
    }
    
  stackClear (part) ;

#if defined(WIN32)
  /* filPathFix() is needed to
	 convert '/' => '\\' in path string
	 The function is non-reentrant, but since
     the name is copied onto the part/full stacks
     this should not be a problem */
  name = filPathFix(name) ;
#endif

  catText (part, name) ;
  if (ending && *ending)
    { catText (part, ".") ;
      catText (part, ending) ;
    }
	/* NB filName is reentrant in the sense that it can be called 
	   on strings it generates, because they first get copied into 
	   part, and then the new name is constructed in full.
	*/
  if (ABSOLUTE_PATH(name))
    { stackClear (full) ;
      catText (full, stackText (part, 0)) ;
      result = stackText (full, 0) ;
      if (filCheck (result, spec))
	return result ;
      else
	return 0 ;
    }
  if (!dirPath)			/* add cwd as default to search */
    filAddDir (getcwd(stackText(full, 0), full->safe - stackText(full, 0))) ;
  stackCursor (dirPath, 0) ;
  while ((dir = stackNextText (dirPath)))
    { stackClear (full) ;
      catText (full, dir) ;
      catText (full, stackText (part, 0)) ;
      result = stackText (full, 0) ;
      if (filCheck (result, spec))
	return result ;
    }
  return 0 ;
}

BOOL filremove (char *name, char *ending) /* TRUE if file is deleted. -HJC*/
{
  char *s = filName (name, ending, "r") ;
  if (s)
    return unlink(s) ? FALSE : TRUE ;
  else
    return FALSE ;
}

FILE *filopen (char *name, char *ending, char *spec)
{
  char *s = filName (name, ending, spec) ;
  FILE *result = 0 ;
   
  if (!s)
    messerror ("Failed to find %s", filName (name, ending,0)) ;
  else if (!(result = fopen (s, spec)))
    messerror ("Failed to open %s", s) ;
  return result ;
}

/********************* temporary file stuff *****************/

static Associator tmpFiles = 0 ;

FILE *filtmpopen (char **nameptr, char *spec)
{
  if (!nameptr)
    messcrash ("filtmpopen requires a non-null nameptr") ;

  if (!strcmp (spec, "r"))
    return filopen (*nameptr, 0, spec) ;

  if (!(*nameptr = tempnam ("/var/tmp", "ACEDB")))
    { messerror ("failed to create temporary file name") ;
      return 0 ;
    }
  if (!tmpFiles)
    tmpFiles = assCreate () ;
  assInsert (tmpFiles, *nameptr, *nameptr) ;

  return filopen (*nameptr, 0, spec) ;
}

BOOL filtmpremove (char *name)	/* delete and free()  */
{ BOOL result = filremove (name, 0) ;

  free (name) ;	/* NB free since allocated by tempnam */
  assRemove (tmpFiles, name) ;
  return result ;
}

void filtmpcleanup (void)
{ char *name = 0 ;
 
  if (tmpFiles)
    while (assNext (tmpFiles, &name, 0))
      { filremove (name, 0) ;
	free (name) ;
      }
}

/*******************************/

static Associator mailFile = 0, mailAddress = 0 ;

void filclose (FILE *fil)
{
  char *address ;
  char *filename ;

  if (!fil || fil == stdin || fil == stdout || fil == stderr)
    return ;
  fclose (fil) ;
  if (mailFile && assFind (mailFile, fil, &filename))
    { if (assFind (mailAddress, fil, &address))
	callScript ("mail", messprintf ("%s %s", address, filename)) ;
      else
	messerror ("Have lost the address for mailfile %s",
		   filename) ;
      assRemove (mailFile, fil) ;
      assRemove (mailAddress, fil) ;
      unlink (filename) ;
      free (filename) ;
    }
}

/***********************************/

FILE *filmail (char *address)	/* requires filclose() */
{
  char *filename ;
  FILE *fil ;

  if (!mailFile)
    { mailFile = assCreate () ;
      mailAddress = assCreate () ;
    }
  if (!(fil = filtmpopen (&filename, "w")))
    { messout ("failed to open temporary mail file %s", filename) ;
      return 0 ;
    }
  assInsert (mailFile, fil, filename) ;
  assInsert (mailAddress, fil, address) ;
  return fil ;
}

/******************* directory stuff *************************/

static int dirOrder(void *a, void *b)
{
  char *cp1 = *(char **)a, *cp2 = *(char**)b;
  return strcmp(cp1, cp2) ;
}

/* returns a sorted 0-terminated list of character strings of filenames
   in a given directory "dirName" having the specified ending and spec's.
   If the dirName does not exist or cannot be accessed, returns 0
   The array should be arrayDestroy()'d once no longer needed, so that
   the memory occupied by the strings and the array can be messfree'd
 */

Array filDirectory (char *dirName, char *ending, char *spec)
{
  Array a ;
#if !defined(WIN32)
  DIR	*dirp ;
  char	*dName, entryPathName[MAXPATHLEN], *leaf ;
  int	dLen, endLen ;
  MYDIRENT *dent ;

  if (!dirName || !(dirp = opendir (dirName)))
    return 0 ;

  if (ending)
    endLen = strlen (ending) ;
  else
    endLen = 0 ;

  strcpy (entryPathName, dirName) ;
  strcat (entryPathName, "/") ;
  leaf = entryPathName + strlen(dirName) + 1 ;

  a = arrayCreate (16, char*) ;
  while ((dent = readdir (dirp)))           
    { dName = dent->d_name ;
      dLen = strlen (dName) ;
      if (endLen && (dLen <= endLen ||
		     dName[dLen-endLen-1] != '.'  ||
		     strcmp (&dName[dLen-endLen],ending)))
	continue ;

      strcpy (leaf, dName) ;
      if (!filCheck (entryPathName, spec))
	continue ;

      if (ending && dName[dLen - endLen - 1] == '.') /* remove ending */
	dName[dLen - endLen - 1] = 0 ;

      array(a,arrayMax(a),char*) = strnew(dName,0) ;
    }
  
  closedir (dirp) ;
  
  /************* reorder ********************/
    
  arraySort(a, dirOrder) ;
  return a ;
#else   /* defined(WIN32) */
  return 0 ;
#endif	/* defined(WIN32) */
}

/************ Some WIN32-specific filsub-like code ***************/

#if defined(WIN32)

/************************************************************
 *	Converts MSDOS-like pathnames to POSIX-like ones.   *
 *  Warning: This function is not reentrant, to avoid	    *
 *  the complexities of heap memory allocation and release  *
 ************************************************************/
char *DosToPosix(char *path)
{
  static char newFilName[MAXPATHLEN] ;
  char cwdpath[MAXPATHLEN], *cwd ;
  int i , drive ;

  if( !path || !*path ) return NULL ;

	/* POSIX with drive letters starts with "//" */
  newFilName[0] = SUBDIR_DELIMITER ;
  newFilName[1] = SUBDIR_DELIMITER ;

ReScan:
  i = 2 ;
	/* Drive letter as "A:\" format converted to "A/" format */
  if( strlen(path) >= 2  && 
     (isalpha( (int)*path ) ) && 
     (*(path+1) == DRIVE_DELIMITER) )
    {
      newFilName[i++] = drive = tolower(*path) ;
      path += 2 ; /* skip over drive letter... */
		/* If a root delimiter is present, then path from root (skip delimiter) */
      if( *path == SUBDIR_DELIMITER )
	path++ ;
      else /* else, a relative path on specified drive (append to current directory) */
	{	 /*  If a non-NULL current working directory on specified drive is found */
	  if( (cwd = _getdcwd(DRIVE_NO(drive),cwdpath,MAXPATHLEN - 2)) != NULL )
	    {	/* Then append relative path to current working directory*/
	      if( strlen(cwd)+strlen(path)-1 > MAXPATHLEN )
		messcrash("DosToPosix(): Path buffer overflow?") ;
	      /* If current working directory is not just a root directory pathname*/
	      if( *(cwd+3) ) /* i.e. non-null fourth character?*/
		strcat(cwd, SUBDIR_DELIMITER_STR) ; /* then tack on a SUBDIR_DELIMITER*/
	      strcat(cwd, path) ;
	      path = cwd ;  /* Reset path to new total path*/
	      goto ReScan ; /* Need to rescan because cwd is of DOS format?*/
	    } /* else, assume path from root*/
	}
    }
  else
    if( *path == SUBDIR_DELIMITER	/* If root directory specified only, with no drive letter? */
       && *path++								/* always TRUE thus skips over delimiter... */ )
      newFilName[i++] = GET_CURRENT_DRIVE ;	/* Then assume current drive at root directory*/

    else {	/* Else, no drive letter, no root delimiter => relative path not at root */
      if( (cwd = getwd(cwdpath)) != NULL )	/* If a non-NULL current working */
	{										/* directory on default drive is found */
	  if( strlen(cwd)+strlen(path)-1 > MAXPATHLEN ) /* Append relative path to cwd */
	    messcrash("DosToPosix(): Path buffer overflow?") ;
	  
	  if( *(cwd+3) )	/* If cwd is not just a root directory */
	    strcat(cwd, SUBDIR_DELIMITER_STR) ; /* then tack on a SUBDIR_DELIMITER */
	  strcat(cwd, path) ;
	  path = cwd ;  /* Reset path to new total path*/
	  goto ReScan ; /* Need to rescan because cwd is of DOS format?*/
	}
      else /* just use the current drive*/
	newFilName[i++] = GET_CURRENT_DRIVE ;
    }
  newFilName[i++] = SUBDIR_DELIMITER ;
  
  while(*path) /* Until '\0' encountered */
    {
      /* Path delimiter '\' format */
      if( (*path == SUBDIR_DELIMITER) )
	newFilName[i++] = SUBDIR_DELIMITER ; /* replace ...*/
      else
	newFilName[i++] = *path ; /* else, just copy letter */
      ++path ;	/* ... then skip over */
    }
  newFilName[i] = '\0' ;
  return newFilName ;
}

#endif /* #defined(WIN32) */

/*************** end of file ****************/
 
 
