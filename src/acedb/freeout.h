/*  Last edited: Jun 12 18:30 2001 (rmd) */

/* @(#)freeout.h	1.2 12/13/95 */

#ifndef FREEOUT_H_DEF
#define FREEOUT_H_DEF

#include <regular.h>
#include <array.h>

int freeOutSetFile (FILE *fil) ;
int freeOutSetStack (Stack s) ;
void freeOutTee (FILE *fil, Stack s) ;

void freeOutInit (void) ;
void freeOut (char *text) ;
void freeOutf (char *format,...) ;
void freeOutxy (char *text, int x, int y) ; 

void freeOutClose (int level) ;

int freeOutLine (void) ;
int freeOutByte (void) ;
int freeOutPos (void) ;

#endif
