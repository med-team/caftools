/*  Last edited: Jun  4 09:08 1996 (rd) */

/* @(#)keyset.h	1.3    6/4/96 */

#ifndef DEFINE_KEYSET_H
#define DEFINE_KEYSET_H

#include "array.h"

/***************************************************************/
/*  a KEYSET is an ordered array of KEYs.                         */
/***************************************************************/

typedef Array KEYSET ;  /* really KEYSET = array(,,KEY) always ordered */
#define keySetCreate()		arrayCreate(32,KEY)
#define keySetReCreate(s)	arrayReCreate(s,32,KEY)
#define keySet(s,i)		array(s,i,KEY)
#define keySetDestroy(s)	arrayDestroy(s)

#define keySetInsert(s,k)	arrayInsert(s,&(k),keySetOrder)
#define keySetRemove(s,k)	arrayRemove(s,&(k),keySetOrder)
#define keySetSort(s)		arraySort((s),keySetOrder) 
#define keySetCompress(s)	arrayCompress(s)
#define keySetFind(s,k,ip)	arrayFind ((s),&(k),(ip),keySetOrder)
#define keySetMax(s)		arrayMax(s)
#define keySetExists(s)		(arrayExists(s) && (s)->size == sizeof(KEY))
#define keySetCopy(s)		arrayCopy(s)

KEYSET  keySetAND (KEYSET x, KEYSET y) ;
KEYSET  keySetOR (KEYSET x, KEYSET y) ;
KEYSET  keySetXOR (KEYSET x, KEYSET y) ;
KEYSET  keySetMINUS(KEYSET x, KEYSET y) ;
int     keySetOrder (void *a, void*b) ;
int     keySetAlphaOrder (void *a, void*b) ;
KEYSET  keySetHeap (KEYSET source, int nn, int (*order)(KEY *, KEY *)) ;
KEYSET  keySetNeighbours (KEYSET ks) ;
KEYSET  keySetAlphaHeap(KEYSET ks, int nn);

/**************************************************************/

BOOL keySetActive (KEYSET *setp, void** lookp) ;
void keySetSelect () ;

#endif
/*************************************************************/


