/*  File: messclean.c
 *  Author: Jean Thierry-Mieg (mieg@mrc-lmb.cam.ac.uk)
 *  Copyright (C) J Thierry-Mieg and R Durbin, 1991
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (MRC LMB, UK) rd@mrc-lmb.cam.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.cnrs-mop.fr
 *
 * Description:  version of messubs.c for small programs 
                 that avoids devsubs and freesubs calls.

 * Exported functions:
 * HISTORY:
 * Last edited: Feb  6 00:20 1993 (mieg)
 * Created: Tue Nov  5 13:51:05 1991 (mieg)
 *-------------------------------------------------------------------
 */

/* @(#)messclean.c	1.2    7/15/94 */

#define MESSCLEAN

#include "messubs.c"

void help(void) { }  /* Because we do not link the help package */
void mainActivity(char *text) { } /* Normally in mainpick.c */

/* canny definitions for if freesubs missing
static char card[4096] ;

void  freesettext (char* text, char *parms) { strcpy (card, text) ; }
BOOL  freecheck (char* format) { return TRUE ; }
char* freeword (void) { return card ; }
FILE* filopen (char* name, char* ending, char*mode) { return fopen (name,mode) ; }
*/
