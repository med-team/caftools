/*  File: messubs.c
 *  Author: Richard Durbin (rd@mrc-lmb.cam.ac.uk)
 *  Copyright (C) J Thierry-Mieg and R Durbin, 1992
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (MRC LMB, UK) rd@mrc-lmb.cam.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.cnrs-mop.fr
 *
 * Description: low level: encapsulates vararg messages, *printf,
 			crash handler, malloc/free
			interface in regular.h
			set MALLOC_CHECK at top of file if wanted
 * Exported functions:
 * HISTORY:
 * Last edited: Mar 22 11:57 2002 (rmd)
 * 	-	messout(): defined(_WINDOW) =>!defined(NON_GRAPHIC)
 * * Aug 15 13:29 1996 (srk)
 *	-	WIN32 and MACINTOSH: seteuid() etc. are stub functions
 * * Jun 6 10:50 1996 (rbrusk): compile error fixes
 * * Jun  4 23:31 1996 (rd)
 * Created: Mon Jun 29 14:15:56 1992 (rd)
 *-------------------------------------------------------------------
 */

/* @(#)messubs.c	1.45 10/24/96 */

#define MESSCLEAN

#include <stdarg.h>		/* stdarg.h */
#include <errno.h>

#if defined(NEXT) || defined(HP) || defined(MACINTOSH)
#include "mystdlib.h" /* needed for mysize_t */
extern void* malloc(mysize_t size) ;
#else    /* normal machines  */
#include <malloc.h> 
#endif

#if defined(WIN32) && !defined(NON_GRAPHIC) 
extern void SysBeep(int) ;
#if defined(_DEBUG)
#include <crtdbg.h>
#endif 
#endif

	/* define MALLOC_CHECK here to check mallocs - also in regular.h */

struct allocUnit {
  struct allocUnit *next ;	/* for chaining together on Handles */
  struct allocUnit *back ;	/* to unchain */
  void (*final)(void*) ;	/* finalisation routine */
  int size ;			/* of user memory to follow */
#ifdef MALLOC_CHECK
  int check1 ;			/* set to known value */
#endif
} ;
typedef struct allocUnit *STORE_HANDLE ;
#define STORE_HANDLE_DEFINED	/* used in regular.h */

#include "regular.h"
#include "mytime.h"

#ifndef MESSCLEAN
#include "graph.h"	/* for graphOut() */
#endif

#if !defined(MACINTOSH) && !defined(SOLARIS)

#if defined(WIN32) && !defined(NON_GRAPHIC) && defined(_AFXDLL)
#define DLL_LINKAGE extern __declspec( dllimport )  /* Windows hocus-pocus */
#else
#define DLL_LINKAGE
#endif
  DLL_LINKAGE int vfprintf (FILE *stream, const char *format, va_list arglist);
  DLL_LINKAGE int vfscanf  (FILE *stream, const char *format, va_list arglist);
  DLL_LINKAGE int vprintf  (const char *format, va_list arglist);
  DLL_LINKAGE int vscanf   (const char *format, va_list arglist);
  DLL_LINKAGE int vsprintf (char *buffer, const char *format, va_list arglist);
  DLL_LINKAGE int vsscanf  (const char *buffer, const char *format, va_list arglist);
#endif

#include "array.h"

char *messcrashbuf = 0 ;	/* program initialisation can messalloc this */
VoidRoutine messcrashroutine = 0 ;
char *messdumpfilename = "logfile" ;
BOOL messFlag = FALSE ;		/* set whenever messout/messerror
				   for user testing to avoid message loops 
				*/

static FILE *dumpfil = 0 ;
#define BUFSIZE 4096
static char messbuf[BUFSIZE] ;	/* buffer for messages */

#ifdef SGI			/* work around SGI library bug */
#include "math.h"
double 	log10 (double x) { return log(x) / 2.3025851 ; }
#endif

/*******************************/

void messbeep (void)
{
#if defined(MACINTOSH) || (defined(WIN32) && !defined(NON_GRAPHIC))
  SysBeep (1) ;
#else
  printf ("%c",0x07) ;  /* bell character, I hope */
  fflush (stdout) ;	/* added by fw 02.Feb 1994 */
#endif
}

/*******************************/

void messout (char *format,...)
{ va_list args ;

  messFlag = TRUE ;

  va_start (args,format) ;
    vsprintf (messbuf,format,args) ;
  va_end (args) ;

#if defined(MACINTOSH) || (defined(WIN32) && defined(NON_GRAPHIC))
  graphOut (messbuf) ;
#else
#ifdef MESSCLEAN
  fprintf (stderr,"//!! %s\n",messbuf) ;
#else
  if (!isGraphics)
    printf ("// Message: %s\n",messbuf) ;
  else
    graphOut (messbuf) ;
#endif
#endif /* MACINTOSH */
}

/*****************************/

BOOL messPrompt (char *prompt, char *dfault, char *fmt)
{ 
#ifdef MESSCLEAN
  return freeprompt(prompt,dfault,fmt) ;
#else
  if (!isGraphics)
    return freeprompt(prompt,dfault,fmt) ; 
  else
    return graphPrompt(prompt,dfault,fmt) ; 
#endif
}

/*****************************/

BOOL messQuery (char *text)
{ 
#ifdef MESSCLEAN
  return freequery(text) ;
#else
#if defined(WIN32)
    return graphQuery(text) ;
	/* graphQuery() ALWAYS works within Microsoft Windows
	   irrespective of whether or not the ACEDB 
	   graphical environment is fully initialized... */ 
#else
  if (!isGraphics)
    return freequery(text) ; 
  else
    {
#if defined(ACEDB)
      BOOL bb ;
      extern void mainActivity (char*) ;
      mainActivity("Please Answer") ;
      bb = graphQuery(text) ; 
      mainActivity("Please wait") ;
      return bb ;
#else
      return graphQuery(text) ; 
#endif  /* defined(ACEDB) */
  }
#endif  /* defined(WIN32) */
#endif  /* defined(MESSCLEAN) */
}

/*****************************/
#if  defined(ACEDB)
extern uid_t ruid, euid;
uid_t ruid = 0, euid = 0; /* euid = ruid disables this unless other code
			    puts a value in there */
#if defined(MACINTOSH) || defined(HP) || defined(WIN32)

/* Stub functions for user ID's in non-multiuser systems
Note: A WIN32 implementation for Windows NT may be possible */
void seteuid(uid_t uid) { return ; }
uid_t getuid() { return 0 ; }
uid_t geteuid() { return 0 ; }
int gethostname(char *b, int l) { *b = 0; }
typedef int pid_t;
pid_t getpid (void) { return 0; }
#endif	/* #if  defined(HP) || defined(WIN32) */
#endif	/* #if  defined(ACEDB) */


void messdump (char *format,...)
        /* write to dumpfil (opens if need be) */
{ va_list args ;
  char buf[100];
  if (!dumpfil)
    {
#ifndef ACEDB
      if (filName (messdumpfilename, 0, "a"))
	dumpfil = filopen (messdumpfilename, 0, "a") ;
      else if (filName (messdumpfilename, 0, "w"))
	dumpfil = filopen (messdumpfilename, 0, "w") ;
#else
	/* the filName/filopen mechanism cannot cope with access via effective
	uids, hence use fopen, messdumpfilname is an absolute path here. 
	(See session.c, which would of course never dream of messing around 
	inside messubs.c ............ ) */
	if(euid != ruid) /* euid == ruid disables this */
		seteuid(euid);
	dumpfil = fopen(messdumpfilename, "a");
#endif
      if (!dumpfil)
	{ dumpfil = stderr ;
	  fprintf (stderr, "\n// ** Can't open log file %s - using stderr **\n",
		   messdumpfilename) ;
	}
      gethostname(buf, 100);
      fprintf (dumpfil,"\nNew start:%s Host:%s Pid:%d\n", 
	       timeShow(timeParse("now")), buf, getpid()) ;
    }

#ifdef ACEDB
  if (euid != ruid)
	seteuid(euid);
#endif

  va_start (args, format) ;
  vfprintf (dumpfil, format, args) ;
  va_end (args) ;

  fflush (dumpfil) ;
#ifdef ACEDB
  if (euid != ruid)
    seteuid(ruid);
#endif
}

/*****************************/

/* put "break invokeDebugger" in your favourite debugger init file */

void invokeDebugger (void) 
{
  static BOOL reentrant = FALSE ;

  if (!reentrant)
    { reentrant = TRUE ;
      messalloccheck() ;
      reentrant = FALSE ;
    }
}

/*****************************************/

void messerror (char *format,...)
                /* my version of perror() - accepts format + argument list */
{ va_list args ;
  char *messbuf2 = &messbuf[BUFSIZE/2];
  static int total = 50 ;
  /* extern int errno ; */
#if !defined(MACINTOSH) && !defined(__linux__)
  extern char *sys_errlist[] ;
  extern int sys_nerr ;
#endif

  messFlag = TRUE ;

  va_start (args,format) ;
    vsprintf (messbuf2,format,args) ;
  va_end (args) ;

  if (errno <= 0)
    { messout ("internal system error : %s", messbuf2) ;
      messdump ("internal system error : %s", messbuf2) ;
    }
#if !defined(MACINTOSH)
  else if (errno < sys_nerr)
    { messout ("%s : %s", messbuf2, sys_errlist[errno]) ;
      messdump ("%s : %s", messbuf2, sys_errlist[errno]) ;
    }
#endif
  else
    { messout ("%s : system error %d", messbuf2, errno) ;
      messdump ("%s : system error %d", messbuf2, errno) ;
    }

  invokeDebugger () ;
  errno = 0 ;
    /* stops looping in parse bad tags */
  if (!total--)
    messcrash ("Too many minor errors, sorry, see the log file") ;
}

/*******************************/

void messcrash (char *format,...)
     /* does a printf, beeps bell, and exits */
{
  va_list args ;
  char *crashText = messbuf + 13 ;
#if defined(MACINTOSH)
  extern void crashOut (char* text) ;
#else
  /* extern int errno ; */
#if !defined(__linux__)
  extern char *sys_errlist[] ;
  extern int sys_nerr ;
#endif
#endif

  if (messcrashbuf)
    messfree (messcrashbuf) ;     /* frees up space that might be useful */

  strcpy (messbuf, "FATAL ERROR: ") ;
  va_start (args, format) ;
    vsprintf (crashText, format, args) ;
  va_end (args) ;

#if defined(MACINTOSH)
  crashOut (crashText) ;
#else
  messbeep();
  if (errno <= 0 || errno >= sys_nerr)
    fprintf (stderr,"!! FATAL ERROR %d: ", errno) ;
  else
    fprintf (stderr,"!! FATAL ERROR: system error %d %s \n",
	     errno, sys_errlist[errno]) ;
  fprintf (stderr, "!! %s\n", crashText) ;
#if !defined(MESSCLEAN)
  graphOut (messbuf) ;		/* to display on screen */
#endif
#endif

  if (dumpfil)
    { fprintf (dumpfil, messbuf) ;
      fflush (dumpfil) ;
    }
  
  invokeDebugger () ;

  if (messcrashroutine)
    { VoidRoutine vr = messcrashroutine ;
      messcrashroutine = 0 ;	/* avoids blind recursion */
      (*vr)() ;			/* call user cleanup routine */
    }

  exit(1);
}

/*****************************/

char *messprintf (char *format, ...) /* beware finite buffer size */
{
  va_list args ;

  va_start (args,format) ;
    vsprintf (messbuf,format,args) ;
  va_end (args) ;

  if (messbuf[BUFSIZE-1])
    messcrash ("messprintf buffer size exceeded - length is %d",
	       strlen (messbuf)) ;

  return messbuf ;
}

/*********************************************************************/
/********** memory allocation - messalloc() and handles  *************/

static int numMessAlloc = 0 ;
static int totMessAlloc = 0 ;


  /* Calculate to size of an allocUnit rounded to the nearest upward
     multiple of sizeof(double). This avoids alignment problems when
     we put an allocUnit at the start of a memory block */

#define STORE_OFFSET ((((sizeof(struct allocUnit)-1)/MALLOC_ALIGNMENT)+1)\
                             * MALLOC_ALIGNMENT)


  /* macros to convert between a void* and the corresponding STORE_HANDLE */

#define toAllocUnit(x) (STORE_HANDLE) ((char*)(x) - STORE_OFFSET)
#define toMemPtr(unit)((void*)((char*)(unit) + STORE_OFFSET))

#ifdef MALLOC_CHECK
BOOL handlesInitialised = FALSE;
static Array handles = 0 ;

  /* macro to give the terminal check int for an allocUnit */
  /* unit->size must be a multiple of sizeof(int) */
#define check2(unit)  *(int*)(toMemPtr(unit) + ((unit)->size))

static void checkUnit (STORE_HANDLE unit) ;
static int handleOrder (STORE_HANDLE *a, STORE_HANDLE *b)
{ return (*a == *b) ? 0 : (*a > *b) ? 1 : -1 ;
}
#endif
#if defined(MALLOC_CHECK) || defined(MEM_DEBUG)
struct allocUnit handle0 ;
#endif

/************** halloc(): key function - messalloc() calls this ****************/

#ifdef MEM_DEBUG
#define malloc(size) _malloc_dbg(size,_NORMAL_BLOCK,hfname,hlineno)
char *halloc_dbg(int size, STORE_HANDLE handle,const char *hfname, int hlineno) 
#else
void *halloc(int size, STORE_HANDLE handle)
#endif
{ 
  STORE_HANDLE unit ;
  
#ifdef MALLOC_CHECK
if (!handlesInitialised)		/* initialise */
    { handlesInitialised = TRUE;
      /* BEWARE, arrayCreate calls handleAlloc, line above must precede
         following line to avoid infinite recursion */
      handles = arrayCreate (16, STORE_HANDLE) ;
      array (handles, 0, STORE_HANDLE) = &handle0 ;
      handle0.next = 0 ;
    }

  while (size % INT_ALIGNMENT) size++ ; /* so check2 alignment is OK */
  unit = (STORE_HANDLE) malloc(STORE_OFFSET + size + sizeof(int)) ;
  if (!unit)
    messcrash ("Memory allocation failure when requesting %d bytes", size) ;
  memset (unit, 0, STORE_OFFSET + size + sizeof(int)) ;
#else
  unit = (STORE_HANDLE) malloc(STORE_OFFSET + size) ;
  if (!unit)
    messcrash ("Memory allocation failure when requesting %d bytes", size) ;
  memset (unit, 0, STORE_OFFSET + size) ;
#endif

#if defined(MALLOC_CHECK) || defined(MEM_DEBUG)
  if (!handle)
    handle = &handle0 ;
#endif
  if (handle) 
    { unit->next = handle->next ;
      unit->back = handle ;
      if (handle->next) (handle->next)->back = unit ;
      handle->next = unit ;
    }

  unit->size = size ;
#ifdef MALLOC_CHECK
  unit->check1 = 0x12345678 ;
  check2(unit) = 0x12345678 ;
#endif

  ++numMessAlloc ;
  totMessAlloc += size ;

  return toMemPtr(unit) ;
}

void blockSetFinalise(void *block, void (*final)(void *))
{ STORE_HANDLE unit = toAllocUnit(block);
  unit->final = final ;
}  

/***** handleAlloc() - does halloc() + blockSetFinalise() - archaic *****/

#ifdef MEM_DEBUG
void *handleAlloc_dbg (void (*final)(void*), STORE_HANDLE handle, int size,
		   const char *hfname, int hlineno)
{
  void *result = halloc_dbg(size, handle, hfname, hlineno) ;
#else
void *handleAlloc (void (*final)(void*), STORE_HANDLE handle, int size)
{
  void *result = halloc(size, handle);
#endif
  if (final) 
    blockSetFinalise(result, final);

  return result;
}

/****************** useful utility ************/

#ifdef MEM_DEBUG
char *strnew_dbg(char *old, STORE_HANDLE handle, const char *hfname, int hlineno)
{ char *result = 0 ;
  if (old)
    { result = (char *)halloc_dbg(1+strlen(old), handle, hfname, hlineno) ;
#else
char *strnew(char *old, STORE_HANDLE handle)
{ char *result = 0 ;
  if (old)
    { result = (char *)halloc(1+strlen(old), handle);
#endif
      strcpy(result, old);
    }
  return result;
}

/****************** messfree ***************/

void umessfree (void *cp)
{
  STORE_HANDLE unit = toAllocUnit(cp) ;

#ifdef MALLOC_CHECK
  checkUnit (unit) ;
  unit->check1 = 0x87654321; /* test for double free */
#endif

  if (unit->final)
    (*unit->final)(cp) ;

  if (unit->back) 
    { (unit->back)->next = unit->next;
      if (unit->next) (unit->next)->back = unit->back;
    }
  
  --numMessAlloc ;
  totMessAlloc -= unit->size ;
  free (unit) ;
}

/************** create and destroy handles **************/

/* NOTE: handleDestroy is #defined in regular.h to be messfree */
/* The actual work is done by handleFinalise, which is the finalisation */
/* routine attached to all STORE_HANDLEs. This allows multiple levels */
/* of free-ing by allocating new STORE_HANDLES on old ones, using */
/* handleHandleCreate. handleCreate is simply defined as handleHandleCreate(0) */

static void handleFinalise (void *p)
{
  STORE_HANDLE handle = (STORE_HANDLE)p;
  STORE_HANDLE next, unit = handle->next ;

/* do handle finalisation first */  
  if (handle->final)
    (*handle->final)((void *)handle->back);

      while (unit)
    { 
#ifdef MALLOC_CHECK
      checkUnit (unit) ;
      unit->check1 = 0x87654321; /* test for double free */
#endif
      if (unit->final)
	(*unit->final)(toMemPtr(unit)) ;
      next = unit->next ;
      --numMessAlloc ;
      totMessAlloc -= unit->size ;
      free (unit) ;
      unit = next ;
    }

#ifdef MALLOC_CHECK
  arrayRemove (handles, &p, handleOrder) ;
#endif

/* This is a finalisation routine, the actual store is freed in messfree,
   or another invokation of itself. */
}
  
void handleSetFinalise(STORE_HANDLE handle, void (*final)(void *), void *arg)
{ handle->final = final;
  handle->back = (struct allocUnit *)arg;
}

STORE_HANDLE handleHandleCreate(STORE_HANDLE handle)
{ 
  STORE_HANDLE res = (STORE_HANDLE) handleAlloc(handleFinalise, 
						handle,
						sizeof(struct allocUnit));
#ifdef MALLOC_CHECK
  /* NB call to handleAlloc above ensures that handles is initialised here */
  arrayInsert (handles, &res, handleOrder) ;
#endif
  res->next = res->back = 0 ; /* No blocks on this handle yet. */
  res->final = 0 ; /* No handle finalisation */
  return res ;
}

BOOL finalCleanup = FALSE ;
#ifdef MEM_DEBUG
void handleCleanUp (void) 
{ finalCleanup = TRUE ;
  handleFinalise ((void *)&handle0) ;
}
#endif

/************** checking functions, require MALLOC_CHECK *****/

#ifdef MALLOC_CHECK
static void checkUnit (STORE_HANDLE unit)
{
  if (unit->check1 == 0x87654321)
    messerror ("Block at %x freed twice - bad things will happen.",
	       toMemPtr(unit));
  else
    if (unit->check1 != 0x12345678)
      messerror ("Malloc error at %x length %d: "
		 "start overwritten with %x",
		 toMemPtr(unit), unit->size, unit->check1) ;
  
  if (check2(unit) != 0x12345678)
    messerror ("Malloc error at %x length %d: "
	       "end overwritten with %x",
	       toMemPtr(unit), unit->size, check2(unit)) ;
}

void messalloccheck (void)
{
  int i ;
  STORE_HANDLE unit ;

  if (!handles) return ;

  for (i = 0 ; i < arrayMax(handles) ; ++i) 
    for (unit = arr(handles,i,STORE_HANDLE)->next ; unit ; unit=unit->next)
      checkUnit (unit) ;
}
#else
void messalloccheck (void) {}
#endif

/******************* status monitoring functions ******************/

void handleInfo (STORE_HANDLE handle, int *number, int *size)
{
  STORE_HANDLE unit = handle->next;

  *number = 0;
  *size = 0;

  while (unit)
    { ++*number ;
      *size += unit->size ;
      unit = unit->next ;
    }
}

int messAllocStatus (int *mem)
{ 
  *mem = totMessAlloc ;
  return numMessAlloc ;
}


/*************************************************************************/
/* match to reg expression 

   returns 0 if not found
           1 + pos of first sigificant match (i.e. not a *) if found
*/

int regExpMatch (char *cp,char *tp)
{
  char *c=cp, *t=tp;
  char *ts=tp, *cs=cp, *s = 0 ;
  int star=0;

  while (TRUE)
    switch(*t)
      {
      case '\0':
 	if(!*c)
	  return  ( s ? 1 + (s - cp) : 1) ;
	if (!star)
	  return 0 ;
        /* else not success yet go back in template */
	t=ts; c=cs+1;
	if(ts == tp) s = 0 ;
	break ;
      case '?' :
	if (!*c)
	  return 0 ;
	if(!s) s = c ;
        t++ ;  c++ ;
        break;
      case '*' :
        ts=t;
        while( *t == '?' || *t == '*')
          t++;
        if (!*t)
          return s ? 1 + (s-cp) : 1 ;
        while (freeupper(*c) != freeupper(*t))
          if(*c)
            c++;
          else
            return 0 ;
        star=1;
        cs=c;
	if(!s) s = c ;
        break;
      case 'A' :
	if (!*c || (*c < 'A' || *c > 'Z'))
	  return 0 ;
	if(!s) s = c ;
        t++ ;  c++ ;
        break;
      default  :
        if (freeupper(*t++) != freeupper(*c++))
          { if(!star)
              return 0 ;
            t=ts; c=cs+1;
	    if(ts == tp) s = 0 ;
          }
	else
	  if(!s) s = c - 1 ;
        break;
      }
}


/**** end of file ****/


 
 
 
