/*  File: mystdlib.h
 *  Author: Jean Thierry-Mieg (mieg@mrc-lmb.cam.ac.uk)
 *  Copyright (C) J Thierry-Mieg and R Durbin, 1992
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (MRC LMB, UK) rd@mrc-lmb.cam.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.cnrs-mop.fr
 *
 * Description:
 ** Prototypes of system calls
 ** One should in principle use stdlib, however on the various machines,
    stdlibs do not always agree, I found easier to look by hand 
    and copy here my human interpretation of what I need.
    Examples of problems are: reservations for multi processor
    architectures on some Silicon machines, necessity to define myFile_t on
    some machines and not on others etc.
 * Exported functions:
 * HISTORY:
 * Last edited: Jun 13 11:22 2001 (rmd)
 * Last edited:Jun 11 16:46 1996 (rbrusk): WIN32 tace fixes
 * * Jun 10 17:46 1996 (rbrusk): strcasecmp etc. back to simple defines...
 * * Jun  9 19:29 1996 (rd)
 * * Jun 5 15:36 1996 (rbrusk): WIN32 port details
 *	-	Added O/S specific pathname syntax token conventions as #defined symbols
 * * Jun 5 10:06 1996 (rbrusk): moved X_OK etc. from filsubs.c for IBM
 * Jun  4 23:33 1996 (rd)
 * * Jun  4 21:19 1996 (rd): WIN32 changes
 * Created: Fri Jun  5 18:29:09 1992 (mieg)
 *-------------------------------------------------------------------
 */

/* @(#)mystdlib.h	1.33 6/12/96 */

#ifndef DEF_MYSTDLIB_H
#define DEF_MYSTDLIB_H

#include <config.h>

#define PATH_DELIMITER ':'
#define SUBDIR_DELIMITER '/'
#define SUBDIR_DELIMITER_STR "/"

#define UNIX_PATHNAME(z) z   /* Already a UNIX filename? */

  /*<<--neil 16Sep92: to avoid using values.h*/
#define ACEDB_MAXINT 2147483647
#define ACEDB_MINDOUBLE 1.0e-305
#define ACEDB_LN_MINDOUBLE -700

/* The next few are designed to determine how the compiler aligns structures,
   not what we can get away with; change only if extreme circumstances */

#define INT_ALIGNMENT (sizeof(struct{char c; int i; }) - sizeof(int))
#define DOUBLE_ALIGNMENT (sizeof(struct {char c; double d; }) - sizeof(double))
#define SHORT_ALIGNMENT (sizeof(struct {char c; short s; }) - sizeof(short))
#define FLOAT_ALIGNMENT (sizeof(struct {char c; float f; }) - sizeof(float))
#define PTR_ALIGNMENT (sizeof(struct {char c; void *p; }) - sizeof(void *))

/* Constants for store alignment */

/* These are defined as follows:
   MALLOC_ALIGNMENT
   Alignment of most restrictive data type, the system malloc will 
   return addresses aligned to this, and we do the same in messalloc.

   STACK_ALIGNMENT 
   Alignment of data objects on a Stack; this should really be
   the same as MALLOC_ALIGNMENT, but for most 32 bit pointer machines
   we align stacks to 4 bytes to save memory.

   STACK_DOUBLE_ALIGNMENT
   Alignment of doubles required on stack, if this is greater than
   STACK_ALIGNMENT, we read and write doubles on a stack by steam.


   Put specific exceptions first, the defaults below should cope 
   with most cases. Oh, one more thing, STACK_ALIGNMENT and
   STACK_DOUBLE ALIGNMENT are used on pre-processor constant 
   expressions so no sizeofs, sorry.
*/

/* Alpha pointers are 8 bytes, so align the stack to that */
#if defined(SIZEOF_INT_P)
#  define STACK_ALIGNMENT SIZEOF_INT_P
#endif

#if !defined(STACK_ALIGNMENT)
#  define STACK_ALIGNMENT 4
#endif

#if !defined(STACK_DOUBLE_ALIGNMENT)
#  define STACK_DOUBLE_ALIGNMENT 8
#endif

#if !defined(MALLOC_ALIGNMENT) 
#  define MALLOC_ALIGNMENT DOUBLE_ALIGNMENT
#endif

typedef unsigned int mytime_t;	/* for all machines */

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#if HAVE_UNISTD_H
#   include <unistd.h>
#endif
#if HAVE_SYS_PARAM_H
#   include <sys/param.h>
#endif

typedef size_t mysize_t;
typedef fpos_t myoff_t;
typedef mysize_t myFile_t;

#define FIL_BUFFER_SIZE 256
#define DIR_BUFFER_SIZE (MAXPATHLEN - 256)
#endif  /* DEF_MYSTDLIB_H */

/***********************************************************/

 
 
 
 
