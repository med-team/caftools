/*  File: mytime.h
 *  Author: Richard Durbin (rd@sanger.ac.uk)
 *  Copyright (C) J Thierry-Mieg and R Durbin, 1996
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (MRC LMB, UK) rd@sanger.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.cnrs-mop.fr
 *
 * @(#)mytime.h	1.9 10/16/96
 * Description:
 * Exported functions:
 * HISTORY:
 * Last edited: Jan 25 21:30 1996 (rd)
 * Created: Thu Jan 25 21:30:55 1996 (rd)
 *-------------------------------------------------------------------
 */

#ifndef DEFINE_MYTIME_h
#define DEFINE_MYTIME_h

   /* mars 94: these functions can be used in conjunction
      with the _DateType fundamental type which can
      now be used in the same way as _Int _Float in models.wrm
   */


mytime_t timeParse (char *cp) ;
char*    timeShow (mytime_t t) ;
BOOL	 timeDiffSecs (mytime_t t1, mytime_t t2, int *tdiff) ; 
				/* *tdiff updated only if both times show seconds */
BOOL	 timeDiffDays (mytime_t t1, mytime_t t2, int *tdiff) ; 
				/* *tdiff updated only if both times show days */
char*    timeDiffShow (mytime_t t1, mytime_t t2) ;
char*	 timeShowFormat (mytime_t t, char *format, char *buf, int len) ;
char*    timeShowJava (mytime_t t) ;
mytime_t timeNow (void) ;

#endif

