/*  Last edited: Jun 12 18:03 2001 (rmd) */
/*	-	moved filDirectory() declaration to mydirent.h since */
/* Array type undefined here!  */

/* @(#)regular.h	1.29    10/24/96 */

#ifndef DEF_REGULAR_H
#define DEF_REGULAR_H

#include <mystdlib.h>   /* contains full prototypes of system calls */
#include <time.h>

#ifdef FALSE
  typedef int BOOL ;
#else
  typedef enum {FALSE=0,TRUE=1} BOOL ;
#endif

typedef unsigned char UCHAR ;	/* for convenience */

typedef unsigned int KEY ;

typedef void (*VoidRoutine)(void) ;
typedef void (*Arg1Routine)(void *arg1) ;

typedef struct freestruct
  { KEY  key ;
    char *text ;
  } FREEOPT ;

/* put "break invokeDebugger" in your favourite debugger init file */
/* this function is empty, it is defined in messubs.c used in
   messerror, messcrash and when ever you need it.
*/
void invokeDebugger(void) ;

void freeinit (void) ;
char* freecard (int level) ;	/* 0 if below level (returned by freeset*) */
void freecardback (void) ;  /* goes back one card */
void freeforcecard (char *string);
int  freesettext (char *string, char *parms) ; /* returns level to be used in freecard () */
int  freesetfile (FILE *fil, char *parms) ;
void freeclose(int level) ; /* closes the above */
void freespecial (char *set) ;	/* set of chars to be recognized from "\n;/%\\@$" */
BOOL freeread (FILE *fil) ;	/* returns FALSE if EOF */
int  freeline (FILE *fil) ;	/* line number in file */
int  freestreamline (int level) ;/* line number in stream(level)*/
char *freeword (void) ;

#if defined(WIN32)  /* A variation to correctly parse MS DOS/Windows pathnames */
  char *freepath (void) ;
#else	/* NOT defined(WIN32) */
#define freepath freeword  /* freeword() works fine if not in WIN32 */
#endif	/* defined(WIN32) */

char *freewordcut (char *cutset, char *cutter) ;
void freeback (void) ;		/* goes back one word */
BOOL freeint (int *p) ;
BOOL freetime_t (time_t *p) ;
BOOL freefloat (float *p) ;
BOOL freedouble (double *p) ;
BOOL freekey (KEY *kpt, FREEOPT *options) ;
BOOL freekeymatch (char *text, KEY *kpt, FREEOPT *options) ;
void freemenu (void (*proc)(KEY), FREEOPT *options) ;
char *freekey2text (KEY k, FREEOPT *o)  ;  /* Return text corresponding to key */
BOOL freeselect (KEY *kpt, FREEOPT *options) ;
void freedump (FREEOPT *options) ;
BOOL freestep (char x) ;
void freenext (void) ;
BOOL freeprompt (char *prompt, char *dfault, char *fmt) ;/* gets a card */
BOOL freecheck (char *fmt) ;	/* checks remaining card fits fmt */
int  freefmtlength (char *fmt) ;
BOOL freequery (char *query) ;
char *freepos (void) ;		/* pointer to present position in card */
char *freeprotect (char* text) ; /* protect so freeword() reads correctly */
char* freeunprotect (char *text) ; /* reverse of protect, removes \ etc */

extern char FREE_UPPER[] ;
#define freeupper(x)	(FREE_UPPER[(x) & 0xff])  /* table is only 128 long */

extern char FREE_LOWER[] ;
#define freelower(x)	(FREE_LOWER[(x) & 0xff])

void messout (char *format,...) ;
void messdump (char *format,...) ;
void messerror (char *format,...) ;
void messbeep (void) ;
void messcrash (char *format,...) ; /* frees messcrashbuf before cleanup */
char *messprintf (char *format,...) ; /* sprintf into (static!) string */

BOOL messQuery(char *text) ;
BOOL messPrompt (char *prompt, char *dfault, char *fmt) ;

#if !defined(STORE_HANDLE_DEFINED)
typedef void* STORE_HANDLE ;
#endif

STORE_HANDLE handleHandleCreate (STORE_HANDLE handle) ;
#define handleCreate() handleHandleCreate(0)
#define handleDestroy(handle) messfree(handle)

#if defined(WIN32) && defined(_DEBUG)
#define MEM_DEBUG
#if defined(NON_GRAPHIC)
#include <crtdbg.h>
#endif  
#endif

#if !defined(MEM_DEBUG)

void *handleAlloc (void (*final)(void *), STORE_HANDLE handle, int size) ;
    /* handleAlloc is deprecated, use halloc, and blockSetFinalize instead */
void *halloc(int size, STORE_HANDLE handle) ;
char *strnew(char *old, STORE_HANDLE handle) ;

#else		/* MEM_DEBUG from rbrusk */

void *halloc_dbg(int size, STORE_HANDLE handle, const char *hfname, int hlineno) ;
void *handleAlloc_dbg(void (*final)(char *), STORE_HANDLE handle, int size,
					  const char *hfname, int hlineno) ;
void *strnew_dbg(char *old, STORE_HANDLE handle, const char *hfname, int hlineno) ;
#define halloc(s, h) halloc_dbg(s, h, __FILE__, __LINE__)
#define handleAlloc(f, h, s) handleAlloc_dbg(f, h, s, __FILE__, __LINE__)
#define strnew(o, h) strnew_dbg(o, h, __FILE__, __LINE__)
#define messalloc_dbg(size,fname,lineno) halloc_dbg(size, 0, fname, lineno)

#endif

void blockSetFinalise(void *block, void (*final)(void *)) ;
void handleSetFinalise(STORE_HANDLE handle, void (*final)(void *), void *arg) ;
void handleInfo (STORE_HANDLE handle, int *number, int *size) ;
#define messalloc(size) halloc(size, 0)
void umessfree (void *cp) ;
#define messfree(cp)  ((cp) ? umessfree((void*)(cp)),(cp)=0,TRUE : FALSE)
void messalloccheck (void) ;	/* can be used anywhere - does nothing
				   unless MALLOC_CHECK set in messubs.c */
int messAllocStatus (int *np) ; /* returns number of outstanding allocs
				   *np is total mem if MALLOC_CHECK */

int regExpMatch (char *cp,char *tp) ; /* in messubs.c CLH 5/23/95 */

void filAddPath (char *path) ;	/* Adds a set of pathnames to the pathname stack */
void filAddDir (char *dir) ;	/* Adds a single pathname to the pathname stack */
char *filGetFullPath (char *dir) ; /* returns an absolute path string for dir */
char *filName (char *name, char *ending, char *spec) ;
FILE *filopen (char *name, char *ending, char *spec) ;
FILE *filmail (char *address) ;
void filclose (FILE* fil) ;
FILE *filqueryopen (char *dirname, char *filname, /* [48], [24] */
		    char *ending, char *spec, char *title) ;
BOOL filremove (char *name, char *ending) ;
FILE *filtmpopen (char **nameptr, char *spec) ;
BOOL filtmpremove (char *name) ;
void filtmpcleanup (void) ;

double randfloat (void) ;
double randgauss (void) ;
int randint (void) ;
void randsave (int *arr) ;
void randrestore (int *arr) ;

#endif /* defined(DEF_REGULAR_H) */

/******************************* End of File **********************************/





 
 
