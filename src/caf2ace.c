/* $Id: caf2ace.c 17467 2006-05-19 11:19:54Z rmd $ */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <consensus.h>
#include <cl.h>
#include <cafseq.h>

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
#define BQ_SIZE signed char
#define MAX_QUAL_VAL 99

typedef struct {
  CAFSEQ *read;  /* Read info            */
  int cs;        /* Start in contig      */
  int ce;        /* End in contig + 1    */
  int rs;        /* Start in read        */
  int re;        /* End in read + 1      */
  int comp;      /* Align reverse strand */
} Seg_entry;

typedef struct {
  int read;
  int contig;
  char comp[5];
  int start;
  int end;
  int tags;
  int clip_l;
  int clip_r;
} READ;

typedef struct {
  int read;
  int comp;
} BASE;


void usage (void);

int find_contig_info(cafAssembly *);

static void update_segments(cafAssembly *, int , Array,
                            ASSINF **, ASSINF *);

static void check_base (cafAssembly*, CAFSEQ*, int, Array, Array, void *);

static int af_compare(const void *, const void *);

static int seg_compare(const void *, const void *);

static char complement(char b);

typedef void (*do_iterator_callback)(cafAssembly*,CAFSEQ*, int, Array, Array, void *);

static int write_ace_format(cafAssembly*,CAFSEQ *, Array, Array, char *);

static int write_bs_lines (cafAssembly *,Array);

static int write_seq (CAFSEQ *seq,char *);

static int write_quality (CAFSEQ *seq);

static int write_reads (cafAssembly *, CAFSEQ *, Array, char *);

static int write_read_clipping (cafAssembly *, CAFSEQ *, READ *, Array);

static int write_chromat (cafAssembly *, CAFSEQ *, int, Array, int, char *);

static void reverse_seq(char *dna, int len);

static void seq_complement(char *dna, int len);

static void reverse_complement(char *dna, int len);

int iterate_contig(cafAssembly* caf, CAFSEQ* seq, char *t_string, do_iterator_callback callback,void *callback_params);

int main (int argc, char **argv)
{
  cafAssembly *CAF;
  char CAFfile[256] = {0};
  int help = 0;
  
  getboolean("-h", &help, argc, argv);
  
  if(help) 
    usage();
  

  if ((CAF = readCAF(argc,argv,CAFfile)) != 0 ) {
    requirePadded(CAF);    
    find_contig_info(CAF);
    return 0;
  }
  
  return 1;
}


int find_contig_info(cafAssembly * caf)
{
  Array   seqs;
  CAFSEQ *seq;
  int i, ctgnum = 0;
  int readnum = 0;
  time_t now;
  char *t_string;
  struct tm *thistime;
  now = time(0);
  thistime = localtime(&now);
  t_string = asctime(thistime);


  seqs = caf->seqs;
    
  for (i = 0; i < arrayMax(seqs); i++) {
    seq = arrp(seqs, i, CAFSEQ);      
    (seq->type == is_contig) ? ++ctgnum : ++readnum;
  }
  fprintf(stdout,"AS %i %i\n\n",ctgnum,readnum);
  
  for (i = 0; i < arrayMax(seqs); i++) {
    seq = arrp(seqs, i, CAFSEQ);      
    if (seq->type == is_contig) {
      fprintf(stderr,"%s ",dictName(caf->seqDict,seq->id));
      iterate_contig(caf, seq, t_string, check_base, 0);
    }
  }
  
  
  return 0;
}

int iterate_contig(cafAssembly* caf, CAFSEQ* seq, char *t_string, do_iterator_callback callback,void *callback_params)
{
  ASSINF *assembled_froms; /* First ASSINF struct */
  ASSINF *last_af;         /* Last  ASSINF struct */
  ASSINF *curr_af;         /* pointer to current ASSINF struct        */
  ASSINF *temp_af;
  Array  segments;         /* Array holding all Seg_entry at current base */   
  Array  reads;
  Array  base;
  READ   *rinfo;
  CAFSEQ *rd;
  int    start=-1;         /* First good base in contig consensus     */
  int    end=0;            /* Last  good base in contig consensus + 1 */
  int    i;                /* The current consensus base */
  int    k;
  

  /* Check if this contig has any assembled_from information */
 if (!seq->assinf)               return 0;
 if (arrayMax(seq->assinf) == 0) return 0;
 
 reads = arrayCreate(arrayMax(seq->assinf)-1,READ);
 arrayp(reads,arrayMax(seq->assinf)-1,READ);
 
 /* Sort the assembled_from information into order along the contig */
 
 assembled_froms = arrp(seq->assinf, 0, ASSINF);
 qsort(assembled_froms, arrayMax(seq->assinf), sizeof(ASSINF), af_compare);
 
 /* Find the first and last bases in the assembled_froms
    Also find the full extent of the read, including the clipped bits at the
    ends */

 /* FIXME: This will only work if there is just one Assembled_from line for
    each read.  Is this always the case for padded sequences? */
 for(k=0;k<arrayMax(seq->assinf);k++) {
   temp_af = arrp(seq->assinf,k,ASSINF);
   rd = arrp(caf->seqs, temp_af->seq, CAFSEQ);
   rinfo = arrp(reads,k,READ);
   rinfo->read = temp_af->seq;
   rinfo->contig = seq->id;
   rinfo->tags = 0;   

   if(temp_af->s1 > temp_af->s2) {
     if(start < 0 || start > temp_af->s2) { start = temp_af->s2; }
     if(end < temp_af->s1) { end = temp_af->s1; }
     strcpy(rinfo->comp,"C");
     /* rinfo->start = temp_af->s2 - (rd->len - arrp(rd->clipping,0,TAG)->x2);
	rinfo->end   = temp_af->s1 + arrp(rd->clipping,0,TAG)->x1; */
     rinfo->start = temp_af->s2 - (rd->len - temp_af->r2);
     rinfo->end = temp_af->s1 + temp_af->r1;
   }else{
     if(start < 0 || start > temp_af->s1) { start = temp_af->s1; }
     if(end < temp_af->s2) { end = temp_af->s2; }
     strcpy(rinfo->comp,"U");
     /* rinfo->start = temp_af->s1 - arrp(rd->clipping,0,TAG)->x1;
	rinfo->end   = temp_af->s2 + (rd->len - arrp(rd->clipping,0,TAG)->x2); */
     rinfo->start = temp_af->s1 - temp_af->r1;
     rinfo->end   = temp_af->s2 + temp_af->r2;
   }

   if (rd->clipping) {
     rinfo->clip_l = arrp(rd->clipping,0,TAG)->x1;
     rinfo->clip_r = arrp(rd->clipping,0,TAG)->x2;
   } else {
     rinfo->clip_l = temp_af->r1;
     rinfo->clip_r = temp_af->r2;
   }
 }
 
 last_af = arrp(seq->assinf, arrayMax(seq->assinf)-1, ASSINF);
 
 if (start < 0) return 1;
 
 /* Go through each consensus base, work out which read segments are
    contributing to the base */
 
 fprintf(stderr,"%d..%d\n",start,end);
 
 curr_af  = assembled_froms;
 segments = arrayCreate(128, Seg_entry);

 base = arrayCreate(end, BASE);
 arrayp(base,end,BASE);
 
 for (i = start-1; i < end; i++) {
   update_segments(caf, i, segments, &curr_af, last_af);    
   (*callback)(caf, seq, i, segments, base, callback_params);    
 }  
 
 write_ace_format(caf,seq,base,reads,t_string);
 
 arrayDestroy(segments);
 arrayDestroy(base);
 arrayDestroy(reads);
 
 return 0;
}

static int write_seq (CAFSEQ *seq, char *iscomp)
{
  int j, k;
  char *s = stackText(seq->dna, 0);
  char b[51];
  int len = strlen(s);
    
  if(!strcmp(iscomp,"C")) {
    reverse_complement(s,strlen(s));
  }

  for(j = 0; j < len; j += 50)
    {
      int n = ( j+50 < len ? 50: len-j );
 
      for (k = 0; k < n; k++)
	{
	  b[k] = s[j + k] == '-' ? '*' : s[j + k];
	}

      fwrite(b, sizeof(char), n, stdout);
      fputc('\n', stdout);
    }
  fputc ('\n', stdout) ;

  return 0;
}

static int write_quality (CAFSEQ *seq)
{
  int j;
  char buffer[1056], *s, *t, *dna;
  int n;

  dna = stackText(seq->dna, 0);

  fputs("BQ\n", stdout);
  s = buffer;
  *s = 0;
  for(j = 0; j < arrayMax(seq->base_quality); j++)
    {
      if (dna[j] != '-')
	{
	  n = (int)arr(seq->base_quality,j,BQ_SIZE);
	  if ( n > MAX_QUAL_VAL ) n = MAX_QUAL_VAL;
	  
	  t = itoa_cache( n ); /* cache integer to string conversions */
	  while (*t)
	    *s++ = *t++;
	  if ( s-buffer > 100 || j == arrayMax(seq->base_quality)-1)
	    {
	      *s++='\n';
	      *s = 0;
	      fputs(buffer , stdout );
	      s = buffer;
	      *s = 0;
	    }
	}
    }
  fputc ('\n', stdout) ;
  return 0;
}


static int write_ace_format(cafAssembly *caf, CAFSEQ *seq, Array base, Array reads, char *t_string)
{
  int k,l,i;
  int j = 0;
  READ *rp;
  /* ASSINF *af; */
  BASE *bs;

  bs = arrp(base,0,BASE); 
  l = bs->read; 
  for(i=0;i<arrayMax(base);i++) {
    do {
      bs = arrp(base,i,BASE);
      i++;      
    }while(bs->read == l && i < arrayMax(base));
    ++j;
    l = bs->read;
  }
  
  fprintf(stdout,"CO %s %i %i %i U\n", dictName(caf->seqDict,seq->id), seq->len, arrayMax(seq->assinf),j);
  
  write_seq(seq,"U");

  write_quality(seq);
  
  for(k=0;k<arrayMax(seq->assinf);k++) {
    /* af = arrp(seq->assinf,k,ASSINF); */
    rp = arrp(reads,k,READ);
    if(rp->contig == seq->id) {
      fprintf(stdout,"AF %s %s %i\n",dictName(caf->seqDict,rp->read),rp->comp,(!strcmp(rp->comp,"C"))? rp->start : rp->start+1);
    }
  }

  write_bs_lines(caf,base);
  
  write_reads(caf,seq,reads,t_string);
  
  return 0;

}

static int write_reads (cafAssembly *caf, CAFSEQ *seq, Array reads, char *t_string)
{
  int i;
  READ *rp;
  CAFSEQ *cseq;
  
  for(i=0;i<arrayMax(reads);i++) {
    rp = arrp(reads,i,READ);
    if(rp->contig == seq->id) {
      fprintf(stdout,"RD %s %i 0 0\n",dictName(caf->seqDict,rp->read),arrp(caf->seqs,rp->read,CAFSEQ)->len);
      cseq = arrp(caf->seqs,rp->read,CAFSEQ);
      write_seq(cseq,rp->comp); 
      write_read_clipping(caf,cseq,rp,reads);
      write_chromat(caf,cseq,rp->read,reads,i,t_string);
    }
  }
  
  return 0;
}

static int write_chromat (cafAssembly *caf, CAFSEQ *read, int read_id, Array reads, int pos, char *t_string) 
{
  /* READ *rp; */
  char readname[100] = {0};
  char *dyes[] = {"unknown", "primer", "term" };

  /* rp = arrp(reads,pos,READ);   */
  snprintf(readname, sizeof(readname), "%s", dictName(caf->seqDict,read_id));

  fprintf(stdout,"DS CHROMAT_FILE: %s PHD_FILE: %s.phd.1 CHEM: %s DYE: UNK TIME: %s TEMPLATE: %s DIRECTION: %s INSERT: %i-%i\n\n",readname,readname,dyes[read->dye],strtok(t_string,"\n"),dictName(caf->templateDict,read->template_id),(read->strand == forward_strand ? "fwd" : "rev"),(read->insert_size1 < 0) ? 0 : read->insert_size1 ,(read->insert_size2 < 0) ? 0 : read->insert_size2);


return 0;
}


static int write_read_clipping (cafAssembly *caf, CAFSEQ *read_seq, READ *rp, Array reads)
{
  fprintf(stdout,"QA %i %i %i %i\n",
	  (!strcmp(rp->comp,"U"))? rp->clip_l : read_seq->len - rp->clip_r+1, 
	  (!strcmp(rp->comp,"U"))? rp->clip_r : read_seq->len - rp->clip_l+1,
	  1,
	  read_seq->len
	  );
  
  return 0;
}


static int write_bs_lines (cafAssembly *caf, Array base) 
{
  
  int i,cur,end,cread,j=0;
  BASE *bp;
  
  bp = arrp(base,0,BASE); 
  cread = bp->read;  
  cur = 0;
  
  for(i=0;i<arrayMax(base);i++) {
    do {
      bp = arrp(base,i,BASE);
      end = i;
      i++;      
    }while(bp->read == cread && i < arrayMax(base));
    fprintf(stdout,"BS %i %i %s\n",cur+1,end,dictName(caf->seqDict,cread));  
    ++j;
    cur = i;
    cread = bp->read;
  }
  fputs("\n",stdout);
  
  return 0;
}

static void update_segments(cafAssembly *caf, int i, Array segments,
                            ASSINF **curr_af, ASSINF *last_af)
{
  Seg_entry *new_seg; /* New segment */
  int seg;       /* Segment counter */
  int inserted;  /* Flag if a new segment has been inserted */
 
  /* Remove any segments we have finished with (i.e. i has gone past the end).
     As the segments array is sorted by segment end, the ones we need to get
     rid of should be at the end of the array */

  for (seg = arrayMax(segments) - 1; seg >= 0; seg--) {
    
    if (arrp(segments, seg, Seg_entry)->ce > i) break;

    arrayMax(segments)--;
  }
  
  inserted = 0;

  /* Go through the remaining ASSINF entries and see if they should be put
     into the segments array yet. */

  for (; (*curr_af) <= last_af; (*curr_af)++) {

    if (min((*curr_af)->s1, (*curr_af)->s2) - 1 > i) break;

    /* Insert the next segment */

    new_seg = arrayp(segments, arrayMax(segments), Seg_entry);

    if ((*curr_af)->s1 < (*curr_af)->s2) {  /* Forward strand */

      new_seg->read = arrp(caf->seqs, (*curr_af)->seq, CAFSEQ);
      new_seg->cs   = (*curr_af)->s1 - 1;
      new_seg->ce   = (*curr_af)->s2;
      new_seg->rs   = (*curr_af)->r1 - 1;
      new_seg->re   = (*curr_af)->r2;
      new_seg->comp = 0;
    } else {                          /* Reverse strand */
      
      new_seg->read = arrp(caf->seqs, (*curr_af)->seq, CAFSEQ);
      new_seg->cs   = (*curr_af)->s2 - 1;
      new_seg->ce   = (*curr_af)->s1;
      new_seg->rs   = (*curr_af)->r1 - 1;
      new_seg->re   = (*curr_af)->r2;
      new_seg->comp = 1;
    }
    inserted = 1;
  }

  if (inserted) {

    /* re-sort the array of segments by end position */

    qsort(arrp(segments, 0, Seg_entry), arrayMax(segments),
          sizeof(Seg_entry), seg_compare);
  }
}


static void check_base (cafAssembly* caf, CAFSEQ* contig,
                        int i, Array segments, Array BasePos, void * params)
{
  
  int seg, rp, max_q = 0;
  char base,qual;
  Seg_entry *se;
  BASE *bp;
  
  bp = arrp(BasePos,i,BASE);
  bp->read = -1;
  
  for (seg = 0; seg < arrayMax(segments); seg++) {
    se = arrp(segments, seg, Seg_entry);
    if (se->comp) { /* reverse strand */   
      rp = se->re - (i - se->cs) - 1;
      base = (char) toupper((int) complement(*(stackText(se->read->dna, rp))));
      qual = (int) arr(se->read->base_quality, rp, char);      
    } else {        /* forward strand */      
      rp = i - se->cs + se->rs;
      base = (char) toupper((int) *(stackText(se->read->dna, rp)));
      qual = (int) arr(se->read->base_quality, rp, char);      
    }    
    
    if((base == *stackText(contig->dna, i)) && (qual >= max_q)) {
      /* Get max Q val for read */ 
      bp->read = (se->read)->id;
      max_q = qual;
      bp->comp = se->comp;
    }else{
      bp->read = (se->read)->id;
      bp->comp = se->comp;
    }
  }
}

void usage (void)
{
  fprintf(stdout,"\nUsage: caf_find_misassemblies [-h] [options] < caf.in > caf.out\n\n");
  exit(0);
}

static int af_compare(const void *a, const void *b)
{
  int mina,minb;
  ASSINF *aa = (ASSINF *) a;
  ASSINF *bb = (ASSINF *) b;
  
  mina = min(aa->s1, aa->s2);
  minb = min(bb->s1, bb->s2);


  return (mina - minb);
}

/* Compare Seg_entry entries.  When used for sorting, they will be ordered by
   end position (ce), high to low */

static int seg_compare(const void *a, const void *b)
{
  Seg_entry *aa = (Seg_entry *) a;
  Seg_entry *bb = (Seg_entry *) b;

  return (bb->ce - aa->ce);
}
static char complement(char b)
{
  static char tab[256] = {0};
  int i;

  if (!tab['a']) {
    for (i = 0; i < 256; i++) tab[i] = 'N';
    tab['a'] = 't';
    tab['A'] = 'T';
    tab['c'] = 'g';
    tab['C'] = 'G';
    tab['g'] = 'c';
    tab['G'] = 'C';
    tab['t'] = 'a';
    tab['T'] = 'A';
    tab['n'] = 'n';
    tab['-'] = '-';
  }

  return tab[(int) b];
}

static void reverse_seq(char *dna, int len)
{
  char *start = dna;
  char *end   = dna + len - 1;
  char t;

  for (; start < end; start++, end--) {
    t = *start;
    *start = *end;
    *end = t;
  }
}

static void seq_complement(char *dna, int len)
{
  static char mx[256];
  static unsigned char bases[]  = "CTAGNctagn*";
  static unsigned char cbases[] = "GATCNgatcn*";
  static int init = 0;
  int i;
  
  if(!init) {
    init++;
    for (i=0;i<256;i++) { mx[i] = '-'; }
    
    for (i=0;i<sizeof(bases);i++) {
      mx[bases[i]] = cbases[i];
    }
  }

  for(i=0; i<len; i++) {
    dna[i] = mx[(unsigned char) dna[i]];  
  }
}

static void reverse_complement(char *dna, int len)
{
  reverse_seq(dna, len);
  seq_complement(dna, len);
}
