/* $Id: caf2fastq.c 15168 2005-06-16 12:58:18Z rmd $ */
#include <stdio.h>
#include <cl.h>
#include <caf.h>
#include <errno.h>

static void get_seq_template_lists(cafAssembly * caf,
				   Array *template_list_heads,
				   Array *seq_template_list);

static void write_fastq(cafAssembly * caf, int svclip, int min_len, FILE *out,
			Array *fastq_index);

static void write_mates_file(cafAssembly *caf, char *mates_file,
			     Array fastq_index, Array template_list_heads,
			     Array seq_template_list);
static void write_templates_file(cafAssembly *caf,
				 Array fastq_index, char *templates_file);
static void write_direction_file(cafAssembly *caf,
				 Array fastq_index, char *direction_file);

int main(int argc, char **argv) {
  Array seq_template_list;  /* of ssize_t, links seqs with the same template */
  Array template_list_heads; /* of ssize_t, start of seq lists */
  Array fastq_index;        /* index of read in fastq file */
  char caffile[256];
  char mates_file[256]     = {0};
  char templates_file[256] = {0};
  char direction_file[256] = {0};
  char out_file[256]       = {0};
  int svclip = 1;
  int min_len = 20;
  cafAssembly * caf = NULL;
  FILE *out = stdout;

  /* Command line options */

  getboolean("-svclip", &svclip, argc, argv);
  getarg("-mates", mates_file,  argc, argv);
  getarg("-templates", templates_file, argc, argv);
  getarg("-direction", direction_file, argc, argv);
  getarg("-out", out_file, argc, argv);
  getint("-minlen", &min_len, argc, argv);
  
  caf = readCAF(argc, argv, caffile);

  gethelp(argc, argv);

  /* Make linked lists of which reads come from the same template */
  get_seq_template_lists(caf, &template_list_heads, &seq_template_list);

  /* Write the fastq file */

  if (*out_file) {
    out = openfile(out_file, "w");
  }

  write_fastq(caf, svclip, min_len, out, &fastq_index);

  if (*mates_file) write_mates_file(caf, mates_file, fastq_index,
				    template_list_heads, seq_template_list);
  if (*templates_file) write_templates_file(caf, fastq_index, templates_file);
  if (*direction_file) write_direction_file(caf, fastq_index, direction_file);

  return 0;
}

static void get_seq_template_lists(cafAssembly * caf,
				   Array *template_list_heads,
				   Array *seq_template_list) {
  Array seqs;
  CAFSEQ *seq;
  int i;

  seqs     = caf->seqs;
  
  *seq_template_list   = arrayCreate(arrayMax(seqs), ssize_t);
  *template_list_heads = arrayCreate(dictMax(caf->templateDict), ssize_t);

  for (i = 0; i < dictMax(caf->templateDict); i++) {
    array(*template_list_heads, i, ssize_t) = -1;
  }

  for (i = 0; i < arrayMax(seqs); i++) {
    seq = arrp(seqs, i, CAFSEQ);

    if (seq->type == is_read
	&& seq->dna != NULL) {
      if (seq->template_id) {
	if (array(*template_list_heads, seq->template_id, ssize_t) >= 0) {

	  array(*seq_template_list, i, ssize_t) = 
	    array(*template_list_heads, seq->template_id, ssize_t);
	  array(*template_list_heads, seq->template_id, ssize_t) = i;

	} else {

	  array(*seq_template_list, i, ssize_t) = -1;
	  array(*template_list_heads, seq->template_id, ssize_t) = i;

	}
      } else {
	array(*seq_template_list, i, ssize_t) = -1;
      }
    }
  }
}

static void write_fastq(cafAssembly * caf, int svclip, int min_len, FILE *out,
			Array *fastq_index) {
  Array seqs;
  
  CAFSEQ *seq;
  DICT *seq_dict;
  TAG *svtag;
  char *dna;
  char *name;
  int i;
  int j;
  int left;
  int right;
  int index;

  seqs     = caf->seqs;
  seq_dict = caf->seqDict;

  *fastq_index         = arrayCreate(arrayMax(seqs), ssize_t);

  /* Make linked lists of which reads come from the same template */

  
  index = 0;
  for (i = 0; i < arrayMax(seqs); i++) {
    seq = arrp(seqs, i, CAFSEQ);

    if (seq->type == is_read
	&& seq->dna != NULL
	&& (dna = stackText(seq->dna, 0)) != NULL) {

      name = dictName(seq_dict, i);

      left  = 0;
      right = strlen(dna);

      if (!seq->base_quality
	  || arrayMax(seq->base_quality) != right) {

	fprintf(stderr, "Sequence and quality length differ for %s\n", name);
	exit(1);

      }

      if (svclip && seq->svector) {
	for (j = 0; j < arrayMax(seq->svector); j++) {
	  svtag = arrp(seq->svector, j, TAG);

	  if (svtag->x1 == 1) {
	    if (left < svtag->x2 - 1) left = svtag->x2;
	  } else {
	    if (right > svtag->x1)    right = svtag->x1 - 1;
	  }

	}
      }

      if (left + min_len < right) { /* There is good sequence available */

	fprintf(out, "@%s bases %d to %d\n", name, left + 1, right);
	fprintf(out, "%.*s\n", right - left, dna + left);
	fprintf(out, "+%s bases %d to %d\n", name, left + 1, right);
	putc('!', out);
	for (j = left + 1; j < right; j++) {
	  putc(arr(seq->base_quality, j, BQ_SIZE) + 041, out);
	}
	putc('\n', out);

	array(*fastq_index, i, ssize_t) = index++;
      } else {
	array(*fastq_index, i, ssize_t) = -1;
      }
    } else {
      array(*fastq_index, i, ssize_t) = -1;
    }
  }
}

static void write_mates_file(cafAssembly *caf, char *mates_file,
			     Array fastq_index, Array template_list_heads,
			     Array seq_template_list) {
  Array seqs;
  CAFSEQ *seq;
  FILE *mates = NULL;
  size_t i;
  size_t count;
  ssize_t fastq_entry;
  ssize_t linked;
  ssize_t template_id;

  mates = fopen(mates_file, "w");
  if (!mates) {
    fprintf(stderr, "Couldn't open %s: %s\n", mates_file, strerror(errno));
    exit(EXIT_FAILURE);
  }

  seqs = caf->seqs;

  for (i = 0; i < arrayMax(seqs); i++) {
    fastq_entry = array(fastq_index, i, ssize_t);

    if (fastq_entry >= 0) {

      seq = arrp(seqs, i, CAFSEQ);
      template_id = seq->template_id;

      count = 0;

      for (linked = array(template_list_heads, template_id, ssize_t);
	   linked >= 0;
	   linked = array(seq_template_list, linked, ssize_t)) {

	if (linked != i && array(fastq_index, linked, ssize_t) > 0) count++;
      }

      fprintf(mates, "%ld %ld", fastq_entry, count);

      for (linked = array(template_list_heads, template_id, ssize_t);
	   linked >= 0;
	   linked = array(seq_template_list, linked, ssize_t)) {
	
	if (linked != i && array(fastq_index, linked, ssize_t) > 0)
	  fprintf(mates, " %ld", array(fastq_index, linked, ssize_t));
      }

      if (count > 0) {
	if (seq->insert_size1 || seq->insert_size2) {
	  fprintf(mates, " %d %d",
		  (seq->insert_size1 + seq->insert_size2) / 2,
		  (seq->insert_size2 - seq->insert_size1) / 2);
	} else {
	  fprintf(mates, " 10000 10000");
	}
      }

      fprintf(mates, "\n");
    }
  }
}

static void write_templates_file(cafAssembly *caf,
				 Array fastq_index, char *templates_file) {

  Array seqs;
  CAFSEQ *seq;
  FILE *templates = NULL;
  DICT *seq_dict;
  DICT *template_dict;
  size_t i;
  ssize_t fastq_entry;
  ssize_t template_id;

  templates = fopen(templates_file, "w");
  if (!templates) {
    fprintf(stderr, "Couldn't open %s: %s\n", templates_file, strerror(errno));
    exit(EXIT_FAILURE);
  }

  seqs = caf->seqs;
  seq_dict = caf->seqDict;
  template_dict = caf->templateDict;
  
  for (i = 0; i < arrayMax(seqs); i++) {
    fastq_entry = array(fastq_index, i, ssize_t);

    if (fastq_entry >= 0) {
      
      seq = arrp(seqs, i, CAFSEQ);
      template_id = seq->template_id;
      
      if (template_id) {
	fprintf(templates, "%s %s\n",
		dictName(seq_dict, i),
		dictName(template_dict, template_id));
      } else {
	fprintf(templates, "%s %s\n",
		dictName(seq_dict, i), dictName(seq_dict, i));
      }
    }
  }
}

static void write_direction_file(cafAssembly *caf,
				 Array fastq_index, char *direction_file) {

  Array seqs;
  CAFSEQ *seq;
  FILE *direction = NULL;
  DICT *seq_dict;
  size_t i;
  ssize_t fastq_entry;

  direction = fopen(direction_file, "w");
  if (!direction) {
    fprintf(stderr, "Couldn't open %s: %s\n", direction_file, strerror(errno));
    exit(EXIT_FAILURE);
  }

  seqs = caf->seqs;
  seq_dict = caf->seqDict;
  
  for (i = 0; i < arrayMax(seqs); i++) {
    fastq_entry = array(fastq_index, i, ssize_t);

    if (fastq_entry >= 0) {
      
      seq = arrp(seqs, i, CAFSEQ);
      
      fprintf(direction, "%s %s\n",
	      dictName(seq_dict, i),
	      seq->strand != reverse_strand ? "F" : "R");
    }
  }
}
