/* $Id: caf2gap.c 36373 2013-11-20 12:01:41Z aw7 $ */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 * its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

#define MAIN 1
#define FAKED_QVALUE 2

#include "caf2gap.h"

/*************************************************************
 * IO Routines
 *************************************************************/

static void write_1(FILE *f, void *a, int num)
{
  if (fwrite(a, 1, num, f) != num)
    messcrash("Error writing to image\n");
}

static void write_2(FILE *f, void *a, int num)
{
  if (fwrite(a, 2, num, f) != num)
    messcrash("Error writing to image\n");
}

static void write_4(FILE *f, void *a, int num)
{
  if (fwrite(a, 4, num, f) != num)
    messcrash("Error writing to image\n");
}

static void write_N(FILE *f, void *a, int num, int N)
{
  if (fwrite(a, N, num, f) != num)
    messcrash("Error writing to image\n");
}

static void write_2_swapped(FILE *f, void *a, int len)
{
  int i;
  static Array buf;
  if (buf == NULL) buf = arrayCreate(BLOCKSIZE, uint_2);
  arrayp(buf, len, uint_2);	/* force resize */
  for(i=0;i<len;i++) {
    swap_int2(((uint_2 *)a)[i],arr(buf,i,uint_2));
  }
  write_2(f, arrp(buf, 0, uint_2), len);
}

static void write_4_swapped(FILE *f, void *a, int len)
{
  int i;
  static Array buf = NULL;
  if (buf == NULL) buf = arrayCreate(BLOCKSIZE,uint_4);
  arrayp(buf,len,uint_4);	/* force resize */
  for(i=0;i<len;i++) {
    swap_int4(((uint_4 *)a)[i],arr(buf,i,uint_4));
  }
  write_4(f,arrp(buf,0,uint_4),len);
}

static void write_N_swapped(FILE *f, void *a, int len, int N)
{
  int i;
  int j;
  static Array buf = NULL;
  if (buf == NULL) buf = arrayCreate(BLOCKSIZE,char);
  arrayp(buf,len*N,char);	/* force resize */
  for(i=0;i<len;i++) {
    for(j=0;j<N;j++) {
      arr(buf,i*N+j,char) = ((char *)a)[i*N+(N-j-1)];
    }
  }
  write_1(f, arrp(buf,0,char),len*N);
}

static void write_GImage32(FILE *f, void *a, int len)
{
  static uint8_t c[4];
  GImage * g = (GImage *) a;
  int i;
  for (i = 0; i < len; i++) {
    c[0] = (uint8_t) ((g[i] & 0xff000000) >> 24);
    c[1] = (uint8_t) ((g[i] & 0x00ff0000) >> 16);
    c[2] = (uint8_t) ((g[i] & 0x0000ff00) >> 8);
    c[3] = (uint8_t) (g[i] & 0x000000ff);

    if (fwrite(c, 1, 4, f) != 4)
      messcrash("Error writing to image\n");
  }
}

static void write_GImage64(FILE *f, void *a, int len)
{
  static uint8_t c[8];
  GImage * g = (GImage *) a;
  int i;
  for (i = 0; i < len; i++) {
    c[0] = (uint8_t) ((g[i] & 0xff00000000000000LL) >> 56);
    c[1] = (uint8_t) ((g[i] & 0x00ff000000000000LL) >> 48);
    c[2] = (uint8_t) ((g[i] & 0x0000ff0000000000LL) >> 40);
    c[3] = (uint8_t) ((g[i] & 0x000000ff00000000LL) >> 32);
    c[4] = (uint8_t) ((g[i] & 0x00000000ff000000LL) >> 24);
    c[5] = (uint8_t) ((g[i] & 0x0000000000ff0000LL) >> 16);
    c[6] = (uint8_t) ((g[i] & 0x000000000000ff00LL) >> 8);
    c[7] = (uint8_t) (g[i] & 0x00000000000000ffLL);

    if (fwrite(c, 1, 8, f) != 8)
      messcrash("Error writing to image\n");
  }
}

void (*writes_[5])() = {
  write_1,
  write_2,
  write_4,
  write_N,
  write_GImage64
};

void (*writes_swapped[5])() = {
  write_1,
  write_2_swapped,
  write_4_swapped,
  write_N_swapped,
  write_GImage64
};


/*
 * The following code is, frankly, horrible. It appears to try and determine
 * at run-time what the endian type of the running architecture is. This
 * should probably be replaced by checking one of the system #defines to
 * see what should be used at compile time. (I've NEVER seen systems
 * change their endian nature during runtime). dgm - 14/8/2003
 */
void (*(*writes))();

static void set_writes(void)
{
  int indian = 1;
  if (*(char *) &indian)
    writes = writes_swapped;
  else
    writes = writes_;
}

/**
 * Opens the two files for the GAP database. The file names are as follows:
 * "project.version" and "project.version.aux". If the force flag is true
 * then the files will be truncated if they already exist. If this flag
 * evaluates to false then the open will fail and terminate the application
 * with a fatal error.
 *
 * @param project is the string depicting the name of the project.
 * @param version is the string depicting the version of the project.
 * @param force is a flag that indicates whether the routine should truncate
 *     the database files or whether it should insist that they do not already
 *     exist.
 * @param gap_db data structure is initalised and returned through the final
 *     parameter.
 */
static void open_gap_db_write(char *project, char *version, int force,
			      Gap_db *gap_db)
{
  char image[MAX_FILENAME_LEN * 2 + 2];    /* Name of image file */
  char aux[MAX_FILENAME_LEN * 2 + 6];      /* Name of aux file   */
  char *s;
  
  int oflag;           /* File permissions */
  int fdimage;         /* Descriptor of image file */
  int fdaux;           /* Descriptor of aux file   */

  /* Used to be done in init_gap_db() */
  gap_db->records     = arrayCreate(1024, AuxIndex);

  /* When initializing num_records, take into account the database header */
#ifdef GAP4_SOURCES
  gap_db->num_records = GR_Contig_Order + 1;
#else /* GAP4_SOURCES */
  gap_db->num_records = GR_Unknown + 1;
#endif  /* GAP4_SOURCES */

  gap_db->image_count = 0;
  gap_db->image       = 0;
  gap_db->aux         = 0;
  gap_db->flags       = 0;

  /*open here*/
  set_writes();
  
  for(s=project;*s;s++) *s = toupper((int)*s);
  for(s=version;*s;s++) *s = toupper((int)*s);
  
  snprintf(image, sizeof(image), "%s.%s",     project, version);
  snprintf(aux,   sizeof(aux),   "%s.%s.aux", project, version);

#ifndef S_DEFFILEMODE
#define S_DEFFILEMODE   (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)
#endif
  if (force) {
    oflag = O_WRONLY | O_CREAT | O_TRUNC;
  } else {
    oflag = O_WRONLY | O_CREAT | O_EXCL;
  }

  /* Here we use open(2) to open the files as we have more control over
     permissions than is possible with fopen(3).  However, we want to use
     fwrite for its buffering, so we have to use fdopen to get a FILE*
     for each descriptor */

  if (-1 == (fdimage = open(image, oflag, S_DEFFILEMODE)))
    messcrash("Error opening database for create\n");

  if (!(gap_db->image = fdopen(fdimage, "wb")))
    messcrash("Error opening database for create\n");

  if (-1 == (fdaux   = open(aux,   oflag, S_DEFFILEMODE)))
    messcrash("Error opening database index for create\n");

  if (!(gap_db->aux = fdopen(fdaux, "wb")))
    messcrash("Error opening database index for create\n");
}

static void close_gap_db(Gap_db *gap_db)
{
  fclose(gap_db->image);
  fclose(gap_db->aux);
}

/*************************************************************
 * Higher level IO Routines
 *************************************************************/

static char * zeros(int N)
{
  static Array buf = NULL;
  
  if(buf==NULL) {
    buf = arrayCreate(BLOCKSIZE,char);
  }
  arrayp(buf, N, char);
  return arrp(buf, 0, char);
}

/**
 * Return the next available gap 'object' number this also increments the
 * record number. This provides a source of gap identifiers that are valid
 * for the supplied gap database object.
 *
 * @param gap_db is the handle for the gap database that is currently being
 *     used.
 *
 * @returns the next available gap object identifier. Also updates the record
 *     in the gata database object to indicate that the id has been allocated.
 */
static GCardinal get_next_record_id(Gap_db *gap_db) {
    return(gap_db->num_records++);
}

/**
 * Write the supplied record to the gap database. The record number (according
 * to the gap database is returned.
 *
 * @param gap_db is the handle for the gapa database to be written to.
 * @param record is the record number within the gap database that should be
 *     modified. If this is set to be GR_NEW then the next available record
 *     in the gap database will be used.
 * @param type is the type of data that is represented by the gap record.
 * @param buffer is the pointer to the data that should be written.
 * @param size is the length in bytes of the data stored in buffer
 * @param number is the number of items in the buffer.
 *
 * @returns the gap database record number.
 */
static GCardinal write_record(Gap_db * gap_db,
			      GCardinal record, GCardinal type,
			      void *buffer, int size, int number)
{
  int used;
  int part;		/* part block used */
  int require;		/* required to fill block */

  if (record == GR_NEW) {
    record = get_next_record_id(gap_db);
  }
 
#ifdef DEBUG 
  printf("WRITERECORD: %d type=%d offset=%d offset=%d bytes=%d\n",
	 record,
	 type,
	 gap_db->image_count,
	 ftell(gap_db->image),
	 size * number);
#endif
  
  used = size * number + sizeof(GCardinal); /*4 is for type*/

  arrayp(gap_db->records, record, AuxIndex); /* force resize */
  arr(  gap_db->records, record, AuxIndex).image[0] = gap_db->image_count;
  arr(  gap_db->records, record, AuxIndex).time[0]  = STARTDATE;
  arr(  gap_db->records, record, AuxIndex).used[0]  = used;
  arr(  gap_db->records, record, AuxIndex).image[1] = 0;
  arr(  gap_db->records, record, AuxIndex).time[1]  = 0;
  arr(  gap_db->records, record, AuxIndex).used[1]  = 0;
  
  (writes[WRITE_INT_4])(gap_db->image, &type, 1);
  switch (size)
    {
    case 1:  (writes[WRITE_INT_1])(gap_db->image, buffer, number);       break;
    case 2:  (writes[WRITE_INT_2])(gap_db->image, buffer, number);       break;
    case 4:  (writes[WRITE_INT_4])(gap_db->image, buffer, number);       break;
    default: (writes[WRITE_INT_N])(gap_db->image, buffer, number, size); break;
    }

  part = used % BLOCKSIZE;
  require = part ? (BLOCKSIZE - part) : 0;
  (writes[WRITE_INT_1])(gap_db->image, zeros(require), require);
  gap_db->image_count += used + require;
  
  return record;
}

static int block(int N, int Mx)
{
  return ((N / Mx) + 1) * Mx;
}

static void write_aux_header(Gap_db *gap_db, AuxHeader *header)
{
  int zero = 0;
  int one  = 1;

  if ((gap_db->flags & AUX_64_BIT_MODE) != 0) {
    writes[WRITE_GIMAGE] = write_GImage64;
  } else {
    writes[WRITE_GIMAGE] = write_GImage32;
  }

  writes[WRITE_GIMAGE    ](gap_db->aux, &header->file_size,   1);
  writes[WRITE_GCARDINAL ](gap_db->aux, &header->block_size,  1);
  writes[WRITE_GCARDINAL ](gap_db->aux, &header->num_records, 1);
  writes[WRITE_GCARDINAL ](gap_db->aux, &header->max_records, 1);
  writes[WRITE_GTIMESTAMP](gap_db->aux, &header->last_time,   1);
  writes[WRITE_GHFLAGS   ](gap_db->aux, &header->flags,       1);
  writes[WRITE_GHFLAGS   ](gap_db->aux, &header->spare1,      1);
  writes[WRITE_GTIMESTAMP](gap_db->aux, &zero, 1); /* freetree */
  if ((gap_db->flags & AUX_64_BIT_MODE) != 0) {
    writes[WRITE_INT_4     ](gap_db->aux,  header->spare,      7);
    writes[WRITE_INT_4     ](gap_db->aux,  &one,               1);
  } else {
    writes[WRITE_INT_4     ](gap_db->aux,  header->spare,      8);
    writes[WRITE_INT_4     ](gap_db->aux,  &zero,              1);
  }
}

static void write_aux_index(Gap_db *gap_db, AuxIndex *idx)
{
  writes[WRITE_GIMAGE    ](gap_db->aux, idx->image, 2);
  writes[WRITE_GTIMESTAMP](gap_db->aux, idx->time,  2);
  writes[WRITE_GCARDINAL ](gap_db->aux, idx->used,  2);
}

static void write_index(Gap_db *gap_db)
{
  AuxHeader header;
  int i;

  /*************************************************************
   * Header
   *************************************************************/
  gap_db->flags |= AUX_64_BIT_MODE; /* always on now */
  header.file_size   = gap_db->image_count;
  header.block_size  = BLOCKSIZE;
  header.num_records = gap_db->num_records;
  header.max_records = block(gap_db->num_records + 50000, 100000);
  header.last_time   = STARTDATE;
  header.flags       = 0;
  header.spare1      = 0;
  for(i=0;i<10;i++) header.spare[i] = 0;
  /* check = (low_level_vector[GOP_WRITE_AUX_HEADER])(fdaux, &header); */
  write_aux_header(gap_db, &header);

  /*************************************************************
   * Index
   *************************************************************/
  for(i=0; i < gap_db->num_records; i++) {

    write_aux_index(gap_db, arrayp(gap_db->records, i, AuxIndex));
    
#ifdef DEBUG
    printf("Index %d: offset = %d bytes = %d bytes\n",i,
	   array(gap_db->records, i, AuxIndex).image[0],
	   array(gap_db->records, i, AuxIndex).used[0]);
#endif
  }
}

/*************************************************************
 * Main gap database construction routines
 *************************************************************/

static int check_padded(cafAssembly *caf)
{
  Array   seqs;
  CAFSEQ *seq;
  int i;

  seqs = caf->seqs;
  for (i = 0; i < arrayMax(seqs); i++) {
    seq = arrp(seqs, i, CAFSEQ);
    if (seq->pad_state != padded_state) return 0;
  }

  return 1;
}

/**
 * Checks for consistency of the data. Specificaly that all contigs have
 * reads from which they are assembled. That there are no neads which are
 * not referenced by a contig and that no contigs share reads.
 */
static void check_caf_consistency(cafAssembly *caf, Array contig_ids,
                                  int num_contigs, int num_readings,
                                  int silent) {
    Array  seqs;
    int    i, j, lastsidx=0, lastfidx=0;
    Array  stbl, ftbl;

    /* 
     */
    stbl = arrayCreate(num_readings, int);
    arrayp(stbl, num_readings, int); /* Force resize */

    seqs = caf->seqs;

    ftbl = arrayCreate(arrayMax(seqs), int);
    arrayp(ftbl, arrayMax(seqs), int); /* Force resize */

    /* Scan for all sequences that have type 'is_read' */

    for(i = 0; i < arrayMax(seqs); i++) {
        CAFSEQ *seq;

        seq = arrp(seqs, i, CAFSEQ);

        if(seq->type == is_read) {
            arr(ftbl, lastfidx++, int) = seq->id;
        }
    }

    /* Traverse over all contigs gathering all referenced reads */

    for (i = 0; i < num_contigs; i++) {

        CAFSEQ *contig_seq;
        int    contig_id;
        char   *contig_name;

        contig_id = arr(contig_ids, i, int);
        contig_seq = arrp(seqs, contig_id, CAFSEQ);

        contig_name = dictName(caf->seqDict, contig_seq->id);

        if (contig_seq->assinf && arrayMax(contig_seq->assinf) > 0){

            for (j = 0; j < arrayMax(contig_seq->assinf); j++) {

                ASSINF *af;
                int    read_id;

                af = arrp(contig_seq->assinf, j, ASSINF);
                read_id = af->seq;
                arr(stbl, lastsidx++, int) = read_id;
            }

        } else {
            fprintf(stderr, "ERROR: contig (%s) contains no reads\n",
                contig_name);
        }
    }

    if(lastsidx < lastfidx) {

        /* Currently a very crude search, may need to be speeded up! */

        for(i = 0; i < lastfidx; i++) {
            int sid, found = 0;

            sid = arr(ftbl, i, int);

            for(j = 0; j < lastsidx; j++) {
                if(arr(stbl, j, int) == sid) {
                    found = 1;
                    break;
                }
            }
            if(!found) {
                fprintf(stderr, "    Read (%s) is unreferenced.\n",
                     dictName(caf->seqDict, sid));
            }

        }
        fprintf(stderr, "Ignored...\n");
    } else if(lastsidx > lastfidx) {
        /*
         * Should never happen but may indicate multiple references to
         * a given read. Will probably cause a core dump before we get
         * here if the array is not big enough.
         */
    }

}

/**
 * Perform a complete scan through the components of the supplied CAF data
 * structure and extract counts of the various elements within it.
 *
 * @param caf is the input CAF datastructure.
 * @param num_reads is a return parameter which will have its value modified
 *     to be equal to the total number of sequences (not including contigs)
 *     found in the CAF data structure/file.
 * @param num_contigs as with num_reads but only includes those sequences
 *     that are identified as being contigs.
 * @param num_templates is a return parameter which will have its value
 *     modified to be equal to the number of templates found in the CAF 
 *     data.
 * @param num_clones is a return parameter which will have its value
 *     modified to be equal to the number of clones found in the CAF 
 *     data.
 * @param num_annotations is a return parameter which will have its value
 *     modified to be equal to the number of annotations found in the CAF 
 *     data. (Tags lines)
 * @param silent is a boolean flag which indicates if verbose diagnostics
 *     should be output or not.
 */
static void count_caf_items(cafAssembly *caf,
			    int *num_reads,     int *num_contigs,
			    int *num_templates, int *num_clones,
			    int *num_tags, int *num_notes, int silent)
{
    Array   seqs, contig_ids;
    int i = 0, all_read_count = 0;
    CAFSEQ *seq=NULL;
  
    *num_reads     = 0;
    *num_contigs   = 0;
    *num_templates = dictMax(caf->templateDict);
    *num_clones    = dictMax(caf->cloneDict);
    *num_notes     = 0;
    *num_tags      = 0;
  
    /*
     * temporary collection of ids of non-empty contigs grows as required
     */
    contig_ids = arrayCreate(BLOCKSIZE, int);
   
    seqs = caf->seqs;
  
    /*
     * Because of the problem with empty contigs and disconnected reads (those
     * where a read is present in the file but no contig refers to it) this
     * code now works by finding the number of non-empty contigs and then
     * following the 'assembled from information (assinf)' 
     */
  
    for (i = 0; i < arrayMax(seqs); i++) {
        seq = arrp(seqs, i, CAFSEQ);
  
        if(seq->type == is_contig) {
            if(seq->assinf && arrayMax(seq->assinf) > 0) {
                array(contig_ids, (*num_contigs)++, int) = i;
            } else {
                fprintf(stderr, "WARNING: Empty contig (%s) found. Ignoring it...\n",
                    dictName(caf->seqDict, seq->id));
            }
        } else if(seq->type == is_read) {
            all_read_count++;
        }
    }

    for (i = 0; i < *num_contigs; i++) {

        CAFSEQ *contig_seq;
        int    j, contig_id;

        contig_id = arr(contig_ids, i, int);
        contig_seq = arrp(seqs, contig_id, CAFSEQ);

        /*
         * Because we only stored contigs that contained reads we dont need
         * to check again.
         */
        for (j = 0; j < arrayMax(contig_seq->assinf); j++) {

            ASSINF    *af;
            CAFSEQ    *read;

            af = arrp(contig_seq->assinf, j, ASSINF);
            read = arrp(seqs, af->seq, CAFSEQ);

            if(read->type != is_read) {
                fprintf(stderr, "Fatal internal error: invalid assembled from\n");
                exit(1);
            }

            (*num_reads)++;

	    if (!seq->template_id) {
	    /* Reads with no template_id will get a fake template record later,
	       as gap4 doesn't like reads with no template.  We need to account
	       for this when counting templates. */
	        (*num_templates)++;
	    }

            if (read->tags)
                (*num_tags) += arrayMax(read->tags);

            if (read->notes)
                (*num_notes) += arrayMax(read->notes);
        }
    }

    if(!silent && all_read_count != *num_reads) {
        fprintf(stderr, "WARNING: Un-referenced reads found : Searching...\n");
	check_caf_consistency(caf, contig_ids, *num_contigs, *num_reads, silent);
    }

    if(!silent) {
        fprintf(stderr, "Parsed data for %d sequences\n",
            *num_reads + *num_contigs);
    }

#ifdef DEBUG
    fprintf(stderr,
            "num_reads = %d num_contigs = %d num_tags = %d num_notes = %d\n",
            *num_reads, *num_contigs, *num_tags, *num_notes);
#endif

    arrayDestroy(contig_ids);
}

/**
 * Gather the indicies of the contig sequences from the complete set
 * of sequences contained in the CAF file.
 *
 * It does this by performing a complete scan of all the sequences found
 * in the CAF file and assigns the value of the index to consecutive 
 * elements in the results array when a contig sequence is found.
 * 
 * @param caf is the source of the CAF file data.
 * @param contig_ids is the in-out parameter which is passed an, initially,
 *    empty array and will, at the return of this routine contain the
 *    positions of the contig sequences within the complete pool of
 *    sequences found in the CAF file.
 * @param num_contigs is the number of contigs expected. This appears to
 *    only be used for initial sizing of the results array.
 */
static void get_contig_seqids(cafAssembly *caf, Array contig_ids,
			      int num_contigs)
{
  Array   seqs;
  CAFSEQ *seq;
  int i = 0;
  int j = 0;
  
  array(contig_ids, num_contigs, int) = 0; /* Force resize */

  seqs = caf->seqs;
  for (i = 0; i < arrayMax(seqs); i++) {
    seq = arrp(seqs, i, CAFSEQ);
    if (seq->type == is_contig) {
    	if(seq->assinf && arrayMax(seq->assinf) > 0) { // ignore empty contigs
      	    arr(contig_ids, j, int) = i;
      	    j++;
	}
    }
  }
}

static int af_compare(const void *a, const void *b)
{
  int mina;
  int minb;
  ASSINF *aa = (ASSINF *) a;
  ASSINF *bb = (ASSINF *) b;

  mina = min(aa->s1, aa->s2);
  minb = min(bb->s1, bb->s2);
  
  return (mina - minb);
}

void sort_assembled_from(cafAssembly *caf, Array contig_ids, int num_contigs)
{
  Array   seqs;
  CAFSEQ *seq;
  ASSINF *assembled_froms;
  int i = 0;
  int contig_id;
  
  seqs = caf->seqs;
  for (i = 0; i < num_contigs; i++) {
    contig_id = arr(contig_ids, i, int);
    seq = arrp(seqs, contig_id, CAFSEQ);
    if (!seq->assinf) continue;

    assembled_froms = arrp(seq->assinf, 0, ASSINF);
    qsort(assembled_froms, arrayMax(seq->assinf), sizeof(ASSINF), af_compare);
  }
}

/**
 * Generate a data structure containing references to all of the reads
 * that are contained within the supplied CAF data structure.
 *
 * @param caf is the data structure generated by parsing the CAF data file.
 * @param contig_ids is an array containing the sequence id's (relative to
 *     caf->seqs) of the contigs within the caf data structure.
 * @param num_contigs is the number of contigs that are present in the CAF
 *     data structure.
 * @param Readings is the output parameter where the data structure containing
 *     the reads will be generated.
 */
void assign_staden_ids(cafAssembly *caf, Array contig_ids, int num_contigs,
		       Id_map *Readings)
{
  Array  seqs;
  CAFSEQ *contig_seq;
  ASSINF *af;
  int i, j;
  int contig_id;
  int read_id;

  seqs = caf->seqs;
  array(Readings->obj_num, arrayMax(seqs), int) = 0; /* Force resize */
  
  for (i = 0; i < num_contigs; i++) {
    contig_id = arr(contig_ids, i, int);
    contig_seq = arrp(seqs, contig_id, CAFSEQ);
    if (!contig_seq->assinf) continue;
    
    for (j = 0; j < arrayMax(contig_seq->assinf); j++) {
      af = arrp(contig_seq->assinf, j, ASSINF);
      read_id = af->seq;
      
      if (arr(Readings->obj_num, read_id, int) != 0) {
	continue;
      }
      
      arr(Readings->obj_num, read_id, int) = ++Readings->curr_id;
    }
  }
}


/**
 * Version of assign_staden_ids() that attempts to make use of the Staden_id
 * fields in the CAF file. It will attempt (although there are untold
 * pathological situations that could occur) to generate a GAP database where
 * the Staden_id's are correctly preserved.
 *
 * @param caf is the data structure generated by parsing the CAF data file.
 * @param contig_ids is an array containing the sequence id's (relative to
 *     caf->seqs) of the contigs within the caf data structure.
 * @param num_contigs is the number of contigs that are present in the CAF
 *     data structure.
 * @param num_readings is the count of the number of sequences in the CAF 
 *     data structure that have been marked as Is_read.
 * @param Readings is the output parameter where the data structure containing
 *     the reads will be generated.
 * @param silent is a boolena flag that indicates if diagnostic warnings should
 *     be output.
 */

void assign_staden_ids_preserve(cafAssembly *caf, Array contig_ids,
                       int num_contigs, int num_readings, Id_map *Readings,
                       int silent)
{
    Array    seqs;
    int      i, j;

    seqs = caf->seqs;


    /* Checks and corrects Staden id assignements */
    sanitise_staden_ids(caf, contig_ids, num_contigs, num_readings, silent);

    array(Readings->obj_num, arrayMax(seqs), int) = 0; /* Force resize */
  
    for (i = 0; i < num_contigs; i++) {
        CAFSEQ *contig_seq;
        int    contig_id;

        contig_id = arr(contig_ids, i, int);
        contig_seq = arrp(seqs, contig_id, CAFSEQ);

        if (!contig_seq->assinf)
            continue;

        for (j = 0; j < arrayMax(contig_seq->assinf); j++) {

            CAFSEQ *seq;
            ASSINF *af;
            int    read_id;

            af = arrp(contig_seq->assinf, j, ASSINF);
            read_id = af->seq;

            if (arr(Readings->obj_num, read_id, int) != 0) {
	        continue;
            }

            seq = arrp(seqs, read_id, CAFSEQ);

            arr(Readings->obj_num, read_id, int) = seq->staden_id;
            Readings->curr_id++;
        }
    }
}

/**
 * 'Sanitises' the staden identifiers found in the CAF records that have been
 * read in. It checks that the Staden identifiers are reasonable, that is, that
 * they are positive integers, greater than zero and less than the total 
 * number of reads in the file. It also checks that there are no duplicates
 * of staden ids. For all valid identifiers the staden id is left untouched.
 * All invalid id's are changed to be valid (although random) and may be
 * distributed throughout the valid range. <B>NOTE</B> This should probably be
 * split into multiple subroutines at some point. However it does perform a
 * reasonable chunk of work that logically hangs together. Review later.
 *
 * @param seqs is the Array of CAFSEQ 'objects' that have been read in from
 *     the source CAF file. This is in the same order as the CAF file. This
 *     is effectively a in-out parameter with the data pointed-to by this
 *     parameter being modified directly by this routine.
 * @param num_readings is the count of actual reads within the seqs set. That
 *     is elements that are not contigs.
 */ 

static void sanitise_staden_ids(cafAssembly *caf, Array contig_ids,
                                int num_contigs, int num_readings,
                                int silent) {

    Array    seqs, stbl;
    Id_array invalid_tbl, free_tbl;
    int      i;
    int      excess_sid = 0, negative_sid = 0, zero_sid = 0, valid_sid = 0;
    int      duplicate_sid = 0, max_sid;
    int      num_ref_reads = 0;
    
    seqs = caf->seqs;

    /*
     * Sanity checks on the Staden id's. No negative ids, no id's > than
     * the number of sequences in the database and no duplicate id's.
     */

    /* 
     * Array is indexed by staden id and contains a count of the number
     * of times each staden id are ecountered during a complete pass
     * through the sequences. NB. to simplify the logic I allocate the array
     * the correct size but subtract 1 from staden id's when indexing.
     */
    stbl = arrayCreate(num_readings, Staden_id_map);
    arrayp(stbl, num_readings, Staden_id_map); /* Force resize */

    /*
     * table of read identifiers (in the CAF sense, not the staden sense)
     */
    invalid_tbl.count = 0;
    invalid_tbl.data = arrayCreate(0, int); 

    /*
     * table of slots within the table (as sorted by staden id) that are
     * available for use (i.e. have not been mapped to preserved staden
     * id's and are not duplicates).
     */
    free_tbl.count = 0;
    free_tbl.data = arrayCreate(0, int); 

    max_sid = num_readings; /* Should never see a staden id > than this */
    
    for(i = 0; i < num_contigs; i++) {

        CAFSEQ *contig_seq;
        int    contig_id;
        int    j;

        contig_id = arr(contig_ids, i, int);
        contig_seq = arrp(seqs, contig_id, CAFSEQ);

        if (!contig_seq->assinf)
            continue;

        for (j = 0; j < arrayMax(contig_seq->assinf); j++) {

           CAFSEQ *seq;
           ASSINF *af;
           int    read_id;
           char *read_name;
    
           af = arrp(contig_seq->assinf, j, ASSINF);
           read_id = af->seq;

           seq = arrp(seqs, read_id, CAFSEQ);
           read_name = dictName(caf->seqDict, read_id);
    
           /*
            * Count the number of valid, invalid staden identifiers. If they are
            * invalid we clearly mark the staden id as being invalid by setting
            * it to -1 for that record.
            */
           if(seq->type == is_read) { /* Must be a read */

               num_ref_reads++;

               if(seq->staden_id == 0) {
                   zero_sid++;
                   seq->staden_id = -1;
                   array(invalid_tbl.data, invalid_tbl.count, int) = read_id;
                   invalid_tbl.count++;
                   
                   if(!silent) {
                       fprintf(stderr, "WARNING: zero staden ID found for %s\n",
                           read_name?read_name:"read name not available");
                   }
               } else if(seq->staden_id > max_sid) {
                   excess_sid++;
                   seq->staden_id = -1;
                   array(invalid_tbl.data, invalid_tbl.count, int) = read_id;
                   invalid_tbl.count++;
    
                   if(!silent) {
                       fprintf(stderr, "WARNING: staden ID (out of range) found for %s\n",
                           read_name?read_name:"read name not available");
                   }
               } else if(seq->staden_id < 0) {
                   negative_sid++;
                   seq->staden_id = -1;
                   array(invalid_tbl.data, invalid_tbl.count, int) = read_id;
                   invalid_tbl.count++;
    
                   if(!silent) {
                       fprintf(stderr, "WARNING: negative staden ID found for %s\n",
                           read_name?read_name:"read name not available");
                   }
    
               } else {
    
                   /*
                    * Builds an array of records, indexed by staden_id and
                    * which contains a count of the reads having that staden
                    * id and also the list of the read ids that had that 
                    * staden id.
                    */
                   Staden_id_map *stats;
    
                   /*
                    * NOTE: the seq->staden_id-1, remember to add one if using
                    * this value as a staden id later..
                    */
                   stats = arrp(stbl, seq->staden_id-1, Staden_id_map);
    
                   /*
                    * Initalise the array to hold the CAF read identifier
                    * this will end up being of length 'stats->count'
                    */
                   if(stats->read_ids == NULL) {
                       stats->read_ids = arrayCreate(1, int);
                   }
    
                   array(stats->read_ids, stats->count, int) = read_id;
    
                   stats->count++;
    
                   if(stats->count > 1) { /* duplicate */
                       duplicate_sid++;
    
                       /*
                        * First element in array associated with this id is now
                        * known to be duplicate. Set its id to -1
                        */
                       if(!stats->dup_processed) {
                           int seq_id;
                           CAFSEQ *seq;
    
    
    		       /* Single shot processing of this entry */
                           stats->dup_processed = 1;
    
                           duplicate_sid++; /* count the first one */
    
           		   seq_id = arr(stats->read_ids, 0, int);
                           seq =  arrp(seqs, seq_id, CAFSEQ);
                           read_name = dictName(caf->seqDict, seq_id);
    
                           if(!silent) {
                               fprintf(stderr, "WARNING: duplicate staden ID found: %d for %s\n",
                                  seq->staden_id,
                                  read_name?read_name:"read name not available");
                           }
    
                           array(invalid_tbl.data, invalid_tbl.count, int) = seq_id;
                           invalid_tbl.count++;
    
                           seq->staden_id = -1;
                       }
    
                       read_name = dictName(caf->seqDict, read_id);
    
                       if(!silent) {
                           fprintf(stderr, "WARNING: duplicate staden ID found: %d for %s\n",
                              seq->staden_id,
                              read_name?read_name:"read name not available");
                       }
    
                       seq->staden_id = -1;
    
                       array(invalid_tbl.data, invalid_tbl.count, int) = read_id;
                       invalid_tbl.count++;
    
                   } else {
                       valid_sid++;
                   }
               }
           }
       }
    }

   /*
    * Gather the list of slots (staden id's) that have not been used by
    * the reads which have valid staden identifiers. We will use these later
    * to hold those reads that didn't have valid staden ids.
    *
    * Out by 1 error, for some reason arrayMax seems to be 1 > than expected
    */
   for(i = 0; i < num_ref_reads; i++) {
       Staden_id_map *stats;

       stats = arrp(stbl, i, Staden_id_map);

       if(stats->count != 1) {
           array(free_tbl.data, free_tbl.count, int) = i;
           free_tbl.count++;
       }
   }

   /*
    * Sanity check. We could probably proceed by resizing the array. However
    * my understanding of the code has failed to cover some case if this
    * occurs.
    */
   if(invalid_tbl.count != free_tbl.count) {
       fprintf(stderr, "Unexpected mismatch in staden id table... DEBUG:\n");
       fprintf(stderr, "invalid_tbl.count = %d\n free_tbl.count = %d\n", invalid_tbl.count, free_tbl.count);
       fprintf(stderr, "arrayMax(seqs) = %d\n", arrayMax(seqs));
       exit(1);
   }

   /*
    * Assign the staden identifiers for each of the sequence records that
    * originally had invalid ones. These are drawn, in order, from the free
    * ones that remained in the table.
    */
   for(i = 0; i < invalid_tbl.count; i++) {
       CAFSEQ *seq;
       int sid, cid; /* staden id, caf id */
       char *read_name;


       cid = arr(invalid_tbl.data, i, int);
       sid = arr(free_tbl.data, i, int);

       seq = arrp(seqs, cid, CAFSEQ);

       seq->staden_id = sid+1; /* convert to 1 based */

       read_name = dictName(caf->seqDict, cid);

       if(!silent) {
           fprintf(stderr, "INFO: Assigned Staden id %d to %s\n",
               seq->staden_id, read_name);
       }
       
   }

  if(!silent) {
      fprintf(stderr, "Staden-Id Stats: %d-valid, %d-duplicates, %d-invalid, %d-zero\n",
               valid_sid, duplicate_sid, excess_sid + negative_sid, zero_sid);
  }
}


static int write_string_len(Gap_db *gap_db, char* str, int len)
{
  return write_record(gap_db, GR_NEW, GT_Text, str, sizeof(char), len);
}

static int write_string(Gap_db *gap_db, char* str)
{
  return write_string_len(gap_db, str, strlen(str));
}

static void convert_to_staden(char* dna, int len)
{
  /* New:
   *  
   *  - => *  Padding char
   *  N => -  Undefined base
   *  n => -  Undefined base
   *  * => *  Padding char (should not happen :)
   *
   */

    static char mx[256];
    static unsigned char bases[]   = "CTAGctag-NnXx";
    static unsigned char cbases[]  = "CTAGctag*----";
    static int init = 0;
    int i;

    if (!init) {
      init++;
      for (i=0;i<256;i++) { mx[i] = '-'; }
      
      for (i=0;i<sizeof(bases);i++) {
	mx[bases[i]] = cbases[i];
      }
    }
    
    for(i = 0; i < len; i++) {
      dna[i] = mx[(unsigned char) dna[i]];
    }
}

static void len_reverse_seq(char *dna, int len)
{
  char *start = dna;
  char *end   = dna + len - 1;
  char t;

  for (; start < end; start++, end--) {
    t = *start;
    *start = *end;
    *end = t;
  }
}

static void len_complement_seq(char *dna, int len)
{
  static char mx[256];
  static unsigned char bases[]  = "CTAGctag*NnXx";
  static unsigned char cbases[] = "GATCgatc*nnnx";
  static int init = 0;
  int i;
  
  if(!init) {
    init++;
    for (i=0;i<256;i++) { mx[i] = '-'; }
    
    for (i=0;i<sizeof(bases);i++) {
      mx[bases[i]] = cbases[i];
    }
  }

  for(i=0; i<len; i++) {
    dna[i] = mx[(unsigned char) dna[i]];  
  }
}

static void len_reverse_complement(char *dna, int len)
{
  len_reverse_seq(dna, len);
  len_complement_seq(dna, len);
}

static int write_sequence(Gap_db *gap_db, Stack dna_stack, int read_sense,
			  int len)
{
  char *dna, *converted_dna;
  static Array buf = NULL;

  dna = stackText(dna_stack, 0);
  
  /* Make some temp space to store the dna sequence */

  if (buf == NULL) {
    /* 30000 is as big as a read gets at the moment.  It will expand anyway
       if we try to make a bigger one */
    buf = arrayCreate(30000, char);
  }
  arrayp(buf, len, char);
  converted_dna = arrp(buf, 0, char);

  strncpy(converted_dna, dna, len);
  convert_to_staden(converted_dna, len);
  if (read_sense == GAP_SENSE_REVERSE) {
    len_reverse_complement(converted_dna, len);
  }
  
  return write_string_len(gap_db, converted_dna, len);
}

static int write_confidence(Gap_db *gap_db, Array quals, int read_sense,
			    int len)
{
  /* The base qualities should really be unsigned char, but if we make them
     a char here we can call all sorts of string-based routines like
     complement_seq without having to typecast.  The qualities never get
     outside 0 .. 100 and we don't do any maths on them, so we should get
     away with it.  */

  char *q;
  char qfaked = FALSE; /* if quals is null then we fake a set of values */

  int string_num;

  if(quals) {
      q = arrp(quals, 0, char);
  } else {
      int i;

      qfaked = TRUE; /* signal cleanup needed */
      if((q = (char *) malloc(len)) == NULL) {
          fprintf(stderr, "Malloc failed to allocate %d bytes... exiting\n",
              len);
          exit(1);
      }

      for(i=0; i <len; i++) {
          q[i] = FAKED_QVALUE;
      }
  }

  if (read_sense == GAP_SENSE_REVERSE) {
    len_reverse_seq(q, len);
  }

  string_num = write_record(gap_db, GR_NEW, GT_Data, q, sizeof(char), len);

  if (read_sense == GAP_SENSE_REVERSE) {
    /* put it back as it was in case anything else needs to look at it... */
    len_reverse_seq(q, len);
  }

  if(qfaked)
      free(q);

  return string_num;
}

static int write_positions(Gap_db *gap_db, Array assinf, int read_sense,
			   int len)
{
  static Array buf = NULL;
  int2 *positions;
  ASSINF *a, *align_to_scf;
  int af_num, read_pos, scf_pos;
  int last_range_end;
  int i, j, t, end;

  /* Make some temp space to store the base positions */

  if (buf == NULL) {
    /* 30000 is as big as a read gets at the moment.  It will expand anyway
       if we try to make a bigger one */
    buf = arrayCreate(30000, int2);
  }
  arrayp(buf, len, int2);
  positions = arrp(buf, 0, int2);
  
  align_to_scf = arrp(assinf, 0, ASSINF);
  qsort(align_to_scf, arrayMax(assinf), sizeof(ASSINF), af_compare);

  last_range_end = 0;

  for (af_num = 0; af_num < arrayMax(assinf); af_num++) {
    a = align_to_scf + af_num;

    /* just in case there are any holes... */
    end = a->s1 - 1 < len ? a->s1 - 1 : len;
    for (read_pos = last_range_end; read_pos < end; read_pos++) {
      positions[read_pos] = 0;
    }

    scf_pos = a->r1;
    end = a->s2 < len ? a->s2 : len;

    for (read_pos = a->s1 - 1; read_pos < end; read_pos++, scf_pos++) {
      positions[read_pos] = scf_pos;
    }

    last_range_end = a->s2;
  }

  /* just in case there are any holes... */
  for (read_pos = last_range_end; read_pos < len; read_pos++) {
    positions[read_pos] = 0;
  }

  if (read_sense == GAP_SENSE_REVERSE) {
    /* Need to turn everything around */
    for (i = 0, j = len - 1; i < j; i++, j--) {
      t = positions[i];
      positions[i] = positions[j];
      positions[j] = t;
    }
  }

  return write_record(gap_db, GR_NEW, GT_Data, positions, sizeof(int2), len);
}

#if 0
static int write_positions_pads(Gap_db *gap_db, Stack dna_stack,
				int read_sense, int read_start, int read_end)
#endif
static int write_positions_pads(Gap_db *gap_db, Stack dna_stack,
				int read_sense, int len)
{
  static Array buf = NULL;
  int2 *positions;
  int2 t;
  char *dna;
  int read_pos, scf_pos, i, j;
  
  if (NULL ==  buf) {
    /* 30000 is as big as a read gets at the moment.  It will expand anyway
       if we try to make a bigger one */
    buf = arrayCreate(30000, int2);

    /* Now would be a good time to print a whingy message, 'cos calling this
       function is a bad thing to have to do */
    fprintf(stderr,
	    "Warning: A read with no Align_to_SCF record has been found.\n"
	    "Some traces may not align correctly to the corresponding reads\n");
  }
  arrayp(buf, len, int2);
  positions = arrp(buf, 0, int2);

  dna = stackText(dna_stack, 0);

  scf_pos = 0;
  for (read_pos = 0; read_pos < len; read_pos++) {
    if ('-' == dna[read_pos]) {
      positions[read_pos] = 0;
    } else {
      positions[read_pos] = ++scf_pos;
    }
  }

  if (read_sense == GAP_SENSE_REVERSE) {
    /* Need to turn everything around */
    for (i = 0, j = len - 1; i < j; i++, j--) {
      t = positions[i];
      positions[i] = positions[j];
      positions[j] = t;
    }
  }

  return write_record(gap_db, GR_NEW, GT_Data, positions, sizeof(int2), len);

#if 0
  fprintf(stderr, "FIXME: write_positions_pads not implemented\n");
  exit(3);
  return 1;
#endif
}

static int clip_coords(int *from, int *to, int clip_left, int clip_right)
{
  if (*from <= *to) { /* Forward strand */

    if (clip_left > 0) {
      if (*to   < clip_left) return 1;
      if (*from < clip_left) *from = clip_left;
    }
    if (clip_right > 0) {
      if (*from > clip_right) return 2;
      if (*to   > clip_right) *to = clip_right;
    }

  } else {            /* Reverse strand */

    if (clip_left > 0) {
      if (*from < clip_left) return 1;
      if (*to   < clip_left) *to = clip_left;
    }
    if (clip_right > 0) {
      if (*to   > clip_right) return 2;
      if (*from > clip_right) *from = clip_right;
    }
  }
  return 0;
}

static GCardinal char2type(char *s)
{
  unsigned char t[sizeof(GCardinal)];
  int len = min(strlen(s), sizeof(GCardinal));

  memcpy(&t[0],s,len);
  if (len < sizeof(GCardinal)) memset(&t[len],' ',sizeof(GCardinal) - len);

  return (t[0]<<24) + (t[1]<<16) + (t[2]<<8) + (t[3]<<0);
}

static void add_tags(Gap_db *gap_db, cafAssembly *caf, Array annotations,
		     Array caf_tags, int clip_left, int clip_right, int adjust,
		     char *expected_type)
{
  int          i;
  int          strand;
  int          from;
  int          to;
  GAnnotations *gap_tag     = 0;
  TAG          *caf_tag     = 0;
  char         *tag_type    = 0;
  char         *tag_comment = 0;
  static Array new_comment  = 0;
  int          tag_num      = arrayMax(annotations);

  if (!caf_tags) return;

  for (i = 0; i < arrayMax(caf_tags); i++) {
    caf_tag = arrp(caf_tags, i, TAG);
    
    from = caf_tag->x1;
    to   = caf_tag->x2;
    tag_type = dictName(caf->tagTypeDict, caf_tag->type);
    tag_comment = caf_tag->text;
    /*
     * Compute strand before clipping because strand cannot be determined from
     * a tag clipped to length of 1 base.
     */

    strand = from <= to ? GAP_STRAND_FORWARD : GAP_STRAND_REVERSE;

    if (!clip_coords(&from, &to, clip_left, clip_right)) {
      gap_tag = arrayp(annotations, tag_num++, GAnnotations);
      
      gap_tag->strand   = strand;

      if (strand == GAP_STRAND_FORWARD) {
	/* FORWARD STRAND */
	gap_tag->position = from - adjust;
	gap_tag->length   = to - from + 1;

      } else {
	/* REVERSE STRAND */
	gap_tag->position = to - adjust;
	gap_tag->length   = from - to + 1;
	
      }

      /* For SVEC and CVEC tags, if the tag type is not SVEC or CVEC
	 it has to be encoded in the annotation comment so we can get it
	 out later */

      if (expected_type && strcmp(tag_type, expected_type)) {
	if (!new_comment) {
	  new_comment = arrayCreate(1000, char);
	}
	arrayp(new_comment,
	       (strlen(tag_type)
		+ (tag_comment ? strlen(tag_comment) : 0)
		+ 14),
	       char);
	sprintf(arrp(new_comment, 0, char),
		"CAF:Method=%s::%s", tag_type, tag_comment ? tag_comment : "");
	tag_type    = expected_type;
	tag_comment = arrp(new_comment, 0, char);
      }

      gap_tag->type = char2type(tag_type);
      gap_tag->annotation = (tag_comment
			     ? write_string(gap_db, tag_comment)
			     : 0);
    }
  }
}

/**
 * Adds the notes found in the caf_notes array to the gap_notes array
 * extending the latter if required. The format of the record types in
 * each of these two arrays are different. This routine also performs
 * the translation. In additon, the annotation strings contained within
 * each of the CAF notes is serialised to the gap database.
 * 
 * This mixing of data structure translation and external file access is
 * very poor design. It should be isolated at some point but is done this
 * way to remain consistent with add_tags().
 *
 * @param gap_db is the gap database that should have the data written to.
 * @param caf is the CAF assembly object containing the symbol table for
 *     translation of note 'types' into a string.
 * @param gap_notes is the input-output array reference for the array that
 *     contains the notes in GAP format (that is GNotes structures).
 * @param caf_notes is the input array of CAF NOTES structures to be 
 *     translated and trabscribed into gap_notes.
 */
static void add_notes(Gap_db *gap_db, cafAssembly *caf, Array gap_notes,
		     Array caf_notes)
{
    int          i;

    GNotes       *gap_note   = NULL;
    NOTE         *caf_note   = NULL; /* internal format, no next, prev links */

    int          ctime       = 0;
    int          ctime_top   = 0;
    int          mtime       = 0;
    int          mtime_top   = 0;
    char         *note_type  = NULL;
    char         *note_text  = NULL;


    int          note_num    = arrayMax(gap_notes);

    if (!caf_notes) return;

    for (i = 0; i < arrayMax(caf_notes); i++) {

        /*
         * FOREACH 'note' in CAF format:
         *    extract the content fields (translating to strings if neccessary)
         *    ready to re-pack into a GAP data structure and if neccessary
         *    create gap 'objects' for the annotation strings contained in
         *    the CAF data structures.
         */

        caf_note = arrp(caf_notes, i, NOTE);
    
        ctime_top   = caf_note->ctime_top; /* creation time, upper 32 bits */
        ctime       = caf_note->ctime;     /* creation time, lower 32 bits */

        mtime_top   = caf_note->mtime_top; /* as above but modification time */
        mtime       = caf_note->mtime;

        note_type = dictName(caf->noteTypeDict, caf_note->type); /* as string */
        note_text = caf_note->text;

        /*
         * Extends the supplied array of notes (in GAP format) adding the
         * current note (from the set in CAF format).
         */
        gap_note = arrayp(gap_notes, note_num++, GNotes);

        note_type = dictName(caf->noteTypeDict, caf_note->type);
        gap_note->type = char2type(note_type); /* Pack the note type with GAP 
                                                 system */
        gap_note->ctime_top = ctime_top;
        gap_note->ctime     = ctime;          /* Copy time information */
        gap_note->mtime_top = mtime_top;
        gap_note->mtime     = mtime;
                                        
        /*
         * For non-null annotation strings this forces the actual text to be
         * written to the gap_db and have its gap id returned and stored in
         * the GNotes structure.
         */
        gap_note->annotation = (note_text
			     ? write_string(gap_db, note_text)
			     : 0);
    }
}

static int GAnnotations_cmp(const void*a, const void *b)
{
  /* For sorting GAnnotations structures into order */

    GAnnotations *A = (GAnnotations *)a;
    GAnnotations *B = (GAnnotations *)b;

    return (A->position - B->position);  
}

static GCardinal process_tags(Gap_db *gap_db, cafAssembly *caf, CAFSEQ *seq,
			      Gap_id_maps *map, int clip_left, int clip_right,
			      int adjust)
{
  static Array tags     = 0;
  GAnnotations *tag     = 0;
  int          num_tags = 0;
  int          tag_num  = 0;
  Id_map       *amap    = &map->annotations;
  int          first    = 0;
  int          i;

  /* Calculate approximate number of tags */
  if (seq->tags)      num_tags += arrayMax(seq->tags);
  if (seq->svector)   num_tags += arrayMax(seq->svector);
  if (seq->cvector)   num_tags += arrayMax(seq->cvector);
  if (seq->is_stolen) num_tags++;

  tags = arrayReCreate(tags, max(num_tags, 100), GAnnotations);

  /* Stolen reads are tagged across the entire read */
  if (seq->is_stolen) {
    tag = arrayp(tags, tag_num++, GAnnotations);
    tag->type       = char2type("STOL");
    tag->position   = 1;
    tag->length     = seq->len;
    tag->strand     = GAP_STRAND_FORWARD;
    tag->annotation = (seq->stolen_from
		       ? write_string(gap_db, seq->stolen_from)
		       : 0);
  }

  add_tags(gap_db, caf, tags, seq->tags,
	   clip_left, clip_right, adjust, 0);
  add_tags(gap_db, caf, tags, seq->svector,
	   clip_left, clip_right, adjust, "SVEC");
  add_tags(gap_db, caf, tags, seq->cvector,
	   clip_left, clip_right, adjust, "CVEC");

  tag_num = arrayMax(tags);
  if (tag_num) {
    tag = arrp(tags, 0, GAnnotations);

    /* Sort tags by start position */
    qsort(tag, tag_num, sizeof(GAnnotations), GAnnotations_cmp);

    /* Remember the record number of the first tag in the list */
    first = amap->curr_id + 1;

    /* Create tag records */
    for (i = 0; i < tag_num; i++) {
      /* Fill in the next record to make the linked list */
      tag[i].next = (i < tag_num - 1) ? amap->curr_id + 2 : 0;

      array(amap->record_num, amap->curr_id++, GCardinal)
	= write_record(gap_db, GR_NEW, GT_Annotations, &tag[i],
		       sizeof(GCardinal),
		       sizeof(GAnnotations) / sizeof(GCardinal));
    }
  }

  return first;
}


/**
 * @param parent_type is the type of the object that contains this set
 *     of notes.
 * @parent is the reading/contig number (NB not the staden identifier) of
 *     the object that contains this note. Thus the first note in the linked
 *     list of notes points back to the object that contained the note.
 */
static GCardinal process_notes(Gap_db *gap_db, cafAssembly *caf, CAFSEQ *seq,
			       Gap_id_maps *map, GCardinal parent_type,
                               GCardinal parent)
{
    static Array notes     = 0;
    GNotes       *note     = 0;
    int          num_notes = 0;
    int          note_num  = 0;
    Id_map       *nmap     = &map->notes;
    int          first     = 0;
    int          i;

    /* Get the number of notes */
    if (seq->notes)      num_notes += arrayMax(seq->notes);

    /*
     * Minimum allocation is 100
     */
    notes = arrayReCreate(notes, max(num_notes, 100), GNotes);

    /*
     * Transcribe the internal 'NOTE' format into records suitable for
     * storage in the GAP database (GNotes). reads from seq->notes and
     * writes to notes.
     */
    add_notes(gap_db, caf, notes, seq->notes);

    note_num = arrayMax(notes);
    /* Create tag records with prev and next links established */

    if(note_num) {

        /* 
         * NB. 'note' points to the first element of the array. The 
         * current implementation has an assumption that the array entries
         * follow in contiguious memory (hence the array index in the loop
         * below).
         */
        note = arrp(notes, 0, GNotes);

        /* Remember the record number of the first note in the list */
        first = nmap->curr_id + 1;

        for (i = 0; i < note_num; i++) {

            /* Fill in the next record to make the linked list */
            note[i].next = (i == note_num-1) ? 0 : nmap->curr_id + 2;
            note[i].prev = (i == 0) ? parent : nmap->curr_id;

            note[i].prev_type = (i == 0) ? parent_type : GT_Notes;

            array(nmap->record_num, nmap->curr_id++, GCardinal)
                = write_record(gap_db, GR_NEW, GT_Notes, &note[i],
        	       sizeof(GCardinal),
        	       sizeof(GNotes) / sizeof(GCardinal));
        }
    }

    return first;
}

static GCardinal process_vector(Gap_db *gap_db, cafAssembly *caf,
				CAFSEQ *read_seq, Gap_id_maps *map,
				int vector_type)
{
  GVectors vec = { 0 };
  int vec_id;
  int vec_map_id;
  Id_map *vmap = &map->vectors;
  GCardinal vec_id_record;

  /* The shinanigans with vec_map_id here allow us to use one array for
     the mapping of both sequencing and cloning vectors. */

  vec_id = (vector_type == GAP_LEVEL_CLONE
	    ? read_seq->cloning_vector
	    : read_seq->sequencing_vector);
  vec_map_id = vec_id * 2;
  
  if (vector_type == GAP_LEVEL_CLONE) vec_map_id++;

  /* Resize the vectors array if necessary */
  vec_id_record = array(vmap->obj_num, vec_map_id, GCardinal);
  if (vec_id_record) return vec_id_record;

  vec.name = write_string(gap_db, dictName(caf->vectorDict, vec_id));
  vec.level = vector_type;
  
  array(vmap->record_num, vmap->curr_id++, GCardinal)
    = write_record(gap_db, GR_NEW, GT_Vectors, &vec,
		   sizeof(GCardinal),
		   sizeof(GVectors) / sizeof(GCardinal));

  /* Use incremented vmap->curr_id here as gap indexes seem to start at 1 */

  array(vmap->obj_num, vec_map_id, GCardinal) = vmap->curr_id;
  return vmap->curr_id;
}

static GCardinal process_clone(Gap_db *gap_db, cafAssembly *caf,
			       CAFSEQ *read_seq, Gap_id_maps *map)
{
  GClones clone = { 0 };
  int clone_id = read_seq->clone_id;
  Id_map *cmap = &map->clones;
  GCardinal clone_id_record;
  
  clone_id_record = arr(cmap->obj_num, clone_id, GCardinal);
  if (clone_id_record) return clone_id_record;

  clone.name = write_string(gap_db, dictName(caf->cloneDict, clone_id));
  clone.vector = process_vector(gap_db, caf, read_seq, map, GAP_LEVEL_CLONE);

#ifdef DEBUG
  fprintf(stderr, "%s %d\n",
	  dictName(caf->cloneDict, clone_id), clone.vector);
#endif

  array(cmap->record_num, cmap->curr_id++, GCardinal)
    = write_record(gap_db, GR_NEW, GT_Clones, &clone,
		   sizeof(GCardinal),
		   sizeof(GClones) / sizeof(GCardinal));

  /* Use incremented cmap->curr_id here as gap indexes seem to start at 1 */

  array(cmap->obj_num, clone_id, GCardinal) = cmap->curr_id;
  return cmap->curr_id;
}

static GCardinal process_template(Gap_db *gap_db, cafAssembly *caf,
				  CAFSEQ *read_seq, Gap_id_maps *map)
{
  GTemplates templ = { 0 };
  int template_id = read_seq->template_id;
  Id_map *tmap = &map->templates;
  GCardinal template_id_record = 0;

  if (!template_id) {
    /* Gap4 doesn't like reads with no template record.  As reads with this
       feature will mostly be consensus, it is best to add a new template
       with the same name as the read.  */

    char *read_name = dictName(caf->seqDict, read_seq->id);
    fprintf(stderr, "Added missing template for read %s\n", read_name);

    dictAdd(caf->templateDict, read_name, &template_id);
    read_seq->template_id = template_id;
  }
  
  template_id_record = array(tmap->obj_num, template_id, GCardinal);
  if (template_id_record) return template_id_record;

  templ.name = write_string(gap_db, dictName(caf->templateDict, template_id));
  templ.strands = 1; /* FIXME: This should be 1 or 2, really */
  templ.insert_length_min = read_seq->insert_size1;
  templ.insert_length_max = read_seq->insert_size2;
  templ.clone = process_clone(gap_db, caf, read_seq, map);
  templ.vector = process_vector(gap_db, caf, read_seq, map,
				GAP_LEVEL_SUBCLONE);

  /* fprintf(stderr, "%s %d %d %d %d %d\n",
	  dictName(caf->templateDict, template_id),
	  templ.strands, templ.insert_length_min,
	  templ.insert_length_max, templ.clone, templ.vector); */

  array(tmap->record_num, tmap->curr_id++, GCardinal)
    = write_record(gap_db, GR_NEW, GT_Templates, &templ,
		  sizeof(GCardinal),
		  sizeof(GTemplates)/sizeof(GCardinal));

  /* Use incremented tmap->curr_id here as gap indexes seem to start at 1 */

  array(tmap->obj_num, template_id, GCardinal) = tmap->curr_id;
  return tmap->curr_id;
}

static void process_read(Gap_db *gap_db, cafAssembly *caf, CAFSEQ *read_seq,
			 int caf_id, int staden_id, ASSINF *af, int adjust,
			 Gap_id_maps *map, GCardinal last_read,
			 GCardinal next_read)
{
  GReadings read;
  int read_sense;
  int read_pos;
  int read_start;
  int read_end;
  char *read_name;
  Id_map *rmap = &map->readings;
  GCardinal read_id_record;
  GCardinal rr;

  read_name = dictName(caf->seqDict, caf_id);

  /* fprintf(stderr, "%s %d %d %d %d len %d\n", read_name,
     af->s1, af->s2, af->r1, af->r2, read_seq->len); */

  if (af->s1 < af->s2) {
    read_sense = GAP_SENSE_ORIGINAL;
    read_pos   = af->s1 - adjust;
    read_start = af->r1 - 1;
    read_end   = af->r2 + 1;
  } else if (af->s1 > af->s2) {
    read_sense = GAP_SENSE_REVERSE;
    read_pos   = af->s2 - adjust;
    read_end   = read_seq->len - (af->r1 - 1) + 1;
    read_start = read_seq->len - (af->r2 + 1) + 1;
  } else {
    fprintf(stderr,
	    "Sequence %s - cannot deduce strand as it is clipped to only one base.\n",
	    read_name);
    read_sense = GAP_SENSE_ORIGINAL;
    read_pos   = af->s1 - adjust;
    read_start = af->r1 - 1;
    read_end   = af->r2 + 1;
  }

  read.name = write_string(gap_db, read_name);
  if (read_seq->SCF_File) {
    read.trace_name = write_string(gap_db, read_seq->SCF_File);
    read.trace_type = write_string(gap_db, "ANY");
  } else {
    read.trace_name = 0;
    read.trace_type = 0;
  }

  read.left  = last_read;
  read.right = next_read;
  read.position = read_pos;
  read.length = read_seq->len;
  read.sense  = read_sense;
  read.sequence = write_sequence(gap_db, read_seq->dna, read_sense,
				 read.length);

  if(!read_seq->base_quality) {
    fprintf(stderr,
        "WARNING: Sequence %s - Missing quality info: substituting Qvalue=2.\n",
        read_name);
  }
  read.confidence = write_confidence(gap_db, read_seq->base_quality,
				     read_sense, read.length);
  if (read_seq->assinf) {
    read.orig_positions = write_positions(gap_db, read_seq->assinf, read_sense,
					  read.length);
  } else {
#if 0
    read.orig_positions = write_positions_pads(gap_db, read_seq->dna,
					       read_sense,
					       read_start, read_end);
#endif
    read.orig_positions = write_positions_pads(gap_db, read_seq->dna,
					       read_sense, read.length);
  }
  read.chemistry = read_seq->dye == dye_terminator ? 1 : 0;
  read.annotations = process_tags(gap_db, caf, read_seq, map,
				  1, read.length, 0);

  read.notes = process_notes(gap_db, caf, read_seq, map,
                             GT_Readings, staden_id);

  read.sequence_length = af->r2 - af->r1 + 1;
  read.start = read_start;
  read.end   = read_end;
  read.template = process_template(gap_db, caf, read_seq, map);
  read.strand = (read_seq->strand == reverse_strand
		 ? GAP_STRAND_REVERSE : GAP_STRAND_FORWARD);
#ifdef GAP4_SOURCES
  if (read_seq->primer == custom_primer) {
    read.primer = (read_seq->strand == reverse_strand
		   ? GAP_PRIMER_CUSTREV : GAP_PRIMER_CUSTFOR);
  } else {
    read.primer = (read_seq->strand == reverse_strand
		   ? GAP_PRIMER_REVERSE : GAP_PRIMER_FORWARD);    
  }
#else  /* GAP4_SOURCES */
  read.primer = (read_seq->primer == custom_primer
		 ? GAP_PRIMER_CUSTOM : GAP_PRIMER_UNKNOWN);
#endif /* GAP4_SOURCES */

  read_id_record = arr(rmap->obj_num, caf_id, GCardinal);
  if (!read_id_record) {
    read_id_record = rmap->curr_id++;
    arr(rmap->obj_num, caf_id, GCardinal) = rmap->curr_id;
  }

  rr = write_record(gap_db, GR_NEW, GT_Readings, &read,
		  sizeof(GCardinal),
		  sizeof(GReadings) / sizeof(GCardinal));
  array(rmap->record_num, read_id_record - 1, GCardinal) = rr;
}

static void process_contig(Gap_db *gap_db, cafAssembly *caf, int contig_id,
			   CAFSEQ *contig_seq, Gap_id_maps *map, int silent)
{
  int adjust = 0;
  Array assinf = contig_seq->assinf;
  Array seqs   = caf->seqs;
  Array selected_afs;
  ASSINF *af;
  ASSINF *next_af;
  CAFSEQ *read_seq;
  int reads_in_contig;
  GContigs contig_info = {0};
  int i;
  int num_afs;
  int staden_id;
  GCardinal length;
  GCardinal last_staden_id = 0;
  GCardinal next_staden_id = 0;

  if (!assinf) return;

  reads_in_contig = arrayMax(assinf);
  if (!reads_in_contig) return;

  adjust = min(arr(assinf, 0, ASSINF).s1, arr(assinf, 0, ASSINF).s2) - 1;
  
  selected_afs = arrayCreate(reads_in_contig, ASSINF*);

  num_afs = 0;
  for (i = 0; i < reads_in_contig; i++) {
    af = arrp(assinf, i, ASSINF);
    read_seq = arrp(seqs, af->seq, CAFSEQ);
    
    array(selected_afs, num_afs++, ASSINF*) = af;
  }

  contig_info.length = 0;

  for (i = 0; i < num_afs; i++) {
    af = arr(selected_afs, i, ASSINF*);
    next_af = (i < num_afs - 1) ? arr(selected_afs, i + 1, ASSINF*) : 0;
    read_seq = arrp(seqs, af->seq, CAFSEQ);

    staden_id = arr(map->readings.obj_num, af->seq, int);
    next_staden_id= next_af ? arr(map->readings.obj_num, next_af->seq, int):0;
    
    if (!contig_info.left) contig_info.left = staden_id;
    contig_info.right  = staden_id;

    /* Rightmost read end is not necessarily the last one */ 
    length = max(af->s1, af->s2) - adjust;
    contig_info.length = max(contig_info.length, length);

    process_read(gap_db, caf, read_seq, af->seq, staden_id, af, adjust, map,
		 last_staden_id, next_staden_id);
    last_staden_id = staden_id;
  }

  contig_info.annotations = process_tags(gap_db, caf, contig_seq, map,
					 adjust + 1,
					 adjust + contig_info.length,
					 adjust);
  /*
   * Process any notes associated with the contig.
   */
  contig_info.notes = process_notes(gap_db, caf, contig_seq, map,
                                    GT_Contigs, contig_id);

  array(map->contigs.record_num, map->contigs.curr_id++, GCardinal)
    = write_record(gap_db, GR_NEW, GT_Contigs, &contig_info, sizeof(GCardinal),
		  sizeof(GContigs) / sizeof(GCardinal));

  arrayDestroy(selected_afs);
}

#define BMAP_BASE_TYPE        uint4
#define BMAP_ELEMENT_SIZE     (sizeof(BMAP_BASE_TYPE))
#define BMAP_BITS_PER_ELEMENT (8 * BMAP_ELEMENT_SIZE)

static BMAP_BASE_TYPE *bits(int N)
{
    int whole;
    int rem;
    static Array buf = NULL;

    whole = N / BMAP_BITS_PER_ELEMENT;
    rem   = N % BMAP_BITS_PER_ELEMENT;

    if (buf == NULL) buf = arrayCreate(whole + 1, BMAP_BASE_TYPE);
    arrayp(buf, whole, BMAP_BASE_TYPE);	/* force resize */

    memset(arrp(buf, 0, BMAP_BASE_TYPE), '\377', whole * BMAP_ELEMENT_SIZE);
    if (rem) arr(buf, whole, BMAP_BASE_TYPE) = (1 << rem) - 1;

    /* produce a string of N bits long  */
    return(arrp(buf, 0, BMAP_BASE_TYPE));
}

static int bitmap_num_elements(int num_bits)
{
  return (num_bits + BMAP_BITS_PER_ELEMENT - 1) / BMAP_BITS_PER_ELEMENT;
}

static void write_database_record(Gap_db *gap_db, Gap_id_maps *map,
				  int db_version)
{
  GDatabase d;

  int num_contigs  = map->contigs.curr_id;
  int num_readings = map->readings.curr_id;

  d.version         = db_version;
  d.maximum_db_size = block(num_contigs + num_readings + 4000, 8000);
  d.actual_db_size  = d.maximum_db_size;

  d.max_gel_len     = (db_version > 2) ? 30000 : 4096;
  d.data_class      = GAP_DNA;

  d.num_contigs     = num_contigs;
  d.num_readings    = num_readings;

  d.Ncontigs        = num_contigs;
  d.contigs         = GR_Contigs;

  d.Nreadings       = num_readings;
  d.readings        = GR_Readings;

  d.Nannotations    = map->annotations.curr_id;
  d.annotations	    = GR_Annotations;
  d.free_annotations = 0;

  d.Ntemplates      = map->templates.curr_id;
  d.templates       = GR_Templates;

  d.Nclones         = map->clones.curr_id;
  d.clones          = GR_Clones;

  d.Nvectors        = map->vectors.curr_id;
  d.vectors         = GR_Vectors;
#ifdef GR_Contig_Order
  d.contig_order    = 0;
#endif

  /*
   * Code to deal with notes, slightly different case to Annotations since
   * there is no #define for the entry in the master object table for Notes.
   * This is done here by allocating the next available free one.
   */

  d.Nnotes          = map->notes.curr_id;
  d.notes_a         = write_record(gap_db, GR_NEW, GT_Array,
				   arrayp(map->notes.record_num, 0, GCardinal),
				   sizeof(GCardinal), d.Nnotes);
  d.notes           = 0; /* Notes attached to DATABASE, currently not
			    implemented. */
  d.free_notes      = 0;


  /* Now we know how many freerecs there are... */

  d.Nfreerecs       = bitmap_num_elements(gap_db->num_records);
  d.freerecs        = GR_Freerecs;

  /*
   * Now write out the Freerecs bitmap.  After we have done this, we can't
   * allocate any new record numbers or the bitmap will be the wrong length.
   * The records that are written afterwards are all included in the count
   * as they have fixed record numbers.
   */
  
  write_record(gap_db, GR_Freerecs,    GT_Bitmap, bits(gap_db->num_records),
	      BMAP_ELEMENT_SIZE, d.Nfreerecs);

  write_record(gap_db, GR_Contigs,     GT_Array,
	      arrayp(map->contigs.record_num,     0, GCardinal),
	      sizeof(GCardinal), d.Ncontigs);

  write_record(gap_db, GR_Readings,    GT_Array,
	      arrayp(map->readings.record_num,    0, GCardinal),
	      sizeof(GCardinal), d.Nreadings);

  write_record(gap_db, GR_Annotations, GT_Array,
	      arrayp(map->annotations.record_num, 0, GCardinal),
	      sizeof(GCardinal), d.Nannotations);

  write_record(gap_db, GR_Templates,   GT_Array,
	      arrayp(map->templates.record_num,   0, GCardinal),
	      sizeof(GCardinal), d.Ntemplates);

  write_record(gap_db, GR_Clones,      GT_Array,
	      arrayp(map->clones.record_num,      0, GCardinal),
	      sizeof(GCardinal), d.Nclones);

  write_record(gap_db, GR_Vectors,     GT_Array,
	      arrayp(map->vectors.record_num,     0, GCardinal),
	      sizeof(GCardinal), d.Nvectors);
  
  /*
   * Now writes the database record last to allow for changes to the
   * 'd' data structure right up to the last minute.
   */
  write_record(gap_db, GR_Database,    GT_Database, &d,
	      sizeof(GCardinal), sizeof(d) / sizeof(GCardinal));

}

static void init_id_map(Id_map *map, int num_objs, int num_records)
{
  map->obj_num    = arrayCreate(num_objs,    GCardinal);
  map->record_num = arrayCreate(num_records, GCardinal);
  map->curr_id    = 0;
}

static int
caf2gap(cafAssembly *caf, int silent, int preserve, int db_version,
	Gap_db *gap_db)
{
  Array  contig_ids;   /* index in caf seqs array of each contig */
  Gap_id_maps map;     /* map caf ids to staden ids to record numbers */
  int    num_reads       = 0;
  int    num_contigs     = 0;
  int    num_templates   = 0;
  int    num_clones      = 0;
  int    num_annotations = 0;
  int    num_notes = 0;
  int    i;
  int    contig_id;

  Array  seqs;
  CAFSEQ *contig_seq;
  
  if (!check_padded(caf)) requirePadded(caf);

  count_caf_items(caf, &num_reads, &num_contigs, &num_templates,
		  &num_clones, &num_annotations, &num_notes, silent);

  if (!num_contigs)
      messcrash("caf file contains no contigs");

  if (!num_reads)
      messcrash("caf file contains no reads");

  contig_ids             = arrayCreate(num_contigs,     int);
  init_id_map(&map.readings, num_reads, num_reads);
  init_id_map(&map.contigs,  num_contigs, num_contigs);
  init_id_map(&map.templates, num_templates, num_templates);
  init_id_map(&map.clones, num_clones, num_clones);
  init_id_map(&map.vectors, 10, 10);
  init_id_map(&map.annotations, num_annotations, num_annotations);
  init_id_map(&map.notes, num_notes, num_notes);

  get_contig_seqids(caf, contig_ids, num_contigs);

  sort_assembled_from(caf, contig_ids, num_contigs);
  
  if (preserve) {
      assign_staden_ids_preserve(caf, contig_ids, num_contigs,
                               num_reads, &map.readings, silent);
  } else {
      assign_staden_ids(caf, contig_ids, num_contigs, &map.readings);
  }

  seqs = caf->seqs;
  for (i = 0; i < num_contigs; i++) {
    contig_id  = arr(contig_ids, i, int);
    contig_seq = arrp(seqs, contig_id, CAFSEQ);

    if (!silent) {
      fprintf(stderr, "Contig %s\n", dictName(caf->seqDict, contig_id));
    }

    process_contig(gap_db, caf, contig_id, contig_seq, &map, silent);
  }

  write_database_record(gap_db, &map, db_version);

  write_index(gap_db);

return 0;
}

int main(int argc, char** argv)
{
  cafAssembly *CAF;                    /* CAF file data                      */
  Gap_db      gap_db;                  /* Gap database files on disk         */
  FILE *fp           = 0;              /* CAF file to read                   */
  char project[MAX_FILENAME_LEN] = {0};/* project to create                  */
  char version[MAX_FILENAME_LEN] = {0};/* version to create                  */
  char acefile[MAX_FILENAME_LEN] = {0};/* caf file to read                   */
  int force          = 0;              /* overwrite existing database files  */
  int silent         = 0;              /* low verbosity                      */
  int preserve       = 0;              /* keep staden ID numbers if possible */
  int db_version     = GAP_DB_VERSION; /* type of gap database to make       */
  int expected_seqs  = 4000;           /* expected number of reads           */
  int caf            = 1;              /* read caf format                    */

  strcpy(acefile,"stdin");
  strcpy(version,"0");
  

  getarg(    "-project",    project,        argc, argv);
  getarg(    "-version",    version,        argc, argv);
  getint(    "-expected",   &expected_seqs, argc, argv);
  getint(    "-db_version", &db_version,    argc, argv);
  getboolean("-caf",        &caf,           argc, argv);
  getboolean("-force",      &force,         argc, argv);
  getboolean("-silent",     &silent,        argc, argv);
  getboolean("-preserve",   &preserve,      argc, argv);
  fp = argfile("-ace", "r", argc, argv, acefile);
  if (!fp) fp = stdin;

  gethelp(argc, argv);

  if (!*project) {
    print_usage(argc, argv, 1);
    exit(2);
  }

  if (db_version < 1 || db_version > 3) {
    fprintf(stderr, "caf2gap: Database version should be between 1 and 3\n");
    exit(2);
  }

  open_gap_db_write(project, version, force, &gap_db);

  CAF = readCafAssembly(fp, acefile, NULL);
  
  caf2gap(CAF, silent, preserve, db_version, &gap_db);

  close_gap_db(&gap_db);

  return 0;
}
