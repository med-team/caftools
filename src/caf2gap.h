#include <stdio.h>
#include <ctype.h>

#if defined(__linux__)
#include <sys/stat.h>
#endif

#include "cl.h"
#include "caf.h"
#include "gap-dbstruct.h"
#include "g-filedefs.h"
#include "g-io.h"

#define MAX_FILENAME_LEN 1024

#define BLOCKSIZE 16
#define STARTDATE 1
#define GR_NEW (-1)
#define GAP4_SOURCES

/* Gap_db struct flags */
#define AUX_64_BIT_MODE 1


typedef struct Id_map {
  Array obj_num;    /* Map caf object to gap record number              */
  Array record_num; /* Array of gap record numbers - used to make index */
  int   curr_id;    /* Current entry in record_num array                */
} Id_map;

typedef struct Gap_id_maps {
  Id_map readings;
  Id_map contigs;
  Id_map templates;
  Id_map clones;
  Id_map vectors;
  Id_map annotations; /* Tags */
  Id_map notes;	      /* Notes */
} Gap_id_maps;

/*
 * Gap_db struct - holds information about the gap database being written
 * to disk.
 */

typedef struct Gap_db {
  Array  records;      /* Array of AuxIndex records - used to make aux file */
  int    num_records;  /* Number of records */
  GImage image_count;  /* Location of next record in image file */
  int    flags;
  FILE   *image;       /* Handle of main database file */
  FILE   *aux;         /* Handle of aux file */
} Gap_db;

typedef struct Staden_id_map {
    char dup_processed; // Flag indicating duplicate identified and processed
    int count; // Number of times this staden_id has been encountered
    Array read_ids; // CAF read id's array, length = count
} Staden_id_map;

/*
 * Data structure used to store collections of integers. These are used
 * to represent free positions in the table of staden identifiers and 
 * also CAF identifiers for sequences that have invalid staden id's.
 */
typedef struct Id_array {
    Array data;
    int count;
} Id_array;

#define min(A,B) ((A)<(B)?(A):(B))
#define max(A,B) ((A)>(B)?(A):(B))

#define WRITE_INT_1 0
#define WRITE_INT_2 1
#define WRITE_INT_4 2
#define WRITE_INT_N 3

#define WRITE_GHFLAGS    1
#define WRITE_GCARDINAL  2
#define WRITE_GIMAGE     4
#define WRITE_GTIMESTAMP 2

/*
 * Function prototypes
 */
static void sanitise_staden_ids(cafAssembly *caf, Array contig_ids,
                                int num_contigs, int num_readings,
                                int silent);
