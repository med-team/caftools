/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * $Id: caf2phrap.c 20935 2007-06-15 10:49:19Z rmd $
 */
/*
 * caf2phrap performs the following functions:
 *
 * Takes an (unpadded) caf file, and (optionally) n file of read names
 * Creates a fasta sequence file for every read listed in the fofn, or all
 * reads if omitted.  Optionally creates corresponding quality file, using
 * base_quality, and fakes missing quality files.  Also creates a caf file
 * corresponding to the fasta files containing all the stuff to be merged back 
 * after running phrap (essentially everything !)
 *
 * Sequencing vector is X'd out and quality set to 0
 *
 * Optionally tagged regions are x'd out as well
 *
 */

#define MAIN
#define DEBUG 0

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include <cl.h>
#include <caf.h>
/* #include"scf.h" */

typedef enum { reads_only, contigs_only, all_seqs } modes;

int faked=0;
int total=0;

int
qualClip(CAFSEQ *seq, int tag_id, int high, int low);

char *
file_part(char *path);

void 
writeFasta(cafAssembly *CAF, CAFSEQ *seq, char *name, FILE *fasta, FILE *qual,
	   int high, int low, int mask, int svector_clip, int cvector_clip,
	   Array mask_ids, char mask_char, int silent, int descrip);


void get_description(cafAssembly *CAF, CAFSEQ * seq, char * description);

void get_read_info_from_name(char *name, char **strand, char **chem,
			     char **dye, char **templ);

int skip_comments(FILE *, char * );

int
main(int argc, char **argv)
{
  DICT *seqDict;
  Array seqs;
  CAFSEQ *seq;
  FILE *fasta, *qual, *fofn, *cfp;
  int i;
  char *dna;
  char caffile[256];
  char outfile[256];
  char fofnfile[256];
  char qualfile[256];
  char cafout[256];
  char read[256];
  char line[256];
  cafAssembly *CAF;
  int id;
  int high=0; /* high quality */
  int low=0; /* low quality */
  int use_qual = TRUE;
  int svector_clip = TRUE;
  int cvector_clip = FALSE;
  int write_contigs = FALSE;
  int write_reads = TRUE;
  int qual_clip = FALSE, clip_id;
  int write_failures = FALSE;
  int silent = FALSE;
  int descrip = FALSE;
  int mask=0; /* mask qual values below this threshold to 0 */
  char clip[256], tag_mask[256];
  char **tags;
  int ntags=0, tag_id;
  int minlen=0;
  char mask_char = 'x', mask_text[256];
  
  Array ids;

  outfile[0] = 0;
  qualfile[0] = 0;

  /* Options :
     -high     : fake quality high values if not in caf
     -low      : fake quality low values if not in caf
     -mask     : mask threshold
     -minlen   : min length of sequence
     -fofn     : use just the sequences listed in fofn - otherwise use all
     -contigs  : produce seq and qual for contigs
     -reads    : produce seq and qual for reads
     -silent   : run with no warnings
     -fasta    : name of fasta file to write
     -qualfile : name of qual file
     -qual     : whether to write quality measures
     -clipping : generate base quality from clipping if it does not already
                 exist
     -svclip   : do seq vector clipping (on by default)
     -cvclip   : do clone vector clipping (off by default)
     -failures : include filed (bad) reads. Off by default
     -tag_mask : comma-separated list of tags to mask
     -maskchar : the symbol to mask out sequence
     -descrip  : put sample info in fasta description
   */

  getint("-high", &high, argc, argv);
  getint("-low", &low, argc, argv);
  getint("-mask", &mask, argc, argv);
  getint("-minlen", &minlen, argc, argv);
  fofn = argfile("-fofn", "r", argc, argv, fofnfile);
  getboolean("-contigs", &write_contigs, argc, argv);
  getboolean("-reads", &write_reads, argc, argv);
  getboolean("-silent", &silent, argc, argv);

  fasta =  argfile( "-fasta","w", argc, argv, outfile);
  qual = argfile( "-qualfile", "w", argc, argv, qualfile );
  getboolean("-qual", &use_qual, argc, argv);

  strcpy(clip, "QUAL"); /* default quality clipping */
  qual_clip = getarg("-clipping", clip, argc, argv);
  getboolean("-svclip", &svector_clip, argc, argv);
  getboolean("-cvclip", &cvector_clip, argc, argv);

  getboolean("-failures", &write_failures, argc, argv);

  strcpy(tag_mask, "");
  getarg("-tag_mask", tag_mask, argc, argv);
  tags = split_on_separator(tag_mask, ',', &ntags);

  strcpy(mask_text, "x");
  getarg("-maskchar", mask_text, argc, argv);
  mask_char = mask_text[0];

  getboolean("-descrip", &descrip, argc, argv);

  CAF = readCAF( argc, argv, caffile );

  gethelp(argc,argv);

/* BaseQuality behaviour. This is as follows:

   If the switch -qual is true (which it is by default) then quality
   data are written out to the file specified by the -qualfile switch
   or derived by appending ".qual" to the file specified by -fasta

   If base_quality exists for a sequence then it is dumped out. If
   base-quality does not exist then it is faked from clipping data
   according to the clipping tag given by the
   -clipping switch, by default "QUAL". High quality regions are given
   quality "high" (1 by default), otherwise they are "low" (0 by
   default). If a sequence has no base quality or clipping tags then
   it is given the base quality 0
   
*/

  if ( CAF && fasta )
    {
      requireUnpadded(CAF);

      seqs = CAF->seqs;
      seqDict = CAF->seqDict;

      ids = arrayCreate(ntags, int);
      
      for(i = 0; i < ntags; i++)
	{
	  if (dictFind(CAF->tagTypeDict, tags[i], &tag_id))
	    *arrayp(ids, arrayMax(ids), int) = tag_id;
	}

      if (qual_clip) /* fake base quality using Clipping tags */
	{
	  if (!dictFind(CAF->tagTypeDict, clip, &clip_id))
	    fprintf(stderr, "WARNING: No Tags of type %s\n", clip );
	  else
	    fprintf(stderr, "qual_clip : %s\n", clip );
	}

      fprintf(stderr,"read CAF data %s\n", caffile );
      fprintf(stderr, "writing fasta %s\n", outfile );

      if (qual)
	use_qual = TRUE;
      else if (use_qual)
	{
	  strcpy(qualfile,outfile);
	  strcat(qualfile,".qual");
	  qual = openfile(qualfile,"w");
	}
      
      if (qual)
	fprintf(stderr,"writing qual %s\n", qualfile );

      if (write_contigs) /* just write fasta seq and qual files for contigs */
	{
	  fprintf(stderr, "writing contigs\n");
	  for(i = 0; i < arrayMax(seqs); i++)
	    {
	      seq = arrp(seqs, i, CAFSEQ);
	      if (seq->type == is_contig
		  && seq->dna
		  && (dna = stackText(seq->dna,0)) && seq->len > minlen)
		{
		  writeFasta(CAF, seq, dictName(seqDict, i), fasta, qual,
			     high, low, mask, svector_clip, cvector_clip, ids,
			     mask_char, silent, FALSE);
		}
	    }
	}

      if (write_reads) /* for the reads */
	{
	  if (!(cfp = argfile("-cafout", "w", argc, argv, cafout)))
	    {
	      strcpy(cafout, outfile);
	      strcat(cafout, ".caf");
	      cfp = openfile(cafout, "w");
	    }
	  
	  if (fofn)
	    {
	      while (skip_comments(fofn, line) != EOF)
		{
		  char *readname;
		  sscanf(line, "%s", read );
		  readname = file_part(read);
		
		  if (dictFind(seqDict, readname, &id))
		    {
		      seq = arrp(seqs, id, CAFSEQ);
		      if ((write_failures == TRUE)
			  || (!is_failure(seq->process_status)))
			{
			  depad_isolated_reading(seq, CAF);
			  if (seq->len > minlen) 
			    {
			      writeCafSeq(CAF, cfp, seq);
			      /* note we preserve the case of the DNA in the
				 CAF file */
			      if (qual_clip) qualClip(seq,clip_id,high,low); 
			      writeFasta(CAF, seq, readname, fasta, qual,
					 high, low, mask, svector_clip,
					 cvector_clip, ids, mask_char, silent,
					 descrip);
			    }
			}
		    }
		  else
		    fprintf(stderr, "Unknown Reading: %s\n", readname);
		}
	    }
	  else
	    {
	      for(i=0; i < arrayMax(seqs); i++)
		{
		  seq = arrp(seqs, i, CAFSEQ);
		  if (seq->type == is_read
		      && seq->dna
		      && (dna = stackText(seq->dna, 0))
		      && seq->len > minlen)
		    {
		      depad_isolated_reading(seq, CAF);
		      writeCafSeq(CAF, cfp, seq);
		      if (qual_clip) qualClip(seq, clip_id, high, low); 
		      writeFasta(CAF, seq, dictName(seqDict, i), fasta, qual,
				 high, low, mask, svector_clip, cvector_clip,
				 ids, mask_char, silent, descrip);
		    }
		}
	    }
	}
      fclose(fasta);
      if (qual) fclose(qual);
      
      if (faked)
	fprintf(stderr, "%d/%d Faked BaseQuality\n", faked, total);
      return 0;
    }
  return 1;
}

int
qualClip(CAFSEQ *seq, int tag_id, int high, int low)

/* use Clipping on tagid to create base quality if base quality does not exist */


{
  int i, k;
  TAG *tag;
  Array bq;

  if (seq->clipping && !seq->base_quality )
    {
      for(k = 0; k < arrayMax(seq->clipping); k++)
	{
	  tag = arrp(seq->clipping, k, TAG);

	  if (tag->x2 > seq->len) tag->x2 = seq->len;

	  if (tag->type == tag_id)
	    {
	      bq = seq->base_quality = arrayCreate(seq->len, BQ_SIZE);

	      for(i = 1; i < tag->x1; i++)
		*arrayp(bq, i - 1, BQ_SIZE) = low;

	      for(i= tag->x1; i <= tag->x2; i++)
		*arrayp(bq, i - 1, BQ_SIZE) = high;

	      for(i = tag->x2 + 1; i <= seq->len; i++)
		*arrayp(bq, i - 1, BQ_SIZE) = low;
	      
	      return TRUE;
	    }
	}
    }

  if (!seq->base_quality) /* fake quality all 0 */
    {
      bq = seq->base_quality = arrayCreate(seq->len, BQ_SIZE);

      for(i = 1; i <= seq->len; i++)
	*arrayp(bq, i - 1, BQ_SIZE) = low;
    }

  return FALSE;
}


void 
writeFasta(cafAssembly *CAF, CAFSEQ *seq, char *name, FILE *fasta, FILE *qual,
	   int high, int low, int mask, int svector_clip, int cvector_clip,
	   Array mask_ids, char mask_char, int silent, int descrip)
{
  int i, j, k, n;
  int len;
  char *dna;
  TAG *tag;

  total++;
  if (seq->dna)
    {
      dna = stackText(seq->dna, 0);
      len = strlen(dna);

      if (qual && (!seq->base_quality
		   || !arrayMax(seq->base_quality)))
	{
	   /* fake the base quality using case of sequence*/

	  if (!silent) fprintf(stderr, "faking quality for %s\n", name);
 
	  faked++;
	  seq->base_quality = arrayCreate(len, BQ_SIZE);

	  for(k = 0; k < len; k++)
	    {
	      n = i_base(dna[k]);
	      if (proper_base(n))
		{
		  if (isupper((int) dna[k]))
		    *arrayp(seq->base_quality, k, BQ_SIZE) = high;
		  else
		    *arrayp(seq->base_quality, k, BQ_SIZE) = low;
		}
	      else
		*arrayp(seq->base_quality, k, BQ_SIZE) = 0;
	    }
	}

      if (svector_clip && seq->svector)
	{
	  /* X -out the seq vector clipped regions */

	  for(j=0; j < arrayMax(seq->svector); j++)
	    {
	      tag = arrp(seq->svector, j, TAG);

	      if (len < tag->x2 - 1)
		fprintf(stderr, "length error for %s len %d x2-1 %d\n",
			name, len, tag->x2 - 1);

		  
	      for(k = tag->x1 - 1; k <= tag->x2 - 1; k++)
		{
		  dna[k] = mask_char;
		  if (seq->base_quality)
		    {
		      /* and fix the base quality */
		      *arrayp(seq->base_quality, k, BQ_SIZE) = 0;
		    }
		}
	    }
	}

      if (cvector_clip && seq->cvector)
	{
	  for(j=0;j<arrayMax(seq->cvector);j++)
	    {
	      /* X -out the clone vector clipped regions */
	      tag = arrp(seq->cvector, j, TAG);
	      for(k = tag->x1 - 1; k <= tag->x2 - 1; k++)
		{
		  dna[k] = 'x';
		  if (seq->base_quality)
		    {
		      /* and fix the base quality */
		      *arrayp(seq->base_quality, k, BQ_SIZE) = 0;
		    }
		}
	    }
	}

      if ( arrayExists(mask_ids)
	   && arrayExists(seq->tags))
	{
	  /* X-out specially tagged regions */

	  for(j = 0; j < arrayMax(seq->tags); j++)
	    {
	      tag = arrp(seq->tags, j, TAG);

	      for(i = 0; i < arrayMax(mask_ids); i++)
		{
		  if (tag->type == arr(mask_ids,i,int))
		    {
		      if (!silent)
			{
			  fprintf(stderr,
				  "Masking tag type %d %5d %5d in %s\n",
				  tag->type, tag->x1, tag->x2, name);
			}

		      for(k = tag->x1 - 1; k <= tag->x2 - 1; k++)
			{
			  dna[k] = mask_char;
			  if (seq->base_quality)
			    {
			      /* and fix the base quality */
			      *arrayp(seq->base_quality, k, BQ_SIZE) = 0;
			    }
			}
		    }
		}
	    }
	}

      if (arrayExists(mask_ids) && arrayExists(seq->clipping))
	{
	  /* X-out specially tagged clipping regions */
	  for(j = 0; j < arrayMax(seq->clipping); j++)
	    {
	      tag = arrp(seq->clipping, j, TAG);
	      for(i = 0; i < arrayMax(mask_ids); i++)
		{
		  if (tag->type == arr(mask_ids, i, int))
		    {
		      if (!silent)
			fprintf(stderr,
				"Masking clipping type %d %5d %5d in %s\n",
				tag->type, tag->x1, tag->x2, name );

		      for(k = 1; k < tag->x1; k++)
			{
			  dna[k] = mask_char;
			  if (seq->base_quality) 
			    *arrayp(seq->base_quality, k, BQ_SIZE) = 0;
			}

		      for(k = tag->x2 + 1; k <= seq->len; k++)
			{
			  dna[k] = mask_char;
			  if ( seq->base_quality)
			    *arrayp(seq->base_quality, k, BQ_SIZE) = 0;
			}
		    }
		}
	    }
	}

      /*fprintf(stderr, "%s len %d qual %d\n",
	      name, len, arrayMax(seq->base_quality) ); */
      if (descrip)
	{
	  char description[1024];
	  get_description(CAF, seq, description);
	  fprintf(fasta, ">%s %s\n", name, description);
	}
      else
	{
	  fprintf(fasta, ">%s\n", name); 
	}
      for(i = 0; i < len; i += 60)
	{
	  int n = (i + 60 < len ? 60 : len - i);
	  fwrite(&dna[i], sizeof(char), n, fasta);
	  fputc('\n', fasta);
	}
      fputc('\n', fasta);

/*      fprintf(stderr, "%-20s %5d \n", name, len );  */

      if (qual) /* write out the base quality */
	{
	  /* mask out low quality values */

	  for(k = 0; k < arrayMax(seq->base_quality); k++)
	    if (arr(seq->base_quality, k, BQ_SIZE) < mask)
	      *arrp(seq->base_quality, k, BQ_SIZE) = 0;

	  fprintf(qual, ">%s\n", name);
	  write_base_quality(seq->base_quality, qual);
	}
    }
}

void get_description(cafAssembly *CAF, CAFSEQ * seq, char * description)
{
  char *strand = NULL, *chem = NULL, *dye = NULL, *templ = NULL;
  char name_copy[256];
  char *name = dictName(CAF->seqDict, seq->id);

  if (seq->strand) strand = (seq->strand == forward_strand) ? "fwd" : "rev";
  if (seq->dye) chem = (seq->dye == dye_terminator) ? "term" : "prim";
  if (seq->template_id) templ = dictName(CAF->templateDict,seq->template_id);

  if (snprintf(name_copy, sizeof(name_copy), "%s", name) < sizeof(name_copy))
    get_read_info_from_name(name_copy, &strand, &chem, &dye, &templ);
  
  if (!strand) strand = "fwd";
  if (!dye) dye = "unknown";
  if (!chem) chem = "unknown";
  if (!templ) templ = name;

  sprintf(description, "DIRECTION: %s CHEM: %s DYE: %s TEMPLATE: %s",
	  strand,
	  chem,
	  dye,
	  templ);
}

void get_read_info_from_name(char *name, char **strand, char **chem,
			     char **dye, char **templ)
{
  char *c = name;

  /* Find end of template */
  while (*c && *c != '.') c++;

  /* Return if no . found */
  if (!*c) return;
  
  if (!*templ) *templ = name;
  *c++ = 0;

  /* c should now point to the strand bit of the name */
  if (!*c) return;

  if (!*strand) *strand = (*c == 'q' || *c == 'r') ? "rev" : "fwd";
  
  if (!*(++c)) return; /* Skip the primer bit of the name */
  if (!*(++c)) return; /* c now points to the read chemistry */
  
  if (!*chem)
    {
      switch (*c)
	{
	case 'b':
	case 'e':
	case 'm':
	case 'p':
	  *chem = "prim";
	  break;
	  
	case 'c':
	case 'd':
	case 'f':
	case 't':
	case 'k':
	  *chem = "term";
	  break;

	default:
	  *chem = "unknown";
	}
    }
  
  if (!*dye)
    {
      switch (*c)
	{
	case 'b':
	case 'c':
	case 'k':
	  *dye = "big";
	  break;
	  
	case 'd':
	  *dye = "d-rhod";
	  break;

	case 'e':
	case 'f':
	case 'm':
	  *dye = "ET";
	  break;

	case 'l':
	case 'p':
	case 't':
	  *dye = "rhod";
	  break;

	default:
	  *dye = "unknown";
	}
    }
}

char *
file_part(char *path)
/*
 * Returns a pointer to the file part of a full path name
 */
{
    int len;
    char *s;

    len = strlen(path);
    for (s = path + len - 1; len && *s != '/'; len--, s--) {}
    s++;

    return s;
}

/*
 * $Log$
 * Revision 1.9  2005/06/16 11:31:08  rmd
 * Moved Log.
 * Removed compiler warnings.
 *
 * Revision 1.8  2003/07/25 14:54:57  dgm
 * Updated comments to remove nested comments.
 *
 * Revision 1.7  2002/05/31 11:12:42  mng
 * Added caf2ace
 *
 * Revision 1.6  2002/03/04 17:00:19  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.5  2002/03/04 15:37:56  rmd
 * Added support for 'k' big dye terminator III suffix
 *
 * Revision 1.4  2001/12/12 18:00:54  rmd
 * Tidied up code a bit.
 * Added -descrip option to put sample info in the fasta file.
 *
 * Revision 1.3  1997/04/17  15:43:08  badger
 * Strip path name from experiment files when
 * using -fofn option.
 *
 * Revision 1.2  1997/04/17  15:34:38  badger
 * Added -silent.
 *
 * Revision 1.1  1997/04/03  13:44:09  badger
 * Initial revision
 *
 * Revision 1.4  1996/09/23  11:07:05  rmott
 * version prior to removal of use_case  option (replace by base quality)
 *
 * Revision 1.3  1996/06/24  15:45:20  rmott
 * sets quality data of stolen reads to 0
 *
 * Revision 1.2  1996/06/19  09:25:57  rmott
 * added option
 * -mask 15
 * to set base quality values < mask to 0 for phrap assembly
 *
 * Revision 1.1  1996/06/19  09:19:14  rmott
 * Initial revision
 *
 */
