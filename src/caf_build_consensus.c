/*  Last edited: Aug 17 10:23 2001 (rmd) */
#include <stdio.h>
#include <string.h>
#include <cl.h>
#include <caf.h>
#include <consensus.h>

int main(int argc, char **argv)
{
  cafAssembly *caf;
  char input_file[256]  = {0};
  char output_file[256] = {0};
  FILE *caf_file = 0;
  FILE *out_file = 0;

  /* get options */

  caf_file = argfile("-caf", "r", argc, argv, input_file);
  out_file = argfile("-out", "w", argc, argv, output_file);

  if (!caf_file) {
    caf_file = stdin;
    strcpy(input_file, "stdin");
  }
  if (!out_file) {
    out_file = stdout;
    strcpy(output_file, "stdout");
  }

  caf = readCafAssembly(caf_file, input_file, NULL);

  recalc_consensus(caf);

  writeCafAssembly(caf, out_file, output_file);

  return 0;
}
