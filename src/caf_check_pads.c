/*  Last edited: Nov 27 11:56 2003 (mng) */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "consensus.h"
#include "cl.h"
#include "caf.h"
#include "caf_check_pads.h"

int main (int argc, char **argv)
{
  cafAssembly *CAF;
  char CAFfile[256] = {0};
  int help = 0;

  getboolean("-h", &help, argc, argv);

  if(help) 
    usage();
  
  if ((CAF = readCAF(argc,argv,CAFfile)) != 0 )
    find_pad_problems(CAF);
  writeCAF( CAF, CAFfile, argc, argv);
  
  return 0;
}


int find_pad_problems(cafAssembly * caf)
{
  Array   seqs;
  Array   tag_info;
  CAFSEQ *seq;
  int i;
  seqs = caf->seqs;
  
  tag_info = arrayCreate(arrayMax(caf->seqs), Read_pad);
  arrayp(tag_info, arrayMax(caf->seqs), Read_pad);

  for (i = 0; i < arrayMax(seqs); i++) {
    seq = arrp(seqs, i, CAFSEQ);      
    if (seq->type == is_contig) {
      fprintf(stderr,"%s ",dictName(caf->seqDict,seq->id));
      iterate_contig(caf, seq, tag_info, check_base, 0);
    }
  }  
  arrayDestroy(tag_info);
  return 0;
}

static int iterate_contig(cafAssembly* caf, CAFSEQ* seq, Array tag_info,
                   do_iterator_callback callback, void *callback_params)
{
  ASSINF *assembled_froms; /* First ASSINF struct */
  ASSINF *last_af;         /* Last  ASSINF struct */
  ASSINF *curr_af;         /* pointer to current ASSINF struct        */
  ASSINF *temp_af;
  Read_pad *tPtr;
  Array  segments;         /* Array holding all Seg_entry at current base */ 
  Array  matched;          /* Array holding histogram of run lengths */
  int    start=0;          /* First good base in contig consensus     */
  int    end=0;            /* Last  good base in contig consensus + 1 */
  int    i;                /* The current consensus base */
  int    k;
 
  CAFSEQ *rPtr;
  Read_pad *tpr;
  int base[4] ={'A','C','G','T'};
  int y = 0;

  int x;
  /* int matched[100]; */
  int maxMatch;
  int majority;
  int dash;
  int big_len;
  int sub_len;
  int last_i;
  int j;
  int match_j;
  int validBase;
  int padColCount;
  int colCount;
  char string[256];
  
  /* Check if this contig has any assembled_from information */
  if (!seq->assinf)               return 0;
  if (arrayMax(seq->assinf) == 0) return 0;
  
  matched = arrayCreate(100, int);

  /* Sort the assembled_from information into order along the contig */
  
  assembled_froms = arrp(seq->assinf, 0, ASSINF);
  qsort(assembled_froms, arrayMax(seq->assinf), sizeof(ASSINF), af_compare);
  
  /* Find the first and last bases in the assembled_froms */
  for(k=0;k<arrayMax(seq->assinf);k++) {
    temp_af = arrp(seq->assinf,k,ASSINF);
    /* Initialise struct counters to 0 and assign name. */
    tPtr = arrp(tag_info,temp_af->seq,Read_pad);    
    tPtr->match = 0;
    tPtr->read = temp_af->seq;
    if(temp_af->s1 > temp_af->s2) {
      if(start > temp_af->s2) { start = temp_af->s2; }
      if(end < temp_af->s1) { end = temp_af->s1; }
    }else{
      if(start > temp_af->s1) { start = temp_af->s1; }
      if(end < temp_af->s2) { end = temp_af->s2; }
    }
  }

  if (start < 0) 
    return 1;
  
  /* Go through each consensus base, work out which read segments are
     matching the consensus base */
  
  fprintf(stderr,"%d..%d\n",start,end);
  
  
  for(y=0;y<4;y++) {

    int z = 0;

    last_af = arrp(seq->assinf, arrayMax(seq->assinf)-1, ASSINF);
    curr_af  = assembled_froms;
    segments = arrayCreate(128, Seg_entry);
    
    x = 0;
    /* matched[0] = 0; */
    array(matched, 0, int) = 0;
    maxMatch = 0;
    majority = 0;
    dash = 0;
    big_len = seq->len;
    sub_len = 2;
    last_i = big_len - sub_len;
    j = 0;
    match_j = 0;
    validBase = 0;
    padColCount = 0;
    colCount = 0;
    string[0] = 0;

    /* Loop through each position of the consensus run */
    for(i=0; i<= last_i; i++) {
      for(j=0, match_j = 0;      
          (base[y] == *stackText(seq->dna, i+j)) || ('-' == *stackText(seq->dna, i+j));
          j++) {       
        if('-' == *stackText(seq->dna, i+j+1)) {
          do{
            ++j;
          }while('-' == *stackText(seq->dna, i+j+1) && (i+j) < last_i);          
        }      
        match_j = j + 1;
      }
      
      /* cons length greater then subseq len ...*/
      if(match_j >= sub_len) {
        
        /* count dashes and valid bases */
        for(z=0;z<match_j;z++) {
          if('-' == *stackText(seq->dna, i+z)) {
            dash++;
          }else{
            validBase++;
          }
        }
        /* skip if valid bases smaller then subseq */
        if(validBase < sub_len) {
          i += match_j - 1;
          dash = 0;
          maxMatch = 0;
          majority = 0;
          validBase = 0;
          for(k=0;k<arrayMax(seq->assinf);k++) {
            temp_af = arrp(seq->assinf,k,ASSINF);
            tPtr = arrp(tag_info,temp_af->seq,Read_pad);    
            tPtr->match = 0;
          }
          continue;
        }
             
        /* iterate through reads covering match_j cons length updating
           read struct counting bases mathching the consensus base */
        for (z = i,colCount = 0; z < i+match_j+1; z++) {
          update_segments(caf, z, segments, &curr_af, last_af);
          (*callback)(caf, seq, z, segments, tag_info, base[y], &padColCount, callback_params);
          /* count pads in each column, need at least one in a column for it to be a problem */
          colCount += padColCount;
          padColCount = 0;
        }
        /* skip if not enoung columns with pads in */
        if(colCount < sub_len) {          
          i += match_j - 1;            
          dash = 0;
          maxMatch = 0;
          majority = 0;
          validBase = 0;
          colCount = 0;          
          for(k=0;k<arrayMax(seq->assinf);k++) {
            temp_af = arrp(seq->assinf,k,ASSINF);
            tPtr = arrp(tag_info,temp_af->seq,Read_pad);    
            tPtr->match = 0;
          }
          continue;
        }
        /* Initialise match historgram to zero */ 
        for (x = 0; x < arrayMax(matched); x++) {
          array(matched, x, int) = 0;
        }
        /* Loop through read structs populating histogram */
        for(x=0;x<arrayMax(caf->seqs);x++) {
          rPtr = arrp(caf->seqs,x,CAFSEQ);
          if(rPtr->type == is_read) {
            tpr = arrp(tag_info,x,Read_pad);
            /*if (tpr->match >= arrayMax(matched)) {
              fprintf(stderr, "Kazam!!!! %d %d %s\n", tpr->match, arrayMax(matched), dictName(caf->seqDict, rPtr->id));
              abort();
              }
            */
            if(tpr->match > 0) 
              array(matched, tpr->match, int)++;  /* Array will grow as needed */
            
            tpr->match = 0;
          }
        }
        
        for (x = 0; x < arrayMax(matched); x++) {
          if (arr(matched, x, int) > majority) {
            maxMatch = x;
            majority = arr(matched, x, int);
          }
        }
        
        /* hist match doesn't match consensus length minus pads...*/
        if(maxMatch != match_j - dash) {
          sprintf(string,"Consensus suggests %d %c's, Reads suggest %d",match_j-dash,base[y],maxMatch);
          newTag(seq,caf,"PADS",i+1,i+match_j,string);
          maxMatch = 0;
          majority = 0;
        }
      
        i += match_j - 1;
        dash = 0;
        maxMatch = 0;
        majority = 0;
        validBase = 0;
        colCount = 0;
      }
    }
    arrayDestroy(segments);
  }
  
  arrayDestroy(matched);

  return 0;
}


static void update_segments(cafAssembly *caf, int i, Array segments,
                            ASSINF **curr_af, ASSINF *last_af)
{
  Seg_entry *new_seg; /* New segment */
  int seg;       /* Segment counter */
  int inserted;  /* Flag if a new segment has been inserted */
  
  /* Remove any segments we have finished with (i.e. i has gone past the end).
     As the segments array is sorted by segment end, the ones we need to get
      rid of should be at the end of the array */

  for (seg = arrayMax(segments) - 1; seg >= 0; seg--) {
    
    if (arrp(segments, seg, Seg_entry)->ce > i) break;

    arrayMax(segments)--;
  }
  
  inserted = 0;

  /* Go through the remaining ASSINF entries and see if they should be put
     into the segments array yet. */

  for (; (*curr_af) <= last_af; (*curr_af)++) {

    if (min((*curr_af)->s1, (*curr_af)->s2) - 1 > i) break;

    /* Insert the next segment */

    new_seg = arrayp(segments, arrayMax(segments), Seg_entry);

    if ((*curr_af)->s1 < (*curr_af)->s2) {  /* Forward strand */
        new_seg->read = arrp(caf->seqs, (*curr_af)->seq, CAFSEQ);
        new_seg->cs   = (*curr_af)->s1 - 1;
        new_seg->ce   = (*curr_af)->s2;
        new_seg->rs   = (*curr_af)->r1 - 1;
        new_seg->re   = (*curr_af)->r2;
        new_seg->comp = 0;
    } else {                          /* Reverse strand */
        new_seg->read = arrp(caf->seqs, (*curr_af)->seq, CAFSEQ);
        new_seg->cs   = (*curr_af)->s2 - 1;
        new_seg->ce   = (*curr_af)->s1;
        new_seg->rs   = (*curr_af)->r1 - 1;
        new_seg->re   = (*curr_af)->r2;
        new_seg->comp = 1;
    }
    inserted = 1;
  }

  if (inserted) {

    /* re-sort the array of segments by end position */

    qsort(arrp(segments, 0, Seg_entry), arrayMax(segments),
          sizeof(Seg_entry), seg_compare);
  }
}


static void check_base (cafAssembly* caf, CAFSEQ* contig,
                        int i, Array segments, Array tag_info, int consBase, int *pcc, void * params)
{
  int seg, rp;
  char base;
  Seg_entry *se;
  Read_pad *tag;
  
  for (seg = 0; seg < arrayMax(segments); seg++) {
    se = arrp(segments, seg, Seg_entry);
    
    tag = arrp(tag_info,se->read->id,Read_pad);
    
    if (se->comp) { /* reverse strand */   
      rp = se->re - (i - se->cs) - 1;
      base = (char) toupper((int) complement(*(stackText(se->read->dna, rp))));
    } else {        /* forward strand */      
      rp = i - se->cs + se->rs;
      base = (char) toupper((int) *(stackText(se->read->dna, rp)));
    }    

    /* read base matches consensus base */
    if(base == consBase)
      tag->match++;

    /* count pad chars */
    if((base == '-') && (*pcc == 0)) 
      ++*pcc;

  }
}

void usage (void)
{
  fprintf(stderr,"\nUsage: caf_check_pads [-h] < caf.in > caf.out\n\n");
  fprintf(stderr,"Program to check pad alignments within database and flag\n");
  fprintf(stderr,"those areas where the consensus could have an added base.\n\n");
  exit(0);
}

static int af_compare(const void *a, const void *b)
{
  int mina,minb;
  ASSINF *aa = (ASSINF *) a;
  ASSINF *bb = (ASSINF *) b;
  
  mina = min(aa->s1, aa->s2);
  minb = min(bb->s1, bb->s2);


  return (mina - minb);
}

/* Compare Seg_entry entries.  When used for sorting, they will be ordered by
   end position (ce), high to low */

static int seg_compare(const void *a, const void *b)
{
  Seg_entry *aa = (Seg_entry *) a;
  Seg_entry *bb = (Seg_entry *) b;

  return (bb->ce - aa->ce);
}

static char complement(char b)
{
  static char tab[256] = {0};
  int i;

  if (!tab['a']) {
    for (i = 0; i < 256; i++) tab[i] = 'N';
    tab['a'] = 't';
    tab['A'] = 'T';
    tab['c'] = 'g';
    tab['C'] = 'G';
    tab['g'] = 'c';
    tab['G'] = 'C';
    tab['t'] = 'a';
    tab['T'] = 'A';
    tab['n'] = 'n';
    tab['-'] = '-';
  }

  return tab[(int) b];
}
