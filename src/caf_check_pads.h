/*  Last edited: Jul 31 11:31 2003 (mng) */
#ifndef _CAF_CHECK_PADS_H_
#define _CAF_CHECK_PADS_H_

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

typedef struct {
  CAFSEQ *read;  /* Read info            */
  int cs;        /* Start in contig      */
  int ce;        /* End in contig + 1    */
  int rs;        /* Start in read        */
  int re;        /* End in read + 1      */
  int comp;      /* Align reverse strand */
} Seg_entry;

typedef struct {
  int read;
  int match;
} Read_pad;

void usage (void);

int find_pad_problems(cafAssembly *);

typedef void (*do_iterator_callback)(cafAssembly*,CAFSEQ*, int, Array, Array, int, int *, void *);

static void update_segments(cafAssembly *, int , Array,
                            ASSINF **, ASSINF *);

static int iterate_contig(cafAssembly*, CAFSEQ*, Array,do_iterator_callback callback, void *);

static void check_base (cafAssembly*, CAFSEQ*, int, Array, Array, int, int *, void *);

static int af_compare(const void *, const void *);

static int seg_compare(const void *, const void *);

static char complement(char b);

#endif /*_CAF_CHECK_PADS_H_*/
