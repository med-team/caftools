/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 *
 * $Id: caf_pad.c 15167 2005-06-16 12:56:49Z rmd $
 *
 * $Log$
 * Revision 1.3  2005/06/16 12:56:49  rmd
 * Fixed compiler warning.
 *
 * Revision 1.2  2003/07/25 14:54:57  dgm
 * Updated comments to remove nested comments.
 *
 * Revision 1.1  1997/04/03 13:44:09  badger
 * Initial revision
 *
 */

#define MAIN

#include<stdio.h>
#include"caf.h"

int
main(int argc, char **argv)
{
  char filename[256];
  cafAssembly *CAF;

  if (NULL != (CAF = readCAF(argc,argv,filename)))
    {
      requirePadded( CAF ); 
      writeCAF( CAF,  filename, argc, argv );
      return 0;
    }
  return 1;
}
