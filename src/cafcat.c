/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * $Id: cafcat.c 19742 2006-12-06 14:27:17Z aw7 $
 * $Log$
 * Revision 1.5  2006/12/06 14:27:17  aw7
 * Remove full path from read names.
 *
 * Revision 1.4  2003/07/25 14:54:57  dgm
 * Updated comments to remove nested comments.
 *
 * Revision 1.3  2003/07/25 11:16:33  rmd
 * Changes for new version of autoconf.
 * Stop cafcat exploding if you try to write a contig with no assembled_from
 * Added -depad and -template_info options to templates.
 *
 * Revision 1.2  2002/03/04 17:00:19  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.1  1997/04/03  13:44:09  badger
 * Initial revision
 *
 * Revision 1.3  1996/10/04  13:22:02  rmott
 * nothing
 *
 * Revision 1.2  1996/10/03  17:22:29  rmott
 * *** empty log message ***
 *
 * Revision 1.1  1996/03/08  10:06:56  rmott
 * Initial revision
 *
 */

#define MAIN 1

#include <stdio.h>
#include <cl.h>
#include <caf.h>
#include <caf_check.h>
#include <cafseq.h>
#include <readline.h>

char *file_part(char *path);

int
main(int argc, char **argv)
{
	DICT *seqDict;
	Array seqs;
	CAFSEQ *seq;
	FILE *fofn;
	int i, j, len;
	ASSINF *a;
	cafAssembly *CAF;
	char CAFfile[256];
	char outfile[256];
	char fofnfile[256];
	char *dna;
	int reformat = 0;
	int verbose = 0;
	int check = 0;
	int summary = 0;
	int sanitise = 1;

	strcpy(CAFfile, "stdin");
	strcpy(outfile, "stdout");

	getboolean("-reformat", &reformat, argc, argv);
	getboolean("-verbose", &verbose, argc, argv);
	getboolean("-check", &check, argc, argv);
	getboolean("-summary", &summary, argc, argv);
	getboolean("-sanitise", &sanitise, argc, argv);

	fofn = argfile("-fofn", "r", argc, argv, fofnfile); /* use just the sequences listed in fofn - otherwise use all */
	CAF = readCAF(argc, argv, CAFfile);
	getarg("-out", outfile, argc, argv);

	gethelp(argc, argv);
  
	if (CAF) {
		if (reformat) {
			seqs = CAF->seqs;
			seqDict = CAF->seqDict;

			for(i = 0; i < arrayMax(seqs); i++) {
				seq = arrp(seqs, i, CAFSEQ);
				
				if (seq->SCF_File) seq->type = is_read;
				
				if (seq->dna) {
					dna = stackText(seq->dna, 0);
					len = strlen(dna);
					
					for(j = 0; j < len; j++) {
					
						if (dna[j] == '-')
							dna[j] = 'n';
						else if (dna[j] == '*')
							dna[j] = '-';
					}
				}
			}
		}


		if (summary) {
			summariseCAF(CAF);
			exit(0);
		}


		if (check) exit(check_holes(CAF, verbose) + checkCAF(CAF, verbose));


		if (fofn) {
			char read[256];
			char line[256];
			Array write_seq;
			CAFSEQ *contig;
			int j, nseq = 0, gseq = 0;

			seqs = CAF->seqs;
			seqDict = CAF->seqDict;
			write_seq = arrayCreate(arrayMax(seqs), int);

			for(i = 0; i < arrayMax(seqs); i++) *arrayp(write_seq, i, int) = 0;

			fprintf(stderr, "Only writing sequences in %s...\n", fofnfile);

			while(skip_comments(fofn, line) != EOF) {
				char *readname;

				sscanf(line, "%s", read);
				readname = file_part(read);

				if (dictFind(seqDict, readname, &i)) {

					(*arrayp(write_seq, i, int))++;
					contig = arrp(seqs, i, CAFSEQ);

					if (is_contig == contig->type) {
						/* select all reads in the contig too */

						if (contig->assinf) {

							for(j = 0; j < arrayMax(contig->assinf); j++) {
								a = arrp(contig->assinf, j, ASSINF);
								(*arrayp(write_seq, a->seq, int))++;
							}
						} else {
							fprintf(stderr, "Warning: Contig %s has no assembly information\n", readname);
						}
					}
				} else {
					fprintf(stderr, "Unknown Sequence: %s\n", readname);
				}
			}

			for(i = 0; i < arrayMax(seqs); i++) {
				seq = arrp(seqs, i, CAFSEQ);
				nseq++;

		  		if (array(write_seq, i, int) == 0) {
	    			seq->bad = TRUE;
				} else {
					seq->bad = FALSE;
					gseq++;
		  		}
			}

			fprintf(stderr, "%d/%d Sequences selected\n", gseq, nseq);
		}

		if (sanitise) sanitiseCAF(CAF);

		writeCAF(CAF, CAFfile, argc, argv);

		if (verbose) {
			seqs = CAF->seqs;
			seqDict = CAF->seqDict;

			for(i = 0; i < arrayMax(seqs); i++) {
				seq = arrp(seqs, i, CAFSEQ);

				if (seq->type == is_contig && seq->assinf) {
					printf("\n\n ***** %s ***** \n\n", dictName(seqDict, i));

					for(j = 0; j < arrayMax(seq->assinf); j++) {
						a = arrayp(seq->assinf, j, ASSINF) ;
						fprintf (stdout, "Assembled_from %s %d %d %d %d\n", 
							dictName(seqDict, a->seq), a->s1, a->s2, a->r1, a->r2);

						a->s = ass_subseq(seqs, a);
						fprintf(stdout,"%s\n", a->s); 
					}
				}

				if (seq->SCF_File && seq->assinf) {
					printf("\n\n ***** %s ***** SCF \n\n", dictName(seqDict, i));

					for(j = 0; j < arrayMax(seq->assinf); j++) {
						a = arrayp(seq->assinf, j, ASSINF);
						fprintf(stdout, "Assembled_from %s %d %d %d %d\n", 
							dictName(seqDict, a->seq), a->s1, a->s2, a->r1, a->r2);
					}
				}
			}
		}

		return 0;
	}
	
	return 1;
}

char *file_part(char *path) 
{
	/* returns a pointer to the file part of a full path name */
    int len;
    char *s;

    len = strlen(path);
    for(s = path + len - 1; len && *s != '/'; len--, s--);
    s++;

    return s;
}
		
	
