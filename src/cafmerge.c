/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * Last edited: Oct  4 15:42 1996 (rmott)
 * $Id: cafmerge.c 11038 2003-07-25 14:54:57Z dgm $
 * $Log$
 * Revision 1.4  2003/07/25 14:54:57  dgm
 * Updated comments to remove nested comments.
 *
 * Revision 1.3  2002/03/04 17:00:19  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.2  1997/07/22  11:21:15  badger
 * added base_position (work in saint louis)
 *
 * Revision 1.2  1997/07/15  19:13:52  sdear
 * Support for BasePosition
 *
 * Revision 1.1  1997/04/03  13:44:09  badger
 * Initial revision
 *
 * Revision 1.4  1996/10/03  17:21:46  rmott
 * *** empty log message ***
 *
 * Revision 1.3  1996/06/24  16:33:16  rmott
 * *** empty log message ***
 *
 * Revision 1.2  1996/06/24  16:32:07  rmott
 * merge two caf files. Any information in the first file is overwritten by
 * correponding info in the second.
 *
 * Revision 1.1  1996/03/08  10:10:49  rmott
 * Initial revision
 *
 */

/*
 * cafmerge will merge two caf files. 
 *
 * They should both be in the same state ie padded or unpadded.
 *
 * Conflicting data in the second file overwrites the first - ie there
 * is no Appending 
 *
 * eg if a sequence has a tag of type X in both files then only the X
 * tags from the second file are written - all the X tags from the first
 * are deleted. However, if the sequence ahs X tags in the first file but
 * not in the second then they persist. Note that this arrangement is NOT
 * guarranteed to give a consistent database!
 * 
 */

#define MAIN
#define DEBUG 0

#include<stdio.h>
#include<string.h>

#include <cl.h>
#include <caf.h>
/*#include <scf.h>*/

int 
main( int argc, char **argv )
{

  char caffile1[256], caffile2[256];
  FILE *cfp1, *cfp2;
  cafAssembly *caf1, *caf2;
  Array seqs1, seqs2;
  DICT *seqDict1, *seqDict2;
  int id1, id2;
  char *name1;
  CAFSEQ *seq1, *seq2;
  FILE *out_fp;
  char outfile[256];
  extern int binary_mode;

  binary_mode=FALSE; /* MUST write out in ascii mode */

  if ( ( 
       ( (cfp2=argfile("-caf2","r",argc,argv,caffile2)) && (caf2=readCafAssembly(cfp2,caffile2,NULL ) ) ) &&
	(cfp1=argfile("-caf1","r",argc,argv,caffile1)) && (caf1=readCafAssembly(cfp1,caffile1,NULL ) ) ) &&
       ( ( (out_fp=argfile("-out","w",argc,argv,outfile)) || (out_fp = stdout ))))
    {
      seqs1 = caf1->seqs;
      seqDict1 = caf1->seqDict;

      seqs2 = caf2->seqs;
      seqDict2 = caf2->seqDict;

      for(id1=0;id1<arrayMax(seqs1);id1++)
	{
	  seq1 = arrp(seqs1,id1,CAFSEQ);
	  name1 = dictName(seqDict1,id1);
	  if ( dictFind(seqDict2,name1,&id2) )
	    {
	      seq2 = arrp(seqs2,id2,CAFSEQ);
	      
	      if ( seq2->type )
		seq1->type = 0;

	      if ( seq2->pad_state )
		seq1->pad_state = 0;

	      if (seq2->is_circular)
		seq1->is_circular = 0;

	      if (seq2->is_terminator)
		seq1->is_terminator = 0;

	      if ( seq2->is_long )
		seq1->is_long = 0;

	      if (seq2->is_stolen)
		{
		  seq1->is_stolen=0;
		  seq1->stolen_from = NULL;
		}

	      if ( seq2->dye )
		seq1->dye = 0;

	      if ( seq2->primer )
		seq1->primer = 0;

	      if( seq2->primer )
		{
		  seq1->primer = 0;
		  seq1->custom_primer = NULL;
		}

	      if ( seq2->template_id )
		seq1->template_id = 0;

	      if ( seq2->dna )
		seq1->dna = NULL;

	      if ( seq2->assinf )
		seq1->assinf = NULL;

	      if ( seq2->base_quality )
		seq1->base_quality = NULL;

	      if ( seq2->base_position )
		seq1->base_position = NULL;

	      if ( seq2->golden_path ) 
		seq1->golden_path = NULL;

	      if ( seq2->tags && arrayMax(seq2->tags) > 0 ) 
		seq1->tags = NULL;

	      if ( seq2->svector && arrayMax(seq2->svector) > 0  ) 
		seq1->svector = NULL;

	      if ( seq2->cvector && arrayMax(seq2->cvector) > 0 )  
		seq1->cvector = NULL;

	      if ( seq2->clipping && arrayMax(seq2->clipping) > 0  )
		seq1->clipping = NULL;

	      if ( seq2->edit )
		seq1->edit = NULL;

	      if ( seq2->SCF_File )
		seq1->SCF_File = NULL;
	    }
	}

      writeCafAssembly( caf1, out_fp, caffile1 );
      writeCafAssembly( caf2, out_fp, caffile2 );

      return 0;
    }
  return 1;
}
