/*  Last edited: Aug 16 10:04 2001 (rmd) */
#define MAIN 1

#include <stdio.h>
#include <cl.h>
#include <caf.h>
/* #include"scf.h" */

int
main(int argc, char **argv)
{
  char assembly_file[256] = { 0 };
  char raw_file[256] = { 0 };
  FILE *afp;
  FILE *rfp;
  cafAssembly *assembly;
  cafAssembly *raw;
  Array assembly_seqs;
  int i;
  CAFSEQ *aseq;
  int j;
  int found;
  int raw_id;

  afp = argfile("-assembly", "r", argc, argv, assembly_file);
  rfp = argfile("-raw", "r", argc, argv, raw_file);

  gethelp(argc,argv);
  
  assembly = readCafAssembly(afp, assembly_file, NULL);
  raw = readCafAssembly(rfp, raw_file, NULL);

  assembly_seqs = assembly->seqs;
  for (i = 0; i < arrayMax(assembly_seqs); i++) {
    aseq = arrp(assembly_seqs, i, CAFSEQ);
    
    if (aseq->type != is_read) continue;
    if (!aseq->base_quality) continue;

    found = 0;
    for (j = 0; j < arrayMax(aseq->base_quality); j++) {
      if (arr(aseq->base_quality, j, char) != 99) {
	found = 1;
	break;
      }
    }
    if (!found) {
      char *read_name = dictName(assembly->seqDict, i);
      if (dictFind(raw->seqDict, read_name, &raw_id)) {
	CAFSEQ *rawseq = arrp(raw->seqs, raw_id, CAFSEQ);
	
	int num_aligns = arrayMax(aseq->assinf);
	if (num_aligns != 1) {
	  /* int seg;
	     char *rawdna, *assdna;
	     int shift = 0; */

	  fprintf(stderr,
		  "Read %s can't be fixed as it is in %d segments\n",
		  read_name, num_aligns);

	  /*	  fprintf(stderr, "WARNING: Read %s is in %d segments\n",
		  read_name, num_aligns);
	  
	  rawdna = stackText(rawseq->dna, 0);
	  assdna = stackText(aseq->dna, 0);

	  for (seg = 0; seg < num_aligns; seg++) {
	    ASSINF* ascf = arrp(aseq->assinf, seg, ASSINF);

	    while (seg == 0 && rawdna[ascf->r1 - 1 + shift] == '-') {
	      shift++;
	    }

	    ascf->r1 += shift;
	    ascf->r2 += shift;

	    fprintf(stderr, "seg = %d s1 =  %d s2 =  %d r1 = %d r2 = %d\n",
		    seg, ascf->s1, ascf->s2, ascf->r1, ascf->r2);
	    for (j = ascf->s1 - 1; j < ascf->s2; j++) {
	      if (assdna[j] != rawdna[j + ascf->r1 - ascf->s1]) {
		fprintf(stderr, "%4d %c <=> %c %4d\n",
			j, assdna[j],
			rawdna[j + ascf->r1 - ascf->s1],
			j + ascf->r1 - ascf->s1);
	      }
	      arr(aseq->base_quality, j, char)
		= arr(rawseq->base_quality, j + ascf->r1 - ascf->s1, char);
	    }
	    }*/
	} else {
	  if (arrayMax(rawseq->base_quality) != arrayMax(aseq->base_quality)) {
	    fprintf(stderr,
		    "Read %s can't be fixed as it's quality lengths differ %d %d\n",
		    read_name,
		    arrayMax(rawseq->base_quality),
		    arrayMax(aseq->base_quality));
	  } else {
	    for (j = 0; j < arrayMax(aseq->base_quality); j++) {
	      arr(aseq->base_quality, j, char) = arr(rawseq->base_quality, j, char);
	    }
	    fprintf(stderr, "Fixed read %s\n", read_name);
	  }
	}
      } else {
	fprintf(stderr,
		"Read %s can't be fixed as it is not in the raw file\n",
		read_name);
      }
    }
  }

  writeCafAssembly(assembly, stdout, NULL);

  return 0;
}
