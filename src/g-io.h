/*
 * File: g-io.h
 *
 * Author: Simon Dear
 *         MRC Laboratory of Molecular Biology
 *	   Hills Road
 *	   Cambridge CB2 2QH
 *	   United Kingdom
 *
 * Description: header file for low level io
 *
 * Created:
 * Updated:
 *
 */

#ifndef _G_IO_H_
#define _G_IO_H_

#include "g-filedefs.h"

extern int write_aux_header_(int fd, AuxHeader *rec);
extern int write_aux_header_swapped_(int fd, AuxHeader *header);
extern int read_aux_header_(int fd, AuxHeader *rec);
extern int read_aux_header_swapped_(int fd, AuxHeader *header);
extern int write_aux_index_(int fd, AuxIndex *rec);
extern int write_aux_index_swapped_(int fd, AuxIndex *idx);
extern int read_aux_index_(int fd, AuxIndex *rec);
extern int read_aux_index_swapped_(int fd, AuxIndex *idx);


extern int (*low_level_vectors[4])();
extern int (*low_level_vectors_swapped[4])();
extern int (*(*low_level_vector))();
extern int (*(*set_low_level_vector()))();

#define GOP_WRITE_AUX_HEADER 0
#define GOP_WRITE_AUX_INDEX  1
#define GOP_READ_AUX_HEADER  2
#define GOP_READ_AUX_INDEX   3

#endif /*_G_IO_H_*/
