/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 *  Last edited: Mar  4 17:16 2002 (rmd)
 *
 *  Last edited: Oct  4 14:30 1996 (rmott)
 * $Id: assembly.c 13622 2004-11-26 17:14:25Z rmd $
 * $Log$
 * Revision 1.9  2004/11/26 17:14:25  rmd
 * Fixed bug in close_holes caused by break being in the wrong place.
 *
 * Revision 1.8  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 * Revision 1.7  2002/03/04 17:54:36  rmd
 * Commented out include myscf.h
 *
 * Revision 1.6  2002/03/04 17:00:37  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.5  2001/12/12 18:05:03  rmd
 * Fixed bug in assinf_from_pads that made it fall off the end of the mapping
 * array if pads were at the end of a read.
 *
 * Revision 1.4  1999/04/01 09:23:56  rmd
 * Added caf_read_scf to read compressed SCF files directly rather than
 * use a temp file. Also protect stolen read tags when printing them out.
 *
 * Revision 1.3  1998/03/17  18:15:01  badger
 * Changes to makefiles
 *
 * Revision 1.2  1998/01/12  16:37:46  badger
 * Changed initial value of min to MAXINT in getContigAlignment
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Revision 1.9  1996/10/14  13:59:36  rmott
 * removed refs to fit_fp (no longer used)
 *
 * Revision 1.8  1996/10/04  13:30:10  rmott
 * removed use_CAF
 *
 * Revision 1.7  1996/04/03  14:06:25  rmott
 * version prior to changing contigAlignment struct
 *
 * Revision 1.6  1996/03/25  15:18:26  rmott
 * version prior to changing aligndata
 *
 * Revision 1.5  1996/03/19  10:18:09  rmott
 * buggy preconsensus
 *
 * Revision 1.4  1996/02/21  11:31:34  rmott
 * CAF version
 *
 */

#include<stdio.h>
/*#include<stdlib.h>  */
#include<values.h>
#include<string.h>
#include<ctype.h>
#include"caf.h"
/*#include"myscf.h"*/
#include"cl.h"
#include"seq_util.h"

#undef DEBUG

/* return a string containing the subsequence specified in assinfo. It
   is reverse-complemented if implied by its contig position */

char 
*ass_subseq( Array seqs, ASSINF *assinfo )
{
  CAFSEQ *seq = arrp( seqs, assinfo->seq, CAFSEQ );
  char *dna;
  int i, k;
  char *s=NULL;
  if ( seq && seq->dna)
    {
      dna = stackText(seq->dna,0)-1;
      s =(char*)messalloc(assinfo->r2-assinfo->r1+2);
      for(i=0,k=assinfo->r1;k<=assinfo->r2;i++,k++)
	s[i] = dna[k];
      s[i] = 0;
      
      if ( assinfo->s1 > assinfo->s2 )
	complement_seq(s);
    }
  return s;
}

/*********************************************************************
 Compute the contig alignment struct for a given contig 

getContigAlignment() takes a padded contig and its set of aligned
readings and returns a structure, A, describing the alignment in great
detail. This is used by other functions to auto-edit the contig


The information essentially comprises two things:

1. A->Status, A 1-D array of alignStatus structs, one per contig position,
summarising the state of the alignment at each position

2 A->aligned, A 2-D array of alignData structs, describing in detail the state of
each aligned read at every contig position

*********************************************************************/

contigAlignment 
*getContigAlignment( cafAssembly *CAF, CAFSEQ *contig, int s1_only, float badthresh )

/*  switches:

    s1_only        only use s1 reads - simulates the first phase of assembly 
    badthresh      min allowed percent identity of read with consensus 

*/

{
  int i, j, readslot, d;
  Array slot, last;
  ASSINF *a;
  int min = MAXINT;
  int max = -1;
  int a_s1, a_s2;
  int *l; 
  Array aligned=NULL;
  Array *B;
  char  *dot;
  CAFSEQ *seq;
  int k;
  contigAlignment *A=NULL;
  char *dna;
  Array seqs = CAF->seqs;
  DICT *seqDict = CAF->seqDict;
  int strand;
  alignData *ad;
  double *q, *Q;
  int pos;
  /* int is_s1; */
  STORE_HANDLE handle;
  int tot = 0;

  if ( contig->type == is_contig && contig->assinf ) /* sanity check */
    {
      /* the memory handle */
      handle = handleCreate();
      /* the slot array. This gives the slot (2nd array index in the
         aligned array) allocated to each read */
      slot = arrayHandleCreate( 1000, int, handle );
      last = arrayHandleCreate( 1000, int, handle );

      /* sort the aligned reads in the contig so they are processed
	 left to right, in order to minimise space */

      arraySort( contig->assinf, ass_cmp ); 

      A = (contigAlignment*)halloc(sizeof(contigAlignment),handle);
      A->handle = handle;
      A->aligned = A->depth = A->status = A->cons = A->trace_quality = A->alignment_quality = NULL;

      dna = stackText( contig->dna, 0 );

 /* get the size of the contig. Go through each aligned read */

      for(j=0;j<arrayMax(contig->assinf);j++)
	{
	  a = arrp(contig->assinf, j, ASSINF );
	  seq = arrp(seqs,a->seq,CAFSEQ);
	  seq->bad = FALSE;

	  /* a_s1, a_s2 is the range in the contig of the read's
             alignment */

	  getAlignedRange(a, &a_s1, &a_s2);

	  /* min, max are the first and last positions in the contig
             aligned to a read (we obviously can't edit outside this
             range */

	  if ( a_s1 < min )
	    min = a_s1;
	  if ( a_s2 > max )
	    max = a_s2;

/* compute the slot for each read */

	  for(i=0;i<arrayMax(last)+1;i++)
	    {
	      l = arrayp(last,i,int);
	      if ( a_s1 > *l )
		{
		  *l = a_s2;
		  *arrayp(slot,a->seq,int) = i;
		  break;
		}
	    }
	}


/* initalise the alignData */

      aligned = arrayHandleCreate( max+1 , Array, handle);

      A->min = min;
      A->max = max;
      A->width = arrayMax(last);
      A->aligned = aligned;
      A->depth = arrayHandleCreate( max+1, int, handle );
      A->contig = contig;
      A->cons = arrayHandleCreate(max+2,char,handle);
      A->trace_quality = arrayHandleCreate( arrayMax(contig->assinf), double, handle );
      A->alignment_quality = arrayHandleCreate( A->max+1, double, handle );
      A->slot = slot;

/* find the depth of the alignment at each position */

      *arrayp(A->depth,max,int) = 0;
      for(j=0;j<=max;j++)
	*arrp(A->depth,j,int) = 0;

      for(j=0;j<arrayMax(contig->assinf);j++)
	{
	  a = arrp(contig->assinf, j, ASSINF );
	  seq = arrp(seqs,a->seq,CAFSEQ);

	  getAlignedRange(a, &a_s1, &a_s2);

	  i = arr(slot,a->seq,int);

	  for(k=a_s1;k<=a_s2;k++)
	    {
	      d = arr(A->depth,k,int);
	      if ( d < i )
		*arrp(A->depth,k,int) = i;
	    }
	}

/* create arrays for each alignment position with variable depth */

      arrayp(aligned,max,Array);

      for(j=min;j<=max;j++)
	{
	  B = arrp(aligned,j,Array);
	  *B = arrayHandleCreate( arr(A->depth,j,int)+1, alignData, handle );
	  tot += arr(A->depth,j,int)+1;
	  arrayp(*B,arr(A->depth,j,int),alignData);
	  for(i=0;i<arrayMax(*B);i++)
	    {
	      ad = arrp(*B,i,alignData);
	      ad->base = '.';
	      ad->id = -1;
	    }
	}

      /* array of floats giving the alignment quality at a given position */

      arrayp(A->alignment_quality,A->max,double);
      for(j=A->min;j<=A->max;j++)
	*arrp(A->alignment_quality,j,double) = 1.0e10;
      
      /* initialise the alignData 2-D array */
      
      arrayp(A->trace_quality,arrayMax(contig->assinf)-1,double);
      for(j=0;j<arrayMax(contig->assinf);j++)
	{
	  a = arrp(contig->assinf, j, ASSINF );
	  seq = arrp( seqs, a->seq, CAFSEQ );

	  strand = getAlignedRange(a, &a_s1, &a_s2);

	  if ( seq->is_terminator )
	    strand *= terminator; /* encode terminator info as -2 or +2 so we can keep strand info as well */

	  readslot = arr( slot, a->seq, int ); 

	  q = arrp(A->trace_quality,j,double);
	  *q = 0.0;

	  for(i=a_s1,k=0,pos=a->r1;i<=a_s2;i++,k++,pos++)
	    {
	      B = arrp(aligned,i,Array);
	      ad = arrp(*B,readslot,alignData);

	      ad->base = a->s[k];
	      if (ad->base == 0) {
		fprintf(stderr, "Off end of subsequence i = %d, k = %d, pos = %d, a_s1 = %d, a_s2 = %d, a->r1 = %d, a->r2 = %d\n", i, k, pos, a_s1, a_s2, a->r1, a->r2);
	      }
	      ad->id = a->seq;
	      ad->ass_index = j;
	      ad->edited = ok;
	      ad->strand = strand;
	      if ( is_reverse(strand) )
		ad->pos = a->s1 - i + a->r1;
	      else
		ad->pos = pos;
	    }
	  
	  seq = arrp( seqs, a->seq, CAFSEQ );

	  seq->bad = FALSE;
	  if ( s1_only ) /* this code is highly dodgy and only used for debugging ! */
	    {
	      if ( (dot = strchr( dictName(seqDict,a->seq), '.' )) && !strcmp(dot,".s1") )
		{
		/* is_s1 = TRUE; */
		}
	      else
		{
		  /* is_s1 = FALSE; */
		  seq->bad = TRUE;
		}
	    } 
	}

      /* the contig consensus (NOT the same as the majority vote !) */

      arrayp(A->cons,A->max,char);
      for(j=A->min;j<=A->max;j++)
	*arrp(A->cons,j,char) = dna[j-1];

      for(i=0;i<arrayMax(contig->assinf);i++)
	{
	  a = arrp(contig->assinf, i, ASSINF );
	  seq = arrp( seqs, a->seq, CAFSEQ );
	  seq->q = 0.0;
	  seq->len = 0;
	}
      
      /* make a first stab at getting the maj. vote consensus. This
         will be revised when we delete bad reads */

      for(j=A->min;j<=A->max;j++)
	get_preconsensus( *arrp(aligned,j,Array), seqs );


      /* Identify reads which align badly */

      for(i=0;i<arrayMax(contig->assinf);i++)
	{
	  a = arrp(contig->assinf, i, ASSINF );
	  seq = arrp( seqs, a->seq, CAFSEQ );
	  seq->q /= (seq->len+1.0e-10);
	  seq->q *= 100.0;
	  if ( seq->q < badthresh )
	    seq->bad = TRUE;
	  seq->len = 0;
	  seq->q = 0.0;
	}

      /* refine the consensus using just the remaining good reads */

      for(j=A->min;j<=A->max;j++)
	get_preconsensus( *arrp(aligned,j,Array), seqs );

      /* Insert quality values into the alignData 2-D array and
         elsewhere */

      for(i=0;i<arrayMax(contig->assinf);i++)
	{
	  a = arrp(contig->assinf, i, ASSINF );
	  seq = arrp( seqs, a->seq, CAFSEQ );

	  readslot = arr( slot, a->seq, int );

	  seq->q /= (seq->len+1.0e-10);
	  seq->q *= 100.0;

	  getAlignedRange(a, &a_s1, &a_s2);

	  for(j=a_s1;j<=a_s2;j++)
	    {
	      Q = arrp(A->alignment_quality,j,double);
	      if ( *Q > seq->q )
		*Q = seq->q;
	      B = arrp(aligned,j,Array);
	      ad = arrp( *B, readslot, alignData );
	      ad->bad = seq->bad;
	      *arrp(A->trace_quality,i,double) = *Q;
	    }
	}
      
    }
  
  return A;
}

/* get the range of an aligned read , and return the orientation */

int
getAlignedRange( ASSINF *a, int *a_s1, int *a_s2 )
{
  if ( a->s1 < a-> s2 )
    {
      *a_s1 = a->s1;
      *a_s2 = a->s2;
      return forward_strand;
    }
  else
    {
      *a_s1 = a->s2;
      *a_s2 = a->s1;
      return reverse_strand;
    }
}


void
close_holes( contigAlignment *Align, cafAssembly *CAF, FILE *logfp )
{
/* check for holes caused by deleting reads */
  int k, i, n;
  int depth, deleted;
  CAFSEQ *seq, *contig = Align->contig;
  alignData *ad;
  Array recover;
  Array *B;
  int s1 = 1, s2 = -1, readslot;
  ASSINF *a;

  /* go through contig looking for places with zero coverage but with deleted reads */

  recover = arrayCreate(Align->max,int); /* stores the index of a sequence to undelete, or -1 */

  for(k=Align->min;k<=Align->max;k++)
    {
      B = arrp(Align->aligned,k,Array);
      depth = deleted = 0;
      for(i=0;i<arrayMax(*B);i++)
	{
	  ad = arrp(*B,i,alignData);
	  if ( ad->id >= 0 )
	    {
	      if ( ad->bad )
		deleted++;
	      else
		depth++;
	    }
	}
      if ( depth == 0 && deleted > 0)
	{
	  for(i=0;i<arrayMax(*B);i++)
	    {
	      ad = arrp(*B,i,alignData);
	      if ( ad->id >= 0 )
		{
		  if ( ad->bad == TRUE )
		    {
		      *arrayp(recover,k,int) = ad->id; /* id of seq to undelete */
		      break;
		    }
		}
	    }
	}
      else if ( depth == 0 )
	*arrayp(recover,k,int) = -1; /* unfilled hole */
      else 
	*arrayp(recover,k,int) = -2; /* no hole */ 
    }


  for(k=Align->min;k<=Align->max;k++)
    {
      if ( (n=array(recover,k,int)) >= 0 )
	{
	  seq = arrp(CAF->seqs,n,CAFSEQ);
	  if ( seq->bad ) /* undelete the sequence and its references in the contig alignment */
	    {
	      seq->bad = FALSE;
	      if ( logfp )
		fprintf(logfp, "%-15s undeleted to fill hole\n", dictName(CAF->seqDict,n));
	      fprintf(stderr, "%-15s undeleted to fill hole\n", dictName(CAF->seqDict,n));
	      readslot = arr(Align->slot,n,int);
	      for(i=0;i<arrayMax(contig->assinf);i++)
		{
		  a = arrp(contig->assinf,i,ASSINF);
		  s1 = 1;
		  s2 = -1;
		  if ( a->seq == n )
		    {
		      if ( a->s1 < a->s2 )
			{
			  s1 = a->s1;
			  s2 = a->s2;
			}
		      else
			{
			  s1 = a->s2;
			  s2 = a->s1;
			}
		      break;
		    }
		}
	      for(i=s1;i<=s2;i++)
		{
		  B = arrp(Align->aligned,i,Array);
		  ad = arrp(*B,readslot,alignData);
		  ad->bad = FALSE;
		}
	    }
	}
    }
}


/* compute vector of nucleotide frequencies at a given position */

void 
get_preconsensus( Array alignment, Array seqs )
{

  int i;
  int good = 0;
  alignData *ad;
  CAFSEQ *seq;
  float consensus[nucleotides];
  float x;
  char best;

  for_any_base(i)
    consensus[i] = 0.0;

/* count the number of votes for each nucleotide */
  
  for(i=0;i<arrayMax(alignment);i++)
    {
      ad = arrp(alignment,i,alignData );

      if ( ad->base != '.' )
	{
	  seq = arrp( seqs, ad->id, CAFSEQ );
	  if ( seq->bad == FALSE )
	    {
	      good++;
	      consensus[i_base(ad->base)]++;
	    }
	}
    }

  if ( good == 0 ) /* no good reads cover so set masks so that they won't be removed*/
    {
      for(i=0;i<arrayMax(alignment);i++)
	{
	  ad = arrp(alignment,i,alignData );
	  
	  if ( ad->base != '.' )
	    {
	      seq = arrp( seqs, ad->id, CAFSEQ );
/*	      seq->masked = TRUE; */
	    }
	}
    }

  best = '.'; /* '.' means there is no consensus */
  if ( good > 0 ) /* see if there is a base with at least 50% of the calls */
    {
      x = 0.5*good;
      for_any_base(i)
	{
	  if ( consensus[i] > x )
	    {
	      best = c_base(i);
	      break;
	    }
	}
    }

/* update the sequence quality in the light of the best base */

  for(i=0;i<arrayMax(alignment);i++)
    {
      ad = arrp(alignment,i,alignData );
      if ( ad->base != '.' )
	{
	  seq = arrp( seqs, ad->id, CAFSEQ );
	  if ( tolower(ad->base) == best ) 
	    seq->q ++;
	  seq->len++;
	}
    }

}
  


void 
compare_edits_to_consensus( cafAssembly *CAF, contigAlignment *Align, FILE *fp, int use_scf, int dump_edits )
{

/* compares proposed edits to the input consensus. Switches are:
   used for logging and debugging purposes only

   use_scf:    use trace info
   dump_edits: write proposed edits out to fp
*/

  char base;
  int i, j;
  int min = Align->min;
  int max = Align->max;
  alignStatus *S;
  Array status = Align->status;
  Array dna = Align->cons; 
  /* Array quality; */
  
  int same[status_count][Consensus_count];
  int different[status_count][Consensus_count];
  int total_same[Consensus_count];
  int total_different[Consensus_count];

  for(i=0;i<status_count;i++)
    for(j=0;j<Consensus_count;j++)
      same[i][j] = different[i][j] = 0;

  for(j=0;j<Consensus_count;j++)
    total_same[j] = total_different[j] = 0;

  if ( fp )
    fprintf(fp, "\nAnalysis of edits:\n\n");

  if ( min < CAF->from )
    min = CAF->from;
  if ( CAF->to > 0 && max > CAF->to )
    max = CAF->to;

  if ( fp )
    fprintf(fp, "from: %d to: %d\n\n", min, max );

  /* quality = arrayHandleCreate( Align->max+1, traceQuality, Align->handle); */

  for(j=min;j<=max;j++)
    {
      base = i_base(arr(dna, j, char ));
      S = arrayp(status,j,alignStatus);


      if ( S->best == base )
	same[S->status][S->consensus]++;
      else
	different[S->status][S->consensus]++;

      if ( use_scf && S->status != ok )
	{
	  if ( fp && dump_edits && S->best != base && S->status == replace )
	    {
	      fprintf(fp, ">>> %5d %d %c %c %-20s %-15s %-15s %-15s %2d %2d %2d %2d %2d %2d %2d %2d\n", j, base==S->best, c_base(base), c_base(S->best), Consensus_text[S->consensus], status_text[S->status], strand_text[S->stranded], compound_text[is_compound(S->compound)], S->calls, S->depth, S->base_calls[base_a],S->base_calls[base_c],S->base_calls[base_g],S->base_calls[base_t],S->base_calls[base_n],S->base_calls[base_i]);
	      
	    }
	}
    }
  
  if ( fp )
    {
      fprintf(fp,"\n\n Comparison with contig consensus:\n\n");


      fprintf(fp,"%-15s ", "" );
      for(j=0;j<Consensus_count;j++)
	fprintf(fp,"%-15s ", Consensus_text[j] );
      fprintf(fp,"\n");
      
      for(i=0;i<status_count;i++)
	{
	  fprintf(fp,"%-15s ", status_text[i] );
	  for(j=0;j<Consensus_count;j++)
	    {
	      fprintf(fp,"   %5d %5d  ", same[i][j], different[i][j] );
	      total_same[j] += same[i][j];
	      total_different[j] += different[i][j];
	    }	  
	  fprintf(fp,"\n");
	}
      fprintf(fp,"%-15s ", "total");
      for(j=0;j<Consensus_count;j++)
	fprintf(fp,"   %5d %5d  ", total_same[j], total_different[j] );
      fprintf(fp,"\n\n");
    }
}


void 
print_contig_statistics( FILE *fp, cafAssembly *CAF, contigAlignment *Align, float badthresh, int min_depth, int print_summary )
{
  int min = Align->min;
  int max = Align->max;
  alignStatus *S;
  int j, i, n;
  double x, q;
  Array status = Align->status;
  static int status_hist[status_count];
  static int consensus_hist[Consensus_count];
  static int edit_hist[status_count];
  static int table[status_count][Consensus_count];
  static int edit_table[status_count][Consensus_count];
  static int edit_consensus_hist[Consensus_count];
  static int made=0;
  static int required=0;
  static int total_len=0;
  static int possible=0;
  CAFSEQ *contig = Align->contig, *seq;
  Array seqs = CAF->seqs;
  ASSINF *a;

  if ( print_summary )
    {
      x = 1.0e-10+total_len/100.0;

      fprintf(fp, "\n\n****** Edit Summary for %s  *******\n", CAF->filename );
      fprintf(fp, "\n\n Total size: %d\n", total_len );
      fprintf(fp, "\n Status report:\n\n");

      fprintf(fp, "%-25s ", "" );
      for(j=0;j<Consensus_count;j++)
	fprintf(fp,"      %-11s ", Consensus_text[j] );
      fprintf(fp,"total\n");
      
      for(i=0;i<status_count;i++)
	{    
	  fprintf(fp, "%-25s ", status_text[i] );
	  for(j=0;j<Consensus_count;j++) {
	    n=table[i][j];
	    fprintf( fp, " %5d  (%5.1f%%)  ",  n, n/x );
	  }
	  n=status_hist[i];
	  fprintf( fp, " %5d  (%5.1f%%)\n",  n, n/x );
	}
      
      fprintf(fp, "%-25s ", "total" );
      for(j=0;j<Consensus_count;j++) {
	n=consensus_hist[j];
	fprintf( fp, " %5d  (%5.1f%%)  ",  n, n/x );
      }
      fprintf(fp,"\n");
      
      fprintf(fp,"\n\nProposed Edits:\n\n");
      
      for(i=0;i<status_count;i++)
	{    
	  fprintf(fp, "%-25s ", status_text[i] );
	  for(j=0;j<Consensus_count;j++) {
	    n=edit_table[i][j];
	    fprintf( fp, " %5d  (%5.1f%%)  ",  n, n/x );
	  }
	  n=edit_hist[i];
	  fprintf( fp, " %5d  (%5.1f%%)\n",  n, n/x );
	}
      
      fprintf(fp, "%-25s ", "total" );
      for(j=0;j<Consensus_count;j++) {
	n=edit_consensus_hist[j];
	fprintf( fp, " %5d  (%5.1f%%)  ",  n, n/x );
      }
      fprintf(fp,"\n");

      fprintf(fp, "\n total edits required: %5d", required );
      fprintf(fp, "\n             possible: %5d (ie double-stranded and depth >= %d", possible, min_depth );
      fprintf(fp, "\n             made    : %5d (%d%%) (%d%%)\n", made, (int)(100*made/(1.0e-5+required)), (int)(100*made/(1.0e-5+possible)) );
      
      fprintf(fp,"\n");
    }
  else
    {
      for(j=min;j<=max;j++)
	{
	  S = arrayp(status,j,alignStatus);
	  table[S->status][S->consensus]++;
	  status_hist[S->status]++;
	  consensus_hist[S->consensus]++;
	  if ( S->status != ok && S->status != too_low )
	    required++;
	  if ( S->consensus == Strong )
	    {
	      if ( S->status != ok )
		made++;
	      edit_hist[S->status] ++;
	      edit_table[S->status][S->consensus] ++;
	      edit_consensus_hist[S->consensus]++;
	    }
	  if ( S->status != ok && ( S->consensus == Strong || (S->stranded == double_stranded && S->depth >= min_depth ) ) )
	    possible++;
	}
      
      total_len += max-min+1;
      
      fprintf(fp,"\n\n Strand Consensus Quality Report for %s:\n\n", dictName(CAF->seqDict, Align->contig->id));
      fprintf(fp,"%-15s %7s %5s %7s %10s\n\n", "read", "from", "to", "edits", "% identity" );
      for(j=0;j<arrayMax(contig->assinf);j++)
	{
	  a = arrp(contig->assinf,j,ASSINF);
	  q = arr(Align->trace_quality,j,double);
	  seq = arrp( seqs, a->seq, CAFSEQ );
	  fprintf(fp, "%-15s %7d %7d %5d %10.1f%%  %s\n", dictName(CAF->seqDict,a->seq), a->s1, a->s2, (seq->edit  ? arrayMax(seq->edit): 0 ), q, ( seq->bad ? " misaligned": ""));
	} 
    }  
}


void 
print_contig_alignment( FILE *fp, contigAlignment *Align )
{
  char base;
  int width = Align->width, i, j;
  int min = Align->min;
  int max = Align->max;
  alignStatus *S;
  Array status = Align->status;
  Array aligned = Align->aligned;
  Array dna = Align->cons; 
  static int number=0;
  Array *B;

  for(j=min;j<=max;j++)
    {
      S = arrayp(status,j,alignStatus);
      fprintf(fp,"%5d %c %c ", j, base=arr(dna, j, char ), c_base(S->best) ); 
      B = arrp(aligned,j,Array);
      for(i=0;i<arrayMax(*B);i++)
	putc( arrp(*B, i, alignData)->base , fp );
      for(i=arrayMax(*B);i<width;i++)
	putc('.',fp);

      fprintf( fp, " %s %-15s %-15s %-15s  %6.2f%% ", compound_text[is_compound(S->compound)], status_text[S->status], Consensus_text[S->consensus], strand_text[S->stranded],arr(Align->alignment_quality,j,double));
      if ( S->best != i_base(base) && proper_edit(S->status) )
	{
	  int k;
	  fprintf(fp, " ERROR %d %c", ++number, c_base(S->best));
	  for_any_base(k)
	    fprintf(fp," %c %d", c_base(k), S->base_calls[k]);
	}
      putc( '\n', fp );

    }
}


int 
ass_cmp( void *A, void *B )
{
  ASSINF *a = A;
  ASSINF *b = B;
  int a_s1, a_s2, b_s1, b_s2;
  int n = a->seq-b->seq;

/*  if ( n )
    return n;
*/
  if ( a->s1 < a-> s2 )
    {
      a_s1 = a->s1;
      a_s2 = a->s2;
    }
  else
    {
      a_s1 = a->s2;
      a_s2 = a->s1;
    }

  if ( b->s1 < b-> s2 )
    {
      b_s1 = b->s1;
      b_s2 = b->s2;
    }
  else
    {
      b_s1 = b->s2;
      b_s2 = b->s1;
    }

  if ( (n = a_s1 -b_s1 ) )
    return n;
  else
    return a_s2 - b_s2;
}

    

  

void 
maskVector( cafAssembly *CAF, char mask )
/* masks out vector clipped sequences with the character "mask" */
{
  Array seqs = CAF->seqs;
  TAG *svec;
  int i, j, k;
  int x, x1, x2;
  CAFSEQ *seq;
  char *dna;

  for(i=0;i<arrayMax(seqs);i++)
    {
      seq = arrp(seqs,i,CAFSEQ);
      if ( seq->svector && seq->dna )
	{
	  dna = stackText(seq->dna,0);
	  for(j=0;j<arrayMax(seq->svector);j++)
	    {
	      svec = arrp(seq->svector,j,TAG);
	      x1 = svec->x1-1;
	      x2 = svec->x2-1;
/*	      printf("masking %-12s %d to %d\n", dictName(CAF->seqDict,i), x1, x2 );  */
	      if ( x1 > x2 )
		{
		  x = x1;
		  x1 = x2;
		  x2 = x;
		}
	      for(k=x1;k<=x2;k++)
		dna[k] = mask;
	    }
	}
    }
}

char 
*mask_vector( CAFSEQ *seq, char mask )
/* masks out vector clipped sequence with the character "mask" 
THis version makes a masked copy , so the original is unchanged */

{
  TAG *svec;
  int j, k;
  int x, x1, x2;
  char *dna=NULL;


  if ( seq->dna )
    dna = stackText(seq->dna,0);

  if ( seq->svector && dna )
    {
      dna = strdup(dna);
      for(j=0;j<arrayMax(seq->svector);j++)
	{
	  svec = arrp(seq->svector,j,TAG);
	  x1 = svec->x1-1;
	  x2 = svec->x2-1;
	  if ( x1 > x2 )
	    {
	      x = x1;
	      x1 = x2;
	      x2 = x;
	    }
	  for(k=x1;k<=x2;k++)
	    dna[k] = mask;
	}
    }

  return dna;
}

Array 
assinf_from_pads( CAFSEQ *seq, DICT *seqDict, char *name )

/* if a sequence has no assembled from info but has dna then generate it from its sequence */

{
  ASSINF *a;
  int i, n, len, id;
  char *dna;
  Array mapping;
  char buf[256];
  
  if ( seq->dna && ( ! seq->assinf || ! arrayMax(seq->assinf) ) )
    {
      seq->assinf = arrayCreate(10,ASSINF);
      dna = stackText(seq->dna,0);
      len = strlen(dna);
      mapping = arrayCreate(len+1,int);
      sprintf(buf,"%s~", name);
      dictAdd(seqDict,buf, &id);
      n = 0;
/*      printf("%s\n", dna);*/
      for(i=0;i<len;i++)
	{
	  if (dna[i] != padding_char )
	    *arrayp(mapping,i,int) = n++;
	  else
	    *arrayp(mapping,i,int) = -1;
	}
      i = 0;
      while( i < len )
	{
	  while( arr(mapping,i,int) == -1 && i < len ) i++;
	  if (i >= len) break;

	  a = arrayp( seq->assinf, arrayMax(seq->assinf), ASSINF );
	  a->s1 = i+1;
	  a->r1 = arr(mapping,i,int)+1;
	  i++;
	  while( arr(mapping,i,int) != -1 && i < len)
	    i++;
	  a->s2 = i;
	  a->r2 = arr(mapping,i-1,int)+1;
	  a->seq=id;
	  /* fprintf(stderr, " new assinf %5d %5d %5d %5d\n", a->s1, a->s2, a->r1, a->r2 ); */
	}
      arrayDestroy(mapping);
/*      for(i=0;i<arrayMax(seq->assinf);i++)
	{
	  a = arrp(seq->assinf,i,ASSINF);
	  printf(" new assinf %5d %5d %5d %5d\n", a->s1, a->s2, a->r1, a->r2 );
	} */

    }
  return seq->assinf;
}

