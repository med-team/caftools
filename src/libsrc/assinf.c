/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

/* ASSINF.C
   These are useful routines for manipulating ASSINF records.
*/
#include <array.h>
#include <caf.h>
#include <assinf.h>

/* Compare ASSINF entries.  When used for sorting, they will be ordered by
   the start of each segment */

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

static int order_along_contig(const void *a, const void *b);
static int order_by_seq_num(const void *a, const void *b);

/* Sort routines */

/* Put ASSINF entries into order along a contig */

void sort_assinf_along_contig(Array assinf) {
  ASSINF * assembled_froms = arrp(assinf, 0, ASSINF);
  
  qsort(assembled_froms, arrayMax(assinf), sizeof(ASSINF), order_along_contig);
}

/* Order ASSINF entries by seq number (i.e. group reads together).  Entries
   for the same read are put into read order */

void sort_assinf_by_seq_number(Array assinf) {
  ASSINF * assembled_froms = arrp(assinf, 0, ASSINF);

  qsort(assembled_froms, arrayMax(assinf), sizeof(ASSINF), order_by_seq_num);
}

/* Comparison routines */

static int order_along_contig(const void *a, const void *b) {
  int mina;
  int minb;
  ASSINF *aa = (ASSINF *) a;
  ASSINF *bb = (ASSINF *) b;

  mina = min(aa->s1, aa->s2);
  minb = min(bb->s1, bb->s2);

  return (mina - minb);
}

static int order_by_seq_num(const void *a, const void *b) {
  ASSINF *aa = (ASSINF *) a;
  ASSINF *bb = (ASSINF *) b;

  if (aa->seq - bb->seq != 0) return aa->seq - bb->seq;
  return aa->r1 - bb->r1;
}
