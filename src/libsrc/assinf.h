/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

/* ASSINF.H
   Contains prototypes for functions in assinf.c
   These are useful routines for manipulating ASSINF records.
*/

#ifndef _ASSINF_H_
#define _ASSINF_H_

/* Put ASSINF entries into order along a contig */

void sort_assinf_along_contig(Array assinf);


/* Order ASSINF entries by seq number (i.e. group reads together).  Entries
   for the same read are put into read order */

void sort_assinf_by_seq_number(Array assinf);


#endif
