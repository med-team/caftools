/*  Last edited: Aug  9 12:37 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* AUTO_EDIT.H

   Definitions specific to auto-edit. Also required by applications using contigAlignment structs

   Author: Richard Mott
*/

#ifndef _AUTO_EDIT_H_
#define _AUTO_EDIT_H_

#ifdef CPLUSPLUS
extern "C" { 
#endif

/* Struct containing an edited Sequence 
   id is the id of original
   seq points to the original

   old2new gives coordinate mapping of old to edited seq 
   new2old is the inverse mapping 
  
   new2lower gives coordinate mapping of the new to either the SCF coords (if a read) 
                                                    or the rads coords ( if a contig)

   new_assinf are the new assembled_from lines for the edited seq
   new_dna is the edited dna
*/

typedef struct 
{
  int id;
  CAFSEQ *seq;
  Array old2new;
  Array new2old;
  Array new2lower;
  Array new_assinf;
  Array new_dna;
}
EDITED_CAFSEQ;

/* The type of a contig position in a contigAlignment */

enum status_types 
{ 
  ok,                           /* all traces agree and base called is proper*/
  replace,                      /* a substitution */
  insert,                       /* an insertion */
  deletion,                     /* a deletion */
  unclear,                      /* not clear what edit to do */
  too_low,                      /* trace coverage is too low ( < 2 ) */
  masked_out,                   /* edit not allowed because region masked out */
  unknown,                      /* all other contingencies */
  status_count 
};

#define proper_edit(x) ( (x) == replace || (x) == insert || (x) == deletion )

/* classification values of the strand coverage at a given base
   position in a contigAlignment */

enum strand_types 
{ 
  no_stranded,
  forward_only,                 /* only + strands cover */
  reverse_only,                 /* only - strands cover */
  terminator_only,              /* only terminators cover */
  single_coverage,
  double_stranded,              /* both + and - */
  terminated,                   /* terminated and non-terminated */
  forward_terminated,           /* terminated and forward reads */
  reverse_terminated,           /* terminated and reverse reads */
  double_coverage,
  all_stranded,                 /* terminated AND + and - */
  strand_count 
};

#define singleStranded(x) ( (x) > no_stranded && (x) <single_coverage )
#define twoStranded(x) ( (x) > single_coverage && (x) < double_coverage )
#define doubleStranded(x) ( (x) >= single_coverage && (x) <= all_stranded ) 

/* types of consensus at a given base position (the actual consensus
   must be assessed in conjunction with the strand type) */


enum Consensus_types
{
  Weak=0,                       /* no base has a frequency higher than the others */
  Medium,                       /* there is a consensus (ie the most frequent base is > second-best base) */
  Strong,                       /* there is a consensus, including majority reads from both strands */
  Consensus_count
};

/* Type of edit - is it isolated or part of a run */
  
enum compound_types
{
  Isolated,
  Compound,
  compound_count
};

#define is_compound(x) ( (x) > 0 )

enum approx_type
{
  exact=1,
  least_upper_bound,
  greatest_lower_bound
};


extern char *status_text[status_count]; 
extern char *compound_text[compound_count];
extern char *strand_text[strand_count];
extern char *TAG_text[status_count];
extern char *Consensus_text[Consensus_count];


/* struct describing how consistent is an contigAlignment at a given position */

typedef struct 
{
  Byte base_calls[nucleotides]; /* how many calls for each nucleotide */
  Byte depth;                   /* depth of coverage (how many reads) */
  Byte base[nucleotides];       /* the bases called, sorted in decreasing order (so base[0] is the most likely base) */
  Byte calls;                   /* total number of different base calls */
  Byte consensus;               /* of consensus_types -gives the qualitative type of consensus */
  Byte status;                  /* the status (takes an value from status_types above) */
  Byte stranded;                /* the strand coverage ( takes value from strand_types ) */
  Byte compound;                /* is part of a group of edits (of compund_types) */
  Byte masked;                  /* mask out this edit */
  Byte edited;                  /* 1 if edit proposed */
  Byte best;                    /* the actual base to be called (usually base[0] unless this is an non-nucleotide )*/
  Byte clear;                   /* 1 if there is a clear consensus (ie top-voted base is not a tie) */
  Byte promoted;                /* 1 if the edit has been promoted */
  Byte count;                   /* no of Strong replacement edits proposed */
  Byte conflict;                /* 1 if there are strong calls for different bases */
  Byte compression;             /* 1 if consistent with being a compression */
}
alignStatus;


/* info about a single position for a read in an alignment */

typedef struct
{
  Byte base;                    /* the base called */
  Byte strand;                  /* which strand of strand_types*/
  Byte bad;                     /* is it bad (ie ignore it) */
  Byte edited;                  /* has it been edited ? */
  int  id;                      /* the read id (index of read in seqs)*/
  int ass_index;                /* index in contig->assinf array */
  int  pos;                     /* the base position in the original sequence */
}
alignData;

/* contigAlignment - 
   a BIG portmanteau struct containing all the info about a contig alignment 

   The important part is a huge 2-D array of alignData describing the
   multiple alignment of reads in great detail. This isreally a trade
   off between space and speed - using a contigAlignment saves having
   to recompute lots of things but at a space premium.
*/

typedef struct
{
  STORE_HANDLE handle;          /* messalloc handle */
  CAFSEQ *contig;               /* pointer to the contig */
  Array depth;                  /* array of ints giving the depth of alignment at each contig position */
  Array aligned;                /* the alignment data, a 2-D array of alignData. The array width varies with the depth. */
  int min, max;                 /* the start and stop coordinates for the contig (only used for debugging)*/
  int width;                    /* the max width of the align_data array (= max depth of coverage )*/
  Array status;                 /* array of alignStatus for each position in the alignment.  */
  Array cons;                   /* the consensus sequence */
  Array trace_quality;          /* array of doubles giving % identity between trace and consensus for each read */
  Array alignment_quality;      /* array of double giving the min trace quality at each position in contig */
  Array slot;                   /* array of int giving the "slot" assigned to each read */
}
contigAlignment;

typedef struct 
{
  int coord;      /* contig coordinate */
  int pos;        /* trace coordinate */
  float q, r, s; /* quality measures */
}
traceQuality;


/* Functions */

void write_edits( cafAssembly *CAF, FILE *fp, char *filename ); /* writes out edits to fp */

contigAlignment *getContigAlignment( cafAssembly *CAF, CAFSEQ *contig, int s1_only, float badthresh );
int getAlignedRange( ASSINF *a, int *a_s1, int *a_s2 );
void destroyContigAlignment( contigAlignment *Alignment );

void create_edit_tags_for_contig( cafAssembly *CAF, contigAlignment *Align, int do_all_edits );

int base_edit( cafAssembly *CAF, alignStatus *S, Array A, int coord );
EDITED_CAFSEQ *edit_read( cafAssembly *CAF, CAFSEQ *seq );
EDITED_CAFSEQ *edit_contig( cafAssembly *CAF, CAFSEQ *seq, Array edited_seqs, int skip, int show_case );
void write_miss_tags( cafAssembly *CAF );
void write_edited( cafAssembly *CAF, Array edited_seqs, int skip, FILE *fp );
void adjust_tags( CAFSEQ *seq, Array old2new);
void adjust_tags1( CAFSEQ *seq, Array old2new);

void fix_tags( Array tags, Array old2new);
void fix_tags1( Array tags, Array old2new);

Array adjust_quality1( CAFSEQ *seq, Array old2new, int len, int insert_quality ); /* version for when old2new is array of shorts */
Array adjust_quality( CAFSEQ *seq, Array old2new, int len, int insert_quality );
Array adjust_position1( CAFSEQ *seq, Array old2new, int len, int insert_position ); /* version for when old2new is array of shorts */
Array adjust_position( CAFSEQ *seq, Array old2new, int len, int insert_position );

void print_fitted( FILE *fp, cafAssembly *CAF, contigAlignment *Align, int coord, Array trace );
void compare_edits_to_consensus( cafAssembly *CAF, contigAlignment *Align, FILE *fp, int use_scf, int dump_edits );

void close_holes( contigAlignment *Align, cafAssembly *CAF, FILE *logfp );

Array propose_edits_for_contig( contigAlignment *Align, cafAssembly *CAF, int depth, int cluster, int use_scf, float stlouis, int use_phrap_consensus );
void check_terminators( contigAlignment *align, cafAssembly *CAF );
void classify_alignment_status( cafAssembly *CAF, Array alignment, alignStatus *S, int min_depth, int use_phrap_consensus, int phrap_consensus );
void classify_alignment_status_st_louis( cafAssembly *CAF, Array alignment, alignStatus *S, int depth, float st_louis_thresh);

char *ass_subseq( Array seqs, ASSINF *a ); 
int ass_cmp( void *A, void *B ); /* for sorting ASSINF * structs */
void alignment_status( Array alignment, alignStatus *as );

Array get_traces( cafAssembly *CAF, contigAlignment *contig_align, int coord, int window, int base );


/* Scf *get_trace_coordinates( cafAssembly *CAF, ASSINF *a, int coord, int window, int *start, int *stop, int *strand ); */

int trace_to_contig( int trace_coord, Array read_assinf, ASSINF *assinf );
void get_preconsensus( Array alignment, Array seqs );
Array  assinf_from_pads( CAFSEQ *seq, DICT *dict, char *name );
void write_compound_tags( cafAssembly *CAF, contigAlignment *Align, int max_run );
void write_stack_tags( cafAssembly *CAF, contigAlignment *Align, int max_stack );
void write_discrepancy_tags( cafAssembly *CAF, contigAlignment *Align);


void print_contig_statistics( FILE *fp, cafAssembly *CAF, contigAlignment *Align, float badthresh, int min_depth, int print_summary );
void print_contig_alignment( FILE *fp, contigAlignment *Align );

void dump_classification( contigAlignment *Align, int argc, char **argv );

void dump_data( contigAlignment *Align, FILE *fp , int discrep);

void propose_clip_alignment( contigAlignment *Align, cafAssembly *CAF, int extend, FILE *logf , int match, int mismatch, int gap, int neutral, char *debug, int original );

int make_base_quality( cafAssembly *CAF, contigAlignment *align, CAFSEQ *contig);

#ifdef CPLUSPLUS
}
#endif

#endif
