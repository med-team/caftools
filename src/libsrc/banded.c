/*  Last edited: Jun 13 17:42 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Sep 11 14:47 1996 (rmott) */
#include <stdio.h>

#include <seq_util.h>
#include <tree.h>
#include <sw.h>

#define WORDSIZE 8
#define DICTSIZE 65536

/* perform a fast banded alignment of seq1 and seq2 */

alignStruct *
bandedAlignment( SEQUENCE *seq1, SEQUENCE *seq2, int match, int mismatch, int gap, int neutral, int window )
{
  WORD_DICT *d1, *d2;
  DICT_HIST *dh;
  WORD_LIST *w2;
  int diagonal;
  int score=-1, k;
  alignStruct *as;
  
  d1 = make_dictionary_for_sequence( seq1, WORDSIZE, NULL, 0 , 0 );

  d2 = make_dictionary_for_sequence( seq2, WORDSIZE, NULL, 0 , 0 );
  w2 = create_word_list( d2, DICTSIZE );

  dh = create_dict_histogram2( d1, w2, -1 );

  score = -1;
  diagonal = -1;
  for(k=dh->start;k<=dh->stop;k++)
    {
      if ( dh->h[k] > score )
	{
	  score = dh->h[k];
	  diagonal = k;
	}
    }

  if ( gap > 0 )
    gap = -gap;
  if ( mismatch > 0 )
    mismatch = -mismatch;

  fprintf(stderr, "max diag %d at %d\n", score, diagonal );

  as = smith_waterman_alignment2(seq1->s, seq2->s, seq1->len, seq2->len, match, mismatch, gap, neutral, diagonal, window);

  free_dict_hist(dh);
  free_word_list(w2);
  free_dict(d1);
  free_dict(d2);

  return as;
}

void
printAlignment( FILE *fp, SEQUENCE *seq1, SEQUENCE *seq2, alignStruct *as, int width )
{
  print_alignment( fp, seq1->s, seq2->s, as, width, seq1->name, seq2->name );
}


void
printMSP( FILE *fp, SEQUENCE *seq1, SEQUENCE *seq2, alignStruct *as, int width )
{
  int i, len;
  double percent;
  Seg *seg;

  for(i=0;i<as->segs;i++)
    {
      seg = &as->seg[i];
      len = seg->end1-seg->start1+1;
      percent = (100.0*seg->matches)/len;

      fprintf(fp, "Ungapped %5d %5.1f  %5d %5d %-12s  %5d %5d %-12s %s\n", seg->score, percent, seg->start1+1, seg->end1+1, seq1->name, seg->start2+1, seg->end2+1, seq2->name, seq1->desc );
    }
}
