/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * $Id: base_edit.c 11027 2003-07-25 10:47:30Z dgm $
 * $Log$
 * Revision 1.4  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 *
 * Last edited: Mar  4 17:29 2002 (rmd)
 * Revision 1.3  2002/03/04 17:54:58  rmd
 * Commented out include scf.h
 *
 * Revision 1.2  2002/03/04 17:00:38  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Last edited: Oct  2 15:28 1996 (rmott)
 * Revision 1.3  1996/10/03  17:43:45  rmott
 * *** empty log message ***
 *
 * Revision 1.2  1996/02/21  11:31:48  rmott
 * CAF version
 *
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <caf.h>
#include <seq_util.h>

int propose_base_edit( cafAssembly *Ass, alignStatus *S,
		       contigAlignment *Align, int coord );

/* functions that propose edits. The information is encoded in Edit
   structures which are eventuallt witten out elsewhere */

void 
create_edit_tags_for_contig( cafAssembly *Ass, contigAlignment *Align, int do_all_edits )

/* create Edit tags for all edits/base positions in the contigAlignment Align */

{
  int j;
  int min = Align->min;
  int max = Align->max;
  Array status = Align->status;
  alignStatus *S;

  for(j=min;j<=max;j++)
    {
      S = arrayp(status,j,alignStatus);
      
      if ( proper_edit( S->status ) || S->status == unclear )
	if ( do_all_edits || S->consensus == Strong )
	  propose_base_edit( Ass, S, Align, j );
    }
}

int 
propose_base_edit( cafAssembly *Ass, alignStatus *S, contigAlignment *Align, int coord )

/* create Edit tags for the contig coordinate coord, using alignStatus structure S */

{
  int i;
  alignData *ad;
  edit_struct *edit=NULL;
  int edits = 0;
  Array seqs = Ass->seqs;
  Array A = Align->aligned, *B;
  CAFSEQ *seq, *contig;
  char /* base1, */ base2;
  /* char cbase; */

  if ( S->status != ok && S->status != too_low )
    {
/* first the edit tags for the consensus */

      contig = Align->contig;
      if ( ! contig->edit )
	contig->edit = arrayCreate(100,edit_struct);

      /* cbase = arr( Align->cons, coord, char ); */
      /* base1 = cbase; */
      base2 = c_base(S->best);
/*      if (  (tolower(cbase) != base2 ) || cbase == padding_char ) */
	{
	  S->edited = TRUE;
	  edit = arrayp( contig->edit, arrayMax(contig->edit), edit_struct );
	  edit->status = S->status;
	  edit->compound = S->compound;
	  edit->stranded = S->stranded;
	  edit->consensus = S->consensus;
	  edit->read = contig->id;
	  edit->pos = coord;
	  edit->cpos = coord;

	  if ( S->status == replace )
	    {
	      edit->New = base2;
	    }
	  else if ( S->status == deletion )
	    {
	      edit->New = ' ';
	    }
	  else if ( S->status == insert )
	    {
	      edit->New = base2;
	    }
	  else
	    {
	      edit->New = ' ';
	    }
	  edits++;

	}

/* then the tags for the reads */
	  
      B = arrp(A,coord,Array);
      for(i=0;i<arrayMax(*B);i++)
	{
	  ad = arrp( *B, i, alignData );
	  base2 = c_base(S->best);

	  if (   ad->base != '.' && (  (tolower(ad->base) != base2 ) || ad->base == padding_char ) )
	    {
	      seq = arrp( seqs, ad->id, CAFSEQ );
	      if ( ! seq->edit )
		seq->edit = arrayCreate(10,edit_struct);
	      if ( ! seq->bad )
		{
		  S->edited = TRUE;
		  edit = arrayp( seq->edit, arrayMax(seq->edit), edit_struct );
		  edit->status = S->status;
		  edit->compound = S->compound;
		  edit->stranded = S->stranded;
		  edit->consensus = S->consensus;
		  edit->read = ad->id;
		  edit->pos = ad->pos;
		  edit->cpos = coord;
		  
		  if ( is_reverse(ad->strand) )
		    {
		      /* base1 = complement_base(ad->base); */
		      base2 = complement_base(base2);
		    }
		  else 
		    {
		      /* base1 = ad->base; */
		      base2 = base2;
		    }
		  
		  if ( S->status == replace )
		    {
		      edit->New = base2;
		    }
		  else if ( S->status == deletion )
		    {
		      edit->New = ' ';
		    }
		  else if ( S->status == insert )
		    {
		      edit->New = base2;
		    }
		  else
		    {
		      edit->New = ' ';
		    }
		  edits++;
		}		
	    }
	}
    }
  return edits; 
}

void 
write_miss_tags( cafAssembly *Ass )

/* write tags flagging reads which are badly aligned but which have
   not been rejected as this would cause a contig to break */

{

  Array seqs = Ass->seqs;
  DICT *seqDict = Ass->seqDict;
  CAFSEQ *seq;
  int i, len;
  char *dna;
  char text[256];

  for(i=0;i<arrayMax(seqs);i++)
    {
      seq = arrp( seqs, i, CAFSEQ );
      if ( seq->bad )
	{
	  if ( seq->dna )
	    {
	      dna = stackText(seq->dna,0);
	      len = strlen(dna);
	    }
	  else
	    len = 1;

	  if ( seq->masked )
	    {
	      sprintf( text, " masked badly aligned read %s  similarity with consensus = %.2f",   
		      dictName(seqDict,i), seq->q  );
	      newTag(seq,Ass,"MASK",1,len,text);
	    }
	  else
	    {
	      sprintf( text, 	" badly aligned read %s  similarity with consensus = %.2f",  
		      dictName(seqDict,i), seq->q  );
	      newTag(seq,Ass,"MISS",1,len,text);
	    }
	}
    }
}


void 
write_compound_tags( cafAssembly *Ass, contigAlignment *Align, int max_run )

/* write tags flagging horozontal runs of stacks of edits */

{
  Array seqs = Ass->seqs;
  int i, j, k;
  Array status = Align->status;
  Array aligned = Align->aligned;
  alignData *ad;
  int max = Align->max;
  int min = Align->min;
  alignStatus *S;
  Array *B;
  int written=0;

  for(i=min;i<=max;i++)
    {
      S = arrp(status,i,alignStatus);
      if ( S->compound == Isolated )
	written = FALSE;
      else if ( ! written && S->compound > max_run )
	{
	  /*	      if ( S->compound > 10 )
		      printf("big stack at %d\n", i); */
	  B = arrp(aligned,i,Array);
	  for(j=0;j<arrayMax(*B);j++)
	    {
	      ad = arrp( *B, j, alignData );
	      if ( ad->id != -1 && ! ad->bad )
		{
		  if ( is_forward(ad->strand) )
		    newTag(arrp(seqs,ad->id,CAFSEQ),Ass,"AMBG",ad->pos, ad->pos+S->compound-1,"AUTOEDIT: Check this edit cluster!" );
		  else
		    {
		      k = ad->pos-S->compound-1;
		      if ( k < 1 ) k = 1;
		      newTag(arrp(seqs,ad->id,CAFSEQ),Ass,"AMBG",k, ad->pos,"AUTOEDIT: Check this edit cluster!");
		    }
		  written = TRUE;
		  break;
		}
	    }
	}
    }
}


void 
write_stack_tags( cafAssembly *Ass, contigAlignment *Align, int max_stack )

/* write tags flagging vertical stacks of edits, and potentially polymorphic edits
To be polymorphic the read must also be stolen */

{
  Array seqs = Ass->seqs;
  CAFSEQ *seq;
  int i, j;
  Array status = Align->status;
  Array aligned = Align->aligned;
  alignData *ad;
  int max = Align->max;
  int min = Align->min;
  alignStatus *S;
  Array *B = 0;
  int polymorphic, matches;
  char stolen[nucleotides];
  char native[nucleotides];
  int stolen_total, native_total;
  int base;

  for(i=min;i<=max;i++)
    {
      S = arrp(status,i,alignStatus);

      if ( S->status != ok && S->status != Strong ) /* check for polymorphic reads */
	{
	  for(j=0;j<nucleotides;j++)
	    stolen[j] = native[j] = 0; /* votes for each nucleotide in stolen and native reads */
	      
	  stolen_total = 0;
	  native_total = 0;
	  B = arrp(aligned,i,Array);
	  for(j=0;j<arrayMax(*B);j++) 
	    {
	      ad = arrp( *B, j, alignData );
	      if ( ad->id != -1 && ! ad->bad && ( base=i_base(ad->base)) != base_n ) /* reads calling an n are useless here */
		{
		  seq = arrp(seqs,ad->id,CAFSEQ);
		  if ( seq->is_stolen )
		    {
		      stolen[base]++;
		      stolen_total++;
		    }
		  else
		    {
		      native[base]++;
		      native_total++;
		    }
		}
	    }

	  polymorphic = 0;
	  if ( stolen_total * native_total > 1 ) /* depth is at least three reads */
	    {
	      matches = 0;
	      for(j=0;j<nucleotides;j++)
		matches += stolen[j]*native[j];
	      polymorphic = ! matches;
	    }

	  if ( polymorphic )
	    {
	      for(j=0;j<arrayMax(*B);j++)
		{
		  ad = arrp( *B, j, alignData );
		  if ( ad->id != -1 && ! ad->bad )
		    {
		      seq = arrp(seqs,ad->id,CAFSEQ);
		      newTag(seq,Ass,"CONF",ad->pos,ad->pos,"AUTOEDIT: Check this edit stack (conflict & possible polymorphism due to stolen read)!");
		      break;
		    }
		}
	    }
	      
	}

      if ( S->status == replace && ( S->conflict || (S->consensus == Strong && S->count >= max_stack) ) )
	{
	  polymorphic = FALSE;
	  for(j=0;j<arrayMax(*B);j++)
	    {
	      ad = arrp( *B, j, alignData );
	      if ( ad->id != -1 && ! ad->bad )
		{
		  seq = arrp(seqs,ad->id,CAFSEQ);
		  if ( S->conflict )
		    {
		      if ( polymorphic )
			newTag(seq,Ass,"CONF",ad->pos,ad->pos,"AUTOEDIT: Check this edit stack (conflict & possible polymorphism due to stolen read)!" );
		      else
			newTag(seq,Ass,"AMBG",ad->pos,ad->pos,"AUTOEDIT: Check this edit stack (conflict & possible misassembly))!");
		    }
		  else
		    newTag(seq,Ass,"AMBG",ad->pos,ad->pos,"AUTOEDIT: Check this edit stack!");
		  break;
		}
	    }
	}
    }
}


void 
write_discrepancy_tags( cafAssembly *Ass, contigAlignment *Align)
{
  
  Array seqs = Ass->seqs;
  CAFSEQ *seq;
  int i, j;
  Array status = Align->status;
  Array aligned = Align->aligned;
  alignData *ad;
  int max = Align->max;
  int min = Align->min;
  alignStatus *S;
  Array *B;

  for(i=min;i<=max;i++)
    {
      S = arrp(status,i,alignStatus);
	  
      if ( S->compression && S->consensus != Strong && S->status != ok ) /* check for compressions */
	{
	  B = arrp(aligned,i,Array);
	  for(j=0;j<arrayMax(*B);j++) 
	    {
	      ad = arrp( *B, j, alignData );
	      if ( ad->id != -1 && ! ad->bad )
		{
		  seq = arrp(seqs,ad->id,CAFSEQ);
		  newTag(seq,Ass,"CONF",ad->pos,ad->pos,"AUTOEDIT: possible compression");
		  break;
		}
	    }
	}
    }
}

int 
make_base_quality( cafAssembly *Ass, contigAlignment *align, CAFSEQ *contig)

/* create base_quality equivalent to the alignment consensus */

{
  char *dna;
  int len=0;
  int i;
  alignStatus *S;

  if ( contig->dna )
    {
      dna = stackText(contig->dna,0);
      len = strlen(dna);
      contig->base_quality = arrayReCreate(contig->base_quality,len,BQ_SIZE);

      *arrayp(contig->base_quality,len-1,BQ_SIZE) = 0;
      for(i=0;i<len;i++)
	*arrp(contig->base_quality,i,BQ_SIZE) = 0;

      for(i=align->min;i<=align->max;i++)
	{
	  S = arrp(align->status,i,alignStatus);
	  *arrayp(contig->base_quality,i-1,BQ_SIZE) = S->consensus;
	}
    }
  return len;
}
