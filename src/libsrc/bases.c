/*  Last edited: Jun 13 18:16 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

#include <ctype.h>

#include <bases.h>


int 
i_base( char base )
{
/* convert a character code for a base into an int as defined in base_type */
  static int ibase[256];
  static int init;

  if ( ! init )
    {
      int i;
      for(i=0;i<256;i++)
	ibase[i] = base_o;

      ibase['a'] = base_a;
      ibase['c'] = base_c;
      ibase['g'] = base_g;
      ibase['t'] = base_t;
      ibase['u'] = base_t;
      ibase['n'] = base_n;
      ibase['x'] = base_n; /* x maps to n */

      ibase['*'] = base_n; /* reverted */
      ibase['-'] = base_i;
      ibase['.'] = -1;

      init = 1;
    }

  return ibase[tolower((int) base)];

}

char 
c_base( int base ) /* inverse (more or less) of i_base */

{
  static char cbase[256];
  static int init=0;

  if ( ! init )
    {
      int i;
      init = 1;
      for(i=0;i<256;i++)
	cbase[i] = '.';

      cbase[base_a] = 'a';
      cbase[base_c] = 'c';
      cbase[base_g] = 'g';
      cbase[base_t] = 't';
      cbase[base_n] = 'n';
      cbase[base_i] = padding_char;
    }

  if ( 256 > base && base >=0 )
    return cbase[base];
  else
    return ' ';
}



