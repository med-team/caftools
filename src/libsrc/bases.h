/*  Last edited: Jan  4 20:48 1997 (badger) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* Definitions for nucleotides */

#ifndef _BASES_H_
#define _BASES_H_ 

#ifdef CPLUSPLUS
extern "C" { 
#endif

/* Definition of the padding-character in CAF */

#define padding_char '-'

enum base_types /* just defines a, c, g, t as 0-3, for indexing purposes. */
{
  base_a, base_c, base_g, base_t, base_n, base_i, base_o, nucleotides, anybase
};

#define for_each_base(n) for(n=base_a;n<=base_t;n++)     /* iterate over a c g t */
#define for_any_base(n)  for(n=base_a;n<nucleotides;n++) /* iterate over a c g t n i o */
#define proper_base(n)  ( (n) >= base_a && (n) <= base_t ) /* test if base is a c g t */


/* interconvert between char and int representations of nucleotides */

char c_base( int base ); /* int to char */
int i_base( char base ); /* char to int */


#ifdef CPLUSPLUS
}
#endif

#endif
