/*  Last edited: Mar  4 15:01 2002 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  File: caf.h
 *  Author: Richard Durbin (rd@sanger.ac.uk), Richard Mott 
 *  Copyright (C) R Durbin, 1995
 *-------------------------------------------------------------------
 * SCCS: %W% %G%
 * Description:
 * Exported functions:
 * HISTORY:
 * Last edited: Feb 29 11:04 1996 (rmott)
 * Created: Tue Feb 28 23:36:31 1995 (rd)
 *-------------------------------------------------------------------
 */

/* $Id: caf.h 15227 2005-06-21 15:45:18Z rmd $ */

/* #define clockid_t int  disgusting hack for gcc */

/* CAF.H

   Contains the definitions for the C CAFTOOLS

   Required by auto-edit, pad/depad, caf2phrap etc etc 

   If included into an application ie with main(argc,argv), then #define MAIN. 

   Look in caf_types.h for definitions of enumerated types used to
   represent some cafseq attributes

   The most important defs are for the CAFSEQ and cafAssembly objects

   A CAF file is represented as a cafAssembly, which is essentially an
   array of CAFSEQ objects plus three dictionaries, and some other
   assorted bits which are not generally used.

   If you change the CAF model and you want the changes to be parsed
   then you must edit the CAFSEQ object definition. (Unknown model
   terms are not discarded, and do not raise an exception, but are
   held in a stack of unparsed data, and are printed out when the CAF
   is written, so no information is lost. Model terms which are known
   but which do not parse correctly will case the program to abort.).


*/


#ifndef _CAF_H

#define _CAF_H

#define I_SIZE short /* assume reads < 32k */
#define BQ_SIZE signed char /* size of quality measures */
#define BP_SIZE int /* size of quality measures */


#ifndef SOLARIS_4
#include <regular.h>        /* ACEDB defs */
#endif
#include <array.h>          /* ACEDB libfree array package  */
#include <dict.h>           /* ACEDB libfree dictionary package  */
/* #include <myscf.h>          Staden SCF with my extensions */
#include <sw.h>             /* Smith-waterman codes */
#include <bases.h>          /* integer ids for nucleotides */
#include <caf_types.h>      /* CAF enumerated types */

#define Byte signed char        /* to cope with SGI IRIX */


#ifdef CPLUSPLUS
extern "C" { 
#endif

extern char *sequence_text[sequence_types];

/* First definitions of structs needed by CAFSEQ */

/* Tag-type objects (ie Tag, Clone_vec, Seq_Vec, Clipping) are held in arrays of TAG structs: */

typedef struct {
  int x1, x2 ;                  /* start and stop coords in sequence */
  int type ;			/* Tag type. This is the index into tagTypeDict */
  char *text ;                  /* The tag text */
} TAG ;

/* Note-type objects (i.e. notes attached to reads and contigs) are held in arrays of NOTE structures. */

typedef struct {
    int  type;
    int  ctime_top;	/* Placeholder for 64bit time_t */
    int  ctime;		/* creation date - a time_t. */
    int  mtime_top;	/* Placeholder for 64bit time_t */
    int  mtime;		/* modification date - a time_t. */
    char *text;
} NOTE ;

/* Assembled_from and Align_to_SCF lines are held as arrays of ASSINF structs: */

typedef struct {
  int seq ;			/* index into seqDict */
  int s1, s2 ;			/* start, stop in sequence */
  int r1, r2 ;			/* start, stop in read */
  char *s;                      /* pointer to actual sequence (not used in readAceAssembly()) */
} ASSINF ;

/* 

   Contig group definition. This is used for both order groups in
   assembly objects AND order contigs in group objects
 */

typedef struct grouping {
  int seq;                      /* Sequence id of parent (assembly or group) */
  double position;              /* coordinate within the group */
} 
GROUP_ORDER;

/* a proposed edit (was in auto_edit.h) */

typedef struct 
{
  Byte status, compound, stranded, consensus;  /* cf alignStatus */
  int read;       /* the id of the read affected by the edit */
  int pos;        /* the position (in the reads original coords) affected */
  int cpos;       /* the position in the contig */
  char New;       /* the replacement character */
  char old;       /* the original */
  Byte done;      /* has the edit been done ? */
}
edit_struct;

/* CAFSEQ definition 

Those fields marked with !!! are important, representing external CAF
attributes. The other fields are internal. Probably the internal
fields should be spun off.

*/
typedef struct {
  int id;              /* Id of this sequence ( = index in seqs array, and in seqDict dictionary ) */
  int type;            /* !!! of sequence_type */
  int process_status;  /* !!! of process_type.  Process Status in ASP */
  int pad_state;       /* !!! padded vs unpadded */
  int is_circular;     /* !!! should be interpreted as a circular dna (not used currently) */
  int is_terminator;   /* !!! is a terminator read */
  int is_long;         /* !!! is a long read */                
  int is_stolen;       /* !!! is a stolen read (see seq->stolen_from ) */
  int bad;             /* flag set if the read is bad (ie disagrees with consensus). Used in sanitise_caf() */
  int masked ;         /* flag set if the read is to be masked out */
  int dye;             /* !!! primer or terminator (of dye_type)*/
  int primer;          /* !!! universal or custom (or primer type) */
  int strand;          /* !!! forward or reverse of strand_type */
  int template_id;     /* !!! id of template (ie m13 clone name) . Indexes templateDict */
  int insert_size1;
  int insert_size2;    /* !!! Range of insert size values (both values == 0 indicates not known) */
  int clone_id;        /* index into cloneDict */
  int cloning_vector;  /* index into vectorDict */
  int sequencing_vector; /* index into vectorDict */
  float q;             /* percent similarity with consensus (auto-edit) */
  int len;             /* length of the sequence. Computed from DNA */
  Stack dna ;	       /* !!! of char, null terminated. Holds the DNA object */
  Array tags ;	       /* !!! Array of TAG, holds Tag attributes */
  Array notes ;	       /* !!! Array of NOTE, holds Note attributes */
  Array assinf ;       /* !!! of ASSINF - Holds Assembled_from or Align_to_SCF */
  Array svector ;      /* !!! of TAG. Holds sequencing vector */
  Array cvector ;      /* !!! of  TAG. Holds cloning vector */
  Array golden_path;   /* !!! of TAG. Holds Phrap Golden Path */
  Array clipping;      /* !!! of TAG. Holds clipping info */
  Array base_quality;  /* !!! Phred base qualities , of signed char*/
  Array base_position; /* !!! Phred trace positions , of int */
  char *SCF_File;      /* !!! the scf-file name (for reads only )*/
  char *custom_primer; /* !!! the name of the custom primer (or null) */
  char *stolen_from;   /* !!! name of clone from which read is stolen */
  char *failed_because; /* !!! Why failed ? */
  char *process_reason; /* !!! process state reason ? */
  /*char *ligation_no;*/   /* !!! The ligation number (library name/id) */
  int ligation_no;     /* index into ligationDict */
  Stack other ;	       /* !!! all other lines, just for carrying along */
  Array edit;          /* of edit_struct (auto-edit only) */
  Array new_dna;       /* the edited sequence  (auto-edit only) */
  /* Array new_pos; not used anywhere*/ /* int array giving  modified assinf after editing  (auto-edit only) */
  Array order;         /* !!! Array of GROUP_ORDER, only valid for assembly and group objects */
  int staden_id;       /* !!! internal staden identifier */

} CAFSEQ ;


/* cafAssembly - the whole CAF assembly */

typedef struct
{
  /* Memory handle for this
  assembly. All data associated with this assembley should be
  registered on this handle so it can be deleted easily and safely */

  STORE_HANDLE handle;      

  Array seqs;               /* Array of CAFSEQ contining the sequence objects */
  DICT *seqDict;            /* Names of the sequences. The index of a seq is the same on seqs and seqDict */
  DICT *tagTypeDict;        /* Tag types */
  DICT *noteTypeDict;       /* Note types */
  DICT *templateDict;       /* Template names */
  DICT *ligationDict;       /* Ligation numbers */
  DICT *cloneDict;          /* Clone names */
  DICT *vectorDict;         /* Vector names */
  int from;                 /* region of interest - not needed except when debugging auto-edit */
  int to;
  char *filename;           /* Name of the file from which  the CAF was read */
  Array digests;            /* of type EQUUS_DIGEST - not used currently */
}
cafAssembly;

#include "auto_edit.h"       /* AUTO-EDIT specific definitions. Have to
			       include it here because it needs CAFSEQ */

/* struct for holding clipping info. Used in ne_clip and nd_clip */

typedef struct
{
  int clip_left;
  int clip_right;
  int start;
  int stop;
}
CLIP;


/*********************************************************************
 Functions 
*********************************************************************/

/*********************************************************************
 Reading CAF files 


 readCafAssembly() reads in an ace file into an assembly struct.
 fp Should be a FILE pointer opened for reading.  
 filename is the name of the file (can be null).  
 CAF is either NULL or points to an existing cafAssembly. 
 [ In the former case a new assembly is
 created, in the latter it is appended to the existing CAF] 

*********************************************************************/

  cafAssembly *readCafAssembly (FILE *fp, char *filename, cafAssembly *CAF ); 

/* convenience function which looks at the command-line for the argument -caf <filename> */

  cafAssembly *readCAF( int argc, char ** argv, char *filename ); 

/*********************************************************************
 Functions for reading and writing cafBinaries. THese are currently broken 
*********************************************************************/

cafAssembly *readCafBinary( FILE *fil, char *filename ); /* binary read */
int writeCafBinary( cafAssembly *CAF, FILE *fil, char *header ); /* binary write */

/*********************************************************************
 Writing CAF files 
*********************************************************************/

/* write A CAF to the opened FILE pointer fil, putting header as a comment */

void writeCafAssembly (cafAssembly *A, FILE *fil, char *header ); 

/* Convenience function - checks the command-line for the argument 
   -out <filename> and writes the CAF to filename */

void writeCAF( cafAssembly *CAF, char *header, int argc, char **argv );

/* Write a single CAFSEQ object (used internally by writeCafAssembly(), but also by other software )*/

int writeCafSeq( cafAssembly *A, FILE *fil, CAFSEQ *seq );

/* Write base quality data */

int write_base_quality( Array base_quality, FILE *fil );

/* Write base position data */

int write_base_position( Array base_position, FILE *fil );
/* int make_base_position( cafAssembly *CAF, contigAlignment *align, CAFSEQ *contig); */

/* free a CAF */

void cafDestroy( cafAssembly *CAF );

/*********************************************************************
  Modifications of assemblies
*********************************************************************/

/* Add new tags to an existing assembly */

TAG *newTag( CAFSEQ *seq, cafAssembly *CAF, char *type, int x1, int x2, char *text );
TAG *newSvec( CAFSEQ *seq, cafAssembly *CAF, char *type, int x1, int x2, char *text );
TAG *newCvec( CAFSEQ *seq, cafAssembly *CAF, char *type, int x1, int x2, char *text );
TAG *newClipping( CAFSEQ *seq, cafAssembly *Ass, char *type, int x1, int x2, char *text );

/* reverse-complement  a contig and its associated reads */

CAFSEQ * reverseContig( CAFSEQ *seq, cafAssembly *CAF );

void reverse_tags( Array tags, int len );


/* Functions for splicing */

CAFSEQ *splice_vector( cafAssembly *CAF );
CAFSEQ *splice_assembly( cafAssembly *CAF, CAFSEQ *assembly );


/* Functions for padding/de-padding. Applications should only call
   requireUnpadded() or requirePadded(),  which are safe, or or (exceptionally) depad_isolated_reading() 

   requireUnpadded(CAF) will check the pad state of CAF and depad it
   if required. If it is already padded it prints a message to stderr
   and returns. requirePadded(CAF) does the opposite. Both functions
   return the CAF in the correct stae, overwriting the original. If
   the assembly is in a mixture of states the functions exit the
   program with a warning and non-zero exit status 

   depad_isolated_reading() should be used with care only on reads
   which are not assembled into contigs but which are padded eg
   because they have been stolen

   count_pad_states() counts up how amny sequences are in each pad state. Used
   for consistency checking
*/


int requireUnpadded( cafAssembly *CAF ); 
int requirePadded( cafAssembly *CAF );
int depad_isolated_reading( CAFSEQ *seq, cafAssembly *CAF );
int *count_pad_states( cafAssembly *CAF );

/* Print a summary of a caf assembly */

void summariseCAF( cafAssembly *CAF );

/* Clipping-related stuff for nd_clip and ne_clip (in clipping.c) */

int do_clip_assembly( cafAssembly *CAF, Array ids, int cvector, int svector, int fail, int warn, FILE *logfp );

char *mask_vector( CAFSEQ *seq, char mask );
void maskVector( cafAssembly *CAF, char mask );

  /* find left and right quality clip points by subtracting threshold
     from quality array and then finding maximal scoring segment */
int maxSegClip( Array base_quality, int threshold, int *left, int *right );




#ifdef CPLUSPLUS
}
#endif


#endif

/******************* end of file *****************/
