/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 * its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * Last edited: Sep 13 10:58 1996 (rmott)
 * $Id: caf_check.c 11027 2003-07-25 10:47:30Z dgm $
 * $Log$
 * Revision 1.4  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 * Revision 1.3  2002/03/04 17:00:38  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.2  2001/12/12 18:17:15  rmd
 * Added read name to Align_to_SCF error output lines.
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 */

/* Functions to check if a CAF assembly is consistent */

#include <stdio.h>

#include <caf.h>
#include <caf_check.h>

int verbose;

int out_of_range( int n, int start, int stop );
int check_padded( cafAssembly *CAF, int ignore, int Verbose );
int tags_out_of_range( cafAssembly *CAF, CAFSEQ *seq );
int tags_out_of_range2( char *text, DICT *dict, int max, Array tags );
int different_lengths( ASSINF *a);
int check_unpadded( cafAssembly *CAF, int ignore, int Verbose );
int overlaps(ASSINF *a, ASSINF *a0);

int 
checkCAF( cafAssembly *CAF, int Verbose )
{
/* checks if an assembly is consistent.

   Return value is the state
   of the assembly, 

   or 0 if unknown , 

   or N < 0 ,
   where n is the number of
   errors encountered

   If The verbose flag is TRUE then errors are reported to stderr

*/
  int *pad_states = count_pad_states( CAF );
  int n = arrayMax(CAF->seqs);
  int errors = 0;
  int state = 0;

  verbose = Verbose;

  if ( pad_states[padded_state] >= 0.9*n ) /* padded state */
    {
      state = padded_state;
      errors = check_padded( CAF, FALSE, Verbose );
    }
  else if ( pad_states[unpadded_state] >= 0.9*n ) /* unpadded state */
    {
      state = unpadded_state;
      errors = check_unpadded( CAF, FALSE, Verbose );
    }
  else if ( pad_states[unknown_state] == n ) /* unknown */
    {
      state = unknown_state;
      errors = check_padded( CAF, TRUE, Verbose ); /* try padded */
      if ( ! errors )
	state = padded_state;
      else
	{
	  errors = check_unpadded( CAF, TRUE, Verbose ); /* try unpadded */
	  if ( ! errors )
	    state = unpadded_state;
	}
    }
  else
    errors = 1;

  if ( errors > 0 )
    return -errors;
  else
    return state;
}

int*
count_pad_states( cafAssembly *CAF )
/* count the numbers of sequences in different states */
{
  int i;
  static int pad_states[pad_types];
  CAFSEQ *seq;

  for(i=0;i<pad_types;i++)
    pad_states[i] = 0;

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp( CAF->seqs, i, CAFSEQ );
      pad_states[seq->pad_state]++;  /* need to count how many sequences are in each pad state */
    }

  return pad_states;
}

int*
count_dye_states( cafAssembly *CAF )
/* count the numbers of sequences in different states */
{
  int i;
  static int dye_states[dye_types];
  CAFSEQ *seq;

  for(i=0;i<dye_types;i++)
    dye_states[i] = 0;

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp( CAF->seqs, i, CAFSEQ );
      dye_states[seq->dye]++;  /* need to count how many sequences are in each pad state */

    }

  return dye_states;
}

int check_unpadded( cafAssembly *CAF, int ignore, int Verbose )
{
/*
   Make the following Checks:

   1. No padding characters in the DNA
   2. Assembled_from lines are same length
   3. Assembled_from lines for same read so not overlap
   4. REads have a single Align_to_SCF
*/

  int i, j;
  CAFSEQ *seq, *contig;
  int errors = 0;
  char *cname, *rname;
  ASSINF *a, *a0;
  char *dna;

  verbose = Verbose;

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp(CAF->seqs,i,CAFSEQ);
      if ( seq->dna )
	{
	  seq->len = strlen(stackText(seq->dna,0));
	  dna = stackText(seq->dna,0);
	  j = 0;
	  while ( *dna )
	    j += ( *dna++ == padding_char );  /* check for padding characters */
	  if ( verbose && j > 0 )
	    fprintf(stderr, "Sequence %s contains pads\n", dictName(CAF->seqDict,i));
	  errors += j;
	}
      else
	{
	  seq->len = 0;
	  if ( is_read == seq->type || is_contig == seq->type )
	    {
	      if ( Verbose )
		fprintf(stderr, "No DNA for %s\n", dictName(CAF->seqDict,seq->id));
	      errors++;
	    }
	}
    }

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      contig = arrp(CAF->seqs,i,CAFSEQ);
      cname = dictName(CAF->seqDict,i);
      if ( ignore || contig->pad_state == unpadded_state  )
	{
	  if ( contig->type == is_contig )
	    {
	      if ( Verbose )
		fprintf(stderr, "Contig %-20s length %d\n", cname, contig->len );
	      errors += ( contig->len == 0 );

	      if ( contig->assinf)
		{
		  for(j=0;j<arrayMax(contig->assinf);j++)
		    {
		      a = arrp(contig->assinf,j,ASSINF);
		      seq = arrp(CAF->seqs,a->seq,CAFSEQ);
		      rname = dictName(CAF->seqDict,a->seq);

		      if ( different_lengths( a ) )
			{
			  errors++;
			  if ( verbose )
			    fprintf(stderr, "Assembled_from %-20s %5d %5d %5d %5d  different lengths\n", rname, a->s1, a->s2, a->r1, a->r2 );
			}

		      if ( out_of_range( a->s1, 0, contig->len ) + out_of_range( a->s2, 0, contig->len ) +
			  out_of_range( a->r1, 0, a->r2 ) + out_of_range( a->r2, a->r1, seq->len ))
			{
			  errors ++;
			  if ( Verbose )
			    fprintf(stderr, "Assembled_from %-20s %5d %5d (%5d)   %5d %5d (%5d)  range error\n", rname, a->s1, a->s2, contig->len, a->r1, a->r2, seq->len);
			}

		      errors += tags_out_of_range( CAF, contig );

		      if ( j > 0 )
			{
			  a0 = arrp(contig->assinf,j-1,ASSINF);
			  if ( a0->seq == a->seq )
			    {
			      if ( overlaps( a, a0 ) )
				{
				  errors++;
				  if ( verbose )
				    fprintf(stderr, "Assembled_from %-20s %5d %5d %5d %5d  overlaps\n", rname, a->s1, a->s2, a->r1, a->r2 );
				}
			    }
			}
		    }
		  if ( ! errors )
		    {
		      contig->pad_state = unpadded_state;
		      if ( verbose )
			fprintf(stderr,"%s: unpadded, consistent\n",dictName(CAF->seqDict,contig->id));
		      
		    }
		  else
		    {
		      errors++;
		      if ( verbose )
			{
			  if ( ! contig->assinf || ! arrayMax(contig->assinf) )
			    fprintf(stderr,"%s: No Assembled_from info\n", dictName(CAF->seqDict,contig->id));
			  fprintf(stderr,"%s: Inconsistent\n", dictName(CAF->seqDict,contig->id));
			}
		    }
		}
	    }
	}
    }

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp(CAF->seqs,i,CAFSEQ);
      rname = dictName(CAF->seqDict,seq->id);
      if ( seq->type == is_read )
	{
	  if ( ignore || seq->pad_state == unpadded_state )
	    {
/*	      if ( Verbose )
		fprintf(stderr, "Read %-20s length %d\n", rname, seq->len ); */
	      errors += ( seq->len == 0 );
	      
	      if ( seq->assinf)
		{
		  a0 = NULL;
		  for(j=0;j<arrayMax(seq->assinf);j++)
		    {
		      a = arrp(seq->assinf,j,ASSINF);

		      if ( different_lengths( a ) )
			{
			  errors++;
			  if ( verbose )
			    fprintf(stderr, "Align_toSCF %5d %5d %5d %5d  different lengths\n", a->s1, a->s2, a->r1, a->r2 );

			}

		      if ( out_of_range( a->s1, 1, a->s2 ) + out_of_range( a->s2, a->s1, seq->len ) +
			  out_of_range( a->r1, 1, a->r2 ) + out_of_range( a->r2, a->r1, a->r2 ) )
			{
			  errors++;
			  if ( verbose )
			    fprintf(stderr, "Align_to_SCF %5d %5d (%5d) %5d %5d  out of range\n", a->s1, a->s2, seq->len, a->r1, a->r2 );
			}

		      if ( a0 )
			{
			  if ( a0->seq == a->seq )
			    {
			      errors++;
			      if ( verbose )
				fprintf(stderr,"Multiple Align_to_SCF entries for Read %s\n", rname );
			    }
			}
		      a0 = a;
		    }
		  if ( ! seq->SCF_File )
		    {
		      errors++;
		      if ( verbose )
			fprintf( stderr, "Read %s has Align_to_SCF but no SCF_File\n", rname);
		    }
	      
		}
	      else if ( seq->SCF_File )
		{
		  errors++;
		  if ( verbose )
		    fprintf(stderr, "Read %s has SCF file %s but no Align_to_SCF\n", rname, seq->SCF_File );
		}
	    }
	  else if ( seq->pad_state == padded_state )
	    {
	      errors++;
	      if ( Verbose )
		fprintf(stderr, "Read %s claims to be padded\n", rname );
	    }
	}
      if ( ! errors )
	seq->pad_state = padded_state;
    }

  if ( Verbose) {
    if ( ! errors )
      fprintf(stderr, "Assembly is unpadded and consistent\n");
    else
      fprintf(stderr, "Assembly is unpadded and inconsistent\n");
  }

  return errors;
}


int 
check_padded( cafAssembly *CAF, int ignore, int Verbose )
{
/* check if an assembly is padded

   Checks the following:

   1. All pad_states are set to padded_state (unless ignore==TRUE)
   2. All sequences have DNA
   3. All Assembled_from data are within range defined by sequence lengths
   4. There is only one Assembled_from line for each read in a contig
   5. Align_to_SCF lines are in range and are consistent. (NB: does not actually open SCF file to check ! )
   4. All tag-like data are in range

   Returns the number of errors found.
   On sucess, the pad_state of each is set to padded_state, so this can be used to fix trivial mislabellings.
*/

  int i, j;
  CAFSEQ *seq, *contig;
  int errors = 0;
  char *cname, *rname;
  ASSINF *a, *a0;

  verbose = Verbose;

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp(CAF->seqs,i,CAFSEQ);
      if ( seq->dna )
	seq->len = strlen(stackText(seq->dna,0));
      else
	{
	  seq->len = 0;
	  if ( is_read == seq->type || is_contig == seq->type )
	    {
	      if ( Verbose )
		fprintf(stderr, "No DNA for %s\n", dictName(CAF->seqDict,seq->id));
	      errors++;
	    }
	}
    }

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      contig = arrp(CAF->seqs,i,CAFSEQ);
      cname = dictName(CAF->seqDict,i);
      if ( ignore || contig->pad_state == padded_state  )
	{
	  if ( contig->type == is_contig )
	    {
	      if ( Verbose )
		fprintf(stderr, "Contig %-20s length %d\n", cname, contig->len );
	      errors += ( contig->len == 0 );

	      if ( contig->assinf)
		{
		  for(j=0;j<arrayMax(contig->assinf);j++)
		    {
		      a = arrp(contig->assinf,j,ASSINF);
		      seq = arrp(CAF->seqs,a->seq,CAFSEQ);
		      rname = dictName(CAF->seqDict,a->seq);

		      if ( different_lengths( a ) )
			{
			  errors++;
			  if ( verbose )
			    fprintf(stderr, "Assembled_from %-20s %5d %5d %5d %5d  different lengths\n", rname, a->s1, a->s2, a->r1, a->r2 );
			}

		      if ( out_of_range( a->s1, 0, contig->len ) + out_of_range( a->s2, 0, contig->len ) +
			  out_of_range( a->r1, 0, a->r2 ) + out_of_range( a->r2, a->r1, seq->len ))
			{
			  errors ++;
			  if ( Verbose )
			    fprintf(stderr, "Assembled_from %-20s %5d %5d (%5d)   %5d %5d (%5d)  range error\n", rname, a->s1, a->s2, contig->len, a->r1, a->r2, seq->len);
			}
		      errors += tags_out_of_range( CAF, contig );
		      if ( j > 0 )
			{
			  a0 = arrp(contig->assinf,j-1,ASSINF);
			  if ( a0->seq == a->seq )
			    {
			      errors++;
			      if ( verbose )
				fprintf(stderr,"Multiple Assembled_from entries for Read %s\n", rname );
			    }
			}
		    }
		  if ( ! errors )
		    {
		      contig->pad_state = padded_state;
		      if ( verbose )
			fprintf(stderr,"%s: consistent, with %d reads\n", dictName(CAF->seqDict,contig->id), arrayMax(contig->assinf));
		    }
		}
	      else
		{
		  errors++;
		  if ( verbose )
		    {
		      if ( ! contig->assinf || ! arrayMax(contig->assinf) )
			fprintf(stderr,"%s: No Assembled_from info\n", dictName(CAF->seqDict,contig->id));
		      fprintf(stderr,"%s: Inconsistent\n", dictName(CAF->seqDict,contig->id));
		    }
		}
	    }
	}
      else if ( is_contig == contig->type )
	{
	  errors++;
	  if ( Verbose )
	    fprintf(stderr, "Contig %s claims to be unpadded\n", cname );
	}
    }

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp(CAF->seqs,i,CAFSEQ);
      rname = dictName(CAF->seqDict,seq->id);
      if ( seq->type == is_read )
	{
	  if ( ignore || seq->pad_state == padded_state )
	    {
/*	      if ( Verbose )
		fprintf(stderr, "Read %-20s length %d\n", rname, seq->len ); */
	      errors += ( seq->len == 0 );
	      
	      if ( seq->assinf)
		{
		  a0 = NULL;
		  for(j=0;j<arrayMax(seq->assinf);j++)
		    {
		      a = arrp(seq->assinf,j,ASSINF);

		      if ( different_lengths( a ) )
			{
			  errors++;
			  if ( verbose )
			    fprintf(stderr, "Align_to_SCF %-20s %5d %5d %5d %5d  different lengths\n", rname, a->s1, a->s2, a->r1, a->r2 );

			}

		      if ( out_of_range( a->s1, 1, a->s2 ) + out_of_range( a->s2, a->s1, seq->len ) +
			  out_of_range( a->r1, 1, a->r2 ) + out_of_range( a->r2, a->r1, a->r2 ) )
			{
			  errors++;
			  if ( verbose )
			    fprintf(stderr, "Align_to_SCF %-20s %5d %5d (%5d) %5d %5d  out of range\n", rname, a->s1, a->s2, seq->len, a->r1, a->r2 );
			}

		      if ( a0 )
			{
			  if ( out_of_range(a->r1,a0->r2,a->r1) )
			    {
			      errors++;
			      fprintf(stderr, "Align_to_SCF %-20s %5d %5d (%5d) %5d %5d  out of order\n", rname, a->s1, a->s2, a0->r2, a->r1, a->r2 );
			    }
			}
		      a0 = a;
		    }
		  if ( ! seq->SCF_File )
		    {
		      errors++;
		      if ( verbose )
			fprintf( stderr, "Read %s has Align_to_SCF but no SCF_File\n", rname);
		    }
		}
	      else if ( seq->SCF_File )
		{
		  errors++;
		  if ( verbose )
		    fprintf(stderr, "Read %s has SCF file %s but no Align_to_SCF\n", rname, seq->SCF_File );
		}
	    }
	  else
	    {
	      errors++;
	      if ( Verbose )
		fprintf(stderr, "Read %s claims to be unpadded\n", rname );
	    }
	}
      if ( ! errors )
	seq->pad_state = padded_state;
    }

  if ( Verbose) {
    if ( ! errors )
      fprintf(stderr, "Assembly is padded and consistent\n");
    else
      fprintf(stderr, "Assembly is padded and inconsistent\n");
  }

  return errors;
}
			
int out_of_range( int n, int start, int stop )
{
  return ( n < start || n > stop );
}

int tags_out_of_range( cafAssembly *CAF, CAFSEQ *seq )
{
  int errors=0;
  errors += tags_out_of_range2( "Seq_vec", CAF->tagTypeDict, seq->len, seq->svector );
  errors += tags_out_of_range2( "Clone_vec", CAF->tagTypeDict, seq->len, seq->cvector );
  errors += tags_out_of_range2( "Tag", CAF->tagTypeDict, seq->len, seq->tags );
  errors += tags_out_of_range2( "Clipping", CAF->tagTypeDict, seq->len, seq->clipping);
  errors += tags_out_of_range2( "GoldenPath", CAF->tagTypeDict, seq->len, seq->golden_path);

  return errors;
}

int tags_out_of_range2( char *text, DICT *dict, int max, Array tags )
{
  int errors = 0;
  int i;
  TAG *tag;

  if ( tags )
    for(i=0;i<arrayMax(tags);i++)
      {
	tag = arrp(tags,i,TAG);
	if ( out_of_range( tag->x1, 1, max ) || out_of_range( tag->x2, 1, max ) )
	  {
	    errors ++;
	    if ( verbose )
	      fprintf(stderr, "%-10s %-10s %5d %5d (%5d) range error\n", text, dictName(dict,tag->type), tag->x1, tag->x2, max );
	  }
      }
  return errors;
}

    
int different_lengths( ASSINF *a)
{
  return ( abs(a->s1-a->s2) - abs(a->r1-a->r2) );
}

int overlaps( ASSINF *a, ASSINF *a0 )
{
  return ( ( a->r1 <= a0->r2)  || ( a->s1 < a->s2 && a->s1 <= a0->s2 ) || (  a->s1 > a->s2 && a->s1 >= a0->s2) );
}

void
summariseCAF( cafAssembly *CAF )
{
  int i, j;
  ASSINF *a, *a0;
  CAFSEQ *seq, *contig;
  int contigs=0;
  int reads=0;
  int with_dna=0;
  int len;
  int sequences=arrayMax(CAF->seqs);
  int *pad_states=count_pad_states(CAF);
  int *dye_states=count_dye_states(CAF);

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp(CAF->seqs,i,CAFSEQ);
      with_dna += ( seq->dna != NULL );
      contigs += ( seq->type == is_contig );
      reads += ( seq->type == is_read );
    }

  printf("CAF %s\n", CAF->filename );
  printf("%d Sequence objects, %d with DNA\n", sequences, with_dna);
  for(i=0;i<pad_types;i++)
    printf("%d %s\n", pad_states[i], pad_strings[i]);
  for(i=0;i<dye_types;i++)
    printf("%d %s\n", dye_states[i], dye_strings[i]);

  printf("%d contigs %d reads\n", contigs, reads );
  
  contigs = 0;
  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      contig = arrp(CAF->seqs,i,CAFSEQ);
      if ( contig->type==is_contig)
	{
	  contigs++;
	  if ( contig->dna )
	    len = strlen(stackText(contig->dna,0));
	  else
	    len = 0;
	  
	  if ( contig->assinf )
	    {
	      if (arrayMax(contig->assinf) > 0)
		{
		  
		  a0 = arrp(contig->assinf, 0, ASSINF);
		  reads = 1;
		  
		  for(j = 1; j < arrayMax(contig->assinf); j++)
		    {
		      a = arrp(contig->assinf,j,ASSINF);
		      
		      if (a->seq != a0->seq) reads++;
		      
		      a0 = a;
		    }
		}
	    }

	  printf("%3d %-10s length %6d reads %5d\n", contigs, dictName(CAF->seqDict,i), len, reads );
	}
    }
}

int
check_holes( cafAssembly *CAF, int verbose )
{
/* checks for holes and abbuttments in the contigs */

  CAFSEQ *contig;
  int i, j, k, min, s1, s2;
  Array coverage=NULL;
  Array interior=NULL;
  int errors=0;
  ASSINF *a;
  char *dna;

  for(k=0;k<arrayMax(CAF->seqs);k++)
    {
      contig = arrp(CAF->seqs,k,CAFSEQ);
      if ( is_contig == contig->type )
	{
	  if ( contig->assinf )
	    {
	      coverage = arrayReCreate(coverage,30000,int);
	      interior = arrayReCreate(interior,30000,int);
	      min = 1000000;
	      dna = stackText(contig->dna,0)-1;
	      for(i=0;i<arrayMax(contig->assinf);i++)
		{
		  a = arrp(contig->assinf,i,ASSINF);
		  if ( a->s1 < a->s2 )
		    {
		      s1 = a->s1;
		      s2 = a->s2;
		    }
		  else
		    {
		      s1 = a->s2;
		      s2 = a->s1;
		    }

		  for(j=s1;j<=s2;j++)
		    (*arrayp(coverage,j,int))++;

		  for(j=s1+1;j<s2;j++)
		    (*arrayp(interior,j,int))++;

		  if ( min > s1 )
		    min = s1;
		}

	      j = min;
	      while(j<arrayMax(coverage))
		{
		  i = j;
		  while( j < arrayMax(coverage) && array(coverage,j,int) == 0 && dna[j] != padding_char)
		    j++;

		  if ( j > i )
		    {
		      if ( verbose )
			fprintf(stderr,"Hole in %-15s  %6d %6d   len %5d\n", dictName(CAF->seqDict,contig->id), i, j-1, j-i );
		      errors++;
		    }
		  j++;
		}

	      j = min+1;
	      while(j<arrayMax(interior)-1)
		{
		  if ( array(interior,j,int) == 0 && array(coverage,j,int) > 0 && array(coverage,j-1,int) > 0 && array(coverage,j+1,int) > 0 )
		    {
		      if ( verbose )
			fprintf(stderr,"Abbuttment in %-15s at %6d \n", dictName(CAF->seqDict,contig->id), j );
		      errors++;
		    }
		  j++;
		}
	    }
	}
    }
  return errors;
}
		  
			   
