/*  Last edited: Aug 15 15:27 2001 (rmd) */

#ifndef CAF_CHECK_H
#define CAF_CHECK_H

#include <caf.h>

int checkCAF(cafAssembly *CAF, int Verbose);
int check_holes(cafAssembly *CAF, int verbose);

#endif /* CAF_CHECK_H */
