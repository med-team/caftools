/*  Last edited: Mar  4 15:07 2002 (rmd) */
/* Hacked version of read_scf.c
   Decompresses entire SCF file into memory, thereby avoiding the
   need for a temporary file
   */

/*
 * Copyright (c) Medical Research Council 1994. All rights reserved.
 *
 * Permission to use, copy, modify and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * this copyright and notice appears in all copies.
 *
 * This file was written by James Bonfield, Simon Dear, Rodger Staden,
 * as part of the Staden Package at the MRC Laboratory of Molecular
 * Biology, Hills Road, Cambridge, CB2 2QH, United Kingdom.
 *
 * MRC disclaims all warranties with regard to this software.
 */

/* 
  Title:       read_scf.c
  
  Purpose:	 read IO of Standard Chromatogram Format sequences
  Last update:   August 18 1994
  
  Change log:
  4 Feb 1992,  Now draft proposal version 2
  20 Feb 1992, Grab info from comment lines
  19 Aug 1992, If SCF file has clip information, don't clip automatically
  10 Nov 1992  SCF comments now stored in seq data structure
  18 Aug 1994  Renamed from  ReadIOSCF.c; now purely SCF IO (no Seq structs)

*/

/* ---- Imports ---- */
#include <config.h>
#include <ctype.h>
#include <stdio.h>    /* IMPORT: fopen, fclose, fseek, ftell, fgetc,
			 EOF */
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#ifdef USE_ZLIB
#include <zlib.h>
#endif

#include <io_lib/scf.h>      /* SCF structures */
/*
   #include "io_lib/include/mach-io.h"
   #include "io_lib/include/xalloc.h"
   #include "io_lib/include/compress.h"
*/

/* SunOS4 has it's definitions in unistd, which we won't include for compat. */
#ifndef SEEK_SET
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#endif

#define READ_FILE 0
#define READ_PIPE 1
#define READ_ZLIB 2

uint_2 be_get_int_2(unsigned char* bytes)
{
  return ((((uint_2) bytes[0]) << 8) +
	  ((uint_2) bytes[1]));
}

uint_4 be_get_int_4(unsigned char* bytes)
{
  return ((((uint_4) bytes[0]) << 24) +
	  (((uint_4) bytes[1]) << 16) +
	  (((uint_4) bytes[2]) <<  8) +
	  ((uint_4) bytes[3]));
}

/* ---- Exported functions ---- */

int caf_read_scf_header(unsigned char *scf_read_buffer, Header *h)
{
    int i;

    h->magic_number = be_get_int_4(scf_read_buffer);

    if (h->magic_number != SCF_MAGIC) {
      /* fprintf(stderr, "Magic : 0x%x 0x%x\n", h->magic_number, SCF_MAGIC); */
      return -1;
    }
    h->samples = be_get_int_4(scf_read_buffer + 4);
    h->samples_offset = be_get_int_4(scf_read_buffer + 8);
    h->bases = be_get_int_4(scf_read_buffer + 12);
    h->bases_left_clip = be_get_int_4(scf_read_buffer + 16);
    h->bases_right_clip = be_get_int_4(scf_read_buffer + 20);
    h->bases_offset = be_get_int_4(scf_read_buffer + 24);
    h->comments_size = be_get_int_4(scf_read_buffer + 28);
    h->comments_offset = be_get_int_4(scf_read_buffer + 32);
    memcpy(h->version, scf_read_buffer + 36, sizeof(h->version));
    h->sample_size = be_get_int_4(scf_read_buffer + 40);
    h->code_set = be_get_int_4(scf_read_buffer + 44);
    h->private_size = be_get_int_4(scf_read_buffer + 48);
    h->private_offset = be_get_int_4(scf_read_buffer + 52);
    for (i=0;i<18;i++)
	h->spare[i] = be_get_int_4(scf_read_buffer + 56 + i * 4);
    
    return 0;
}


void caf_read_scf_sample1(unsigned char *buf, Samples1 *s)
{
  s->sample_A = buf[0];
  s->sample_C = buf[1];
  s->sample_G = buf[2];
  s->sample_T = buf[3];
}


void caf_read_scf_sample2(unsigned char *buf, Samples2 *s)
{
  s->sample_A = be_get_int_2(buf);
  s->sample_C = be_get_int_2(buf + 2);
  s->sample_G = be_get_int_2(buf + 4);
  s->sample_T = be_get_int_2(buf + 6);
}

int caf_read_scf_samples1(unsigned char *samples_data,
			  Samples1 *s, size_t num_samples) {
  size_t i;
  unsigned char *curr_sample = samples_data;
  
  for (i = 0; i < num_samples; i++) {
    caf_read_scf_sample1(curr_sample, &(s[i]));
    curr_sample += 4;
  }

return 0;
}


int caf_read_scf_samples2(unsigned char *samples_data, Samples2 *s,
		      size_t num_samples) {
  size_t i;
  unsigned char *curr_sample = samples_data;
  
  for (i = 0; i < num_samples; i++) {
    caf_read_scf_sample2(curr_sample, &(s[i]));
    curr_sample += 8;
  }
  
  return 0;
}


int caf_read_scf_samples32(unsigned char *samples_data,
			   Samples2 *s, size_t num_samples) {
  size_t i;
  uint2 *samples_out;
  unsigned char *curr_sample = samples_data;
  
  /* version to read delta delta data in 2 bytes */
  
  if ( ! (samples_out = (uint2 *)malloc(num_samples * 
					 sizeof(uint2)))) {
    return -1;
  }
  
  for (i = 0; i < num_samples; i++) {
    samples_out[i] = be_get_int_2(curr_sample);
    curr_sample += 2;
  }
  
  scf_delta_samples2(samples_out, num_samples, 0);

  for (i = 0; i < num_samples; i++) {
	(&s[i])->sample_A = samples_out[i];
  }
  
  for (i = 0; i < num_samples; i++) {
    samples_out[i] = be_get_int_2(curr_sample);
	curr_sample += 2;
  }
  
  scf_delta_samples2(samples_out, num_samples, 0);
  
  for (i = 0; i < num_samples; i++) {
    (&s[i])->sample_C = samples_out[i];
  }
  
  for (i = 0; i < num_samples; i++) {
    samples_out[i] = be_get_int_2(curr_sample);
    curr_sample += 2;
  }
  
  scf_delta_samples2(samples_out, num_samples, 0);
  
  for (i = 0; i < num_samples; i++) {
    (&s[i])->sample_G = samples_out[i];
  }
  
  for (i = 0; i < num_samples; i++) {
    samples_out[i] = be_get_int_2(curr_sample);
    curr_sample += 2;
  }
  
  scf_delta_samples2(samples_out, num_samples, 0);
  
  for (i = 0; i < num_samples; i++) {
    (&s[i])->sample_T = samples_out[i];
  }
  
  free(samples_out);
  return 0;
}

int caf_read_scf_samples31(unsigned char *samples_data,
			   Samples1 *s, size_t num_samples) {
  size_t i;
  int1 *samples_out;
  unsigned char *curr_sample = samples_data;
  
  /* version to read delta delta data in 1 byte */
  
  if ( ! (samples_out = (int1 *)malloc(num_samples * 
					sizeof(int1)))) {
    return -1;
  }
  
  memcpy(samples_out, curr_sample, num_samples);
  curr_sample += num_samples;
  
  scf_delta_samples1 ( samples_out, num_samples, 0);
  for (i = 0; i < num_samples; i++) {
    (&s[i])->sample_A = samples_out[i];
  }
  
  memcpy(samples_out, curr_sample, num_samples);
  curr_sample += num_samples;
  
  scf_delta_samples1 ( samples_out, num_samples, 0);
  for (i = 0; i < num_samples; i++) {
    (&s[i])->sample_C = samples_out[i];
  }
  
  memcpy(samples_out, curr_sample, num_samples);
  curr_sample += num_samples;
  
  scf_delta_samples1 ( samples_out, num_samples, 0);
  for (i = 0; i < num_samples; i++) {
    (&s[i])->sample_G = samples_out[i];
  }
  
  memcpy(samples_out, curr_sample, num_samples);
  curr_sample += num_samples;

  scf_delta_samples1 ( samples_out, num_samples, 0);
  for (i = 0; i < num_samples; i++) {
    (&s[i])->sample_T = samples_out[i];
  }
  
  free(samples_out);
  return 0;
}

void caf_read_scf_base(unsigned char *buf, Bases *b)
{
    b->peak_index = be_get_int_4(buf);
    b->prob_A = buf[4];
    b->prob_C = buf[5];
    b->prob_G = buf[6];
    b->prob_T = buf[7];
    b->base   = buf[8];
    b->spare[0] = buf[9];
    b->spare[1] = buf[10];
    b->spare[2] = buf[11];
}


int caf_read_scf_bases(unsigned char *bases_data, Bases *b, size_t num_bases)
{
  size_t i;
  unsigned char *curr_base = bases_data;
  
  for (i = 0; i < num_bases; i++) {
    caf_read_scf_base(curr_base, &(b[i]));
    curr_base += 12;
  }
  
  return 0;
}

int caf_read_scf_bases3(unsigned char *bases_data, Bases *b, size_t num_bases)
{
  size_t i;
  uint_1 *prob_a_start = bases_data + 4 * num_bases;
  uint_1 *prob_c_start = bases_data + 5 * num_bases;
  uint_1 *prob_g_start = bases_data + 6 * num_bases;
  uint_1 *prob_t_start = bases_data + 7 * num_bases;
  uint_1 *base_start = bases_data + 8 * num_bases;
  uint_1 *spare_0_start = bases_data + 9 * num_bases;
  uint_1 *spare_1_start = bases_data + 10 * num_bases;
  uint_1 *spare_2_start = bases_data + 11 * num_bases;
  
  for (i=0; i < num_bases; i++) {
    (&b[i])->peak_index = be_get_int_4(&bases_data[i * 4]);
    (&b[i])->prob_A   = prob_a_start[i];
    (&b[i])->prob_C   = prob_c_start[i];
    (&b[i])->prob_G   = prob_g_start[i];
    (&b[i])->prob_T   = prob_t_start[i];
    (&b[i])->base     = base_start[i];
    (&b[i])->spare[0] = spare_0_start[i];
    (&b[i])->spare[1] = spare_1_start[i];
    (&b[i])->spare[2] = spare_2_start[i];
  }
  
  return 0;
}



int caf_read_scf_comment(unsigned char * comments_data, Comments *c, size_t s)
{
    memcpy(c, comments_data, s);

    return 0;
}

#define init_buffer_size 65536

unsigned char* read_file_to_memory(FILE *fp, size_t * total_bytes, int read_method) {
  static unsigned char* file_buffer;
  static size_t buffer_size;
  unsigned char* curr_read_pos;
  size_t total_bytes_read = 0;
  ssize_t bytes_read = 1;

  if (!file_buffer) {
    file_buffer = malloc(init_buffer_size);
    buffer_size = init_buffer_size;
  }

  curr_read_pos = file_buffer;

  while (bytes_read > 0) {
    size_t bytes_to_read;

    if (total_bytes_read == buffer_size) {
      unsigned char* old_start = file_buffer;
      buffer_size *= 2;
      file_buffer = realloc(file_buffer, buffer_size);
      
      curr_read_pos = file_buffer + (curr_read_pos - old_start);
    }

    bytes_to_read = buffer_size - total_bytes_read;

#ifdef USE_ZLIB
    switch (read_method) {

    case READ_ZLIB:
      bytes_read = gzread((gzFile) fp, curr_read_pos, bytes_to_read);

      if (bytes_read < 0) {
	char * err_msg;
	int errnum;
	err_msg = gzerror((gzFile) fp, &errnum);
	fprintf(stderr, "zlib error %d %s\n", errnum, err_msg);
	return NULL;
      }

      break;

    default:
#endif
      bytes_read = fread(curr_read_pos, 1, bytes_to_read, fp);
#ifdef USE_ZLIB
      break;
    }
#endif

    curr_read_pos += bytes_read;
    total_bytes_read += bytes_read;
  }

  *total_bytes = total_bytes_read;

  return file_buffer;
}

int caf_check_scf_header(Header *h, size_t len)
{
  /* Check that none of the SCF data structures run over the end of the file */

  if (h->samples * h->sample_size * 4 + h->samples_offset > len)
    return 1;

  if (h->bases * 12 + h->bases_offset > len)
    return 1;

  if (h->comments_size + h->comments_offset > len)
    return 1;

  if (h->private_size + h->private_offset > len)
    return 1;

  return 0;
}

Scf *caf_fread_scf(FILE *fp, int read_method) {
  Scf *scf;
  Header h;
  int err;
  float scf_version;
  unsigned char* scf_read_buffer;
  size_t scf_buffer_len;
  
  scf_read_buffer = read_file_to_memory(fp, &scf_buffer_len, read_method);
  if (scf_read_buffer == NULL) return NULL;

  /* Read header */
  if (caf_read_scf_header(scf_read_buffer, &h) == -1) {
    /* fprintf(stderr, "Couldn't read header\n"); */
    return NULL;
  }

  if (caf_check_scf_header(&h, scf_buffer_len)) {
    return NULL;
  }

  
  /* Allocate memory */
  if (NULL == (scf = scf_allocate(h.samples, h.sample_size,
				  h.bases, h.comments_size,
				  h.private_size))) {
    /* fprintf(stderr, "Couldn't allocate memory\n"); */
    return NULL;
  }
  
  /* fake things for older style SCF -- SD */

  if (h.sample_size != 1 && h.sample_size != 2) h.sample_size = 1;
  
  scf_version = scf_version_str2float(h.version);

  memcpy(&scf->header, &h, sizeof(Header));

  /* Read samples */
  
  if ( 2.9 > scf_version ) {
    
    if (h.sample_size == 1) {
      err= caf_read_scf_samples1(scf_read_buffer + h.samples_offset,
				 scf->samples.samples1, h.samples);
    } else {
      err= caf_read_scf_samples2(scf_read_buffer + h.samples_offset,
				 scf->samples.samples2, h.samples);
    }
  } else {
    if (h.sample_size == 1) {
      err= caf_read_scf_samples31(scf_read_buffer + h.samples_offset,
				  scf->samples.samples1, h.samples);
    } else {
      err= caf_read_scf_samples32(scf_read_buffer + h.samples_offset,
				  scf->samples.samples2, h.samples);
    }
  }

  /* Read bases */
  if ( 2.9 > scf_version ) {
    
    if (-1 == caf_read_scf_bases(scf_read_buffer + h.bases_offset,
				 scf->bases, h.bases)) {
	    scf_deallocate(scf);
	    /* fprintf(stderr, "Couldn't read bases\n"); */
	    return NULL;
    }
  } else {
    if (-1 == caf_read_scf_bases3(scf_read_buffer + h.bases_offset,
			      scf->bases, h.bases)) {
      scf_deallocate(scf);
      /* fprintf(stderr, "Couldn't read bases\n"); */
      return NULL;
    }
  }

  /* Read comments */
  if (scf->comments) {
    if (-1 == caf_read_scf_comment(scf_read_buffer + h.comments_offset,
				   scf->comments,
				   h.comments_size)) {
      /*
       * Was: "scf_deallocate(scf); return NULL;".
       * We now simply clear the comments and gracefully continue.
       */
      fprintf(stderr, "Warning: SCF file had invalid comment field\n");
      free(scf->comments);
      scf->comments = NULL;
    } else {
      scf->comments[h.comments_size] = '\0';
    }
  }
  
  /* Read private data */
  if (h.private_size) {
    memcpy(scf->private_data,
	   scf_read_buffer + h.private_offset,
	   h.private_size);
  }

  return scf;
}

typedef struct {
  unsigned char magic[2];
  char *compress;
  char *uncompress;
  char *extension;
  int read_method;
} Magics;

#define num_magics 4

static Magics magics[num_magics] = {
    {{'B',   'Z'},      "bzip",         "bzip -d <",    ".bz", READ_PIPE},
#ifdef USE_ZLIB
    {{'\037','\213'},   "gzip",         "gzip -d <",    ".gz", READ_ZLIB},
#else
    {{'\037','\213'},   "gzip",         "gzip -d <",    ".gz", READ_PIPE},
#endif
    {{'\037','\235'},   "compress",     "uncompress < ",".Z", READ_PIPE},
    {{'\037','\036'},   "pack",         "pcat",         ".z", READ_PIPE},
};


FILE *caf_open_compressed(char *fn, int * read_method)
{
  char fext[1024];
  char buf[2048];
  int i;
  FILE *orig_file;
  unsigned char magic[2];
  char *real_name = fn;

  *read_method = READ_FILE;
  
  /* Open file */
  orig_file = fopen(fn, "rb");

  /* If file name didn't work, try file.extension */
  if (!orig_file) {
    for (i = 0; i < num_magics; i++) {
      sprintf(fext, "%s%s", fn, magics[i].extension);
      orig_file = fopen(fext, "rb");
      if (orig_file) break;
    }
    if (i >= num_magics) return NULL;
    real_name = fext;
  }

  /* Read magic number */
  if (fread(magic, 1, 2, orig_file) != 2) {
    fclose(orig_file);
    return NULL;
  }
  
  /* Check magic number & return pipe to decompression prog */

  /* fprintf(stderr, "Magic number: 0x%x 0x%x\n", (int) magic[0], (int) magic[1]); */

  for (i = 0; i < num_magics; i++) {
    if (magic[0] == magics[i].magic[0] && magic[1] == magics[i].magic[1]) {

      *read_method = magics[i].read_method;
      /* fprintf(stderr, "Using method %d for %s\n", *read_method, real_name);*/
#ifdef USE_ZLIB
      switch (*read_method) {

      case READ_ZLIB:
	fclose(orig_file);
	return (FILE*) gzopen(real_name, "rb");
       	break;

      default: /* READ_PIPE */
#endif
	sprintf(buf, "%s %s 2>/dev/null",
		magics[i].uncompress, real_name);
	fclose(orig_file);
	/* fprintf(stderr, "Pipe: %s\n", buf); */
      	return popen(buf, "r");
#ifdef USE_ZLIB
      }
#endif
    }
  }

  /* Assume wasn't compressed, so return original file */

  /* fprintf(stderr, "File: %s\n", real_name); */
  rewind(orig_file);
  return orig_file;
}

/*
 * Read the SCF format sequence with name `fn' into a 'scf' structure.
 * A NULL result indicates failure.
 */
Scf *read_scf(char *fn) {
    Scf *scf;
    int read_method = READ_FILE;
    FILE *fp;

    /* Open fn for reading in binary mode */

    if (NULL == (fp = caf_open_compressed(fn, &read_method))) {
      /* fprintf(stderr, "Couldn't open %s %d\n", fn, read_method);*/
      return NULL;
    }
    /* fprintf(stderr, "Reading %4d %s\n", ++read_num, fn); */
    scf = caf_fread_scf(fp, read_method);
    switch (read_method) {

    case READ_PIPE:
      pclose(fp);
      break;

#ifdef USE_ZLIB
    case READ_ZLIB:
      gzclose((gzFile) fp);
      break;
#endif

    default: /* READ_FILE */
      fclose(fp);
      break;
    }

    return scf;
}

