/*  Last edited: Aug  9 12:37 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* CAF_TYPES.H

   Definitions of enumerated types used in CAF for representing sequence attributes

   Also includes associated string defs

   Author: Richard Mott
*/

#ifndef _CAF_TYPES_H_
#define _CAF_TYPES_H_

/* classification of sequence types, Every sequence should have a type */

enum sequence_type
{
  unknown_sequence,             /* this should not happen ! */
  is_read,                      /* a read */
  is_contig,                    /* a contig */
  is_vector,                    /* a vector (not currently used, but may be for restriction digests) */
  is_group,                     /* a group of contigs */
  is_assembly,                  /* a group of groups */
  is_spliced,                   /* not used */
  sequence_types
    
};

/* classification of ASP process status  failure) types */

enum process_type
{
  process_unknown_status,       /* this should not occur */
  process_passed,               /* Passed reads. Normally we only deal with these */
  process_low_qual,             /* low trace quality */
  process_completely_svec,      /* completely sequencing vector */
  process_completely_cvec,      /* completelt cloning vector */
  process_contaminant,          /* yeast, e coli etc */
  process_types
};

/* MACRO to determine if read is good

   Use as:  is_failure( seq->process_status )

 */

#define is_failure(x) ( (x) > process_passed ) 

/* the text strings associated with the process status */

extern char *process_strings[process_types];

/* classification of padded/unpadded */

enum pad_states
{
  unknown_state,
  padded_state,
  unpadded_state,
  pad_types
};

extern char *pad_strings[pad_types];
    
/* classification of dye primer/terminator */

enum dyes
{
  unknown_dye,
  dye_primer,
  dye_terminator,
  dye_types
};

extern char *dye_strings[dye_types];


/* classification or primer type */

enum primers
{
  unknown_primer,
  universal_primer,
  custom_primer,
  primer_types
    
};


extern char *primer_strings[primer_types];

/* Strand direction. terminator is required by auto-edit */
enum strand_direction
{
  reverse_strand = -1,
  unknown_strand = 0,
  forward_strand = 1,  terminator = 2
};


#define is_reverse(x) ( (x) < 0 )
#define is_forward(x) ( (x) > 0 )
#define is_terminated(x) ( abs(x) == terminator )

#endif
