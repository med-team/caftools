/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * $Log$
 * Revision 1.4  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 * Revision 1.3  2002/03/04 17:00:39  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.2  1997/07/22  11:25:42  badger
 * added base_position (saint louis)
 * fixed a few bugs.
 *
 * Revision 1.2  1997/07/15  19:13:13  sdear
 * Support for BasePosition
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 */

#include <stdio.h>
#include <caf.h>
#include <cl.h>

extern int binary_mode;

DICT *dictRead( FILE *fp );
void writeBinCafSeq( FILE *fp, CAFSEQ *seq );
Array readBinTags( FILE *fp );
void writeBinTags( Array tags, FILE *fp );
char *readString( FILE *fp);
void writeString( char *s, FILE *fp );
int readBinCafSeq( FILE *fp, CAFSEQ *seq );

int bin_mode(int argc, char **argv)
{
  return getboolean("-binary",&binary_mode,argc,argv);
}

cafAssembly *readCafBinary( FILE *fil, char *filename )
{

  cafAssembly *Ass=NULL;
  int objects;
  int i;
  CAFSEQ *seq;
  int c;

/* first character of a cafbinary is # */

  if ( (c = getc( fil )) != '#' ) 
    {
      ungetc( c, fil );
      return Ass;
    }

  if ( fscanf( fil, "CAFBINARY %d objects\n", &objects ) )
    {
      Ass = (cafAssembly*)messalloc(sizeof(cafAssembly));
      Ass->seqs = arrayCreate(objects,CAFSEQ);
      Ass->seqDict = dictRead(fil);
      Ass->tagTypeDict = dictRead(fil);
      Ass->templateDict = dictRead(fil);
      for(i=0;i<objects;i++)
	{
	  seq = arrayp(Ass->seqs,i,CAFSEQ);
	  readBinCafSeq(fil,seq);
	}
    }
  else
    rewind(fil);

  return Ass;
}

DICT *dictRead( FILE *fp )
{
  int size=0;
  DICT *dict=NULL;
  char buffer[1000];
  char *s;
  int c, i, id;

  if ( fscanf( fp, "DICT %d\n", &size ) )
    {
      dict = dictCreate(size);
      for(i=0;i<size;i++)
	{
	  s = buffer;
	  while( (c=getc(fp)) && c != EOF ) 
	    *s++ = c;
	  *s = '\0';
	  dictAdd(dict,buffer,&id);
	  if ( id != i )
	    fprintf(stderr, "ERROR dict %s %d %d\n", buffer, i, id );
	}
    }
  return dict;
}

int dictWrite( FILE *fp, DICT *dict )
{
  int size = dictMax(dict);
  char *s;
  int i;

  fprintf(fp,"DICT %d\n", size);
  for(i=0;i<size;i++)
    {
      s = dictName(dict,i);
      do
	{
	  putc(*s,fp);
	}
      while(*s++);
    }
  return size;
}

int writeCafBinary( cafAssembly *Ass, FILE *fil, char *header )
{
  Array seqs;
  int i;

  if ( Ass && fil && binary_mode )
    {
      seqs = Ass->seqs;
      fprintf(fil,"#CAFBINARY %d objects\n", arrayMax(seqs) );
      dictWrite(fil,Ass->seqDict);
      dictWrite(fil,Ass->tagTypeDict);
      dictWrite(fil,Ass->templateDict);
      for(i=0;i<arrayMax(seqs);i++)
	writeBinCafSeq(fil,arrp(Ass->seqs,i,CAFSEQ));
      return 1;
    }
  return 0;
}

void writeBinCafSeq( FILE *fp, CAFSEQ *seq )
{
  char *dna, *other;
  ASSINF *a;
  int i;
  int len=0;

  fwrite( seq, sizeof(CAFSEQ), 1, fp );

  writeString(seq->SCF_File,fp);
  writeString(seq->stolen_from,fp);
  writeString(seq->failed_because,fp);
  writeString(seq->custom_primer,fp);

  if ( seq->dna )
    {
      dna = stackText(seq->dna,0);
      len = strlen(dna)+1;
      putw(len,fp);
      fwrite(dna,sizeof(char),len,fp);
    }

  if ( seq->base_quality )
    {
      len = arrayMax(seq->base_quality);
      fwrite(seq->base_quality->base,sizeof(BQ_SIZE),arrayMax(seq->base_quality),fp);
    }
  if ( seq->base_position )
    {
      len = arrayMax(seq->base_position);
      fwrite(seq->base_position->base,sizeof(BP_SIZE),arrayMax(seq->base_position),fp);
    }

  if ( seq->assinf )
    {
      putw(arrayMax(seq->assinf),fp);
      for(i=0;i<arrayMax(seq->assinf);i++)
	{
	  a = arrp(seq->assinf,i,ASSINF);
	  fwrite( a, sizeof(ASSINF), 1, fp );
	}
    }
  else
    putw(0,fp);

  if ( seq->edit )
    {
      edit_struct *edit;
/*      fprintf(stderr, "seq id %d\n", seq->id );*/

      putw(arrayMax(seq->edit),fp);
      for(i=0;i<arrayMax(seq->edit);i++)
	{
	  edit = arrp(seq->edit,i,edit_struct);
	  fwrite( edit, sizeof(edit_struct), 1, fp );
/*	  fprintf(stderr,"edit pos %5d compound %d\n", edit->pos, edit->compound ); */
	}
    }
  else
    putw(0,fp);
      


  writeBinTags(seq->tags, fp);
  writeBinTags(seq->cvector, fp);
  writeBinTags(seq->svector, fp);
  writeBinTags(seq->clipping, fp);  
  writeBinTags(seq->golden_path, fp);

  if ( seq->other )
    {
      other = stackText(seq->other,0);
      len = strlen(other)+1;
      putw(len,fp);
      fwrite(other,sizeof(char),len,fp);
    }
}

void writeString( char *s, FILE *fp )
{
  if ( s && *s )
    fwrite(s,sizeof(char),strlen(s)+1,fp);
  else
    putc(0,fp);
}

char *readString( FILE *fp)
{
  char *s;
  char buffer[10000];
  int len=0;
  int c;

  s = buffer;
  while((c=getc(fp)) && c != EOF)
    {
      *s++ = c;
    }
  *s = '\0';
  len = strlen(buffer);
/*  fprintf(stderr,"readstring len %d %s\n", len, buffer); */
  if ( len > 0 )
    {
      s = (char*)messalloc((len+1)*sizeof(char));
      strcpy(s,buffer);
    }
  else
    s = 0;

/*  fprintf(stderr,"return %s\n", s); */
  return s;
}

int readBinCafSeq( FILE *fp, CAFSEQ *seq )
{
  char *dna, *other;
  ASSINF *a;
  int i;
  int len=0;
  int size=0;

  fread( seq, sizeof(CAFSEQ), 1, fp );

  seq->SCF_File = readString(fp);
  seq->stolen_from = readString(fp);
  seq->failed_because = readString(fp);
  seq->custom_primer = readString(fp);

  if ( seq->dna )
    {
      len = getw(fp);
      if ( len > 0 )
	{
	  seq->dna = stackCreate(len+1);
	  dna = (char*)messalloc(len*sizeof(char));
	  fread(dna,sizeof(char),len,fp);
	  pushText(seq->dna,dna);
	  len = strlen(dna);
	  messfree(dna);
/*	  fprintf(stderr,"after dna: %d %s\n", len, stackText(seq->dna,0)); */
	}
      else
	seq->dna = NULL;
    }
  if ( seq->base_quality )
    {
      seq->base_quality = arrayCreate(len,BQ_SIZE);
      *arrayp(seq->base_quality,len-1,BQ_SIZE) = 0;
      fread(seq->base_quality->base,sizeof(BQ_SIZE),len,fp);
    }
/*  fprintf(stderr, "after base qual\n"); */
  if ( seq->base_position )
    {
      seq->base_position = arrayCreate(len,BP_SIZE);
      *arrayp(seq->base_position,len-1,BP_SIZE) = 0;
      fread(seq->base_position->base,sizeof(BP_SIZE),len,fp);
    }
/*  fprintf(stderr, "after base position\n"); */

  size = getw(fp);
/*  fprintf(stderr, "size %d\n", size); */
  if ( seq->assinf && size > 0 )
    {
      seq->assinf = arrayCreate(size,ASSINF);
      for(i=0;i<size;i++)
	{
	  a = arrayp(seq->assinf,i,ASSINF);
	  fread( a, sizeof(ASSINF), 1, fp );
/*	  fprintf(stderr,"%5d %5d %5d %5d\n", a->r1, a->r2, a->s1, a->s2); */
	}
    }
  else
    seq->assinf = NULL;
/*  fprintf(stderr,"after assinf\n"); */

  size = getw(fp);
/*  fprintf(stderr, "size %d\n", size); */

  if ( seq->edit && size > 0 )
    {
      edit_struct *edit;

      seq->edit = arrayCreate(size,edit_struct);
      for(i=0;i<size;i++)
	{
	  edit = arrayp(seq->edit,i,edit_struct);
	  fread( edit, sizeof(edit_struct), 1, fp );
/*	  fprintf(stderr,"edit pos %5d compound %d\n", edit->pos, edit->compound ); */
	}
    }
  else
    seq->edit = NULL;
/*  fprintf(stderr,"after assinf\n"); */

  
  seq->tags = readBinTags( fp);
  seq->cvector = readBinTags( fp);
  seq->svector = readBinTags( fp);
  seq->clipping = readBinTags( fp); 
  seq->golden_path = readBinTags( fp);

  if ( seq->other )
    {
      len = getw(fp);
      if ( len > 0 )
	{
	  seq->other = stackCreate(len+1);
	  other = (char*)messalloc(len*sizeof(char));
	  fread(other,sizeof(char),len,fp);
	  pushText(seq->other,other);
	  len = strlen(other);
	  messfree(other);
	}
      else
	seq->other = NULL;
    }
  return 0;
}

void writeBinTags( Array tags, FILE *fp )
{
  int i;
  TAG *tag;

  if ( tags )
    {
      putw(arrayMax(tags),fp);
      for(i=0;i<arrayMax(tags);i++)
	{
	  tag = arrp(tags,i,TAG);
	  putw(tag->type,fp);
	  putw(tag->x1,fp);
	  putw(tag->x2,fp);
	  if ( tag->text )
	    fwrite(tag->text,sizeof(char),strlen(tag->text)+1,fp);
	  else
	    fputc(0,fp);
	}
    }
  else
    putw(0,fp);
}

Array readBinTags( FILE *fp )
{
  Array tags=NULL;
  int size=0, i, len;
  TAG *tag;
  char buffer[10000];
  char *s;

  size = getw( fp );
  if ( size > 0 )
    {
      tags = arrayCreate(size,TAG);
      for(i=0;i<size;i++)
	{
	  tag = arrayp(tags,i,TAG);
	  tag->type = getw(fp);
	  tag->x1 = getw(fp);
	  tag->x2 = getw(fp);
	  s = buffer;
	  *s = '\0';
	  while((*s++=getc(fp)));
	  len = strlen(buffer);
	  if ( len )
	    {
	      tag->text = (char*)messalloc((len+1)*sizeof(char));
	      strcpy(tag->text,buffer);
	    }
	}
    }
  return tags;
}
