/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * $Id: cafseq.c 25667 2008-06-03 09:46:23Z rmd $
 * $Log$
 * Revision 1.17  2005/06/21 15:46:26  rmd
 * Use <> in #include lines instead of "".
 *
 * Revision 1.16  2005/06/16 12:55:27  rmd
 * Moved unpackTimet prototype to cafseq.c as it's static.
 *
 * Revision 1.15  2004/11/26 17:02:37  rmd
 * Added correct prototypes for initParseAttribute and initObjectParse.
 *
 * Revision 1.14  2004/02/26 11:25:52  rmd
 * Fixed double file closure bug in readCAF.
 *
 * Revision 1.13  2004/02/09 14:23:52  rmd
 * Added writeNotes and noteCmp.  It is no longer possible to use writeTags
 * for writing notes, are the TAG and NOTE structures are no longer compatible.
 *
 * Revision 1.12  2003/10/15 09:14:11  dgm
 * Update of unpackTimet to use a autoconf #define SIZEOF_TIME_T to conditionaly
 * compile code for unpacking 64bit time_t's into 2 32bit values.
 *
 * Revision 1.11  2003/10/15 08:24:24  dgm
 * Bug fix of notes
 *
 * Revision 1.10  2003/10/13 09:41:59  dgm
 * Changes to allow for the addition of notes to Contigs and Reads.
 *
 * Revision 1.9  2003/07/31 12:52:24  dgm
 * Added code for mark (initial release)
 *
 * Revision 1.8  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 * Revision 1.7  2002/03/04 17:00:39  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.6  2002/03/04 15:40:51  rmd
 * Now have to write out Clone, Cloning_vector and Sequencing_vector tags.
 *
 * Revision 1.5  2001/12/12 18:22:20  rmd
 * Added new code to put entries in ligationDict, cloneDict and vectorDict.
 * Commented out the 'Downgrading BaseQuality for ...' section.
 *
 * Revision 1.4  1999/04/01 09:23:58  rmd
 * Added caf_read_scf to read compressed SCF files directly rather than
 * use a temp file. Also protect stolen read tags when printing them out.
 *
 * Revision 1.3  1998/01/12  16:39:26  badger
 * *** empty log message ***
 *
 * Revision 1.2  1997/07/22  11:25:42  badger
 * added base_position (saint louis)
 * fixed a few bugs.
 *
 * Revision 1.2  1997/07/15  19:13:13  sdear
 * Support for BasePosition
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Revision 1.2  1996/10/04  13:21:33  rmott
 * fixed bug when tags were lost
 *
 * Revision 1.1  1996/10/03  17:39:35  rmott
 * Initial revision
 *
 */

#include <time.h>
#include <stdint.h>
#include <inttypes.h>
#include <caf.h>    /* CAF definitions */
#include <cafseq.h> /* internal IO functions */
#include <cl.h>     /* Command-line parsing */

/* Prototypes */
void initParseAttribute(void);
void initObjectParse(void);
static void unpackTimet(time_t time, int *u32bits, int *l32bits);

#define ABORT(x)	{ messout ("ace file parse error line %d: %s", \
				   freestreamline (level), x) ; \
			  freeclose (level) ; \
			  return FALSE ; \
			}


/****************************************************************/
#define INIT_SIZE 15000

extern int binary_mode;

static DICT *attributeDict=NULL;
static Array attribute=NULL;
static DICT *objectDict=NULL;
static Array object=NULL;


int level;

cafAssembly *
readCafAssembly ( FILE *fil, char *filename, cafAssembly *CAF )

/* Read in a caf database from fil
   If CAF is non-null then it should point to an existing CAF, which is appended.

   All memory is allocated on a handle, except for the dictionaries
*/

{ 
  int dummy;
  CAFSEQ *seq = 0 ;
  char *word ;
  DICT *tagTypeDict, *noteTypeDict, *templateDict;
  int i;
  int iseq ;
  STORE_HANDLE handle;

  initParseAttribute();
  initObjectParse();


  if ( CAF == NULL ) /* new database */ 
    {
      if ((CAF = readCafBinary( fil, filename )) != 0) /* try to read binary */
	return CAF;
      else  /* allocate memory */
	{
	  handle = handleCreate();
	  CAF = (cafAssembly*)halloc(sizeof(cafAssembly),handle);
	  CAF->handle = handle;
	  CAF->seqDict = dictCreate(INIT_SIZE);
	  CAF->tagTypeDict = tagTypeDict = dictCreate(50);
	  CAF->noteTypeDict = noteTypeDict = dictCreate(50);
	  CAF->templateDict = templateDict = dictCreate(INIT_SIZE);
	  CAF->ligationDict = dictCreate(100);
	  CAF->cloneDict = dictCreate(50);
	  CAF->vectorDict = dictCreate(50);
	  CAF->seqs = arrayHandleCreate(INIT_SIZE,CAFSEQ,handle);
	  CAF->filename = strnew(filename,CAF->handle);

	  /* Fill up entry 0 in all the dictionaries */

	  dictAdd(CAF->templateDict, "unknown", &dummy);
	  dictAdd(CAF->ligationDict, "unknown", &dummy);
	  dictAdd(CAF->cloneDict,    "unknown", &dummy);
	  dictAdd(CAF->vectorDict,   "unknown", &dummy);
	}
    }
  else
    { /* caf already exists so just append */
    }
  
  freeinit() ;
  level = freesetfile (fil, 0) ;

  /* loop round and slurp the data */

  iseq = -1;
  while (freecard (level))
    { 
      if (!(word = freeword()))
	iseq = -1 ;
      else if (iseq == -1)
	{ 
	  char class[128] ;
	      
	  strcpy (class, word) ;
	  if ((word = freeword()) && (0==strcmp(word,":"))) word = freeword();
	  if (!word)
	    {
	      char msg[256];
	      sprintf(msg,"No object name after class name: %s :", class) ;
	      ABORT(msg);
	    }

	  dictAdd (CAF->seqDict, word, &iseq) ;
	  seq = arrayp (CAF->seqs, iseq, CAFSEQ) ;
	  seq->id = iseq;

	  if ( ! parseObject( class, iseq, CAF ) )
	    ABORT("Class not Sequence, DNA or BaseQuality") ;

	  iseq = -1;
	}
    }

  /* some post-processing ... */
  

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp(CAF->seqs,i,CAFSEQ);
      if ( ! seq->edit )
	seq->edit = arrayHandleCreate(5,edit_struct,CAF->handle);
    }
  
  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      /* set terminator flags */
      seq = arrp(CAF->seqs,i,CAFSEQ);
      if ( seq->dye == dye_terminator )
	seq->is_terminator = TRUE;

      /* check length consistency for base_quality data */
      if ( seq->dna && seq->base_quality )
	{
	  if ( seq->len != arrayMax(seq->base_quality) )
	    {
	      fprintf(stderr,"ABORT: quality length %d and seq length %d different for %s\n", arrayMax(seq->base_quality), seq->len, dictName(CAF->seqDict,i));
	      exit(1);
	    }
	}

      /* check length consistency for base_position data */
      if ( seq->dna && seq->base_position )
	{
	  if ( seq->len != arrayMax(seq->base_position) )
	    {
	      fprintf(stderr,"ABORT: position length %d and seq length %d different for %s\n", arrayMax(seq->base_position), seq->len, dictName(CAF->seqDict,i));
	      exit(1);
	    }
	}
    }

  return CAF ;
}

/************************************************/

void 
writeCafAssembly ( cafAssembly *CAF, FILE *fil, char *header )
{ 

  int i;
  CAFSEQ *seq;
  Array seqs = CAF->seqs;
  DICT *seqDict=CAF->seqDict;
  
  if (!fil)
    messcrash ("writeCafAssembly() requires fil") ;
  if (!seqDict || !seqs)
    messcrash ("writeCafAssembly() requires seqDict and seqs to be made") ;

  if ( writeCafBinary( CAF, fil, header ) )
    return;

  if (header)
    fprintf (fil, "// %s\n", header) ;
  fprintf (fil, "// %d sequences\n\n", arrayMax(seqs)) ;

  for (i = 0 ; i < arrayMax(seqs) ; ++i)
    { 
      seq = arrp(seqs, i, CAFSEQ) ;
      writeCafSeq( CAF, fil, seq );
    }

}

int 
writeCafSeq( cafAssembly *CAF, FILE *fil, CAFSEQ *seq )
{
  int i = 0, j;
  DICT *seqDict=CAF->seqDict;
  DICT *tagTypeDict=CAF->tagTypeDict;
  DICT *noteTypeDict=CAF->noteTypeDict;
  Array seqs = CAF->seqs;
  edit_struct *edit;
  int written = 0;

  if (  interesting_sequence ( seq )  )
    { 
      i = seq->id;
      fprintf (fil, "Sequence : %s\n", dictName (seqDict, i)) ;

      if ( seq->type )
	fprintf( fil, "%s\n", sequence_text[seq->type] );

      if ( seq->pad_state )
	fprintf( fil, "%s\n", pad_strings[seq->pad_state]);

      if ( seq->is_circular )
	fprintf(fil,"Is_circular\n");

      if (seq->SCF_File)
	fprintf(fil, "SCF_File %s\n", seq->SCF_File );

      if ( seq->template_id )
	fprintf(fil,"Template %s\n", dictName(CAF->templateDict,seq->template_id));

      if ( seq->insert_size1 || seq->insert_size2 )
	fprintf(fil, "Insert_size %d %d\n", seq->insert_size1, seq->insert_size2 );

      if ( seq->ligation_no )
	fprintf(fil, "Ligation_no %s\n",
		dictName(CAF->ligationDict, seq->ligation_no));

      if ( seq->clone_id )
	fprintf(fil, "Clone %s\n",
		dictName(CAF->cloneDict, seq->clone_id));

      if ( seq->cloning_vector )
	fprintf(fil, "Cloning_vector %s\n",
		freeprotect(dictName(CAF->vectorDict, seq->cloning_vector)));

      if ( seq->sequencing_vector )
	fprintf(fil, "Sequencing_vector %s\n",
		freeprotect(dictName(CAF->vectorDict, seq->sequencing_vector)));

      if ( seq->dye )
	fprintf(fil,"Dye %s\n", dye_strings[seq->dye] );

      if ( seq->primer ) {
	if (seq->custom_primer) {
	  fprintf(fil,"Primer %s %s\n",
		  primer_strings[seq->primer], seq->custom_primer);
	} else {
	  fprintf(fil,"Primer %s\n", primer_strings[seq->primer]);
	}
      }

      if ( seq->strand )
	fprintf(fil,"Strand %s\n", (seq->strand == forward_strand ? "Forward" : "Reverse" ) );

      if ( seq->is_stolen )
	fprintf(fil, "Stolen %s\n",
		(seq->stolen_from ? freeprotect(seq->stolen_from) : "") );

      if ( seq->process_status ) {
	if ( seq->process_reason ) {
	  fprintf(fil,"ProcessStatus %s %s\n",
		  process_strings[seq->process_status], seq->process_reason);
	} else {
	  fprintf(fil,"ProcessStatus %s\n",
		  process_strings[seq->process_status]);
	}
      }

      if (seq->assinf)
	{
	  ASSINF *a ;

	  for (j = 0 ; j < arrayMax(seq->assinf) ; ++j)
	    { a = arrp(seq->assinf, j, ASSINF) ;
	      if ( a->seq >= 0 ) /* NOTE THIS RULE !!!!! forbids deleted (-1) sequences */
		{
		  if ( seq->type == is_contig )
		    fprintf (fil, "Assembled_from %s %d %d %d %d\n", 
			     dictName (seqDict, a->seq), a->s1, a->s2, a->r1, a->r2) ;	  
		  else if ( seq->type == is_read || seq->SCF_File )
		    {
		      fprintf(fil, "Align_to_SCF %d %d %d %d\n", /* CAF support */
			      a->s1, a->s2, a->r1, a->r2) ;	  
		    }
		}
	    }
	}
      
      /* group order for assembly objects */
	  
      if ( seq->order && seq->type == is_assembly )
	{
	  GROUP_ORDER *g;
	  CAFSEQ *group;
	  for(j=0;j<arrayMax(seq->order);j++)
	    {
	      g = arrp(seq->order,j,GROUP_ORDER);
	      if ( g->seq >= 0 ) {
		group = arrp(seqs,g->seq,CAFSEQ);
		if ( is_group == group->type )
		  fprintf(fil, "Group_order %s %g\n",  dictName(seqDict, g->seq), g->position );
		else {
		  fprintf(stderr, "ERROR: Sequence %s is not a group: %s !\n", dictName(seqDict, group->id), sequence_text[group->type]) ;
		}
	      }
	    }
	}
	 
      /* contig order for groups */

      if ( seq->order && seq->type == is_group )
	{
	  GROUP_ORDER *g;
	  CAFSEQ *contig;
	  for(j=0;j<arrayMax(seq->order);j++)
	    {
	      g = arrp(seq->order,j,GROUP_ORDER);
	      if ( g->seq >= 0 ) {
		contig = arrp(seqs,g->seq,CAFSEQ);
		if ( is_contig == contig->type )
		  fprintf(fil, "Contig_order %s %g\n",  dictName(seqDict, g->seq), g->position );
		else {
		  fprintf(stderr, "ERROR: Sequence %s is not a contig: %s\n", dictName(seqDict, g->seq ),  sequence_text[contig->type]);
		}
	      }
	    }
	}

      writeTags( seq->tags, tagTypeDict, "Tag", fil );
      writeNotes( seq->notes, noteTypeDict, "Note", fil );
      writeTags( seq->cvector, tagTypeDict, "Clone_vec", fil );
      writeTags( seq->svector, tagTypeDict, "Seq_vec", fil );
      writeTags( seq->clipping, tagTypeDict, "Clipping", fil );
      writeTags( seq->golden_path, seqDict, "Golden_path", fil );
	  
      if ( seq->edit )
	for(j=0;j<arrayMax(seq->edit);j++)
	  {
	    edit = arrp(seq->edit,j,edit_struct);
	    fprintf(fil, "Edit %d %s %s %s %s %c\n", edit->pos, status_text[edit->status], compound_text[is_compound(edit->compound)], Consensus_text[edit->consensus], strand_text[edit->stranded], edit->New );
	  }
	  
      if (seq->other)
	{ stackCursor (seq->other, 0) ;
	  while (!stackAtEnd (seq->other))
	    fprintf (fil, "%s\n", stackNextText (seq->other)) ;
	}
      fputc ('\n', fil) ;
      written = 1;
    }

  if (seq->dna)
    { char *s = stackText(seq->dna, 0) ;
      int len = strlen(s);

      fprintf (fil, "DNA : %s\n", dictName (seqDict, i)) ;
      for(j=0;j<len;j+=60)
	{
	  int n = ( j+60 < len ? 60: len-j );
	  fwrite(&s[j],sizeof(char),n,fil);
	  fputc('\n', fil);
	}
      fputc ('\n', fil) ;
      written = 1;
    }

  if ( seq->base_quality )
    {
      fprintf(fil,"BaseQuality : %s\n", dictName (seqDict, i) );
      written = write_base_quality( seq->base_quality, fil );
    }

  if ( seq->base_position )
    {
      fprintf(fil,"BasePosition : %s\n", dictName (seqDict, i) );
      written = write_base_position( seq->base_position, fil );
    }

  return written;
}


/* parse a read name to determine if it is a terminator or a long read */
/* return FALSE if the name does not contain a dot and so is unlikely to be a read anyway */

/* NOTE::::  this function is not compatible with CAF */

int 
get_read_type( char *s, int *is_long, int *is_terminator )
{
  int has_dot=FALSE;

  *is_long=FALSE, *is_terminator=FALSE;

  while( *s && *s != '.' )
    s++;

  if ( *s == '.' )
    {
      has_dot = TRUE;
      s++;
      if ( strchr( s, 't' ) )
	*is_terminator = TRUE;
      if ( strchr( s, 'l' ) )
	*is_long = TRUE;
    }
  
  return has_dot;
}
  
      
int 
tagCmp( void *A, void *B )
{
  TAG *a = A;
  TAG *b = B;
  int n;

  if ( ! (n = a->type-b->type ) )
    if ( ! (n = a->x1-b->x1) )
      if ( ! (n=a->x2-b->x2) )
	if ( a->text && b->text )
	  n = strcmp(a->text,b->text);

  return n;
}

int
noteCmp (void *A, void *B)
{
  NOTE *a = A;
  NOTE *b = B;
  int n;

  if (0 != (n = a->type      - b->type))      return n;
  if (0 != (n = a->ctime_top - b->ctime_top)) return n;
  if (0 != (n = a->ctime     - b->ctime))     return n;
  if (0 != (n = a->mtime_top - b->mtime_top)) return n;
  if (0 != (n = a->mtime     - b->mtime))     return n;
  return strcmp(a->text, b->text);
}

/* Functions for adding tag-like attributes */

Array
new_generic_tag( cafAssembly *CAF, Array tags, char *type, int x1, int x2, char *text )
{

  TAG *tag;
  
  if (! tags )
    tags = arrayHandleCreate(10,TAG, CAF->handle);

  tag = arrayp(tags,arrayMax(tags),TAG);

  dictAdd(CAF->tagTypeDict,type,&tag->type);

  tag->x1 = x1;
  tag->x2 = x2;

  if ( text && *text )
    tag->text = strnew(text,CAF->handle);
  else
    tag->text = NULL;

  return tags;
}

/* add a new tag to a sequence. 

Will create the tag array if needed 

 return value is the Tag. Text is protected by "" */

TAG *
newTag( CAFSEQ *seq, cafAssembly *CAF, char *type, int x1, int x2, char *text )
{

  seq->tags = new_generic_tag( CAF, seq->tags, type, x1, x2, text );

  return arrp( seq->tags, arrayMax(seq->tags)-1, TAG );
}

TAG *
newSvec( CAFSEQ *seq, cafAssembly *CAF, char *type, int x1, int x2, char *text )
{

  seq->svector = new_generic_tag( CAF, seq->svector, type, x1, x2, text );

  return arrp( seq->svector, arrayMax(seq->svector)-1, TAG );
}

    
TAG *
newCvec( CAFSEQ *seq, cafAssembly *CAF, char *type, int x1, int x2, char *text )
{

  seq->cvector = new_generic_tag( CAF, seq->cvector, type, x1, x2, text );

  return arrp( seq->cvector, arrayMax(seq->cvector)-1, TAG );
}

TAG *
newClipping( CAFSEQ *seq, cafAssembly *CAF, char *type, int x1, int x2, char *text )
{

  seq->clipping = new_generic_tag( CAF, seq->clipping, type, x1, x2, text );

  return arrp( seq->clipping, arrayMax(seq->clipping)-1, TAG );
}


Array 
addGolden( int seq_id, cafAssembly *CAF )
{
  TAG *tag, Tag ;
  int ok = 0;
  char *word;
  CAFSEQ *newseq;
  CAFSEQ *seq = arrp( CAF->seqs, seq_id, CAFSEQ );

  if ((word = freeword()) != 0)
    {
      dictAdd (CAF->seqDict, word, &Tag.type) ;
      newseq = arrayp(CAF->seqs,Tag.type,CAFSEQ);
      newseq->id = Tag.type;
      if (freeint (&Tag.x1) && freeint (&Tag.x2) )
	{
	  if ((word = freeword()) != 0)	/* comment */
	    { Tag.text = strnew(word,CAF->handle);
	    }
	  else
	    Tag.text = NULL;
	  
	  
	  if (!seq->golden_path)
	    seq->golden_path = arrayHandleCreate (1000, TAG, CAF->handle) ;
	  
	  tag = arrayp(seq->golden_path, arrayMax(seq->golden_path), TAG) ;
	  tag->x1 = Tag.x1;
	  tag->x2 = Tag.x2;
	  tag->type = Tag.type;
	  tag->text = Tag.text;
	  
	  ok = 1;
	}
    }
  if ( ! ok )
    messout ("Faulty tag at line %d: %s %d %d", freestreamline (level), dictName(CAF->seqDict,Tag.type),Tag.x1, Tag.x2);

  return seq->golden_path;
}


Array 
addTag( CAFSEQ *seq, cafAssembly *CAF, Array tags, DICT *tagTypeDict )
{

  TAG *tag, Tag ;
  int ok = 0;
  char *word;

  if ((word = freeword()) != 0)
    {
      dictAdd (tagTypeDict, word, &Tag.type) ;
      if ( ! strcmp( word, "MISS" ) ) /* missaligned reads */
	seq->bad = TRUE;

      if (freeint (&Tag.x1) && freeint (&Tag.x2) )
	{
	  if ((word = freeword()) != 0)	/* comment */
	    Tag.text = strnew(word,CAF->handle);
	  else
	    Tag.text = NULL;
	  

	  if (!tags)
	    tags = arrayHandleCreate (8, TAG, CAF->handle) ;

	  if ( Tag.x1 > Tag.x2 )
	    {
	      int n = Tag.x1;
	      Tag.x1 = Tag.x2;
	      Tag.x2 = n;
	    }
	  tag = arrayp(tags, arrayMax(tags), TAG) ;
	  tag->x1 = Tag.x1;
	  tag->x2 = Tag.x2;
	  tag->type = Tag.type;
	  tag->text = Tag.text;
	  
	  ok = 1;
	}
    }
  if ( ! ok )
    messout ("Faulty tag at line %d: %s %d %d", freestreamline (level), dictName(tagTypeDict,Tag.type),Tag.x1, Tag.x2);

  return tags;
}


Array 
addNote( CAFSEQ *seq, cafAssembly *CAF, Array notes, DICT *noteTypeDict )
{

    NOTE *note, Note ;
    int ok = 0;
    char *word;

    /*
     * Next token from the input is the note 'type'. Its a string that
     * represents a 4 byte integer. Its also added to the dictionary of
     * valid note types. dictAdd packs the string into the Note.type field.
     */
    if((word = freeword()) != 0) {

        time_t ctime, mtime; /* Temp copy of time stamps */

        dictAdd (noteTypeDict, word, &Note.type);

        /*
         * Get the change and modification times. These are stored as
         * four 32 bit integers (one pair each) that store the upper
         * and lower 4 byte blocks of the time() value.
         */
        if (freetime_t(&ctime) && freetime_t(&mtime)) {

            /*
             * Convert a, potentially 64 bit time_t into the neccessary parts
             * for a GAP Note structure.
             */
            unpackTimet(ctime, &Note.ctime_top, &Note.ctime);
            unpackTimet(mtime, &Note.mtime_top, &Note.mtime);

            /*
             * Next token in the input is the actual comment string. This will
             * be the complete contents of the double quoted string found by
             * the tokeniser.
             */
            if ((word = freeword()) != 0) { /* comment text */
	        Note.text = strnew(word,CAF->handle);
	    } else {
	        Note.text = NULL;
            }

#ifdef DEBUG
            printf("Note Found: %d: %s %d %d %d %d: \"%s\"\n",
                freestreamline(level), dictName(noteTypeDict, Note.type),
                Note.ctime_top, Note.ctime, Note.mtime_top, Note.mtime,
                Note.text);
#endif

	    if (!notes)
	        notes = arrayHandleCreate (8, NOTE, CAF->handle) ;

	    note = arrayp(notes, arrayMax(notes), NOTE) ;

            *note = Note; /* Make a copy of the complete structure */

	    ok = 1; /* Success */
        }

    }

    if ( ! ok )
        messout ("Faulty note at line %d", freestreamline(level));

    return notes;
}

/**
 * Unpack a 64 bit time value into the upper and lower 32 bit values.
 * This makes a check of the current size of time_t structure and will
 * assign zero to the upper bits if the time_t is 32 bits.
 *
 * @param time is the input value of a time (as a time_t value).
 * @param u32bits is the upper 32 bit segment of the time (or zero)
 * @param l32bits is the lower 32 bit segment of the time (or time)
 */
static void unpackTimet(time_t time, int *u32bits, int *l32bits) {

#if SIZEOF_TIME_T == 4
        *l32bits = (int)time;
        *u32bits = 0;
#elif SIZEOF_TIME_T == 8
        *l32bits = (int)(time & 0xFFFFFFFF);   
        *u32bits = (int)((time>>32) & 0xFFFFFFFF);   
#else
#error Unsupported time_t size: SIZEOF_TIME_T in (unpackTimet())
#endif
}

/* test if a sequence object has any attributes */

int 
interesting_sequence( CAFSEQ *seq )
{
  return ( seq->assinf || seq->tags || seq->other || seq->type || seq->dye || seq->primer || 
	   seq->process_status || seq->insert_size1 || seq->insert_size2 || seq->ligation_no ||
	   seq->is_circular || seq->edit || seq->template_id || seq->golden_path || 
	   seq->svector || seq->cvector || seq->clipping || seq->pad_state || seq->is_stolen);
}

/**
 * Write out an array of tags to a file.
 *
 * @param tags is the array of tags to write out.
 * @param tagTypeDict is a dictionary of tag types.
 * @param tagname is the class of tag (Tag, Seq_vec, Clipping etc.)
 * @param fil is the FILE * to write to
 */
int 
writeTags( Array tags, DICT *tagTypeDict, char *tagname, FILE *fil)
{
  TAG *tag, *tag1;
  int j;
  int n=0;
  int ok;

  if ( tags )
    {
      arraySort(tags,tagCmp);
      tag = arrp(tags,0,TAG);
      for (j = 0 ; j < arrayMax(tags) ; ++j)
	{
	  tag1 = arrp(tags, j, TAG) ;
	  ok = 1;
	  if ( ok && ( j == 0 || tagCmp(tag,tag1)) )
	    {
	      fprintf (fil, "%s %s %d %d", tagname, 
		       dictName (tagTypeDict, tag1->type), tag1->x1, tag1->x2) ;
	      if (tag1->text)
		fprintf (fil, " %s", freeprotect(tag1->text)) ; 
	      fputc ('\n', fil) ;
	      n++;
	    }
	  tag = tag1;
	}
    }
  return n;
}

/**
 * Write out an array of notes to a file.
 *
 * @param notes is the array of notes to write out.
 * @param noteTypeDict is a dictionary of note types.
 * @param note_name is the class of note (currently only 'Note')
 * @param fil is the FILE * to write to
 */
int 
writeNotes(Array notes, DICT *noteTypeDict, char *note_name, FILE *fil)
{
  NOTE *note, *note1;
  int j;
  int n=0;
  int ok;

  if ( notes )
    {
      arraySort(notes, noteCmp);
      note = arrp(notes, 0, NOTE);
      for (j = 0 ; j < arrayMax(notes) ; ++j)
	{
	  note1 = arrp(notes, j, NOTE) ;
	  ok = 1;
	  if ( ok && ( j == 0 || noteCmp(note, note1)) )
	    {
#if SIZEOF_TIME_T == 4
	      fprintf (fil, "%s %s %d %d", note_name, 
		       dictName(noteTypeDict, note1->type),
		       note1->ctime, note1->mtime);
#elif SIZEOF_TIME_T == 8
	      fprintf (fil, "%s %s %"PRIu64"d %"PRIu64"d", note_name, 
		       dictName(noteTypeDict, note1->type),
		       (((uint64_t) note1->ctime_top << 32)
			+ (((unsigned long)note1->ctime) & 0xFFFFFFFF)),
		       (((uint64_t) note1->mtime_top << 32)
			+ (((unsigned long)note1->mtime) & 0xFFFFFFFF)));
#else
#error Unsupported time_t size: SIZEOF_TIME_T in (writeNotes())
#endif
	      if (note1->text)
		fprintf (fil, " %s", freeprotect(note1->text)) ; 
	      fputc ('\n', fil) ;
	      n++;
	    }
	  note = note1;
	}
    }
  return n;
}

char *
itoa_cache( int i )
{
/* a cache to store text representations of small integers for fast output */
  static char buf[1000][10];
  static int initialised=0;
  static char buffer[10];

  if ( ! initialised )
    {
      int j;
      initialised = 1;
      for(j=0;j<1000;j++)
	sprintf(buf[j],"%d ", j);
    }

  if ( i >= 0 && i <1000 )
    return buf[i];
  else
    {
      sprintf(buffer, "%d ", i);
      return buffer;
    }
}

#define MAX_QUAL_VAL 100

int  
write_base_quality( Array base_quality, FILE *fil )
{
  int j;
  char buffer[1056], *s, *t;
  int written = 0;
  int n;

  if ( base_quality )
    {
      s = buffer;
      *s = 0;
      for(j=0;j<arrayMax(base_quality);j++)
	{
	  n = (int)arr(base_quality,j,BQ_SIZE);
	  if ( n > MAX_QUAL_VAL ) n = MAX_QUAL_VAL;

	  t = itoa_cache( n ); /* cache integer to string conversions */
	  while(*t)
	    *s++ = *t++;
	  if ( s-buffer > 60 || j == arrayMax(base_quality)-1)
	    {
	      *s++='\n';
	      *s = 0;
	      fputs(buffer , fil );
	      s = buffer;
	      *s = 0;
	    }
	}
/*      if ( *buffer )
	{
	  *s++='\n';
	  *s = NULL;
	  fputs(buffer , fil );
	} */
      fputc ('\n', fil) ;
      written = 1;
    }
  return written;
}

int  
write_base_position( Array base_position, FILE *fil )
{
  int j;
  char buffer[1056], *s, *t;
  char tbuffer[1056];
  int written = 0;
  int n;

  if ( base_position )
    {
      s = buffer;
      *s = 0;
      for(j=0;j<arrayMax(base_position);j++)
	{
	  n = (int)arr(base_position,j,BP_SIZE);
	  sprintf(tbuffer,"%d ",n); t = tbuffer;

	  while(*t)
	    *s++ = *t++;
	  if ( s-buffer > 60 || j == arrayMax(base_position)-1)
	    {
	      *s++='\n';
	      *s = 0;
	      fputs(buffer , fil );
	      s = buffer;
	      *s = 0;
	    }
	}
      fputc ('\n', fil) ;
      written = 1;
    }
  return written;
}




cafAssembly *
readCAF( int argc, char ** argv, char *filename )
{
  FILE *fp;
  cafAssembly *CAF=NULL;
  int help;

  help = clcheck("-help", argc, argv );
  fp = argfile("-caf", "r", argc, argv, filename );

  if ( help )
    {}
  else
    {
      if ( ! fp ) 
	{
	  fp=stdin;
	  strcpy(filename,"/dev/tty");
	}
      CAF = readCafAssembly( fp, filename, NULL );
    }

  /* The acedb code called by readCafAssembly should have closed fp
     if it wasn't stdin  */
  if (fp == stdin) { fclose(fp); }

  return CAF;
}
    

void 
writeCAF( cafAssembly *Ass, char *header, int argc, char **argv )
{
  FILE *fp;
  char filename[256];
  strcpy(filename,"stdout");

  if ( (fp = argfile("-out=%s","w",argc,argv, filename) ) || (fp=stdout) )
    writeCafAssembly ( Ass, fp, header );
}

void 
sanitiseCAF( cafAssembly *CAF )
{
/* Performs the following checks and operations:

   Removes contigs with no good readings
   Removes references in golden_path to bad or unaligned readings
   Removes bad sequences
   
   Reads whose base quality measures are all 99 or 100 are set to 0

   Note that unaligned good sequences are not removed  */


  CAFSEQ *seq, *seq2, *contig;
  Array golden_path, seqs;
  int bad, seq_id, last_id = 0;
  int i, j, k;
  TAG *tag, *tag2;
  int deleted=0;
  ASSINF *a;
  int ok = 0;

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp(CAF->seqs,i,CAFSEQ);

      /*
      if ( is_read == seq->type && seq->base_quality )
	{
	  k = 0;
	  for(j=0;j<arrayMax(seq->base_quality);j++)
	    if ( arr(seq->base_quality,j,BQ_SIZE) > 98 )
	      k++;
	  
	  if ( seq->is_stolen || (k == arrayMax(seq->base_quality) ) )
	    {
	      fprintf(stderr,"Downgrading BaseQuality for %s\n", dictName(CAF->seqDict,i));
	      arrayDestroy(seq->base_quality);
	      seq->base_quality = NULL;
	    }
	}
      */
    }

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      contig = arrp(CAF->seqs,i,CAFSEQ);
      if ( contig->type == is_contig )
	{
	  if ( contig->bad == TRUE || ! contig->assinf || arrayMax(contig->assinf) == 0 ) /* remove contigs with no assembled_from lines */
	    {
	      contig->bad = TRUE;
	      deleted++;
/*	      fprintf(stderr, "Removing %s - no readings\n", dictName(CAF->seqDict,contig->id )); */
	    }
	  else if ( contig->golden_path ) /* now check the golden_path for deleted readings */
	    {
	      bad = 0;
	      for(j=0;j<arrayMax(contig->golden_path);j++)
		{
		  tag = arrp(contig->golden_path,j,TAG);
		  seq_id = tag->type;
		  seq = arrp(CAF->seqs,seq_id,CAFSEQ);
		  if ( seq->bad == TRUE )
		    tag->type = -1; /* mark for deletion */
		  else if ( j == 0 || seq_id != last_id )
		    {
		      for(k=0;k<arrayMax(contig->assinf);k++)
			{
			  a = arrp(contig->assinf,k,ASSINF);
			  ok = 0;
			  if ( a->seq == seq_id )
			    {
			      ok = 1;
			      break;
			    }
			}
		      if ( ok )
			{
/*			  fprintf(stderr, "Removing %s from GoldenPath\n", dictName(CAF->seqDict,seq->id )); */
			  tag->type = -1; /* mark for deletion */
			  bad++;
			  deleted++;
			}
		    }
		  last_id = seq_id;
		}
	      if ( bad )
		{
		  golden_path = arrayHandleCreate(arrayMax(contig->golden_path), TAG,CAF->handle );
		  for(j=0;j<arrayMax(contig->golden_path);j++)
		    {
		      tag = arrp(contig->golden_path,j,TAG);
		      if ( tag->type != -1 )
			{
			  tag2 = arrayp(golden_path,arrayMax(golden_path),TAG);
			  memcpy(tag2,tag,sizeof(TAG));
			}
		    }
		  arrayDestroy(contig->golden_path);
		  contig->golden_path = golden_path;
		}
	    }
	}
    }
  if ( deleted ) /* delete the bad sequences */
    {
      seqs = arrayHandleCreate(arrayMax(CAF->seqs),CAFSEQ,CAF->handle);
      for(i=0;i<arrayMax(CAF->seqs);i++)
	{
	  seq = arrp(CAF->seqs,i,CAFSEQ);
	  if ( seq->bad == FALSE )
	    {
	      seq2 = arrayp(seqs,arrayMax(seqs),CAFSEQ);
	      memcpy(seq2,seq,sizeof(CAFSEQ));
	    }
	  else /* clean up a bit */
	    {
	      if ( seq->dna )
		stackDestroy(seq->dna);
	      if ( seq->base_quality )
		arrayDestroy( seq->base_quality );
	      if ( seq->base_position )
		arrayDestroy( seq->base_position );
	      if ( seq->assinf )
		arrayDestroy( seq->assinf );
	    }
	}
      arrayDestroy(CAF->seqs);
      CAF->seqs = seqs;
    }
}

/* Functions to accelerate reading by hashing an Object or Attribute type to a function */
/* Thse are all internal to readCafAssembly() */
int
parseAssembled_from( int seq_id, cafAssembly *CAF )
{
  ASSINF *a;
  char *word;
  CAFSEQ *newseq;
  CAFSEQ *seq = arrp( CAF->seqs, seq_id, CAFSEQ );

  if (!seq->assinf)
    seq->assinf = arrayHandleCreate (8, ASSINF,CAF->handle) ;
  a = arrayp(seq->assinf, arrayMax(seq->assinf), ASSINF) ;
  
  if ( seq->type == is_read )
    fprintf(stderr, "WARNING: Assembled_from info found with read object\n");
  
  if (!(word = freeword()))
    ABORT("No sequence name after Assembled_from") ;

  dictAdd (CAF->seqDict, word, &a->seq) ;
  newseq = arrayp(CAF->seqs,a->seq,CAFSEQ);
  newseq->id = a->seq; /* make sure seq object allocated */

  if (!freeint (&a->s1) || !freeint (&a->s2) || 
      !freeint (&a->r1) || !freeint (&a->r2))
    ABORT("4 integers not found on Assembled_from line") ;

  return 1;
}

int
parseContig_order( int seq_id, cafAssembly *CAF )
/* for assembly and group objects */
{
  GROUP_ORDER *g ;
  char *word;
  CAFSEQ *newseq;
  CAFSEQ *seq = arrp( CAF->seqs, seq_id, CAFSEQ );

  if (!seq->order)
    seq->order = arrayHandleCreate (10, GROUP_ORDER,CAF->handle) ;
  g = arrayp(seq->order, arrayMax(seq->order), GROUP_ORDER) ;
  
  if (!(word = freeword()))
    ABORT("No sequence name after Group_order or Contig_order") ;

  dictAdd (CAF->seqDict, word, &g->seq) ;
  newseq = arrayp(CAF->seqs,g->seq,CAFSEQ);
  newseq->id = g->seq; /* make sure seq object allocated */
  if (!freedouble (&g->position) )
    ABORT("Position not found on Contig_order or Group_order line") ;
  if ( seq->type != is_assembly && seq->type != is_group )
    fprintf(stderr, "WARNING: group info found with non- assembly or group object\n");

  return 1;
}

int 
parseAlign_to_SCF( int seq_id, cafAssembly *CAF )
{ 
  ASSINF *a ;
  CAFSEQ *seq = arrp( CAF->seqs, seq_id, CAFSEQ );
  
  if (!seq->assinf)
    seq->assinf = arrayHandleCreate (8, ASSINF,CAF->handle) ;
  a = arrayp(seq->assinf, arrayMax(seq->assinf), ASSINF) ;
  a->seq = seq->id;
  if (!freeint (&a->s1) || !freeint (&a->s2) || 
      !freeint (&a->r1) || !freeint (&a->r2))
    ABORT("4 integers not found on Align_to_SCF") ;
/*  fprintf(stderr, "Align_to_SCF\n"); */
  return 1;

}

int 
parseGolden_path( int seq_id, cafAssembly *CAF )
{
  addGolden( seq_id, CAF );
  return 1;
}


int 
scf_file( int seq_id, cafAssembly *CAF )
{
  char *word;
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);
  if ((word = freeword()) != 0)
    { 
      seq->SCF_File = strnew(word,CAF->handle);

      if ( seq->type == unknown_sequence )
	seq->type = is_read;
      return 1;
    }  
  return 0;
}


int 
parseStaden_id( int staden_id, cafAssembly *CAF )
{
  char *word;
  CAFSEQ *seq = arrp(CAF->seqs, staden_id, CAFSEQ);
  if ((word = freeword()) != 0)
    { 
      seq->staden_id = atoi(word);
      return 1;
    }  
  return 0;
}

  
int
parseTag( int seq_id, cafAssembly *CAF )
{
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);
  seq->tags = addTag( seq, CAF, seq->tags, CAF->tagTypeDict);
  return 1;
}


int
parseNote( int seq_id, cafAssembly *CAF )
{
  CAFSEQ *seq = arrp(CAF->seqs, seq_id, CAFSEQ);
  seq->notes = addNote( seq, CAF, seq->notes, CAF->noteTypeDict);
  return 1;
}

int
parseClipping( int seq_id, cafAssembly *CAF )
{
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);
  seq->clipping = addTag( seq, CAF, seq->clipping, CAF->tagTypeDict);
  return 1;
}

int
parseSeq_vec( int seq_id, cafAssembly *CAF )
{
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);
  seq->svector = addTag( seq, CAF, seq->svector, CAF->tagTypeDict);
  return 1;
}

int
parseClone_vec( int seq_id, cafAssembly *CAF )
{
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);
  seq->cvector = addTag( seq, CAF, seq->cvector, CAF->tagTypeDict);
  return 1;
}

int 
parseDNA( int seq_id , cafAssembly *CAF )
{
  char *word;
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);

  if (stackExists(seq->dna) ) 
    stackDestroy(seq->dna);

  seq->dna = stackHandleCreate ( 800, CAF->handle) ; 

  while (freecard (level) && (word = freeword()))
    catText (seq->dna, word) ;

  seq->len = strlen(stackText(seq->dna,0));

  return 1;
}

int
parseBaseQuality( int seq_id , cafAssembly *CAF )
{
  int len=1000;
  int qual;
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);

  if ( seq->dna )
    len = strlen(stackText(seq->dna,0))+2;

  if ( arrayExists( seq->base_quality ))
    arrayDestroy(seq->base_quality);

  seq->base_quality = arrayHandleCreate(len,BQ_SIZE,CAF->handle);
  while( freecard(level) && freeint(&qual))
    do 
      *arrayp(seq->base_quality,arrayMax(seq->base_quality),BQ_SIZE) = qual;
    while ( freeint(&qual));

  return 1;
}

int
parseBasePosition( int seq_id , cafAssembly *CAF )
{
  int len=1000;
  int pos;
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);

  if ( seq->dna )
    len = strlen(stackText(seq->dna,0))+2;

  if ( arrayExists( seq->base_position ))
    arrayDestroy(seq->base_position);

  seq->base_position = arrayHandleCreate(len,BP_SIZE,CAF->handle);
  while( freecard(level) && freeint(&pos))
    do 
      *arrayp(seq->base_position,arrayMax(seq->base_position),BP_SIZE) = pos;
    while ( freeint(&pos));

  return 1;
}

int 
parseMinorSequenceStuff( char *word, int seq_id, cafAssembly *CAF )
{
/* handles minor sequence tags which don't have a big performance hit */

  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);

  if ( legal_string( word, sequence_text, sequence_types, &seq->type ) ) {}
  else if ( legal_string( word, pad_strings, pad_types, &seq->pad_state ) ) {}
  else if ( !strcmp(word, "Dye") && (word=freeword()) && legal_string( word, dye_strings, dye_types, &seq->dye ) ) {}
  else if ( !strcmp(word, "Primer") )
    {
      if ( (word=freeword()) && legal_string( word, primer_strings, primer_types, &seq->primer ) )
	{
	  if ( seq->primer == custom_primer )
	    {
	      if ((word=freeword()) != 0)
		seq->custom_primer = strnew(word,CAF->handle);
	    }
	}
    }
  else if (!strcmp(word,"Is_circular") )
    seq->is_circular = TRUE;
  
  else if (!strcmp (word, "Stolen"))
    { 
      seq->is_stolen = TRUE;
      if ((word = freeword()) != 0)
	seq->stolen_from = strnew(word,CAF->handle);
    }
  else if (!strcmp (word, "ProcessStatus"))
    { 
      if ( ((word = freeword()) != 0)
	   && (legal_string( word, process_strings, process_types, &seq->process_status ) ) )
	{
	  if ((word = freeword()) != 0)
	    seq->process_reason = strnew(word,CAF->handle);
	  else
	    seq->process_reason = NULL;
	}
    }
  else if ( !strcmp(word,"Strand") )
    {
      if ((word=freeword()) != 0)
	{
	  if (!strcmp (word, "Forward"))
	    { 
	      seq->strand = forward_strand;
	    }
	  else if (!strcmp (word, "Reverse"))
	    { 
	      seq->strand = reverse_strand;
	    }
	}
      else
	{
	  ABORT("NO Strand Direction");
	}
    }
  else if (!strcmp (word, "Template"))
    { 
      if ((word = freeword()) != 0)
	{ 
	  dictAdd(CAF->templateDict,word,&seq->template_id);
	}
    }
  else if (!strcmp(word, "Clone"))
    {
      if ((word = freeword()) != 0)
	{
	  dictAdd(CAF->cloneDict, word, &seq->clone_id);
	}
    }
  else if (!strcmp(word, "Cloning_vector"))
    {
      if ((word = freeword()) != 0)
	{
	  dictAdd(CAF->vectorDict, word, &seq->cloning_vector);
	}
    }
  else if (!strcmp(word, "Sequencing_vector"))
    {
      if ((word = freeword()) != 0)
	{
	  dictAdd(CAF->vectorDict, word, &seq->sequencing_vector);
	}
    }
  else if (!strcmp (word, "Insert_size"))
    {
      if (!( freeint(&seq->insert_size1) && freeint(&seq->insert_size2) ) )
	ABORT("Two integers not found following Insert_size\n");
    }
  else if (!strcmp (word, "Ligation_no"))
    {
      if ((word = freeword()) != 0)
	/* seq->ligation_no = strnew(word,CAF->handle); */
	dictAdd(CAF->ligationDict, word, &seq->ligation_no);
    }
  else
    return 0; /* matched nothing - failed*/

  return 1; /* success */
}

int
parseEdit( int seq_id, cafAssembly *CAF )
{
/* Auto edit */

  edit_struct *edit;
  char *new;
  int status, compound, consensus, stranded;
  CAFSEQ *seq = arrp(CAF->seqs,seq_id,CAFSEQ);
  
  if (!seq->edit)
    seq->edit = arrayHandleCreate(10,edit_struct,CAF->handle);
  edit = arrayp(seq->edit,arrayMax(seq->edit),edit_struct);
  if ( freeint(&edit->pos)  && 
      legal_string( freeword(), status_text,    status_count,    &status    ) && 
      legal_string( freeword(), compound_text,   compound_count,   &compound   ) &&
      legal_string( freeword(), Consensus_text, Consensus_count, &consensus ) &&
      legal_string( freeword(), strand_text,    strand_count,    &stranded  ) )
    {
      edit->status = status;
      edit->compound = compound;
      edit->consensus = consensus;
      edit->stranded = stranded;
      if ((new = freeword()) != 0)
	edit->New = *new;
      else
	edit->New = ' ';
      edit->read = seq->id;
    }
  else
    ABORT("Incomplete Edit Line");
  return 1;
}


int 
parseSequence( int seq_id, cafAssembly *CAF )
{
  char *word;
  CAFSEQ *seq; 


  while (freecard (level))
    { 
      if (!(word = freeword()))
	return 1;
      else if ( parseAttribute( word, seq_id, CAF ) ) 
	{}
      else if ( parseMinorSequenceStuff( word, seq_id, CAF ) )
	{}
      else
	{ 
	  freeback () ;
	  seq = arrp(CAF->seqs,seq_id,CAFSEQ);
	  if (! stackExists(seq->other) )
	    seq->other = stackHandleCreate (100, CAF->handle) ;
	  pushText (seq->other, freepos()) ; /* whole line */
	}
    }
  seq = arrp(CAF->seqs,seq_id,CAFSEQ);
  if ( seq->is_terminator )
    seq->dye = dye_terminator;


  return 1;

}

void initObjectParse(void)
{
  if ( ! objectDict )
    {
      int id;

      objectDict = dictCreate(100);
      object = arrayCreate(100,void*);

      dictAdd(objectDict,"DNA",&id);
      *arrayp(object,id,void*) = (void*)parseDNA;

      dictAdd(objectDict,"BaseQuality",&id);
      *arrayp(object,id,void*) = (void*)parseBaseQuality;

      dictAdd(objectDict,"BasePosition",&id);
      *arrayp(object,id,void*) = (void*)parseBasePosition;

      dictAdd(objectDict,"Sequence",&id);
      *arrayp(object,id,void*) = (void*)parseSequence;
    }
}

/* initialise the attribute structures 
   
Creates the mapping of attribute name onto parser function
so that the functions are hashed onto the attribute names (for speed)

*/
void initParseAttribute(void)
{
  if ( ! attributeDict )
    {
      attributeDict = dictCreate(100); /* the words to match */
      attribute = arrayCreate(100,void*); /* the functions to call */

      addToParser( "Assembled_from", (void*)parseAssembled_from );
      addToParser( "Contig_order", (void*)parseContig_order );
      addToParser( "Group_order", (void*)parseContig_order );
      addToParser( "Align_to_SCF", (void*)parseAlign_to_SCF );
      addToParser( "Golden_path",(void*)parseGolden_path );
      addToParser( "SCF_File", (void*)scf_file );
      addToParser( "Tag", (void*)parseTag );
      addToParser( "Note", (void*)parseNote );
      addToParser( "Clipping", (void*)parseClipping );
      addToParser( "Seq_vec", (void*)parseSeq_vec );
      addToParser( "Clone_vec", (void*)parseClone_vec );
      addToParser( "Edit", (void*)parseEdit );
      addToParser( "Staden_id", (void*)parseStaden_id );
    }
}

void
addToParser( char *string, void *func )
{
  int id;

  dictAdd(attributeDict,string,&id);
  *arrayp(attribute,id,void*) = func;
}


int 
parseAttribute( char *attr, int seq_id, cafAssembly *CAF )
{
/* search for a procedure matching attr and execute it */

  int id;
  void *func;
  int (*f)( int, void * );

  if ( dictFind( attributeDict, attr, &id )  ) 
    {
      if ((func = arr( attribute, id, void * )) != 0)
	{
	  f = func;
	  return (*f)( seq_id, CAF );
	}
    }
  return 0; /* failure */
}

int 
parseObject( char *obj, int seq_id, cafAssembly *CAF ) 
/* parse an object. Similar to parseAttribute but we may have the same
   string for an attribute and an object so need to maintain separate
   parsers */
{
  int id;
  void *func;
  int (*f)( int , void * );

  if ( dictFind( objectDict, obj, &id )  )
    {
      if ((func = arr( object, id, void * )) != 0)
	{
	  f = func;
	  return (*f)( seq_id, CAF );
	}
    }
  return 0;
}

  
CAFSEQ *
reverseContig( CAFSEQ *contig, cafAssembly *CAF )

/* reverse-complement a contig and its associated data 

NOTE: this actually works on all non-read objects.

Return value is a pointer to the reversed sequence, or NULL on failure

The sequence MUST have DNA

*/

{

  int len=0;
  char *dna = 0;
  int k;
  BQ_SIZE q1, q2;
  BP_SIZE p1, p2;
  ASSINF *a;

/* check type */

  if ( is_read == contig->type )
    return NULL;

/* check it has DNA */

  if ( contig->dna )
    {

      dna = stackText(contig->dna,0);
      len = strlen(dna);
    }
  
  if ( len == 0 )
    {
      fprintf(stderr, "ERROR: Cannot reverse %s - no DNA\n", dictName(CAF->seqDict,contig->id) );
      return NULL;
    }

/* reverse the base-quality info */

  if ( contig->base_quality )
    {
      if ( len != arrayMax(contig->base_quality) )
	{
	  fprintf(stderr, "ERROR: BaseQuality (%d) and DNA (%d) different lengths for %s\n", arrayMax(contig->base_quality), len, dictName(CAF->seqDict,contig->id) );
	  return NULL;
	}

      for(k=0;k<len/2;k++)
	{
	  p1 = array(contig->base_quality,k,BQ_SIZE);
	  p2 = array(contig->base_quality,len-k-1,BQ_SIZE);
	  *arrp(contig->base_quality,k,BQ_SIZE) = p2;
	  *arrp(contig->base_quality,len-k-1,BQ_SIZE) = p1;
	}
    }

/* reverse the base-position info */

  if ( contig->base_position )
    {
      if ( len != arrayMax(contig->base_position) )
	{
	  fprintf(stderr, "ERROR: BasePosition (%d) and DNA (%d) different lengths for %s\n", arrayMax(contig->base_position), len, dictName(CAF->seqDict,contig->id) );
	  return NULL;
	}

      for(k=0;k<len/2;k++)
	{
	  q1 = array(contig->base_position,k,BP_SIZE);
	  q2 = array(contig->base_position,len-k-1,BP_SIZE);
	  *arrp(contig->base_position,k,BP_SIZE) = q2;
	  *arrp(contig->base_position,len-k-1,BP_SIZE) = q1;
	}
    }

/* reverse the DNA */

  complement_seq(dna);

/* reverse tag-like data */

  reverse_tags( contig->clipping, len );
  reverse_tags( contig->tags, len );
  reverse_tags( contig->svector, len );
  reverse_tags( contig->cvector, len );
  reverse_tags( contig->golden_path, len );

/* Assembled_from info */

  if ( contig->assinf )
    {
      for(k=0;k<arrayMax(contig->assinf);k++)
	{
	  a = arrp(contig->assinf,k,ASSINF);
	  a->s1 = len-a->s1+1;
	  a->s2 = len-a->s2+1;
	}
    }

  return contig;
}


void
reverse_tags( Array tags, int len )
{
/* reverses tag coordinates */
  TAG *tag;
  int k;

  if ( tags )
    {
      for(k=0;k<arrayMax(tags);k++)
	{
	  tag = arrp(tags,k,TAG);
	  tag->x1 = len-tag->x1+1; /* preserve orientation */
	  tag->x2 = len-tag->x2+1;
	}
    }
}


int 
rawdata(int argc, char **argv)
/* check the command line for the RAWDATA environment variable (for SCF files) */
{
  static char rawdata[266];
  char arg[256];

  if ( getarg("-rawdata",arg,argc,argv) )
    {
      snprintf( rawdata, sizeof(rawdata), "RAWDATA=%s", arg );
      putenv( rawdata );
      return 1;
    }

  return 0;
}

void 
cafDestroy( cafAssembly *CAF )
{
  if ( CAF )
    {
      dictDestroy(CAF->seqDict);
      dictDestroy(CAF->templateDict);
      dictDestroy(CAF->tagTypeDict);
      dictDestroy(CAF->noteTypeDict);
      dictDestroy(CAF->cloneDict);
      dictDestroy(CAF->ligationDict);
      dictDestroy(CAF->vectorDict);
      handleDestroy(CAF->handle);
    }
}
