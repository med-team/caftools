/*  Last edited: Aug 15 16:23 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* CAFSEQ.H

   Contains definitions of functions in cafseq.c, concerned
   with reading and writing CAF files.

   Author - Richard Mott 

*/

#ifndef _CAFSEQ_H_
#define _CAFSEQ_H_


#ifdef CPLUSPLUS
extern "C" { 
#endif

/* Reading */

/* adds a tag - used internally in readCafAssembly */

  Array addTag( CAFSEQ *seq, cafAssembly *CAF, Array tags, DICT *tagTypeDict ); 

/* parse golden_path info (used internally in cafseq.c) */

  Array addGolden(  int seq, cafAssembly *CAF);

/* parse an attribute */

  int parseAttribute( char *name, int seq_id, cafAssembly *CAF );

void initParseAttribute();
void addToParser( char *string, void *func );

int parseObject( char *name, int seq_id, cafAssembly *CAF );
void initObjectParse();

void parse_tags( int seq, DICT *tagTypeDict );

int get_read_type( char *s, int *is_long, int *is_terminator );

int interesting_sequence( CAFSEQ *seq );


/* Writing */

/* write an array of tags */

int writeTags( Array tags, DICT *tagTypeDict, char *tagname, FILE *fil);

/* write an array of notes */
int writeNotes(Array notes, DICT *noteTypeDict, char *note_name, FILE *fil);

/* sorting function for tags */
int tagCmp( void *A, void *B ); /* for comparing TAG * objects */

/* sorting function for notes */
int noteCmp( void *A, void *B ); /* for comparing NOTE * objects */

char *itoa_cache( int i );

/* Other */


/* 
   Performs the following checks and operations:

   Removes contigs with no good readings
   Removes references in golden_path to bad or unaligned readings
   Removes bad sequences
    
   Reads whose base quality measures are all 99 or 100 are set to 0
    
   Note that unaligned good sequences are not removed
*/
  
void sanitiseCAF(cafAssembly *CAF);

  
#ifdef CPLUSPLUS
}
#endif

/******************* end of file *****************/

#endif


