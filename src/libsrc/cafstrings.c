/*  Last edited: Jan  4 20:48 1997 (badger) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* CAFSTRINGS 

   Contains global definitions of strings

*/

#include"caf.h"

/* CAFSEQ strings */

char *sequence_text[sequence_types] = {   "Unknown_sequence",  "Is_read", "Is_contig", "Is_vector", "Is_group", "Is_assembly", "Is_spliced" };

char *primer_strings[primer_types] = { "Unknown_primer", "Universal_primer", "Custom" };
char *dye_strings[dye_types] = {"Unknown_dye", "Dye_primer", "Dye_terminator" };
char *pad_strings[pad_types] = { "Unknown_state", "Padded", "Unpadded" };
char *process_strings[process_types] = { "unknown", "PASS", "QUAL", "SVEC", "CVEC", "CONT" };


int binary_mode=FALSE;


/* Auto-edit strings */

char *status_text[status_count] = { "+", "replace", "insert", "delete", "unclear", "too_low", "masked_out", "unknown" };

char *strand_text[strand_count] = { "none", "forward", "reverse" , "terminator", "", "double", "terminated", "forward_terminated", "reverse_terminated", "", "all_stranded" };

char *Consensus_text[Consensus_count] = { "weak", "medium", "strong" };

char *compound_text[compound_count] = { "isolated",  "compound" };

char *TAG_text[status_count] = { "YES", "REP", "INS", "DEL", "UNC", "LOW", "UNK" };

