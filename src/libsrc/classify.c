/*  Last edited: Jun 13 12:54 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Sep 20 15:19 1996 (rmott) */

/* $Id: classify.c 15153 2005-06-16 11:22:30Z rmd $ */

#include <stdio.h>
#include <string.h>
#include "caf.h"
#include "trace_edit.h"
#include "myscf.h"
#include "cl.h"
#include "trace_quality.h"

typedef struct
{
  int id, calls;
}
pair_struct;

int pstcmp();

/* 
   propose_edits()

   This is the heart of the auto-editor 

   It analyses a contig alignment strucure to generate the alignment
   status array at each contig position, which indicates whether an
   edit is required, and of so, if it is safe to make the edit.

   There are three phases to the function:

   1. The contigAlignment is scanned rapidly to determine positions
   where edits are needed and the likelihood that the majority vote is
   correct, based on some simple consensus rules, but ignoring traces

   2. Optionally the edits are revisited to examine the traces and
   hopefully make more edits

   3. A post-processing step is done to down-grade edits in situations
   which have proved to be problematical

   This is the function to modify if you want to add new editing
   methods

 */

Array 
propose_edits_for_contig( contigAlignment *Align, cafAssembly *CAF, int depth, int max_run, int use_scf, float st_louis_thresh, int use_phrap_consensus )
{
  int i, j;
  Array *B;
  Array status;
  int min = Align->min, max=Align->max;
  alignStatus *S, *S1;
  Array seqs=CAF->seqs;
  CAFSEQ *seq;
  Array aligned = Align->aligned;
  int run_length;
  alignData *ad;
  Array cum;
  int window=10;
  int badness=10;
  int last_compression = -10000;

/* allocate the alignStatus array */

  status = arrayHandleCreate( max+1, alignStatus, Align->handle );
  Align->status = status;


  if ( min < CAF->from )
    min = CAF->from;
  if ( CAF->to > 0 && max > CAF->to )
    max = CAF->to;

/* mask out regions covered with masked reads */

  arrayp(status, max, alignStatus);
  for(j=min;j<=max;j++)
    {
      S = arrp(status, j, alignStatus); /* S is the alignStatus at position j */
      B = arrp(aligned, j, Array ); /* B is the array of alignData structs at position j */
      for(i=0;i<arrayMax(*B);i++)
	{
	  ad = arrp( *B, i, alignData);
	  if ( ad->id != -1 )
	    {
	      seq = arrp( seqs,ad->id,CAFSEQ);
	      if ( seq->masked == TRUE )
		S->masked = TRUE;
	    }
	}
    }

/* First classfy edits according to consensus rules only (ie ignoring
 traces) Analyse each contig position in isolation of neightbours */

  for(j=min;j<=max;j++)
    {
      S = arrp(status, j, alignStatus);
      B = arrp(Align->aligned,j,Array);

      if ( S->masked == TRUE ) /* mask out edits if covered by masked reads */
	{
	  S->consensus = Weak; /* initialise the status to be Weak */
	  S->status = masked_out;
	  S->depth = 0;
	}
      else
	{
	  if ( st_louis_thresh > 0 )
	    classify_alignment_status_st_louis( CAF, *B, S, depth, st_louis_thresh );
	  else
	    {
	      int phrap_consensus = i_base(arr(Align->cons,j,char));
	      classify_alignment_status( CAF, *B, S, depth, use_phrap_consensus, phrap_consensus ); /* classify the alignment using Sanger Rules */
	    }
	}
    }

/* Now look for complex (compound) problems - ie runs of edits */

  run_length = 0;

  cum = arrayCreate( max+1, int);
  *arrayp(cum,0,int) = 0;
  array(cum,max,int) = 0;

  for(j=min+1;j<=max;j++)
    {
      S = arrp(status,j,alignStatus);    /* alignStatus at current position */
      S1 = arrp(status,j-1,alignStatus); /* alignStatus at preceding position */

      *arrp(cum,j,int) = arr(cum,j-1,int) + proper_edit(S->status); /* cumulate the number of proper edits in the run */

      if ( proper_edit(S->status) && proper_edit(S1->status) ) 
	S->compound = S1->compound = Compound; /* set compound edit flag */

/* check for runs of edits */

      if ( ! proper_edit(S->status) || (S->status == deletion && S->depth == S->base_calls[base_i] ) )
	/*[ tests is S is not an edit or a column entirely consisting of pads ] */
	{
	  if ( run_length > max_run )
	    for(i=j-1;i>=j-run_length;i--)
	      {
		S = arrp(status,i,alignStatus);
		if ( proper_edit(S->status) )
		  {
#ifdef DEBUG 
		    printf("changing %d %d %d\n", i, run_length, max_run); 
#endif
		    S->compound = run_length;
		    if ( S->consensus )
		      S->consensus--; /* down grade edit */
		  }
	      }
	  run_length = 0;
	}
      else /* in a run of edits */
	run_length++;

/* check for compressions - this is not used at the moment*/

      if ( S->compression && last_compression + max_run > j ) /* last compression less than max_run bases earlier */
	{
	  
	}
    }

/* down-grade edits in regions containing a high density of non-contigous edits Currenlt must have > 10 edits in window of 20 positions */

  for(j=min+window;j<=max-window;j++)
    {
      if ( arr(cum,j+window,int)-arr(cum,j-window,int) > badness ) 
	{
	  for(i=j-window;i<=j+window;i++)
	    {
	      S = arrp(status,i,alignStatus);
	      S->compound = 2*window+1;
	      if ( S->consensus == Strong && proper_edit(S->status) )
		{
		  S->consensus = Medium;
		}
	    }
	}
    }

/* Now use traces to make more edits */

  if ( use_scf )
    trace_edit( Align, CAF );

  /* down grade edits which conflict with terminators AG */

  check_terminators( Align, CAF );

/* count edit stacks (vertical stacks of edits) */

  for(j=min+1;j<=max;j++)
    {
      S = arrp(status,j,alignStatus);

      if ( S->status == replace && S->consensus == Strong )
	{
	  B = arrp(aligned,j,Array);
	  for(i=0;i<arrayMax(*B); i++)
	  {
	    ad = arrp( *B, i, alignData );
	    if ( ad->id != -1 && ! ad->bad && i_base(ad->base) != S->best )
	      S->count++;
	  }
	}
    }

/* finally delete columns consisting entirely of pads */

  for(j=min+1;j<=max;j++)
    {
      S = arrp(status,j,alignStatus);

/*      if (S->status == deletion && S->depth == S->base_calls[base_i] ) */
      if ( S->depth > 0 && S->depth == S->base_calls[base_i] ) 
	{
	  S->consensus = Strong;
	  S->status = deletion;
	}
    }

  return status;
}

void 
classify_alignment_status( cafAssembly *CAF, Array alignment, alignStatus *S, int min_depth, int use_phrap_consensus, int phrap_consensus )

/* computes the status of an alignment at a single base position 

The alignment status for the position is defined by the 4-tuple ( status, stranded, consensus, compound )

status is a hint of the type of edit required and takes one of the values

  ok,                           all traces agree and base called is proper
  replace,                      a substitution 
  insert,                       an insertion 
  deleteion                     a deletion
  unclear,                      not clear what edit to do 
  too_low,                      trace coverage is too low ( < 2 ) 
  bad_trace,                    at least one trace has < 90% identity with consensus 
  unknown,                      all other contingencies 

(from enum status_types).

stranded describes the type of coverage, and takes one of the values 


  none, 
  forward_only,                 only + strands cover 
  reverse_only,                 only - strands cover 
  double_stranded,              both + and - 

(from strand_types).

consensus is the certainty that the edit is correct, and takes one of the values 

Weak                       either the most frequent base is a tie, 
                           or if double_stranded then only one 
                           strand supports the consensusa 
Medium                     ie most frequent base is not a tie, and if
                           double stranded then there are reads for the consensus on both strands
Strong                     double-stranded only - each strand considered independently has the same
                           consensus

compound says whether the row is part of a group of edits. It is not
set in this function, but status, stranded and consensus are.

Note that if a particular read is bad (has ad->bad = 1) then it is ignored

*/

{
  int j, bad;
  alignData *ad;
  int splus = 0;
  int sminus = 0;
  int st = 0;
  int snt = 0;
  int ibase;
  pair_struct tmp[anybase];
  int forward_votes[anybase], reverse_votes[anybase];
  int non_terminator_votes[anybase], terminator_votes[anybase]; /* for terminators */
  int width = arrayMax(alignment); /* the width of the alignment */
  int base0, base1, no_tie;
  int n;
  int plus_minus;
  int t_nt;

  for(j=0;j<nucleotides;j++)
    S->base_calls[j] = 0;

  for_any_base(j)
    forward_votes[j] = reverse_votes[j] = terminator_votes[j] = non_terminator_votes[j] = 0;
	  
  for(j=0;j<width;j++)
    {
      ad = arrp( alignment, j, alignData);

      /* count the number of calls for each base (note that *, n,- are "bases" */

      if ( ad && ad->id != -1 && ! ad->bad && (ibase = i_base(ad->base)) >= 0 ) /* ignore bad reads */
	{
	  S->base_calls[ibase]++;
	  
	  /* count no of terminated and + and - strands NOTE: there is no difference between + and - terminators
	   splus counts no of non-terminated + reads
	   sminus counts no of non-terminated - reads
	   st counts number of terminated reads
	   snt = splus+sminus */

	  /* Also count votes for each base on terminated,  + and - strands, excluding bad reads */

	  if ( is_terminated(ad->strand) )
	    {
	      st++;
	      terminator_votes[ibase]++;
	    }
	  else
	    {
	      snt++;
	      non_terminator_votes[ibase]++;
	      if ( is_forward(ad->strand))
		{
		  splus++;
		  forward_votes[ibase]++;
		}
	      else if ( is_reverse(ad->strand))
		{
		  sminus++;
		  reverse_votes[ibase]++;
		}
	    }

	}
    }

  /* total number of different nucleotides called, including unknowns and pads */

  S->calls = 0;
  for_each_base(j)
    S->calls += (S->base_calls[j]>0);

  /* count how many different types non-nucleotide calls (ie tells if we have N and - */

  bad = (S->base_calls[base_n]>0) + (S->base_calls[base_o]>0) + (S->base_calls[base_i]>0);

  /* count the effective depth of the alignment (ie total number of calls of all types, excluding bad reads) */

  S->depth = 0;
  for_any_base(j)
    S->depth += S->base_calls[j];

#ifdef DEBUG
  if ( S->depth == 0 )
    printf("WARNING ZERO DEPTH\n");
#endif

  /* set the strand indicator flag */

  if ( st ) /* terminator reads are present */
    {
      if ( splus && sminus )
	S->stranded = all_stranded; /* terminators and + and - reads */
      else if ( snt )
	S->stranded = terminated; /* terminators and either + or - */
      else
	S->stranded = terminator_only; /* just terminators */
    }
  else if ( splus && sminus )
    S->stranded = double_stranded; /* + and - reads - no terminators */
  else if ( splus )
    S->stranded = forward_only; /* just forward reads */
  else if ( sminus ) 
    S->stranded = reverse_only; /* just reverse reads */
  else
    S->stranded = no_stranded;

  
  /* now sort the bases in decreasing order of their calls */
      
  for_any_base(j)
    {
      tmp[j].id = j;
      tmp[j].calls = S->base_calls[j];
    }

  qsort(tmp,nucleotides,sizeof(pair_struct),pstcmp);

  for_any_base(j)
    S->base[j] = tmp[j].id;

  /* S->base[0] = tmp[0].id is the most frequent base id */

  if ( use_phrap_consensus ) /* use the phrap ie contig consensus */
    {
      base0 = phrap_consensus;
      no_tie = TRUE;
    }
  else  /* use the majority vote */
    {
      if ( tmp[0].id != base_i ) /* is the most frequent base not a pad ? */
	{

	  /* test if there IS a clear consensus (ie most frequent base is not
	     a tie) . We must deal with the case when the most frequent base
	     is an n by looking at the second-best base instead */

	  base0 = base_n;
	  base1 = base_n;
	  for_any_base(j)
	    {
	      if ( proper_base(tmp[j].id) && tmp[j].calls > 0 )
		{
		  if ( base0 == base_n ) /* set most frequent base */
		    base0 = tmp[j].id;
		  else if ( base1 == base_n )
		    base1 = tmp[j].id;
		}
	    }
    
	  if ( proper_base(base0) ) /* at least one proper base has been called */
	    {
	      if ( proper_base(base1) ) /* another proper base hase been called */
		no_tie = ( S->base_calls[base0] > S->base_calls[base1] ); /* but at lower frequency */
	      else 
		no_tie = TRUE;
	    }
	  else
	    no_tie = FALSE;

	  if ( tmp[0].id == base_n ) /* the most frequent base is an n */
	    {
	      if ( proper_base(base0) == FALSE  ) /* there is no proper base called as well */
		base0 = base_n;
	      else
		no_tie = ( no_tie && tmp[0].calls == tmp[base0].calls ); /* allow best proper base to tie with base_n */
	    }
	}
      else if ( tmp[0].id == base_i ) /* most frequent base is a delete */
	{
	  base0 = base_i;
	  base1 = tmp[1].id;
	  no_tie = ( tmp[0].calls > tmp[1].calls );
	}
    }

  S->consensus = Weak;
  S->best = base0;
  S->clear = ( no_tie && proper_base(base0)); /* clear majority for best base which must be proper */

  if  (no_tie)
    {
      S->consensus = Medium;
      if ( S->stranded == all_stranded ) /* terminators and double-stranding */
	{
	  if ( ! terminator_votes[base0] || ! forward_votes[base0] || ! reverse_votes[base0])
	    {
	      if ( st == terminator_votes[base_n] || splus == forward_votes[base_n] || sminus == reverse_votes[base_n] ) 
		/* it's ok the bad strand is all n's */
		S->consensus = Medium;
	      else
		S->consensus = Weak;
	    }
	  else
	    {
	      S->consensus = Strong;
	      
	      /* check if the highest vote on both strands is for the consensus */
	      
	      for_any_base(j)
		if ( j != base0 &&  
		    ( terminator_votes[j]     >= terminator_votes[base0]     || 
		     forward_votes[j]  >= forward_votes[base0]  || 
		     reverse_votes[j] >= reverse_votes[base0] ))
		  {
#ifdef DEBUG 
		    printf("downgrading base0: %c j: %c +:%d -:%d\n", c_base(base0), c_base(j), terminator_votes[j], non_terminator_votes[j]); 
#endif
		    S->consensus = Medium; /* down-grade */
		    break;
		  }
	    }
	}
      else if ( S->stranded == terminated )       /* if terminators present .... */
	{
	  /* now test if double stranded for the consensus - which need not be a proper base !*/
	  /* check if one of the strands has no votes for the consensus - if so then mark it as no consensus */
	  
	  if ( ! forward_votes[base0] || ! non_terminator_votes[base0] )
	    {
	      if ( st == terminator_votes[base_n] || snt == non_terminator_votes[base_n] ) /* its ok the bad strand is all n's */
		S->consensus = Medium;
	      else
		S->consensus = Weak;
	    }
	  else
	    {
	      S->consensus = Strong;
	      
	      /* check if the highest vote on both strands is for the consensus */
	      
	      for_any_base(j)
		if ( j != base0 && ( terminator_votes[j] >= terminator_votes[base0] || non_terminator_votes[j] >= non_terminator_votes[base0] ))
		  {
#ifdef DEBUG 
		    printf("downgrading base0: %c j: %c +:%d -:%d\n", c_base(base0), c_base(j), terminator_votes[j], non_terminator_votes[j]); 
#endif
		    S->consensus = Medium; /* down-grade */
		    break;
		  }
	    }
	}
      else if ( S->stranded == double_stranded )
	{
	  /* now test if double stranded for the consensus - which need not be a base !*/
	  /* check if one of the strands has no votes for the consensus - if so then mark it as no consensus */
	  
	  if ( ! forward_votes[base0] || ! reverse_votes[base0] )
	    {
	      if ( splus == forward_votes[base_n] || sminus == reverse_votes[base_n] ) /* its ok the bad strand is all n's */
		S->consensus = Medium;
	      else
		S->consensus = Weak;
	    }
	  else
	    {
	      S->consensus = Strong;
	      
	      /* check if the highest vote on both strands is for the consensus */
	      
	      for_any_base(j)
		if ( j != base0 && ( forward_votes[j] >= forward_votes[base0] || reverse_votes[j] >= reverse_votes[base0] ))
		  {
#ifdef DEBUG 
		    printf("downgrading base0: %c j: %c +:%d -:%d\n", c_base(base0), c_base(j), forward_votes[j], reverse_votes[j]); 
#endif
		    S->consensus = Medium; /* down-grade */
		    break;
		  }
	    }
	}
    }


  /* ok if we have a Consensus make the additional requirement that no
     more than two different bases have been called */

  if ( S->consensus == Medium )
    {
      if ( S->calls + bad > 2 )
	S->consensus = Weak;
    }


  /* find the type of edit */

  /* coverage is too low */
  
  if ( S->depth < 3 )
    S->status = too_low;

  /* no error - calls are for a single proper base */

  else if ( proper_base(base0) && S->base_calls[base0] == S->depth )
    S->status = ok;
  
  /* unclear edits - mixture of indels and replacements */

  else if ( S->consensus != Strong && bad > 1 )
    S->status = unclear;

  else if ( S->consensus != Strong && S->calls > 1 && bad )
    S->status = unclear;

      
  /* substitution */
  
  else if ( /*S->consensus  && */ (! S->base_calls[base_o]) && (! S->base_calls[base_i]  ) )
    S->status = replace;
  
  /* deletion */
  
  else if ( /*S->consensus && */ S->base_calls[base_i] == S->base_calls[base0] )
    {
      S->status = deletion; 
      S->best = base_i;
    }

  /* insertion */
  
  else if ( /*S->consensus && */ S->base_calls[base_i] && proper_base(base0) )
    S->status = insert;
  
  /* all other messy cases */
  
  else 
    S->status = unclear;

/* check for possible compressions - rule is that the bases called on
   each strand (or terminator) must be different (N's don't count)*/

  plus_minus = ((splus-forward_votes[base_n]) && (sminus-reverse_votes[base_n]));
  t_nt = ( (st-terminator_votes[base_n]) && (snt-non_terminator_votes[base_n]));

  if (  plus_minus || t_nt )
    {
      n = 0;
      if ( plus_minus ) /* forward and rerverse strands */
	{
	  for_each_base(j)
	    n += forward_votes[j]*reverse_votes[j] ;
	  n += forward_votes[base_i]*reverse_votes[base_i];
	}
      
      if ( t_nt ) /* terminator and non-terminator */
	{
	  for_each_base(j)
	    n += terminator_votes[j]*non_terminator_votes[j] ;
	  n += terminator_votes[base_i]*non_terminator_votes[base_i];
	}
      
      if ( ! n )
	S->compression = TRUE;
      else
	S->compression = FALSE;
#ifdef DEBUG
      if ( ! n )
	for_each_base(j)
	  printf(" %c:%d,%d ", c_base(j), terminator_votes[j], non_terminator_votes[j]);
#endif
    }
  else
    S->compression = FALSE;


#ifdef TERMINATOR
/* check that there isn't a terminator read screwing things up */

  if ( S->status != unclear && S->consensus == Medium )
    {
      CAFSEQ *seq;
      for(j=0;j<width;j++)
	{
	  ad = arrp( alignment, j, alignData;
	      
	  if ( ad && ! ad->bad && proper_base(i_base(ad->base)) && i_base(ad->base) != base0 )
	    {
	      seq = arrp( seqs, ad->id, CAFSEQ );
	      if ( seq->is_terminator )
		{
		  S->status = unclear;
#ifdef DEBUG 
		  printf("terminator screwup %d\n", ++number);
#endif
		  break;
		}
	    }
	}
    }
#endif

  /* reclassify simple edits */

  S->promoted = FALSE;
  if ( S->consensus == Medium )
    {
      {
	/* replace n if depth >= min_depth and no ambiguities */
	if ( S->status == replace && 
	    doubleStranded(S->stranded) &&
	    S->calls == 1 && 
	    S->depth >= min_depth && 
	    S->base_calls[base_n] == 1 && 
	    S->base_calls[base_i] == 0 )
	  {
	    S->consensus = Strong;
	    S->promoted = TRUE;
	  }
	/* replace single base mismatches */
	else if ( S->status == replace && 
		 doubleStranded(S->stranded) &&
		 S->calls == 2 && 
		 S->depth >= min_depth && 
		 S->base_calls[base_n] == 0 && 
		 S->base_calls[base_i] == 0 )
	  {
	    S->consensus = Strong;
	    S->promoted = TRUE;
	  }
      }
      /* simple deletes */
      /* rule is:
	 double-stranded
	 depth >= min_depth (usually 4)
	 all but one call is a pad
	 both strands have at least one call for the consensus
	 */
      if ( S->status == deletion &&
	  S->stranded == double_stranded && 
	  ( S->calls + S->base_calls[base_n] == 1 ) &&
	  S->depth >= min_depth && 
	  S->base_calls[base_i] == S->depth-1 &&
	  forward_votes[S->best] && reverse_votes[S->best] )
	{
	  S->consensus = Strong;
	  S->promoted = TRUE;
	} 
      else if ( S->status == deletion &&
	  S->stranded == terminated && 
	  ( S->calls + S->base_calls[base_n] == 1 ) &&
	  S->depth >= min_depth && 
	  S->base_calls[base_i] == S->depth-1 &&
	  terminator_votes[S->best] && non_terminator_votes[S->best] )
	{
	  S->consensus = Strong;
	  S->promoted = TRUE;
	} 
      else if ( S->status == deletion &&
	       S->stranded == all_stranded && 
	       ( S->calls + S->base_calls[base_n] == 1 ) &&
	       S->depth >= min_depth && 
	       S->base_calls[base_i] == S->depth-1 &&
	       terminator_votes[S->best] && forward_votes[S->best] && reverse_votes[S->best] )
	{
	  S->consensus = Strong;
	  S->promoted = TRUE;
	} 
      
	  /* simple inserts */
      if ( S->status == insert &&
	  S->stranded == double_stranded && 
	  S->calls == 1 && 
	  S->depth >= min_depth && 
	  S->base_calls[base_n] == 0 && 
	  S->base_calls[base_i] == 1 &&
	  forward_votes[S->best] && reverse_votes[S->best])
	{
	  S->consensus = Strong;
	  S->promoted = TRUE;
	} 
      else if ( S->status == insert &&
	  S->stranded == terminated &&
	  S->calls == 1 && 
	  S->depth >= min_depth && 
	  S->base_calls[base_n] == 0 && 
	  S->base_calls[base_i] == 1 &&
	  terminator_votes[S->best] && non_terminator_votes[S->best])
	{
	  S->consensus = Strong;
	  S->promoted = TRUE;
	} 
      else if ( S->status == insert &&
	  S->stranded == all_stranded &&
	  S->calls == 1 && 
	  S->depth >= min_depth && 
	  S->base_calls[base_n] == 0 && 
	  S->base_calls[base_i] == 1 &&
	  terminator_votes[S->best] && forward_votes[S->best] && reverse_votes[S->best])
	{
	  S->consensus = Strong;
	  S->promoted = TRUE;
	} 

/* unclear edits */
      if ( S->status == unclear &&
	  tmp[0].calls > tmp[1].calls )
	{
	  if ( S->best == base_i )
	    {
	      S->status = deletion;
	      S->consensus = Strong;
	    }
	  else if ( proper_base(S->best) )
	    {
	      S->status = replace;
	      S->consensus = Strong;
	    }
	}
    }
  /* unclear edits: replace n if depth >= min_depth and no ambiguities */

  else if ( S->consensus == Weak  &&
	   doubleStranded(S->stranded) &&
	   S->calls == 1 && 
	   S->depth >= min_depth && 
	   S->base_calls[base_n] == 1 && 
	   S->base_calls[base_i] == 0 )
    {
      S->consensus = Strong;
      S->status = replace;
      S->promoted = TRUE;
#ifdef DEBUG 
      printf("promoting\n");
#endif
    }

  /* downgrade edits below min_depth */
  
  if ( S->consensus == Strong && S->depth < min_depth )
    S->consensus = Medium;

  /* upgrade columns of pads (from missaligned reads */

  if ( S->status == deletion && S->base_calls[base_i] == S->depth )
    S->consensus = Strong;

  /* finally set this to be a non-compound problem */

  S->compound = Isolated;

#ifdef DEBUG
  if ( S->compression )
    printf("compression st:%d snt:%d t_nt:%d plus:%d minus:%d plus_minus:%d status:%d consensus:%d", st, snt, t_nt, splus, sminus, plus_minus, S->status, S->consensus );
#endif

}

int pstcmp( pair_struct *a, pair_struct *b ) /* for sorting base calls */
{
  int n = b->calls - a->calls;

  if ( ! n )
    n = a->id-b->id;

  return n;
}

void 
classify_alignment_status_st_louis( cafAssembly *CAF, Array alignment, alignStatus *S, int depth, float st_louis_thresh )
/* This replicates St. Louis style editing where we just want a 75%
   consensus (or whatever st_lous_thresh is set to) */
{
  int j, total = 0;
  alignData *ad;
  int ibase, max=0;
  int width = arrayMax(alignment); /* the width of the alignment */

  for(j=0;j<nucleotides;j++)
    S->base_calls[j] = 0;

  for(j=0;j<width;j++)
    {
      ad = arrp( alignment, j, alignData);

      /* count the number of calls for each base (note that *, n,- are "bases" */

      if ( ad && ad->id != -1 && (ibase = i_base(ad->base)) >= 0 ) 
	{
	  S->base_calls[ibase]++;
	  total++;
	}
    }
  max = 0;
  for(j=0;j<nucleotides;j++)
    {
      if ( S->base_calls[j]  > max )
	{
	  max = S->base_calls[j];
	  S->best = j;
	}
    }
  
  if ( (100.0*max)/(total) > st_louis_thresh )
    S->consensus = Strong;
  else
    S->consensus = Medium;
  
  if ( max == total )
    S->status = ok;
  else if ( S->best == base_i )
    S->status = deletion;
  else if ( S->base_calls[base_i] > 0 )
    S->status = insert;
  else if ( proper_base(S->best) )
    S->status = replace;
  else
    S->status = unclear;
}


void
check_terminators( contigAlignment *Align, cafAssembly *CAF )
/* check for deletions in a terminator A(G) or (C)T (reverse) */
{
  int j, i;
  alignStatus *S;
  alignData *ad, *ad1;
  Array *B;
  Array *beforeB;
  Array *afterB;
  
  for(j=Align->min;j<=Align->max;j++)
    {
      S = arrp(Align->status,j,alignStatus);
      if ( S->status == deletion && S->consensus == Strong )
	{
	  beforeB = arrp(Align->aligned, j - 1, Array);
	  B = arrp(Align->aligned, j, Array );
	  afterB = arrp(Align->aligned, j + 1, Array);
	  for(i=0;i<arrayMax(*B);i++)
	    {
	      ad = arrp(*B,i,alignData);
	      
	      if ( is_terminated(ad->strand) && ad->base != padding_char )
		{
		  if ( is_forward(ad->strand) )
		    {
		      if (i < arrayMax(*beforeB))
			{
			  ad1 = arrp(*beforeB, i, alignData);
			  
			  if ( i_base(ad->base) == base_g  && i_base(ad1->base) == base_a )
			    {
#ifdef DEBUG
			      fprintf(stderr, "downgrading deletion at position %d\n", j ); 
#endif
			      S->consensus = Medium;
			    }
			}
		    }
		  else
		    {
		      if (i < arrayMax(*afterB))
			{
			  ad1 = arrp(*afterB, i, alignData);
			  
			  if ( i_base(ad->base) == base_c  && i_base(ad1->base) == base_t )
			    {
#ifdef DEBUG
			      fprintf(stderr, "downgrading deletion at position %d\n", j );
#endif
			      S->consensus = Medium;
			    }
			}
		    }
		}
	    }
	}
    }
}

int nparams=7;
char *p_name[] = { "replace", "insert", "delete", "unclear", "depth", "stranded", "run_length" };

void
dump_classification( contigAlignment *Align, int argc, char **argv )
{
  char filename[256];
  FILE *fp = argfile("-lclassify", "w", argc, argv, filename );
  int discrep_only=0;

  getboolean("-discrep", &discrep_only, argc, argv );

  fprintf(stderr, "Dumping classifications to %s\n", filename );
  dump_data( Align, fp, discrep_only );

  fclose(fp);
}

void 
dump_data( contigAlignment *Align, FILE *fp, int discrep_only  )
/* dump out edit data in designMatrix format for use by lclassify */
{
  int j, i;
  alignStatus *S;
  int ndata = Align->max-Align->min+1;
  int mode;

  if ( discrep_only )
    {
      ndata = 0;
      for(j=Align->min;j<=Align->max;j++)
	{
	  S = arrp(Align->status,j,alignStatus);
	  if ( S->status != ok )
	    ndata++;
	}
    }

  fprintf( fp, "%d %d\n", nparams, ndata );
  for(i=0;i<nparams;i++)
    fprintf(fp,"%s\n", p_name[i] );

  for(j=Align->min;j<=Align->max;j++)
    {
      S = arrp(Align->status,j,alignStatus);
      mode = (S->best == i_base(arr(Align->cons,j,char) ) );
      if ( S->status != ok || ! discrep_only )
	{
	  fprintf(fp,"%d  ", mode );
	  
	  if ( S->status == ok )
	    fprintf( fp, "0 0 0 0");
	  else if ( S->status == replace )
	    fprintf( fp, "1 0 0 0");
	  else if ( S->status == insert )
	    fprintf( fp, "0 1 0 0");
	  else if ( S->status == deletion )
	    fprintf( fp, "0 0 1 0");
	  else 
	    fprintf( fp, "0 0 0 1");
	  
	  fprintf( fp, " %2d %2d %2d\n", S->depth, S->stranded, S->compound );
	}
    }
}

/* 
 * $Log$
 * Revision 1.6  2005/06/16 11:22:30  rmd
 * Moved Log.
 * Include trace_quality.h to remove compiler warnings.
 *
 * Revision 1.5  2002/03/04 17:00:39  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.4  1998/02/06  10:02:53  badger
 * Changed minimum read depth for an edit to 3
 *
 * Revision 1.3  1998/01/12  16:43:35  badger
 * Fixed array access mix-up in check_terminators
 *
 * Revision 1.2  1997/09/04  16:44:02  badger
 * added use_phrap_consensus
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Revision 1.6  1996/10/03  17:43:13  rmott
 * *** empty log message ***
 *
 * Revision 1.5  1996/02/21  11:31:16  rmott
 * CAF version
 * */
