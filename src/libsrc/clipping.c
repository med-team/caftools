/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 * its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * $Id: clipping.c 11027 2003-07-25 10:47:30Z dgm $
 * $Log$
 * Revision 1.4  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 * Revision 1.3  2002/03/04 17:55:34  rmd
 * Commented out include scf.h
 *
 * Revision 1.2  2002/03/04 17:00:40  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Revision 1.5  1996/10/03  17:44:28  rmott
 * *** empty log message ***
 *
 * Revision 1.4  1996/06/18  16:04:21  rmott
 * version prior to making rescore_alignment a separate function
 *
 * Revision 1.3  1996/02/21  11:32:46  rmott
 * CAF version
 *
 * Revision 1.2  1996/02/20  12:27:02  rmott
 * functions for clipping
 *
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <caf.h>
#include <trace_edit.h>
#include <cl.h>
#include <seq_util.h>
#include <tree.h>

extern int sim_mat[256][256];

#define editRequired(x) ((x) != 2)

int debugging=0;
int qual_clip( ASSINF *a, TAG *tag, FILE *fp, cafAssembly *CAF ); /* clip inside tag (for quality)*/
int vec_clip( ASSINF *a, TAG *tag, FILE *fp, cafAssembly *CAF); /* clip outside tag (for vector) */

void
rescore_alignment( CAFSEQ *read, ASSINF *lax, contigAlignment *Align, int original, int match, int mismatch, int gap, int *clip_left_at, int *clip_right_at, int *lax_score, int *strict_score );

/* functions concerned with clipping using autoedit information */

typedef struct
{
  char type;
  char forward_depth;
  char reverse_depth;
  char terminated_depth;
}
DEPTH;

int strand_type( DEPTH *d );

#define singleRead(d) ( (d)->forward_depth + (d)->reverse_depth + (d)->terminated_depth <= 1 ) /* no more than one read covers */

typedef struct 
{
  Array Left;
  Array Right;
  Array edit;
}
qual_struct;

void 
generate_clip_points( contigAlignment *Align, cafAssembly *CAF, FILE *logf, int match, int mismatch, int gap, int neutral, char *debug_read, int original, char *tagType )
{
  /* Generate tags of type tagType for clipping alignments of reads to
     the contig with the given penalties.

     Does not actually do any clipping - just makes tags */

  int k;
  ASSINF *a;
  CAFSEQ *seq;
  CAFSEQ *contig = Align->contig;
  int max_score, lax_s;
  int x1, x2;
  char buf[256];

  mat_init(match,mismatch,gap,neutral,padding_char); /* initialise the matrix for matching sybmbols */

  for(k=0;k<arrayMax(contig->assinf);k++)
    {
      a = arrp(contig->assinf,k,ASSINF);
      seq = arrp(CAF->seqs,a->seq,CAFSEQ);

      rescore_alignment( seq, a, Align, original, match, mismatch, gap, &x1, &x2, &lax_s, &max_score);

      sprintf(buf, "score %5d    match %5d mismatch %5d gap %5d\n", max_score, match, mismatch, gap );
      newTag( seq, CAF, tagType, x1, x2, buf );
      if ( logf )
	fprintf(logf, "%-15s %-15s %-10s %5d %5d %5d\n",
		dictName(CAF->seqDict, contig->id),
		dictName(CAF->seqDict, seq->id),
		tagType, x1, x2, max_score );
    }
}
      
void 
propose_clip_alignment( contigAlignment *Align, cafAssembly *CAF, int extend, FILE *logf, int match, int mismatch, int gap, int neutral, char *debug_read, int original )

/* check all reads in the contigAlignment to compute new left and
right clip points.  Criterion is to find the maximal-scoring
sub-alignment under the new match/mismatch penalties.

Thus on entry it is assumed the readings have been aligned to the
consensus using lax clipping, so that they will need to be trimmed
back. If the trimmed reads yield double stranded coverage of a
position, then fine. If however the lax clipping was double-stranded
while the strict clipping is not, then the readings are extended out
up to (but not exceeding) the lax clip points

if the flag original==TRUE then the input (phrap) consensus is used in place of the majority vote

if the flag extend==FALSE then the reads are not extended out again, which may result in holes

*/



{
  Array clip = arrayCreate(1000,ASSINF);
  Array seqs = CAF->seqs;
  ASSINF *lax, *strict;
  int min = Align->min;
  int max = Align->max;
  Array status = Align->status;
  Array slot = Align->slot;
  Array aligned = Align->aligned;
  int i, j, k, len = 0;
  Array *B;
  alignStatus *S;
  alignData *ad;
  int clipped_left = 0, clipped_right = 0;
  CAFSEQ *seq, *contig = Align->contig;
  int s1, s2;
  Array coverage;
  Array clipped_coverage;
  Array covered;
  Array problem_interval;
  Array read_quality;
  int problems, previous;
  CLIP *problem;
  DEPTH *d, *clipped_d;
  /* qual_struct *qs; */
  int s1c, s2c;
  int clip_left_at, clip_right_at;
  /* char *dna; */
  Array orig_consensus=Align->cons;
  int max_score, strand, readslot;
  float lax_len=0, strict_len=0, total=0;
  TAG *clip_tag;
  Array lax_score, strict_score;

  fprintf(stderr, "extend: %d\n", extend );

  covered = arrayCreate(max+1,int);
  coverage = arrayCreate(max+1,DEPTH);
  arrayp(coverage,max,DEPTH);
  clipped_coverage = arrayCreate(max+1,DEPTH);
  arrayp(clipped_coverage,max,DEPTH);
  read_quality = arrayCreate(1000,qual_struct);

  lax_score = arrayCreate(1000,int);
  strict_score = arrayCreate(1000,int);

  mat_init(match,mismatch,gap,neutral,padding_char); /* initialise the matrix for matching sybmbols */

/* fprintf(stderr, "in clipping reads: %d min: %d max: %d\n",
arrayMax(contig->assinf), min, max ); */

  for(i=0;i<arrayMax(contig->assinf);i++)
    {
      lax = arrp(contig->assinf,i,ASSINF);

      if ( debug_read )
	debugging = ! strcmp(dictName(CAF->seqDict,lax->seq),debug_read ); /* debug on this reading */

      seq = arrp(seqs,lax->seq,CAFSEQ);
      /* dna = stackText(seq->dna,0)-1; */

      /* strict is an array which will eventually hold the clipped
         assembled_from info. Initially it is just a copy of the
         initial assinf */

      strict = arrayp(clip,i,ASSINF); 

      strict->seq = lax->seq;
      strict->r1  = lax->r1;
      strict->r2  = lax->r2;
      strict->s1  = lax->s1;
      strict->s2  = lax->r2;
      strict->s   = lax->s;


      if ( lax->s1 > lax->s2 )
	{
	  s1 = lax->s2;
	  s2 = lax->s1;
	}
      else
	{
	  s1 = lax->s1;
	  s2 = lax->s2;
	}
      len = lax->r2-lax->r1+1;
      lax_len += len;
      total++;

/* compute the coverage using the lax original clipping */


      if ( seq->is_terminator )
	{
	  for(j=s1;j<=s2;j++)
	    {
	      d = arrp(coverage,j,DEPTH);
	      d->terminated_depth++;
	    }
	}
      else if ( lax->s1 <= lax->s2 )
	{
	  for(j=s1;j<=s2;j++)
	    {
	      d = arrp(coverage,j,DEPTH);
	      d->forward_depth++;
	    }
	}
      else
	{
	  for(j=s1;j<=s2;j++)
	    {
	      d = arrp(coverage,j,DEPTH);
	      d->reverse_depth++;
	    }
	}

      clipped_left = 0;
      clipped_right = 0;


      {
	int lax_s;

	rescore_alignment( seq, lax, Align, original, match, mismatch, gap, &strict->r1, &strict->r2, &lax_s, &max_score);
      }


/* strict->r1 is the coord in READ SPACE of the left strict clipping point, 
   strict->r2 is the right clipping point 

   [strict->r1,strict->r2] is always a subinterval of [lax->r1,lax->r2]

   strict->s1, strict->s2 are the corresponding points in contig space 
   ( flipped if the read is reverse-complemented
*/

      clipped_left = strict->r1-lax->r1; /* the length of clipping left and right */
      clipped_right = lax->r2-strict->r2;

      if ( lax->s1 < lax->s2 )
	{
	  s1c = lax->s1+clipped_left;
	  s2c = lax->s2-clipped_right;
	  strict->s1 = s1c;
	  strict->s2 = s2c;
	}
      else
	{
	  s1c = lax->s2+clipped_right;
	  s2c = lax->s1-clipped_left;
	  strict->s1 = s2c;
	  strict->s2 = s1c;
	}
      
/* the depth of coverage for the clipped regions, for forward, reverse and terminated reads */

      if ( seq->is_terminator )
	{
	  for(j=s1c;j<=s2c;j++)
	    {
	      d = arrayp(clipped_coverage,j,DEPTH);
	      d->terminated_depth++;
	    }
	}
      else if ( lax->s1 <= lax->s2 )
	{
	  for(j=s1c;j<=s2c;j++)
	    {
	      d = arrayp(clipped_coverage,j,DEPTH);
	      d->forward_depth++;
	    }
	}
      else
	{
	  for(j=s1c;j<=s2c;j++)
	    {
	      d = arrayp(clipped_coverage,j,DEPTH);
	      d->reverse_depth++;
	    }
	}
    }
  
/* this bit of code is a digression - it finds problem regions. POssibly useful for terminator reactions */

  problems = 0;
  problem_interval = arrayCreate(100,CLIP);
  previous = -100;
  problem = NULL;
  array(covered, max, int) = 0;

  for(j=min;j<=max;j++)
    {
      d = arrp(coverage,j,DEPTH);
      strand_type(d);
      clipped_d = arrayp(clipped_coverage,j,DEPTH);
      strand_type(clipped_d);

      if ( doubleStranded(d->type) )
	{
	  if( ! doubleStranded(clipped_d->type) )
	    {
	      *arrp(covered,j,int) = clipped_d->type; /* problem */
	      problems++;
	      if ( ! problem || previous != clipped_d->type )
		{
		  problem = arrayp(problem_interval,arrayMax(problem_interval),CLIP);
		  problem->clip_left = j;
		}
	      problem->clip_right = j;
	    }
	  else
	    {
	      problem = NULL;
	      *arrayp(covered,j,int) = -1;
	    }
	}
      else
	{
	  problem = NULL;
	  *arrayp(covered,j,int) = -2;
	}
      previous = clipped_d->type;
    }


  for(i=0;i<arrayMax(problem_interval);i++)
    {
      problem = arrp(problem_interval,i,CLIP);
      d = arrayp(coverage,problem->clip_left,DEPTH);
      clipped_d = arrayp(clipped_coverage,problem->clip_left,DEPTH);
/*      printf("problem interval %5d %5d len %d %-15s %-15s\n", problem->clip_left, problem->clip_right, problem->clip_right-problem->clip_left+1, strand_text[d->type], strand_text[clipped_d->type]); */
    }

/* go through all the reads again to determine whether to keep to strict clipping or to extend */

  for(i=0;i<arrayMax(contig->assinf);i++)
    {
      lax = arrp(contig->assinf,i,ASSINF);
      seq = arrp(seqs,lax->seq,CAFSEQ);
      readslot = arr( slot, lax->seq, int );
      strict = arrp(clip,i,ASSINF);
      debugging = ! strcmp(dictName(CAF->seqDict,lax->seq),debug_read );

      if ( extend )
	{
	  if ( lax->s1 < lax->s2 )
	    {
	      s1c = strict->s1;
	      s2c = strict->s2;
	      s1 = lax->s1;
	      s2 = lax->s2;
	    }
	  else
	    {
	      s1c = strict->s2;
	      s2c = strict->s1;
	      s1 = lax->s2;
	      s2 = lax->s1;
	    }
	  
	  
	  clip_left_at = strict->r1;
	  clip_right_at = strict->r2;
	  
	  /* determine the strand type of this read */
	  
	  if ( seq->is_terminator )
	    strand = terminator_only;
	  else if ( lax->s1 < lax->s2 )
	    strand = forward_only;
	  else
	    strand = reverse_only;
	  
	  /* now extend back so long as double-strandedness is not broken */
	  
	  if ( lax->s1 < lax->s2 ) /* forward read */
	    {
	      clip_left_at = strict->r1;
	      k = strict->r1-1;
	      for(j=s1c-1;j>s1;j--,k--)
		{
		  d = arrp(coverage,j,DEPTH);
		  clipped_d = arrayp(clipped_coverage,j,DEPTH);
		  if ( singleRead(clipped_d) ) /* fill holes - extend if no more than one read covers */
		    {
		      if ( debugging )
			fprintf(stderr,"%-15s 1 fillextended clipped left at %d %d %d\n", dictName(CAF->seqDict,lax->seq), j, k, k-strict->r2);  
		    }
		  else if ( doubleStranded(d->type) ) /*if the lax clipping was double-stranded */
		    {
		      if ( doubleStranded(clipped_d->type) ) /* ... and the strict clipping is also, then break ... */
			{
			  if ( debugging )
			    fprintf(stderr,"%-15s extended clipped left at %d %d %d\n", dictName(CAF->seqDict,lax->seq), j, k, k-strict->r1); 
			  break;
			}
		      else if ( clipped_d->type == strand ) /* clipped is same as this strand so don't extend */
			{
			  break;
			}
		    }
		  else		/* as soon as lax region is single-stranded then break */
		    break;
		  s1c = j;
		  clip_left_at = k;
		}

	      clip_right_at = strict->r2;
	      k = strict->r2+1;
	      for(j=s2c+1;j<s2;j++,k++)
		{
		  d = arrp(coverage,j,DEPTH);
		  clipped_d = arrayp(clipped_coverage,j,DEPTH);
		  if ( debugging )
		    fprintf(stderr,"extending %5d %5d depth %d clipped %d\n", j, k, d->type, clipped_d->type); 
		  if ( singleRead(clipped_d) )
		    {
		      if ( debugging )
			fprintf(stderr,"%-15s 2 fillextended clipped right at %d %d %d\n", dictName(CAF->seqDict,lax->seq), j, k, k-strict->r2);  
		    }
		  else if ( doubleStranded(d->type) )
		    {
		      if ( doubleStranded(clipped_d->type) )
			{
			  if ( debugging )
			    fprintf(stderr,"%-15s extended clipped right at %d %d %d\n", dictName(CAF->seqDict,lax->seq), j, k, k-strict->r2);
			  break;
			}
		      else if ( clipped_d->type == strand ) /* clipped is same as this strand so don't extend */
			{
			  break;
			}
		    }
		  else
		    break;
		  s2c = j;
		  clip_right_at = k;
		}

	      B = arrp( aligned, s1c, Array);
	      ad = arrp(*B, readslot, alignData );

	      if ( original )
		while( sim_mat[tolower((int) ad->base)][tolower((int) arr(orig_consensus,s1c,char))] <= 0  && clip_left_at > lax->r1 )
		  {
		    s1c--;
		    B = arrp( aligned, s1c, Array);
		    ad = arrp( *B, readslot, alignData );
		    clip_left_at = ad->pos;
		  }
	      else
		{
		  S = arrp(status,s1c,alignStatus);
		  while( sim_mat[tolower((int) ad->base)][(int) c_base(S->best)] <= 0  && clip_left_at > lax->r1 )
		    {
		      s1c--;
		      B = arrp( aligned, s1c, Array);
		      ad = arrp( *B, readslot, alignData );
		      S = arrp(status,s1c,alignStatus);
		      clip_left_at = ad->pos;
		    }
		}

	      B = arrp( aligned, s2c, Array);
	      ad = arrp(*B, readslot, alignData );

	      if ( original )
		while( sim_mat[tolower((int) ad->base)][tolower((int) arr(orig_consensus,s2c,char))] <= 0 && clip_right_at < lax->r2 )
		  {
		    s2c++;
		    B = arrp( aligned, s2c, Array);
		    ad = arrp( *B, readslot, alignData );
		    S = arrp(status,s2c,alignStatus);
		    clip_right_at = ad->pos;
		  }  
	      else
		{
		  S = arrp(status,s2c,alignStatus);
		  while( sim_mat[tolower((int) ad->base)][(int) c_base(S->best)] <= 0 && clip_right_at < lax->r2 )
		    {
		      s2c++;
		      B = arrp( aligned, s2c, Array);
		      ad = arrp( *B, readslot, alignData );
		      S = arrp(status,s2c,alignStatus);
		      clip_right_at = ad->pos;
		    }  
		}
	    }
	  else
	    {
	      clip_right_at = strict->r2;
	      k = strict->r2+1;

	      for(j=s1c-1;j>s1;j--,k++)
		{
		  d = arrp(coverage,j,DEPTH);
		  clipped_d = arrayp(clipped_coverage,j,DEPTH);
		  if ( singleRead(clipped_d))
		    {
		      if ( debugging )
			fprintf(stderr,"%-15s 3 fillextended clipped right at %d %d %d\n", dictName(CAF->seqDict,lax->seq), j, k, k-strict->r2); 
		    }
		  else if ( doubleStranded(d->type) )
		    {
		      if ( doubleStranded(clipped_d->type) )
			{
			  if ( debugging )
			    fprintf(stderr,"%-15s extended clipped right at %d %d %d\n", dictName(CAF->seqDict,lax->seq), j, k, k-strict->r2);  
			  break;
			}
		      else if ( clipped_d->type == strand ) /* clipped is same as this strand so don't extend */
			{
			  break;
			}
		    }
		  else		/* as soon as lax region is single-stranded then break */
		    break;
		  s1c = j;
		  clip_right_at = k;
		}

	      clip_left_at = strict->r1;
	      k = strict->r1-1;

	      if ( debugging )
		fprintf(stderr, "before0 %-15s %s %5d range %5d %5d  %5d %5d  left %5d (%2d) right %5d (%2d)  %5d %5d\n", 
			dictName(CAF->seqDict,lax->seq), ( lax->s1 > lax->s2 ? "-" : "+" ), len, lax->r1, lax->r2, s1, s2, 
			strict->r1, clipped_left,
			strict->r2, clipped_right, s1c, s2c );

	      for(j=s2c+1;j<s2;j++,k--)
		{
		  d = arrp(coverage,j,DEPTH);
		  clipped_d = arrayp(clipped_coverage,j,DEPTH);
		  if ( debugging )
		    fprintf(stderr,"%s extending %5d %5d depth %d clipped %d\n", dictName(CAF->seqDict,lax->seq), j, k, d->type, clipped_d->type); 
		  if ( singleRead(clipped_d))
		    {
		      /*		  printf("%-15s 4 fillextended clipped left at %d %d %d\n", dictName(CAF->seqDict,lax->read), j, k, k-strict->r2); */
		    }
		  else if ( doubleStranded(d->type) )
		    {
		      if ( doubleStranded(clipped_d->type) )
			{
			  if ( debugging )
			    fprintf(stderr,"%-15s extended clipped left at %d %d %d\n", dictName(CAF->seqDict,lax->seq), j, k, k-strict->r1); 
			  break;
			}
		      else if ( clipped_d->type == strand ) /* clipped is same as this strand so don't extend */
			{
			  break;
			}
		    }
		  else
		    break;
		  s2c = j;
		  clip_left_at = k;
		}

	    
	      if ( debugging )
		fprintf(stderr, "before %-15s %s %5d range %5d %5d  %5d %5d  left %5d (%2d) right %5d (%2d)  %5d %5d\n", 
			dictName(CAF->seqDict,lax->seq), ( lax->s1 > lax->s2 ? "-" : "+" ), len, lax->r1, lax->r2, s1, s2, 
			strict->r1, clipped_left,
			strict->r2, clipped_right, s1c, s2c );


	      B = arrp( aligned, s2c, Array);
	      ad = arrp( *B, readslot, alignData );

	      if ( original )
		while( sim_mat[tolower((int) ad->base)][tolower((int) arr(orig_consensus,s2c,char))] <= 0  && clip_left_at > lax->r1 )
		  {
		    s2c++;
		    B = arrp( aligned, s2c, Array);
		    ad = arrp( *B, readslot, alignData );
		    clip_left_at = ad->pos;
		  }
	      else
		{
		  S = arrp(status,s2c,alignStatus);
		  while( sim_mat[tolower((int) ad->base)][(int) c_base(S->best)] <= 0  && clip_left_at > lax->r1 )
		    {
		      s2c++;
		      B = arrp( aligned, s2c, Array);
		      ad = arrp( *B, readslot, alignData );
		      S = arrp(status,s2c,alignStatus);
		      clip_left_at = ad->pos;
		      if ( debugging )
			fprintf(stderr,"moving %5d %5d %c %c %d\n", s2c, clip_left_at, ad->base, c_base(S->best), sim_mat[tolower((int) ad->base)][(int) c_base(S->best)] );
		    }
		}

	      B = arrp( aligned, s1c, Array);
	      ad = arrp( *B, readslot, alignData );

	      if ( original )
		while( sim_mat[tolower((int) ad->base)][tolower((int) arr(orig_consensus,s1c,char))] <= 0  && clip_right_at < lax->r2 )
		  {
		    s1c--;
		    B = arrp( aligned, s1c, Array);
		    ad = arrp( *B, readslot, alignData );
		    S = arrp(status,s1c,alignStatus);
		    clip_right_at = ad->pos;
		    if ( debugging )
		      fprintf(stderr,"moving %5d %5d %c %c %d\n", s2c, clip_right_at, ad->base, c_base(S->best), sim_mat[tolower((int) ad->base)][(int) c_base(S->best)] );
		  } 
	      else
		{
		  S = arrp(status,s1c,alignStatus);
		  while( sim_mat[tolower((int) ad->base)][(int) c_base(S->best)] <= 0  && clip_right_at < lax->r2 )
		    {
		      s1c--;
		      B = arrp( aligned, s1c, Array);
		      ad = arrp( *B, readslot, alignData );
		      S = arrp(status,s1c,alignStatus);
		      clip_right_at = ad->pos;
		    } 
		}
	      /* NEED TO FIX BUG IF EXTENSION TO NO_EDIT FAILS */
	    }
	  
	  clipped_left = strict->r1 > clip_left_at;
	  clipped_right = strict->r2 < clip_right_at;
	  
	  if ( debugging )
	    fprintf(stderr, "%-15s %s %5d range %5d %5d  %5d %5d  left %5d (%2d) right %5d (%2d)  %5d %5d\n", 
		    dictName(CAF->seqDict,lax->seq), ( lax->s1 > lax->s2 ? "-" : "+" ), len, lax->r1, lax->r2, s1, s2, 
		    strict->r1, clipped_left,
		    strict->r2, clipped_right, s1c, s2c );
	  
	  if ( logf )
	    fprintf(logf, "%-15s %s %3d l %3d %3d  %5d %5d (%5d)  s  %3d %3d  %5d %5d (%5d)  e %3d %3d  %5d %5d  %3d\n",
		    dictName(CAF->seqDict,lax->seq), ( lax->s1 > lax->s2 ? "-" : "+" ), lax->r2-lax->r1+1, 
		    lax->r1, lax->r2, lax->s1, lax->s2, arr(lax_score,lax->seq,int),
		    strict->r1, strict->r2, strict->s1, strict->s2, arr(strict_score,lax->seq,int),
		    clip_left_at, clip_right_at, s1c, s2c, clip_right_at-clip_left_at+1 );
	  
	  if ( clipped_left )
	    {
	      newTag( seq, CAF, "CLIP", clip_left_at, strict->r1, "AUTO-EDIT: clipped region has been auto-extended");
	      strict->r1 = clip_left_at;
	      if ( lax->s1 < lax->s2 )
		strict->s1 = s1c;
	      else
		strict->s1 = s2c;
	    }
	  
	  if ( clipped_right )
	    {
	      newTag( seq, CAF, "CLIP", strict->r2, clip_right_at, "AUTO-EDIT: clipped region has been auto-extended");
	      strict->r2 = clip_right_at; 
	      if ( lax->s1 < lax->s2 )
		strict->s2 = s2c;
	      else
		strict->s2 = s1c;
	    }
	  
	  /* create clipping tags */
	  
	  if ( ! seq->clipping )
	    seq->clipping = arrayCreate(1,TAG);
	  
	  clip_tag = arrayp(seq->clipping,arrayMax(seq->clipping),TAG);
	  dictAdd(CAF->tagTypeDict,"ECLIP",&clip_tag->type);
	  clip_tag->x1 = strict->r1;
	  clip_tag->x2 = strict->r2;
	  
	  /* replace the unclipped assembled_from info */
	}


      lax->r1 = strict->r1;
      lax->r2 = strict->r2;
      lax->s1 = strict->s1;
      lax->s2 = strict->s2;
      
      strict_len += lax->r2-lax->r1+1;
    }    


  if ( logf )
    fprintf(logf,"\nmean lax clip length: %.1f\nmean strict clip length: %.1f\n", lax_len/total, strict_len/total );
#if 0	  
  for(i=0;i<arrayMax(read_quality);i++)
    {
      qs = arrp(read_quality,i,qual_struct);
      if ( qs->edit )
	arrayDestroy(qs->edit );
      if ( qs->Left)
	arrayDestroy(qs->Left);
      if ( qs->Right )
	arrayDestroy(qs->Right);
    }
#endif
  arrayDestroy(read_quality); 
  arrayDestroy(problem_interval); 
  arrayDestroy(coverage); 
  arrayDestroy(covered); 
  arrayDestroy(clipped_coverage); 
  arrayDestroy(clip); 
  arrayDestroy(lax_score); 
  arrayDestroy(strict_score); 
}

int strand_type( DEPTH *d )

/* compute the coverage type */

{
  if ( d->forward_depth )
    {
      if ( d->reverse_depth )
	{
	  if ( d->terminated_depth )
	    d->type = all_stranded;
	  else
	    d->type = double_stranded;
	}
      else if ( d->terminated_depth )
	d->type = forward_terminated;
      else
	d->type = forward_only;
    }
  else
    {
      if ( d->reverse_depth )
	{
	  if ( d->terminated_depth )
	    d->type = reverse_terminated;
	  else
	    d->type = reverse_only;
	}
      else if ( d->terminated_depth )
	d->type = terminator_only;
      else
	d->type = no_stranded;
    }
  return d->type;
}

void
rescore_alignment( CAFSEQ *read, ASSINF *lax, contigAlignment *Align, int original, int match, int mismatch, int gap, int *clip_left_at, int *clip_right_at, int *lax_score, int *strict_score )
{

/* rescores the alignment of read to contig using the given penalties. 

   Returns left and right ends of maximal scoring segment, with the scores 

*/
  int max_score;
  int score, l, start_r, /* start_s, */ j;
  Array *B;
  int /* s1c, s2c, */ readslot;
  alignData *ad;
  int s1, s2;
  int s;
  alignStatus *S;
  Array orig_consensus = Align->cons;

  *clip_left_at = 0;
  *clip_right_at = 0;


  if ( lax->s1 > lax->s2 )
    {
      s1 = lax->s2;
      s2 = lax->s1;
    }
  else
    {
      s1 = lax->s1;
      s2 = lax->s2;
    }
  
/* now rescore the alignment of the read to the consensus, using new
   strict penalties, to find the maximal scoring segment of the
   alignment */
      
  readslot = arr( Align->slot, read->id, int );
  B = arrp( Align->aligned, s1, Array);
  ad = arrp( *B, readslot, alignData );
  
  max_score = -1;
  /* s1c = s1; */
  score = 0;
  l = ad->pos;
  start_r = l;
  /* start_s = s1; */
  
  *lax_score = *strict_score = 0;
  
  for(j=s1;j<=s2;j++)
    {
      B = arrp( Align->aligned, j, Array);
      ad = arrp( *B, readslot, alignData );

      l = ad->pos;
      if ( original )
	s = sim_mat[tolower((int) ad->base)][tolower((int) arr(orig_consensus,j,char))];
      else
	{
	  S = arrp(Align->status,j,alignStatus);
	  s = sim_mat[tolower((int) ad->base)][(int) c_base(S->best)];
	}
      *lax_score += s;

      score += s;
      if ( score < 0 )
	{
	  score = 0;
	}
      if ( score == 0 )
	{
	  if ( lax->s1 <= lax->s2 )
	    start_r = l+1;
	  else
	    start_r = l-1;
	  /* start_s = j+1; */
	}
      else if ( score > max_score )
	{
	  *clip_right_at = l;
	  /* s2c = j; */
	  *clip_left_at = start_r;
	  /* s1c = start_s; */
	  max_score = score;
	  *strict_score = score;
	}

    }
  /* deal with reverse complements */
  
  if ( *clip_right_at < *clip_left_at )
    {
      j = *clip_left_at;
      *clip_left_at = *clip_right_at;
      *clip_right_at = j;
    }
  
}

int
do_clip_assembly( cafAssembly *CAF, Array ids, int cvector, int svector, int fail, int warn, FILE *logfp )

/* perform clipping on an assembly, for clipping type clip_type

Assumes an Unpadded alignment
Failed readings are deleted if fail == TRUE

if svector == TRUE then does sequencing vector clipping
if cvector == TRUE then does cloning vector clipping

*/

{

  int i, j, k, n;
  Array seqs = CAF->seqs;
  CAFSEQ *contig, *seq;
  int clip_id=-1;
  int clipped=0;
  TAG *clip_tag;
  ASSINF *a, *b;
  Array assinf;
  int failed = 0;
  int clipped_ok;
  Array read_min, read_max;
  Array unfailed_read;

/*  for(i=0;i<dictMax(CAF->tagTypeDict);i++)
    fprintf(stderr,"%5d %s\n", i, dictName(CAF->tagTypeDict,i)); */

  read_min = arrayCreate(arrayMax(seqs),int);
  read_max = arrayCreate(arrayMax(seqs),int);
  unfailed_read = arrayCreate(arrayMax(CAF->seqs),int);
  if ( arrayMax(ids) || cvector || svector ) /* check if there are any matching tags */
    {

      
/*      fprintf(stderr, "clip_type %s id: %d\n", clip_type, clip_id ); */


      for(i=0;i<arrayMax(seqs);i++)
	{
	  *arrayp(read_min,i,int) = 10000000;
	  *arrayp(read_max,i,int) = -10000000;
	}

      for(i=0;i<arrayMax(seqs);i++)
	{
	  contig = arrp(seqs,i,CAFSEQ);
	  if ( contig->type == is_contig && contig->assinf )
	    {
	      for(j=0;j<arrayMax(contig->assinf);j++)
		{
		  a = arrp(contig->assinf,j,ASSINF);
		  seq = arrp(seqs,a->seq,CAFSEQ);

		  if ( arr(read_min,a->seq,int) > a->r1 )
		    *arrp(read_min,a->seq,int) = a->r1;

		  if ( arr(read_max,a->seq,int) < a->r2 )
		    *arrp(read_max,a->seq,int) = a->r2;
		}
	    }
	}

      for(i=0;i<arrayMax(seqs);i++)
	{
	  contig = arrp(seqs,i,CAFSEQ);
	  if ( contig->type == is_contig && contig->assinf )
	    {
	      if ( logfp )
		fprintf(logfp, "\nContig %s\n", dictName(CAF->seqDict,contig->id));
	      for(j=0;j<arrayMax(contig->assinf);j++)
		{
		  a = arrp(contig->assinf,j,ASSINF);
		  seq = arrp(seqs,a->seq,CAFSEQ);

		  if ( fail && is_failure(seq->process_status) ) /* delete this read from the contig */
		    {
		      if ( logfp )
			fprintf(logfp, "Deleting Failure %s\n", dictName(CAF->seqDict,a->seq) );
		      a->seq = -1;
		      failed++;
		    }
		  else 
		    {
		      if ( seq->clipping )
			{
			  clipped_ok = FALSE;
			  for(n=0;(n<arrayMax(ids)) && (! clipped_ok);n++)
			    {
			      clip_id = arr(ids,n,int); /* go through each clip type in order */
			      
			      for(k=0;k<arrayMax(seq->clipping);k++)
				{
				  clip_tag = arrp(seq->clipping,k,TAG);
				  if ( clip_id == clip_tag->type )
				    {
/*				      fprintf(stderr, "Clipping %s with %s %d %d\n", dictName(CAF->seqDict,seq->id), dictName(CAF->tagTypeDict,clip_id), clip_id, clipped_ok); */
				      clipped_ok = TRUE;
				      /* check if the clipping region overlaps aligned region */
				      if ( qual_clip( a, clip_tag, logfp, CAF ) )
					clipped ++;
				      else
					{ /* clipping region is disjoint, so delete Assembled_from line */
					  a->seq = -1;
					  failed++;
					}

				      if ( logfp ) /* tell the logfile what we are doing */
					{
					  if ( clip_tag->x1 < arr(read_min,seq->id,int) && a->r1 == arr(read_min,seq->id,int) )
					    fprintf(logfp, "%-12s Clipping %s truncated left  %5d %5d\n", dictName(CAF->seqDict,seq->id), dictName(CAF->tagTypeDict,clip_id), clip_tag->x1, arr(read_min,seq->id,int) );
					  if ( clip_tag->x2 > arr(read_max,seq->id,int) && a->r2 == arr(read_min,seq->id,int) )
					    fprintf(logfp, "%-12s Clipping %s truncated right %5d %5d\n", dictName(CAF->tagTypeDict,clip_id), dictName(CAF->seqDict,seq->id), clip_tag->x2, arr(read_max,seq->id,int) );
					}
				      break;
				    }
				}
			    }
			}
		      if ( svector && seq->svector) /* clip sequencing vector */
			{
			  for(k=0;k<arrayMax(seq->svector);k++)
			    {
			      clip_tag = arrp(seq->svector,k,TAG);		    
			      if ( vec_clip( a, clip_tag, logfp, CAF ) )
				clipped ++;
			      else
				{ /* clipping region is disjoint, so delete Assembled_from line */
				  a->seq = -1;
				  failed++;
				}
			    }
			}
		      if ( cvector && seq->cvector ) /* cloning vector */
			{
			  for(k=0;k<arrayMax(seq->cvector);k++)
			    {
			      clip_tag = arrp(seq->cvector,k,TAG);		    
			      if ( vec_clip( a, clip_tag, logfp, CAF ) )
				clipped ++;
			      else
				{ /* clipping region is disjoint, so delete Assembled_from line */
				  a->seq = -1;
				  failed++;
				}
			    }
			}

		    }
		}

	      assinf = arrayCreate(arrayMax(contig->assinf),ASSINF);
	      
	      for(j=0;j<arrayMax(contig->assinf);j++)
		{
		  a = arrp(contig->assinf,j,ASSINF);
		  if ( a->seq >= 0 )
		    {
		      (*arrayp(unfailed_read,a->seq,int)) ++;
		      b = arrayp(assinf,arrayMax(assinf),ASSINF);
		      b->seq = a->seq;
		      b->r1 = a->r1;
		      b->r2 = a->r2;
		      b->s1 = a->s1;
		      b->s2 = a->s2;
		    }
		  
		}
	      arrayDestroy(contig->assinf);
	      contig->assinf = assinf;
	    }
	}

      /* list deleted entries */

      if ( logfp )
	{
	  fprintf(logfp,"\n\n");

	  for(j=0;j<arrayMax(CAF->seqs);j++)
	    {
	      seq = arrp(CAF->seqs,j,CAFSEQ );
	      if ( is_read == seq->type && arr(unfailed_read,j,int) == 0 )
		fprintf(logfp, "%-20s NDCLIP Failed read\n", dictName(CAF->seqDict,j));
	    }
	}
    }

  arrayDestroy(read_min);
  arrayDestroy(read_max);
  arrayDestroy(unfailed_read);

  return clipped;
}
							     
int qual_clip( ASSINF *a, TAG *tag, FILE *logfp, cafAssembly *CAF) /* clip inside tag (for quality)*/
{
  int x1, x2;
  int left, right;

  x1 = ( tag->x1 > a->r1 ? tag->x1 : a->r1 );
  x2 = ( tag->x2 < a->r2 ? tag->x2 : a->r2 );

  if ( x1 <= x2 )
    {
      left = x1-a->r1;
      right = x2-a->r2;

      a->r1 = x1;
      a->r2 = x2;
      if ( a->s1 <= a->s2 )	/* fix contig range */
	{
	  a->s1 += left;
	  a->s2 += right;
	}
      else
	{
	  a->s1 -= left;
	  a->s2 -= right;
	}

      if ( logfp && ( left || right ) )
	fprintf(logfp, "Clipping  %15s : %10s %5d %5d  %5d %5d %6d %6d\n", dictName(CAF->seqDict,a->seq), dictName(CAF->tagTypeDict,tag->type), tag->x1, tag->x2, a->r1, a->r2, a->s1, a->s2 );

      return 1;      
    }
  if ( logfp )
	fprintf(logfp, "Clipping  %15s : %10s %5d %5d  %5d %5d %6d %6d delete\n", dictName(CAF->seqDict,a->seq), dictName(CAF->tagTypeDict,tag->type), tag->x1, tag->x2, a->r1, a->r2, a->s1, a->s2 );

  return 0; /* out of range - FAILURE */
}

int vec_clip( ASSINF *a, TAG *tag, FILE *logfp, cafAssembly *CAF ) /* clip outside tag (for vector) */
{
  int x1, x2;
  int left, right;

/*  fprintf(stderr, "vec_clip %d %d %d %d\n", tag->x1, tag->x2, a->r1, a->r2 ); */
  if ( tag->x2 >= a->r1 && tag->x2 < a->r2 ) /* left hand end */
    {
/*  .....................                  vector
             ..................            read
                         ......            clipped result
*/
      x1 = tag->x2+1;
      x2 = a->r2;
      if ( logfp )
	fprintf(logfp, "vec_clip %15s : %10s %5d %5d  %5d %5d\n", dictName(CAF->seqDict,a->seq), dictName(CAF->tagTypeDict,tag->type), tag->x1, tag->x2, a->r1, a->r2 );
    }
  else if ( tag->x1 > a->r1 && tag->x1 <= a->r2 ) /* right end */
    {
/*               ...............           vector
      .................                    read
      ...........                          clipped result
*/           
      
      x1 = a->r1;
      x2 = tag->x1-1;
      if ( logfp )
	fprintf(logfp, "vec_clip %15s : %10s %5d %5d  %5d %5d\n", dictName(CAF->seqDict,a->seq), dictName(CAF->tagTypeDict,tag->type), tag->x1, tag->x2, a->r1, a->r2 );
    }
  else if ( tag->x2 >= a->r2 && tag->x1 <= a->r1 ) /* completely inside vector - delete */
    {
/*     ....................                vector
         .................                 read
	         0                         result (deleted) */
      if ( logfp )
	fprintf(logfp, "vec_clip delete %15s : %10s %5d %5d  %5d %5d\n", dictName(CAF->seqDict,a->seq), dictName(CAF->tagTypeDict,tag->type), tag->x1, tag->x2, a->r1, a->r2 );
      return 0;  
    }
  else /* unaffected */
    {
      x1 = a->r1;
      x2 = a->r2;
    }

  if ( x1 <= x2 )
    {
      left = x1-a->r1;
      right = x2-a->r2;

      a->r1 = x1;
      a->r2 = x2;
      if ( a->s1 <= a->s2 )	/* fix contig range */
	{
	  a->s1 += left;
	  a->s2 += right;
	}
      else
	{
	  a->s1 -= left;
	  a->s2 -= right;
	}
      return 1;
    }
  return 0; /* out of range - FAILURE */
}

int
maxSegClip( Array base_quality, int threshold, int *left, int *right )

  /* find left and right quality clip points by subtracting threshold
     from quality array and then finding maximal scoring segment */

{
  Array quality;
  int k, total;
  BQ_SIZE bq;
  Array left_min, right_min;
  int left_minimum, right_minimum, best, left_position, right_position;
  int coord = 0, s;
  int len;

  if ( base_quality )
    {
      len = arrayMax(base_quality);
      quality = arrayCreate(len,int);

      total = 0;
      for(k=0;k<len;k++)
	{
	  bq = arr(base_quality,k,BQ_SIZE);
	  total += bq - threshold;
	  *arrayp(quality,k,int) = total;
	}
      
      left_min = arrayCreate(len,int);
      left_minimum = 1000000;
      left_position = 0;
      for(k=0;k<len;k++)
	{
	  if ( left_minimum > arr(quality,k,int) )
	    {
	      left_minimum = arr(quality,k,int);
	      left_position = k;
	    }
	  *arrayp(left_min,k,int) = left_position;
	}
      
      
      right_min = arrayCreate(len,int);
      right_minimum = 1000000;
      right_position = len-1;
      
      for(k=len-1;k>0;k--)
	{
	  if ( right_minimum > total-arr(quality,k-1,int) )
	    {
	      right_position = k;
	      right_minimum =  total-arr(quality,k-1,int);
	    }
	  *arrayp(right_min,k,int) = right_position;
	}
      *arrayp(right_min,0,int) = right_position;
      
      best = 1000000;
      for(k=0;k<len;k++)
	{
	  s = arr(quality,arr(left_min,k,int),int) + total - arr(quality,arr(right_min,k,int),int);
	  if( s < best )
	    {
	      best = s;
	      coord = k;
	    }
	}
      
      *right = arr(right_min,coord,int) +1;
      *left = arr(left_min,coord,int) +1;
      
      arrayDestroy(right_min);
      arrayDestroy(left_min);
      arrayDestroy(quality);

      return 1; /* success */
    }   
  return 0; /* failure */
}
