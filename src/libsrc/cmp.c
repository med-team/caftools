/*  Last edited: Jun 13 15:02 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* CMP contains standrd comparison functions for use with qsort */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <cmp.h>

int icmp(const void *ap, const void *bp)
{
  const int *a = (const int *) ap;
  const int *b = (const int *) bp;
  
  return *a-*b;
}

int Icmp(const void *ap, const void *bp)
{
  const int **a = (const int **) ap;
  const int **b = (const int **) bp;
  return **a - **b;
}

int fcmp(const void *ap, const void *bp)
{
  const float *a = (const float *) ap;
  const float *b = (const float *) bp;

  float x = *a - *b;
  if ( x > 0.0 )
    return 1;
  else if ( x < 0.0 )
    return -1;
  else
    return 0;
}

int Fcmp(const void *ap, const void *bp)
{
  const float **a = (const float **) ap;
  const float **b = (const float **) bp;

  float x = **a - **b;
  if ( x > 0.0 )
    return 1;
  else if ( x < 0.0 )
    return -1;
  else
    return 0;  
}

/* string comparison with strings reversed, case sensitive */
int Rstrcmp(const void *ap, const void *bp)
{
  const char *a = (const char *) ap;
  const char *b = (const char *) bp;

  int la = strlen(a)-1;
  int lb = strlen(b)-1;
  int n;
  while ( la && lb )
    {
      if ((n = (((int)a[la--]) - ((int)b[lb--]))) != 0)
	return n;
    }
  return la-lb;
}

int Strcmp(const void *ap, const void *bp)
{
  const char **a = (const char **) ap;
  const char **b = (const char **) bp;
  return strcmp(*a, *b);
}
