/* $Id: complement_scf.c 15171 2005-06-16 13:07:33Z rmd $ */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

#include<stdio.h>
#include"myscf.h"

Scf *complement_scf( Scf *scf );

char complement_base( char );

Scf *complement_scf( Scf *scf )

/* reverse complements the data in an scf structure */
/* checks the header entry spare[0] - if this is 9999 then it assumes
   the scf has already been rerverse complemented and leaves it
   unchanged */

{
  Header *h;
  uint_4 clip;
  int i, j;
  Bases bi, *b, *d;
  Samples1 *s1, *t1, x1;
  Samples2 *s2, *t2, x2;

  /* first the header info - swap left and right clip */

  h = &scf->header;

  /* check if the scf is already reversed */

  if ( h->spare[0] == 9999 )
    return scf;

/* set the reverse-complement flag */

  h->spare[0] = 9999;

  clip = h->bases_left_clip;
  h->bases_left_clip = h->bases_right_clip;  
  h->bases_right_clip = clip;

/*  printf("before\n");
  for(i=0;i<h->bases;i++)
    {
      printf("%c", scf->bases[i].base );
      if ( !(i%50) ) printf("\n");
    }
  printf("after\n");
*/
/* then the Bases */

  for(i=0,j=h->bases-1;i<h->bases/2;i++,j--)
    {
      b = &scf->bases[i];
      d = &scf->bases[j];

      bi.peak_index = h->samples-b->peak_index-1;
      bi.prob_A = b->prob_T;
      bi.prob_C = b->prob_G;
      bi.prob_G = b->prob_C;
      bi.prob_T = b->prob_A;
      bi.base = complement_base(b->base);

      b->peak_index = h->samples-d->peak_index-1;
      b->prob_A = d->prob_T;
      b->prob_C = d->prob_G;
      b->prob_G = d->prob_C;
      b->prob_T = d->prob_A;
      b->base = complement_base(d->base);

      d->peak_index = bi.peak_index;
      d->prob_A = bi.prob_A;
      d->prob_C = bi.prob_C;
      d->prob_G = bi.prob_G;
      d->prob_T = bi.prob_T;
      d->base = bi.base;
    }

  if ( h->bases%2 )
    {
      b = &scf->bases[h->bases/2];

      bi.peak_index = h->samples-b->peak_index-1;
      bi.prob_A = b->prob_T;
      bi.prob_C = b->prob_G;
      bi.prob_G = b->prob_C;
      bi.prob_T = b->prob_A;
      bi.base = complement_base(b->base);


      b->peak_index = bi.peak_index;
      b->prob_A = bi.prob_A;
      b->prob_C = bi.prob_C;
      b->prob_G = bi.prob_G;
      b->prob_T = bi.prob_T;
      b->base = bi.base;
    }


/*  for(i=0;i<h->bases;i++)
    {
      if ( !(i%50) ) printf("\n");
      printf("%c", scf->bases[i].base );
    }
  printf("\n");
*/
/* Finally the samples */

  if ( h->sample_size == 1 )  /* bytes */
    {
      for(i=0,j=h->samples-1;i<j;i++,j--)
	{
	  s1 = &scf->samples.samples1[i];
	  t1 = &scf->samples.samples1[j];

	  x1.sample_A = s1->sample_T;
	  x1.sample_G = s1->sample_C;
	  x1.sample_C = s1->sample_G;
	  x1.sample_T = s1->sample_A;

	  s1->sample_A = t1->sample_T;
	  s1->sample_C = t1->sample_G;
	  s1->sample_G = t1->sample_C;
	  s1->sample_T = t1->sample_A;

	  t1->sample_A = x1.sample_A;
	  t1->sample_C = x1.sample_C;
	  t1->sample_G = x1.sample_G;
	  t1->sample_T = x1.sample_T;
	}
      if ( h->samples%2 )
	{
	  s1 = &scf->samples.samples1[h->samples/2];

	  x1.sample_A = s1->sample_T;
	  x1.sample_G = s1->sample_C;
	  x1.sample_C = s1->sample_G;
	  x1.sample_T = s1->sample_A;

	  s1->sample_A = x1.sample_A;
	  s1->sample_C = x1.sample_C;
	  s1->sample_G = x1.sample_G;
	  s1->sample_T = x1.sample_T;
	}
    }
  else if ( h->sample_size == 2 ) /* shorts */
    {
      for(i=0,j=h->samples-1;i<h->samples/2;i++,j--)
	{
	  s2 = &scf->samples.samples2[i];
	  t2 = &scf->samples.samples2[j];

	  x2.sample_A = s2->sample_T;
	  x2.sample_G = s2->sample_C;
	  x2.sample_C = s2->sample_G;
	  x2.sample_T = s2->sample_A;

	  s2->sample_A = t2->sample_T;
	  s2->sample_C = t2->sample_G;
	  s2->sample_G = t2->sample_C;
	  s2->sample_T = t2->sample_A;

	  t2->sample_A = x2.sample_A;
	  t2->sample_C = x2.sample_C;
	  t2->sample_G = x2.sample_G;
	  t2->sample_T = x2.sample_T;
	}
      if ( h->samples%2 )
	{
	  s2 = &scf->samples.samples2[h->samples/2];

	  x2.sample_A = s2->sample_T;
	  x2.sample_G = s2->sample_C;
	  x2.sample_C = s2->sample_G;
	  x2.sample_T = s2->sample_A;

	  s2->sample_A = x2.sample_A;
	  s2->sample_C = x2.sample_C;
	  s2->sample_G = x2.sample_G;
	  s2->sample_T = x2.sample_T;
	}
    }
  return scf;
}
    
/*
main( int argc, char **argv)
{
  Scf *scf = read_scf(argv[1]);
  Header *h;
  Bases *b;
  union Samples *s;
  Samples1 *s1;
  Samples2 *s2;
  int i;

  h = &scf->header;
  b = scf->bases;
  s = &scf->samples;

  printf("original data\n");
  for(i=0;i<h->bases;i++)
    printf("%5d (%5d)  %3d %3d %3d %3d   %5d  %c\n", i, h->bases-i-1, (int)b[i].prob_A, (int)b[i].prob_C, (int)b[i].prob_G, (int)b[i].prob_T , (int)b[i].peak_index, b[i].base );

  printf("\nsamples\n");

  if ( h->sample_size == 1 )
    {
      s1 = scf->samples.samples1;
      for(i=0;i<h->samples;i++)
	printf("%5d  ( %5d)  %3d %3d %3d %3d\n", i, h->samples-i-1, (int)s1[i].sample_A, (int)s1[i].sample_C, (int)s1[i].sample_G, (int)s1[i].sample_T );
    }
  else if ( h->sample_size == 2 )
    {
      s2 = scf->samples.samples2;
      for(i=0;i<h->samples;i++)
	printf("%5d  ( %5d)  %3d %3d %3d %3d\n", i, h->samples-i-1, (int)s2[i].sample_A, (int)s2[i].sample_C, (int)s2[i].sample_G, (int)s2[i].sample_T );

    }

  complement_scf(scf);

  printf("reverse complement data\n");
  for(i=0;i<h->bases;i++)
    printf("%5d (%5d)  %3d %3d %3d %3d   %5d  %c\n", i, h->bases-i-1, (int)b[i].prob_A, (int)b[i].prob_C, (int)b[i].prob_G, (int)b[i].prob_T , (int)b[i].peak_index, b[i].base );

  printf("\nsamples\n");

  if ( h->sample_size == 1 )
    {
      s1 = scf->samples.samples1;
      for(i=0;i<h->samples;i++)
	printf("%5d  ( %5d)  %3d %3d %3d %3d\n", i, h->samples-i-1, (int)s1[i].sample_A, (int)s1[i].sample_C, (int)s1[i].sample_G, (int)s1[i].sample_T );
    }
  else if ( h->sample_size == 2 )
    {
      s2 = scf->samples.samples2;
      for(i=0;i<h->samples;i++)
	printf("%5d  ( %5d)  %3d %3d %3d %3d\n", i, h->samples-i-1, (int)s2[i].sample_A, (int)s2[i].sample_C, (int)s2[i].sample_G, (int)s2[i].sample_T );

    }
}

*/
