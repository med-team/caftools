/*  Last edited: Aug 17 09:59 2001 (rmd) */
#include <ctype.h>
#include <consensus.h>

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))


/* Struct to hold read segment information.  There is one of these for each
   read at the current position.  N.B. cs, ce, rs, re are indexed from 0 */

static void make_bayesian_consensus(cafAssembly* caf, CAFSEQ* contig,
				    int i, Array segments, void * params);
static char complement(char b);
static void update_segments(cafAssembly *caf, int i, Array segments,
			    ASSINF **curr_af, ASSINF *last_af);

typedef struct {
  CAFSEQ *read;  /* Read info            */
  int cs;        /* Start in contig      */
  int ce;        /* End in contig + 1    */
  int rs;        /* Start in read        */
  int re;        /* End in read + 1      */
  int comp;      /* Align reverse strand */
} Seg_entry;

/* Compare ASSINF entries.  When used for sorting, they will be ordered by
   the start of each segment */

static int af_compare(const void *a, const void *b)
{
  int mina;
  int minb;
  ASSINF *aa = (ASSINF *) a;
  ASSINF *bb = (ASSINF *) b;

  mina = min(aa->s1, aa->s2);
  minb = min(bb->s1, bb->s2);

  return (mina - minb);
}

/* Compare Seg_entry entries.  When used for sorting, they will be ordered by
   end position (ce), high to low */

static int seg_compare(const void *a, const void *b)
{
  Seg_entry *aa = (Seg_entry *) a;
  Seg_entry *bb = (Seg_entry *) b;

  return (bb->ce - aa->ce);
}

/* Recalculate the consensus for a complete caf assembly */

int recalc_consensus(cafAssembly* caf)
{
  Array   seqs;
  CAFSEQ *seq;
  int err = 0;
  int i;

  seqs = caf->seqs;
  for (i = 0; i < arrayMax(seqs); i++) {
    seq = arrp(seqs, i, CAFSEQ);
    if (seq->type == is_contig) {
      if ((err = recalc_consensus_contig(caf, seq)) != 0) return err;
    }
  }

  return 0;
}

/* Recalculate the consensus for a single contig */

int recalc_consensus_contig(cafAssembly* caf, CAFSEQ* contig)
{
  return iterate_contig_consensus(caf, contig, make_bayesian_consensus, 0);
}

/* Iterate over each base of the consensus.  For each base, call the function
   'callback' with the current position */

int iterate_contig_consensus(cafAssembly* caf, CAFSEQ* seq,
			     consensus_iterator_callback callback,
			     void *callback_params)
{
  ASSINF *assembled_froms; /* First ASSINF struct */
  ASSINF *last_af;         /* Last  ASSINF struct */
  ASSINF *curr_af;         /* pointer to current ASSINF struct        */
  Array  segments;         /* Array holding all Seg_entry at current base */
  int    start;            /* First good base in contig consensus     */
  int    end;              /* Last  good base in contig consensus + 1 */
  int    i;                /* The current consensus base */

  /* Check if this contig has any assembled_from information */

  if (!seq->assinf)               return 0;
  if (arrayMax(seq->assinf) == 0) return 0;

  /* Sort the assembled_from information into order along the contig */

  assembled_froms = arrp(seq->assinf, 0, ASSINF);
  qsort(assembled_froms, arrayMax(seq->assinf), sizeof(ASSINF), af_compare);

  /* Find the first and last bases in the consensus */

  last_af = arrp(seq->assinf, arrayMax(seq->assinf) - 1, ASSINF);
  start = min(assembled_froms->s1, assembled_froms->s2) - 1;
  end   = max(last_af->s1,         last_af->s2);

  if (start < 0) return 1;
  /* should have similar check for end here?? */

  /* Go through each consensus base, work out which read segments are
     contributing to the base and recalculate the consensus based on these */

  curr_af = assembled_froms;
  segments = arrayCreate(128, Seg_entry);

  for (i = start; i < end; i++) {
    update_segments(caf, i, segments, &curr_af, last_af);

    (*callback)(caf, seq, i, segments, callback_params);
  }

  return 0;
}

static void update_segments(cafAssembly *caf, int i, Array segments,
			    ASSINF **curr_af, ASSINF *last_af)
{
  Seg_entry *new_seg; /* New segment */
  int seg;       /* Segment counter */
  int inserted;  /* Flag if a new segment has been inserted */
  
  /* Remove any segments we have finished with (i.e. i has gone past the end).
     As the segments array is sorted by segment end, the ones we need to get
     rid of should be at the end of the array */

  for (seg = arrayMax(segments) - 1; seg >= 0; seg--) {
    
    if (arrp(segments, seg, Seg_entry)->ce > i) break;

    arrayMax(segments)--;  /* remove segment */
  }

  inserted = 0;

  /* Go through the remaining ASSINF entries and see if they should be put
     into the segments array yet. */

  for (; (*curr_af) <= last_af; (*curr_af)++) {

    if (min((*curr_af)->s1, (*curr_af)->s2) - 1 > i) break;

    /* Insert the next segment */

    new_seg = arrayp(segments, arrayMax(segments), Seg_entry);

    if ((*curr_af)->s1 < (*curr_af)->s2) {  /* Forward strand */

      new_seg->read = arrp(caf->seqs, (*curr_af)->seq, CAFSEQ);
      new_seg->cs   = (*curr_af)->s1 - 1;
      new_seg->ce   = (*curr_af)->s2;
      new_seg->rs   = (*curr_af)->r1 - 1;
      new_seg->re   = (*curr_af)->r2;
      new_seg->comp = 0;

    } else {                          /* Reverse strand */
      
      new_seg->read = arrp(caf->seqs, (*curr_af)->seq, CAFSEQ);
      new_seg->cs   = (*curr_af)->s2 - 1;
      new_seg->ce   = (*curr_af)->s1;
      new_seg->rs   = (*curr_af)->r1 - 1;
      new_seg->re   = (*curr_af)->r2;
      new_seg->comp = 1;
      
    }

    inserted = 1;
  }

  if (inserted) {

    /* re-sort the array of segments by end position */

    qsort(arrp(segments, 0, Seg_entry), arrayMax(segments),
	  sizeof(Seg_entry), seg_compare);
  }
}

static char complement(char b)
{
  static char tab[256] = {0};
  int i;

  if (!tab['a']) {
    for (i = 0; i < 256; i++) tab[i] = 'N';
    tab['a'] = 't';
    tab['A'] = 'T';
    tab['c'] = 'g';
    tab['C'] = 'G';
    tab['g'] = 'c';
    tab['G'] = 'C';
    tab['t'] = 'a';
    tab['T'] = 'A';
    tab['n'] = 'n';
    tab['-'] = '-';
  }

  return tab[(int) b];
}

static void make_bayesian_consensus(cafAssembly* caf, CAFSEQ* contig,
				    int i, Array segments, void * params)
{
  int seg;
  int rp;
  int j;
  int q;
  int best;
  int next_best;
  int  bases[256] = {0};
  int max_quals[256] = {0};
  Seg_entry *se;
  char base;
  char qual;

  char *bb = "ACGT-NN";

  bases['N']     = 1;
  max_quals['N'] = 0;

  for (seg = 0; seg < arrayMax(segments); seg++) {
    se = arrp(segments, seg, Seg_entry);

    if (se->comp) { /* reverse strand */

      rp = se->re - (i - se->cs);
      base = (char) toupper((int) complement(*(stackText(se->read->dna, rp))));
      qual = (int) arr(se->read->base_quality, rp, char);

    } else {        /* forward strand */
      
      rp = i - se->cs + se->rs;
      base = (char) toupper((int) *(stackText(se->read->dna, rp)));
      qual = (int) arr(se->read->base_quality, rp, char);

    }

    bases[(int) base] += qual;
    if (max_quals[(int) base] < qual) max_quals[(int) base] = qual;

  }

  bases[0] = bases['A'];
  max_quals[0] = max_quals['A'];
  bases[1] = bases['C'];
  max_quals[1] = max_quals['C'];
  bases[2] = bases['G'];
  max_quals[2] = max_quals['G'];
  bases[3] = bases['T'];
  max_quals[3] = max_quals['T'];
  bases[4] = bases['-'];
  max_quals[4] = max_quals['-'];
  bases[5] = bases['N'];
  max_quals[5] = max_quals['N'];
  bases[6] = -1;
  max_quals[6] = max_quals['N'];

  best = 6;
  next_best = -1;
  for (j = 0; j < 6; j++) {
    if (bases[j] > bases[best]) {
      next_best = best;
      best = j;
    } else if (bases[j] > bases[next_best]) {
      next_best = j;
    }
  }

  *(stackText(contig->dna, i)) = bb[best];
  q = max_quals[best] - max_quals[next_best];
  if (q < 0) q = 0;
  array(contig->base_quality, i, char) = (char) q;
}
