/*  Last edited: Aug 17 09:39 2001 (rmd) */
#include <caf.h>

typedef void (*consensus_iterator_callback)(cafAssembly*, CAFSEQ*, int, Array, void *);

int recalc_consensus(cafAssembly* caf);
int recalc_consensus_contig(cafAssembly* caf, CAFSEQ* contig);
int iterate_contig_consensus(cafAssembly* caf, CAFSEQ* seq,
			     consensus_iterator_callback callback,
			     void *callback_params);
