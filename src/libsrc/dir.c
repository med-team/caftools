/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <search.h>
#include <string.h>

extern FILE *error_f;

char *has_extension(char *name);
int matches_extension(char * name, char *ext);
int matches_suffix(char * name, char *suffix);

FILE *next_file(char *dir, char *ext, char *name, int restart )
{
  static DIR *current_dir=NULL;
  char buf[1024];
  struct dirent *fil;
  FILE *fp;

  if ( ! current_dir )
    {
      if ( ! (current_dir = (DIR*)opendir(dir) ) )
	{
	  fprintf( stderr, "ERROR: directory %s cannot be opened\n", dir );
	  fprintf( error_f, "ERROR: directory %s cannot be opened\n", dir );
/*	  exit_on_error();*/
	}
    }

  if ( restart )
    rewinddir( current_dir );

  while ((fil = (struct dirent*)readdir( current_dir )) != 0)
    {
      snprintf( buf, sizeof(buf), "%s/%s", dir, fil->d_name );
      if ( matches_extension( fil->d_name, ext )  && (fp = (FILE*)fopen( buf, "r" ) ) ) /* && use_this_probe(fil->d_name) */
	{
	  strcpy(name,fil->d_name);
	  return fp;
	}
    }

  current_dir=NULL;
  return NULL;
}

FILE *next_suffix_file(char *dir, char *suffix, char *name, int restart )
{
  static DIR *current_dir=NULL;
  char buf[256], *buf_ = buf;
  size_t bufsz = 256;
  size_t dir_len = strlen(dir);
  struct dirent *fil;
  FILE *fp;

  if ( ! current_dir )
    {
      if ( ! (current_dir = (DIR*)opendir(dir) ) )
	{
	  fprintf( stderr, "ERROR: directory %s cannot be opened\n", dir );
	  fprintf( error_f, "ERROR: directory %s cannot be opened\n", dir );
/*	  exit_on_error();*/
	}
    }

  if ( restart )
    rewinddir( current_dir );

  while ((fil = readdir(current_dir)) != 0)
    {
      size_t fname_len = strlen(fil->d_name);
      if (dir_len + fname_len + 2 > bufsz)
	{
	  if (buf_ != buf)
	    free(buf_);
	  buf_ = malloc(dir_len + fname_len + 2);
	  if (!buf_)
	    {
	      perror(NULL);
	      return NULL;
	    }
	  bufsz = dir_len + fname_len + 2;
      }
      snprintf( buf_, bufsz, "%s/%s", dir, fil->d_name );
      if ( matches_suffix( fil->d_name, suffix )  && (fp = (FILE*)fopen( buf, "r" ) ) ) /* && use_this_probe(fil->d_name) */
	{
	  strcpy(name,fil->d_name);
	  if (buf_ != buf)
            free(buf_);
	  return fp;
	}
    }
  if (buf_ != buf)
    free(buf_);
  current_dir=NULL;
  return NULL;
}

int matches_suffix(char *name, char *suffix)
{
  int pos = strlen(name)-strlen(suffix);
  if ( pos >= 0 )
    return ! strcmp( &name[pos], suffix );
  else
    return 0;
}

int matches_extension(char *name, char *ext)
{
  char *s = (char*)has_extension( name );

 if ( s && ! strcmp(s, ext ) )
   return 1;
 else
   return 0;
}

char *has_extension(char *name )
{
  return (char*)strrchr( name, '.' );
}

