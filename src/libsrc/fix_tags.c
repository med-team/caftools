/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 * its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * Last edited: May 21 16:56 1996 (rmott)
 * $Id: fix_tags.c 17348 2006-04-27 15:36:56Z jkb $
 * $Log$
 * Revision 1.6  2006/04/27 15:36:56  jkb
 * Added protection for tags being outside the scope of the sequence
 * positions. They now get clipped.
 *
 * Revision 1.5  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 * Revision 1.4  2002/03/04 17:00:40  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.3  1998/07/02 10:06:38  badger
 * (a) Fixed incorrect end conditions checks in quality/position interpolate functions
 * (b) Interpolation only worked correctly when interpolating one base. Now correct.
 * (c) BP_SIZE used instead of BQ_SIZE when interpolating quality. Caused odd zero
 * qual values to appear.
 *
 * Revision 1.2  1997/07/22  11:25:42  badger
 * added base_position (saint louis)
 * fixed a few bugs.
 *
 * Revision 1.6  1997/07/18  20:56:35  sdear
 * Added comments about interpolation.
 *
 * Revision 1.5  1997/07/18  20:51:30  sdear
 * Fixed a number of bugs regarding adjustment of
 * quality and position of bases.
 * Added interpolation of base qualities, when
 * insert_quality is -1.
 *
 * Revision 1.4  1997/07/17  19:35:49  sdear
 * Added interpolate to adjust_position(1).
 * Fixed an out-by-one bug which caused depad operation to
 * delete non-pad qual values!
 *
 * Revision 1.3  1997/07/15  20:56:17  sdear
 * Bug results in first position of quality and position
 * info being set to 0. Fixed.
 *
 * Revision 1.2  1997/07/15  19:13:13  sdear
 * Support for BasePosition
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Revision 1.2  1996/10/03  17:44:38  rmott
 * *** empty log message ***
 *
 * Revision 1.1  1996/02/21  11:32:23  rmott
 * Initial revision
 *
 */

/* functions to adjust tag coordinates */

#include<stdio.h>
#include"caf.h"

void adjust_tags1( CAFSEQ *seq, Array old2new )
{
	  fix_tags1(seq->tags,old2new);
	  fix_tags1(seq->cvector,old2new);
	  fix_tags1(seq->svector,old2new);
	  fix_tags1(seq->clipping,old2new);
	  fix_tags1(seq->golden_path,old2new);
}

void adjust_tags( CAFSEQ *seq, Array old2new )
{
	  fix_tags(seq->tags,old2new);
	  fix_tags(seq->cvector,old2new);
	  fix_tags(seq->svector,old2new);
	  fix_tags(seq->clipping,old2new);
	  fix_tags(seq->golden_path,old2new);
}

#define bounded_array(a,i,type) (                                   \
   ((i) >= 0 && (i) <= arrayMax((a)))                               \
       ? (array((a),(i),type))                                    \
       : ( (i) < 0                                                  \
	   ? (array((a),0,type)+(i))                              \
	   : (array((a), arrayMax((a)), type)+(i)-arrayMax((a)))  \
	 )                                                          \
)

void fix_tags( Array tags, Array old2new )

/* adjusts tag coordinates using the mapping defined in old2new */
{
  TAG *tag;
  int x1, x2;
  int i, j;

  if ( tags )
    for(i=0;i<arrayMax(tags);i++)
      {
	tag = arrp(tags,i,TAG);
	x1 = x2 = 0;
	for(j=tag->x1;j<=tag->x2;j++)
	  if ( (x1=bounded_array(old2new,j,int)) > 0 )
	    break;
	if ( x1 < 1 )
	  for(j=tag->x1;j>0;j--)
	    if ( (x1=bounded_array(old2new,j,int)) > 0 )
	      break;
	    
	for(j=tag->x2;j>=tag->x1;j--)
	  if ( (x2 = bounded_array(old2new,j,int)) > 0 )
	    break;
	if ( x2 < 1 )
	  for(j=tag->x2;j<arrayMax(old2new);j++)
	    if ( (x2=bounded_array(old2new,j,int))>0)
	      break;

	if ( x1 && x2 )
	  {
	    tag->x1 = x1;
	    tag->x2 = x2;
	  }
	else
	  {
/*	    printf("lost tag at %d %d %d %d\n", tag->x1, x1, tag->x2, x2); */
	    tag->x1 = tag->x2 = 0;
	  }
      }
}

void fix_tags1( Array tags, Array old2new )
{
  TAG *tag;
  int x1 = 0, x2 = 0;
  int i, j;

  if ( tags )
    for(i=0;i<arrayMax(tags);i++)
      {
	tag = arrp(tags,i,TAG);
/*	printf("tag 2 %d %d %d\n",i,tag->x1,tag->x2); */
	if ( tag->x1 != 1 || tag->x2 != 0 )
	  {
	    for(j=tag->x1;j<=tag->x2;j++)
	      if ( (x1=array(old2new,j,I_SIZE)) > 0 )
		break;
	    if ( x1 < 1 )
	      for(j=tag->x1;j>0;j--)
		if ( (x1=array(old2new,j,I_SIZE)) > 0 )
		  break;
	    
	    for(j=tag->x2;j>=tag->x1;j--)
	      if ( (x2 = array(old2new,j,I_SIZE)) > 0 )
		break;
	    if ( x2 < 1 )
	      for(j=tag->x2;j<arrayMax(old2new);j++)
		if ( (x2=array(old2new,j,I_SIZE))>0)
		  break;
	    
	    if ( x1 && x2 )
	      {
		tag->x1 = x1;
		tag->x2 = x2;
	      }
	    else
	      {
		/*	    printf("lost tag at %d %d %d %d\n", tag->x1, x1, tag->x2, x2); */
		tag->x1 = tag->x2 = 0;
	      }
	  }
      }
}


static void
interpolate_quality(Array quality, int start, int end)
     /*
       interpolate base qualitys between qualityed
       bases start and end.
       */
{
  int start_val, end_val;
  int max;
  int i;
  max = arrayMax(quality);
  if (start == -1 && end == max) return;
  if (start == end) return;
  if (start == -1) /* first base(s) was inserted */
    start_val = arr(quality, end, BQ_SIZE);
  else
    start_val = arr(quality, start, BQ_SIZE);
  if (end == max) /* last base(s) was inserted */
    end_val = arr(quality, start, BQ_SIZE);
  else
    end_val = arr(quality, end, BQ_SIZE);
  
  for(i=start+1;i<end;i++) {
    arr(quality,i,BQ_SIZE) = start_val + (int)((i-start)*(end_val - start_val)/(end-start));
  }
}

Array adjust_quality( CAFSEQ *seq, Array old2new, int max, int insert_quality )

/* create a new base_quality array using the mapping in old2new
Remember that since the old2new array has origin at 1 we have to shift
as base_quality is centred at 0 

Assumes old2new is monotonically increasing 

base insertions have the value insert_quality
a value of -1 means values are interpolated linearly from
surrounding base values.

*/ 

{ 
  Array quality=NULL; 
  int i, k;

  if ( seq->base_quality )
    {
      quality = arrayCreate(max,BQ_SIZE);
      for(i=0;i<max;i++)
	*arrayp(quality,i,BQ_SIZE) = insert_quality;

      for(i=1;i<arrayMax(old2new);i++)
	{
	  k = arr(old2new,i,int); /* base_quality coords start at 0 */
	  if ( k > 0 )
	    *arrayp(quality,k-1,BQ_SIZE) = arr(seq->base_quality,i-1,BQ_SIZE);
	}

      /* interpolate missing values */
      if (insert_quality == -1) {
	int start, last, this;
	start = -1;
	last = 0;
	for(i=0;i<arrayMax(quality);i++) {
	  this = arr(quality,i,BQ_SIZE);
	  if (last == -1 && this != -1)
	    interpolate_quality(quality,start,i);
	  last = this;
	  if (this != -1)
	    start = i;
	}
	if (last == -1)
	  interpolate_quality(quality,start,i);
      }


      arrayDestroy(seq->base_quality);
      seq->base_quality = quality;
    }
  return quality;
}

Array adjust_quality1( CAFSEQ *seq, Array old2new, int max, int insert_quality )

/* create a new base_quality array using the mapping in old2new
Remember that since the old2new array has origin at 1 we have to shift
as base_quality is centred at 0 

Assumes old2new is monotonically increasing 

base insertions have the value insert_quality
a value of -1 means values are interpolated linearly from
surrounding base values.

*/ 

{ 
  Array quality=NULL; 
  int i, k;

  if ( seq->base_quality )
    {
      quality = arrayCreate(max,BQ_SIZE);

      for(i=0;i<max;i++)
	*arrayp(quality,i,BQ_SIZE) = insert_quality;

      for(i=arrayMax(old2new);i>0;i--)
	{
	  k = arr(old2new,i,I_SIZE); /* base_quality coords start at 0 */
	  if ( k > 0 )
	    *arrayp(quality,k-1,BQ_SIZE) = arr(seq->base_quality,i-1,BQ_SIZE);
	}

      /* interpolate missing values */
      if (insert_quality == -1) {
	int start, last, this;
	start = -1;
	last = 0;
	for(i=0;i<arrayMax(quality);i++) {
	  this = arr(quality,i,BQ_SIZE);
	  if (last == -1 && this != -1)
	    interpolate_quality(quality,start,i);
	  last = this;
	  if (this != -1)
	    start = i;
	}
	if (last == -1)
	  interpolate_quality(quality,start,i);
      }

      arrayDestroy(seq->base_quality);
      seq->base_quality = quality;
    }
  return quality;
}


static void
interpolate_position(Array position, int start, int end)
     /*
       interpolate base positions between positioned
       bases start and end.
       */
{
  int start_val, end_val;
  int max;
  int i;
  max = arrayMax(position);
  if (start == -1 && end == max) return;
  if (start == end) return;
  if (start == -1) /* first base(s) was inserted */
    start_val = arr(position, end, BP_SIZE);
  else
    start_val = arr(position, start, BP_SIZE);
  if (end == max) /* last base(s) was inserted */
    end_val = arr(position, start, BP_SIZE);
  else
    end_val = arr(position, end, BP_SIZE);
  
  for(i=start+1;i<end;i++) {
    arr(position,i,BP_SIZE) = start_val + (int)((i-start)*(end_val - start_val)/(end-start));
  }
}


Array adjust_position( CAFSEQ *seq, Array old2new, int max, int insert_position )

/* create a new base_position array using the mapping in old2new
Remember that since the old2new array has origin at 1 we have to shift
as base_position is centred at 0 

Assumes old2new is monotonically increasing 

base insertions have the value insert_position
a value of -1 means values are interpolated linearly from
surrounding base values.

*/ 

{ 
  Array position=NULL; 
  int i, k;

  if ( seq->base_position )
    {
      position = arrayCreate(max,BP_SIZE);
      for(i=0;i<max;i++)
	*arrayp(position,i,BP_SIZE) = insert_position;

      for(i=1;i<arrayMax(old2new);i++)
	{
	  k = arr(old2new,i,int)-1; /* base_position coords start at 0 */
	  if ( k >= 0 )
	    *arrayp(position,k,BP_SIZE) = arr(seq->base_position,i-1,BP_SIZE);
	}

      /* interpolate missing values */
      if (insert_position == -1) {
	int start, last, this;
	start = -1;
	last = 0;
	for(i=0;i<arrayMax(position);i++) {
	  this = arr(position,i,BP_SIZE);
	  if (last == -1 && this != -1)
	    interpolate_position(position,start,i);
	  last = this;
	  if (this != -1)
	    start = i;
	}
	if (last == -1)
	  interpolate_position(position,start,i);
      }

      arrayDestroy(seq->base_position);
      seq->base_position = position;
    }
  return position;
}

Array adjust_position1( CAFSEQ *seq, Array old2new, int max, int insert_position )

/* create a new base_position array using the mapping in old2new
Remember that since the old2new array has origin at 1 we have to shift
as base_position is centred at 0 

Assumes old2new is monotonically increasing 

base insertions have the value insert_position
a value of -1 means values are interpolated linearly from
surrounding base values.

*/ 

{ 
  Array position=NULL; 
  int i, k;

  if ( seq->base_position )
    {
      position = arrayCreate(max+1,BP_SIZE);

      for(i=0;i<max;i++)
	*arrayp(position,i,BP_SIZE) = insert_position;

      for(i=arrayMax(old2new);i>0;i--)
	{
	  k = arr(old2new,i,I_SIZE); /* base_position coords start at 0 */
	  if ( k > 0 )
	    *arrayp(position,k-1,BP_SIZE) = arr(seq->base_position,i-1,BP_SIZE);
	}

      /* interpolate missing values */
      if (insert_position == -1) {
	int start, last, this;
	start = -1;
	last = 0;
	for(i=0;i<arrayMax(position);i++) {
	  this = arr(position,i,BP_SIZE);
	  if (last == -1 && this != -1)
	    interpolate_position(position,start,i);
	  last = this;
	  if (this != -1)
	    start = i;
	}
	if (last == -1)
	  interpolate_position(position,start,i);
      }

      arrayDestroy(seq->base_position);
      seq->base_position = position;
    }
  return position;
}

	    
