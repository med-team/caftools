/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 * its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * $Log$
 * Revision 1.3  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 * Revision 1.2  2002/03/04 17:00:41  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Revision 1.5  1997/02/10  14:12:09  rmott
 * added code to report direction of splicing
 *
 * Revision 1.4  1997/02/10  14:08:26  rmott
 * searches for reversed splice sites properly
 *
 * Revision 1.2  1997/01/30  17:21:57  rmott
 * fixed bug and removed debugging code
 *
 * Revision 1.1  1997/01/30  17:04:47  rmott
 * Initial revision
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <cl.h>
#include <seq_util.h>
#include <lin_align.h>
#include <pair_memory.h>


ge_alignment *
non_recursive_est_to_genome( SEQUENCE *est, SEQUENCE *genome, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, SEQUENCE *splice_sites, int backtrack, int needleman, int init_path )
  
  /* 
     Modified Smith-Waterman/Needleman to align an EST or mRNA to a Genomic
     sequence, allowing for introns. The recursion is
     
     {  S[gpos-1][epos]   - gap_penalty
     {  S[gpos-1][epos-1] + D[gpos][epos]
     S[gpos][epos] = max {  S[gpos][epos-1]   - gap_penalty
     {  C[epos]           - intron_penalty 
     {  0 (optional, only if ! needleman )
     
     C[epos] = max{ S[gpos][epos], C[epos] }
     
     S[gpos][epos] is the score of the best path to the cell gpos, epos 
     C[epos] is the score of the best path to the column epos
     
     The intron_penalty may be modified to splice_penalty if splice_sites is
     non-null and there are DONOR and ACCEPTOR sites at the start and
     end of the intron.
     
     If backtrack is 0 then only the start and end points and the score
     are computed, and no path matrix is allocated.
     
     If init_path  is DIAGONAL then the boundary conditions are adjusted so that the optimal 
     path enters the cell (0,0) diagonally. Otherwise is enters from the left (ie as a deletion in the EST)

     */
  
{
  unsigned char **ppath = 0, *path = 0;
  int *score1, *score2;
  int *s1, *s2, *s3;
  int *best_intron_score, *best_intron_coord;
  int e_len_pack = est->len/4+1;
  int gpos, epos;
  int emax = -1, gmax = -1;
  int max_score = 0;
  int diagonal, delete_genome, delete_est, intron;
  char *gseq, *eseq, g;
  int max, total = 0;
  int p, pos;
  int *temp_path = 0;
  int is_acceptor;
  ge_alignment *ge = (ge_alignment*)calloc(1,sizeof(ge_alignment));
  coords *start1 = 0, *start2 = 0, *t1 = 0, *t2 = 0, *t3 = 0, *best_intron_start = 0, best_start = { 0 };
  int splice_type = 0;

  unsigned char direction;
  unsigned char diagonal_path[4] = { 1, 4, 16, 64 };
  unsigned char delete_est_path[4] = { 2, 8, 32, 128 };
  unsigned char delete_genome_path[4] = { 3, 12, 48, 192 };
  unsigned char mask[4] = { 3, 12, 48, 192 };

  /* path is encoded as 2 bits per cell:
     
     00 intron
     10 diagonal
     01 delete_est
     11 delete_genome
     
     */

  /* the backtrack path, packed 4 cells per byte */

  if ( backtrack )
    {
      ppath = (unsigned char**)calloc( genome->len, sizeof(unsigned char*));
      for(gpos=0;gpos<genome->len;gpos++)
	ppath[gpos] = (unsigned char*)calloc( e_len_pack, sizeof(unsigned char) );
      
      temp_path = (int*)calloc( genome->len+est->len, sizeof(int));
    }
  else
    {
      start1 = (coords*)calloc(est->len+1,sizeof(coords));
      start2 = (coords*)calloc(est->len+1,sizeof(coords));
      best_intron_start = (coords*)calloc(est->len,sizeof(coords));

      t1 = start1+1;
      t2 = start2+1;
    }

  score1 = (int*)calloc(est->len+1,sizeof(int));
  score2 = (int*)calloc(est->len+1,sizeof(int));
  
  s1 = score1+1;
  s2 = score2+1;

  best_intron_coord = (int*)calloc(est->len+1,sizeof(int));
  best_intron_score = (int*)calloc(est->len+1,sizeof(int));
    
  gseq = downcase(strdup(genome->s));
  eseq = downcase(strdup(est->s));
  
  if ( ! backtrack ) /* initialise the boundaries for the start points */
    for(epos=0;epos<est->len;epos++)
      {
	t1[epos].left = 0;
	t1[epos].right = epos;
	best_intron_start[epos] = t1[epos];
      }
  if ( needleman )
    for(epos=0;epos<est->len;epos++)
      {
	s1[epos] = MINUS_INFINITY;
	best_intron_score[epos] = MINUS_INFINITY;
      }
  
  for(gpos=0;gpos<genome->len;gpos++)
    {
      s3 = s1; s1 = s2; s2 = s3;
      
      g = gseq[gpos];

      if ( backtrack )
	path = ppath[gpos];
      else
	{
	  t3 = t1; t1 = t2; t2 = t3;
	  t1[-1].left = gpos;
	  t1[-1].right = 0;
	}

      if ( splice_sites && (splice_sites->s[gpos] & ACCEPTOR ) )
	is_acceptor = 1;	/* gpos is last base of putative intron */
      else
	is_acceptor = 0;

/* initialisation */

      if ( needleman )
	{
	  if ( init_path == DIAGONAL || gpos > 0  )
	    s1[-1] = MINUS_INFINITY;
	  else
	    s1[-1] = 0;
	}
      else
	s1[-1] = 0;
      
      for(epos=0;epos<est->len;epos++)
	{
	  /* align est and genome */

	  diagonal = s2[epos-1] + lsimmat[(int) g][(int) eseq[epos]];
	  
	  /* single deletion in est */

	  delete_est = s1[epos-1] - gap_penalty;

	  /* single deletion in genome */

	  delete_genome = s2[epos] - gap_penalty;

	  /* intron in genome, possibly modified by donor-acceptor splice sites */

	  if ( is_acceptor && (splice_sites->s[best_intron_coord[epos]] & DONOR ) )
	    intron = best_intron_score[epos] - splice_penalty;
	  else
	    intron = best_intron_score[epos] - intron_penalty;
	    
	  if ( delete_est > delete_genome )
	    max = delete_est;
	  else
	    max = delete_genome;

	  if ( diagonal > max )
	    max = diagonal;
	  
	  if ( intron > max )
	    max = intron;

	  if ( needleman || max > 0 )
	    {
	      if ( max == diagonal )
		{
		  s1[epos] = diagonal;
		  if ( backtrack )
		    path[epos/4] |= diagonal_path[epos%4];
		  else
		    {
		      if ( t2[epos-1].left == -1 )
			{
			  t1[epos].left = gpos;
			  t1[epos].right = epos;
			}
		      else
			t1[epos] = t2[epos-1];
		    }
		}
	      else if ( max == delete_est )
		{
		  s1[epos] = delete_est;
		  if ( backtrack )
		    path[epos/4] |= delete_est_path[epos%4];
		  else
		    t1[epos] = t1[epos-1];
		}
	      else if ( max == delete_genome )
		{
		  s1[epos] = delete_genome;
		  if ( backtrack )
		    path[epos/4] |= delete_genome_path[epos%4];
		  else
		    t1[epos] = t2[epos];
		}
	      else
		{
		  s1[epos] = intron;
		  if ( ! backtrack )
		    t1[epos] = best_intron_start[epos];
		}
	    }
	  else
	    {
	      s1[epos] = 0;
	      if ( ! backtrack )
		{
		  t1[epos].left = -1;
		  t1[epos].right = -1;
		}
	    }

	  if ( best_intron_score[epos] < s1[epos] )
	    {
	      /* if ( intron > 0 ) */ /* will only need to store if this path is positive */
		if ( backtrack )
		  if ( do_not_forget(epos, best_intron_coord[epos]) == 0 ) /* store the previous path just in case we need it */
		    { /* error - stack ran out of memory. Clean up and return NULL */

		      free(score1);
		      free(score2);
		      free(eseq);
		      free(gseq);
		      free(best_intron_score);
		      free(best_intron_coord);
		      free(temp_path);
		      for(gpos=0;gpos<genome->len;gpos++)
			free(ppath[gpos]);
		      free(ppath);
		      free_rpairs();
		      free(ge);

		      return NULL;
		    }

	      best_intron_score[epos] = s1[epos];
	      best_intron_coord[epos] = gpos;
	      if ( ! backtrack )
		best_intron_start[epos] = t1[epos];
	    }

	  if ( ! needleman && max_score < s1[epos] )
	    {
	      max_score = s1[epos];
	      emax = epos;
	      gmax = gpos;
	      if ( ! backtrack )
		best_start = t1[epos];
	    }
	}
    }

  /* back track */

  if ( needleman )
    {
      ge->gstop = genome->len-1;
      ge->estop = est->len-1;
      ge->score = s1[ge->estop];
    }
  else
    {
      ge->gstop = gmax;
      ge->estop = emax;
      ge->score = max_score;
    }

  if ( backtrack )
    {
      pos = 0;
      
      epos = ge->estop;
      gpos = ge->gstop;
      total = 0;

      /* determine the type of spliced intron (forward or reversed) */
      
      if ( splice_sites ) {
	if ( ! strcmp( splice_sites->desc, "forward") )
	  splice_type = FORWARD_SPLICED_INTRON;
	else if ( ! strcmp( splice_sites->desc, "reverse") )
	  splice_type = REVERSE_SPLICED_INTRON;
	else 
	  splice_type = INTRON; /* This is really an error - splice_sites MUST have a direction */
      }
	  
      while( ( needleman || total < max_score) && epos >= 0 && gpos >= 0 )
	{
	  direction = (ppath[gpos][epos/4] & mask[epos%4] ) >> (2*(epos%4)); 
	  temp_path[pos++] = direction;
	  if ( direction == INTRON ) /* intron */
	    {
	      int gpos1;

	      if ( gpos-best_intron_coord[epos]  <= 0 )
		{
 		  if ( verbose ) 
		    fprintf(stderr,"WARNING: NEGATIVE intron gpos: %d %d\n", gpos, gpos-best_intron_coord[epos] ); 
		  gpos1 = remember(epos, gpos ); 
		}
	      else
		{
		  gpos1 = best_intron_coord[epos];	      
		}

	      if ( splice_sites && (splice_sites->s[gpos] & ACCEPTOR ) && ( splice_sites->s[gpos1] & DONOR ) )
		{
		  total -= splice_penalty;
		  temp_path[pos-1] = splice_type; /* make note that this is a proper intron */
		}
	      else
		{
		  total -= intron_penalty;
		}

	      temp_path[pos++] = gpos-gpos1; /* intron this far */
	      gpos = gpos1;
	    }
	  else if ( direction == DIAGONAL ) /* diagonal */
	    {
	      total += lsimmat[(int) gseq[gpos]][(int) eseq[epos]];
	      epos--;
	      gpos--;
	    }
	  else if ( direction == DELETE_EST ) /* delete_est */
	    {
	      total -= gap_penalty;
	      epos--;
	    }
	  else			/* delete_genome */
	    {
	      total -= gap_penalty;
	      gpos--;
	    }
	}
      
      gpos++;
      epos++;
      
      
      ge->gstart = gpos;
      ge->estart = epos;
      ge->len = pos;
      
      ge->align_path = (int*)calloc(ge->len,sizeof(int));

      /* reverse the ge so it starts at the beginning of the sequences */

      for(p=0;p<ge->len;p++)
	{
	  if ( temp_path[p] > INTRON ) /* can be INTRON or FORWARD_SPLICED_INTRON or REVERSE_SPLICED_NTRON */
	    ge->align_path[pos-p-1] = temp_path[p];
	  else
	    {
	      ge->align_path[pos-p-2] = temp_path[p];
	      ge->align_path[pos-p-1] = temp_path[p+1];
	      p++;
	    }
	}
    }
  else
    {
      ge->gstart = best_start.left;
      ge->estart = best_start.right;
    }

  free(score1);
  free(score2);
  free(eseq);
  free(gseq);
  free(best_intron_score);
  free(best_intron_coord);

  if ( backtrack )
    {
      free(temp_path);
      
      for(gpos=0;gpos<genome->len;gpos++)
	free(ppath[gpos]);
      free(ppath);
      free_rpairs();
    }
  else
    {
      free(start1);
      free(start2);
      free(best_intron_start);
    }

  if ( verbose )
    {
      indent();
      printf("non-recursive score %d total: %d gstart %d estart %d gstop %d estop %d\n", ge->score, total, ge->gstart, ge->estart, ge->gstop, ge->estop );
    }


  return ge;
}

void
free_ge( ge_alignment *ge )
{
  if ( ge )
    {
      if ( ge->align_path )
	free( ge->align_path );
      free(ge);
    }
}
     
ge_alignment *
recursive_est_to_genome( SEQUENCE *est, SEQUENCE *genome, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, SEQUENCE *splice_sites, float max_area, int init_path )
{
  int middle, gleft, gright, score, i, j;
  SEQUENCE *left_splice=NULL, *right_splice=NULL;
  SEQUENCE *left_genome, *right_genome;
  SEQUENCE *left_est, *right_est;
  ge_alignment *left_ge, *right_ge, *ge;
  float area;
  int split_on_del;

  area = ((float)genome->len+1.0)*((float)est->len+1.0)/4; /* divide by 4 as we pack 4 cells per byte */

  indentation += 3;

  if ( area <= max_area ) /* sequences small enough to align by standard methods */
    {
      if ( verbose ) { indent(); printf("using non-recursive alignment %d %d   %g %g\n", genome->len, est->len, area, max_area ); }
      ge = non_recursive_est_to_genome( est, genome, match, mismatch, gap_penalty, intron_penalty, splice_penalty, splice_sites, 1, 1, DIAGONAL );

      if ( ge != NULL ) /* success */
	return ge;
      else /* failure because we ran out of memory */
	{
	  indentation -= 3;
	  if ( verbose ) { indent(); printf("Stack memory overflow ... splitting\n"); }
	}
    }
  /* need to recursively split */

  if ( verbose ) { indent(); printf("splitting genome and est\n"); }
  
  middle = est->len/2;
  
  score = midpt_est_to_genome( est, genome, match, mismatch, gap_penalty, intron_penalty, splice_penalty, splice_sites, middle, &gleft, &gright );
  if ( verbose ) { indent(); printf("score %d middle %d gleft %d gright %d\n", score, middle, gleft, gright ); }
  
  split_on_del =  ( gleft == gright );
  
  
  /* split genome */
  
  left_genome = subseq( genome, 0, gleft );
  right_genome = subseq( genome, gright, genome->len-1);
  if ( splice_sites )
    {
      left_splice = subseq( splice_sites, 0, gleft );
      right_splice = subseq( splice_sites, gright, genome->len-1);
    }
  /* split est */
  
  left_est = subseq( est, 0, middle );
  right_est = subseq( est, middle+1, est->len-1 );

  /* align left and right parts separately */

  if ( verbose ) { indent(); printf("LEFT\n"); }
  left_ge = recursive_est_to_genome( left_est, left_genome, match, mismatch, gap_penalty, intron_penalty, splice_penalty, left_splice, max_area, DIAGONAL );  

  if ( verbose ) { indent(); printf("RIGHT\n"); }
  right_ge = recursive_est_to_genome( right_est, right_genome, match, mismatch, gap_penalty, intron_penalty, splice_penalty, right_splice, max_area, DIAGONAL );  


      /* merge the alignments */

  ge = (ge_alignment*)calloc(1,sizeof(ge_alignment));
  ge->score = left_ge->score + right_ge->score;
  ge->gstart = 0;
  ge->estart = 0;
  ge->gstop = genome->len-1;
  ge->estop = est->len-1;

  ge->len = left_ge->len+right_ge->len;
  ge->align_path = (int*)calloc( ge->len, sizeof(int));

  for(i=0,j=0;j<left_ge->len;i++,j++)
    ge->align_path[i] = left_ge->align_path[j];
	  
  if ( split_on_del ) /* merge on an est deletion */
    {
      indent();printf("split_on_del\n");
      ge->align_path[i++] = DELETE_EST;
      for(j=1;j<right_ge->len;i++,j++) /* omit first symbol on right-hand alignment */
	ge->align_path[i] = right_ge->align_path[j];
    }
  else
    for(j=0;j<right_ge->len;i++,j++)
      ge->align_path[i] = right_ge->align_path[j];
	    

  free_seq(left_est); free_seq(right_est);
  free_seq(left_genome); free_seq( right_genome);
  if ( splice_sites ) { free_seq(left_splice); free_seq( right_splice); }
  free_ge( left_ge); free_ge( right_ge );

  indentation -= 3;
  return ge;
}


ge_alignment *
linear_space_est_to_genome( SEQUENCE *est, SEQUENCE *genome, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, SEQUENCE *splice_sites, float megabytes )
{
  ge_alignment *ge, *rge;
  SEQUENCE *genome_subseq, *est_subseq, *splice_subseq;
  float area;
  float max_area = megabytes*1.0e6;

  rpair_init( (int)(1.0e6*megabytes) );

  area = ((float)genome->len+1.0)*((float)est->len+1.0)/4; /* divide by 4 as we pack 4 cells per byte */

  if ( area <= max_area ) /* sequences small enough to align by standard methods */
    {
      return non_recursive_est_to_genome( est, genome, match, mismatch, gap_penalty, intron_penalty, splice_penalty, splice_sites, 1, 0, DIAGONAL );
    }
  else /* need to recursively split */
    {
      /* first do a Smith-Waterman without backtracking to find the start and end of the alignment */
      
      ge = non_recursive_est_to_genome( est, genome, match, mismatch, gap_penalty, intron_penalty, splice_penalty, splice_sites, 0, 0, DIAGONAL );  

      /* extract subsequences corresponding to the aligned regions */

      if ( verbose ) { indent(); printf("sw alignment score %d gstart %d estart %d gstop %d estop %d\n", ge->score, ge->gstart, ge->estart, ge->gstop, ge->estop ); }

      genome_subseq = subseq(genome, ge->gstart, ge->gstop );
      est_subseq = subseq(est, ge->estart, ge->estop );
      if ( splice_sites ) 
	splice_subseq = subseq(splice_sites, ge->gstart, ge->gstop );
      else
	splice_subseq = NULL;
      /* recursively do the alignment */

      rge = recursive_est_to_genome( est_subseq, genome_subseq, match, mismatch, gap_penalty, intron_penalty, splice_penalty, splice_subseq, max_area, DIAGONAL );  

      ge->len = rge->len;
      ge->align_path = rge->align_path;

      free(rge);
      free_seq(genome_subseq);
      free_seq(est_subseq);
      if ( splice_subseq ) 
	free(splice_subseq);
      
      return ge;
    }
}
     

int
midpt_est_to_genome( SEQUENCE *est, SEQUENCE *genome, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, SEQUENCE *splice_sites, int middle, int *gleft, int *gright )
  
  /* 
     Modified Needleman-Wunsch to align an EST or mRNA to a Genomic
     sequence, allowing for introns. The recursion is
     
     {  S[gpos-1][epos]   - gap_penalty
     {  S[gpos-1][epos-1] + D[gpos][epos]
     S[gpos][epos] = max {  S[gpos][epos-1]   - gap_penalty
     {  C[epos]           - intron_penalty 
     
     C[epos] = max{ S[gpos][epos], C[epos] }
     
     S[gpos][epos] is the score of the best path to the cell gpos, epos 
     C[epos] is the score of the best path to the column epos
     
     The intron_penalty may be modified to splice_penalty if splice_sites is
     non-null and there are DONOR and ACCEPTOR sites at the start and
     end of the intron.
     
     NB: IMPORTANT:
     
     The input sequences are assumed to be subsequences chosen so that
     they align end-to end, with NO end gaps. Call
     non_recursive_est_to_genome() to get the initial max scoring segment and
     chop up the sequences appropriately.
     
     This Function does not compute the path, instead it finds the
     genome coordinates where the best path crosses epos=middle, so this
     should be called recursively to generate the complete alignment in
     linear space.
     
     The return value is the alignment score.
     gleft, gright are the genome coordinates at the crossing point.
     
     If the alignment crosses middle in a diagonal fashion then gleft+1 == gright
     If the alignment crosses middle by a est deletion (ie horizontally) then gleft == gright
     
     */
  
{
  int *score1, *score2;
  int *s1, *s2, *s3;
  int *best_intron_score, *best_intron_coord;
  int gpos, epos;
  int score;
  int diagonal, delete_genome, delete_est, intron;
  char *gseq, *eseq, g;
  int max;
  int is_acceptor;
  coords *m1, *m2, *m3;
  coords *midpt1, *midpt2, *best_intron_midpt;

  score1 = (int*)calloc(est->len+1,sizeof(int));
  score2 = (int*)calloc(est->len+1,sizeof(int));
  
  s1 = score1+1;
  s2 = score2+1;

  midpt1 = (coords*)calloc(est->len+1,sizeof(coords));
  midpt2 = (coords*)calloc(est->len+1,sizeof(coords));
  
  m1 = midpt1+1;
  m2 = midpt2+1;

  best_intron_coord = (int*)calloc(est->len+1,sizeof(int));
  best_intron_score = (int*)calloc(est->len+1,sizeof(int));
  best_intron_midpt = (coords*)calloc(est->len+1,sizeof(coords));
    
  gseq = downcase(strdup(genome->s));
  eseq = downcase(strdup(est->s));

  middle++;


  /* initialise the boundary: We want the alignment to start at [0,0] */

  for(epos=0;epos<est->len;epos++)
    {
      s1[epos] = MINUS_INFINITY;
      best_intron_score[epos] = MINUS_INFINITY;
    }

  for(gpos=0;gpos<genome->len;gpos++)
    {
      s3 = s1; s1 = s2; s2 = s3;
      m3 = m1; m1 = m2; m2 = m3;

      g = gseq[gpos];

      if ( splice_sites && ( splice_sites->s[gpos] & ACCEPTOR ) )
	is_acceptor = 1;	/* gpos is last base of putative intron */
      else
	is_acceptor = 0;

      
/* boundary conditions */
      
      s1[-1] = MINUS_INFINITY;

/* the meat */

      for(epos=0;epos<est->len;epos++)
	{
	  /* align est and genome */

	  diagonal = s2[epos-1] + lsimmat[(int) g][(int) eseq[epos]];
	  
	  /* single deletion in est */

	  delete_est = s1[epos-1] - gap_penalty;

	  /* single deletion in genome */

	  delete_genome = s2[epos] - gap_penalty;

	  /* intron in genome, possibly modified by donor-acceptor splice sites */

	  if ( is_acceptor && (splice_sites->s[best_intron_coord[epos]] & DONOR ) )
	    intron = best_intron_score[epos] - splice_penalty;
	  else
	    intron = best_intron_score[epos] - intron_penalty;
	    
	  if ( delete_est > delete_genome )
	    max = delete_est;
	  else
	    max = delete_genome;

	  if ( diagonal > max )
	    max = diagonal;
	  
	  if ( intron > max )
	    max = intron;

	  if ( max == diagonal )
	    {
	      s1[epos] = diagonal;
	      if ( epos == middle ) 
		{
		  m1[epos].left = gpos-1;
		  m1[epos].right = gpos;
		}
	      else
		m1[epos] = m2[epos-1];
	    }
	  else if ( max == delete_est )
	    {
	      s1[epos] = delete_est;
	      if ( epos == middle )
		{
		  m1[epos].left = gpos;
		  m1[epos].right = gpos;
		}
	      else
		m1[epos] = m1[epos-1];
	    }
	  else if ( max == delete_genome )
	    {
	      s1[epos] = delete_genome;
	      m1[epos] = m2[epos];
	    }
	  else			/* intron */
	    {
	      s1[epos] = intron;
	      m1[epos] = best_intron_midpt[epos];
	    }

	  if ( best_intron_score[epos] < s1[epos] )
	    {
	      best_intron_score[epos] = s1[epos];
	      best_intron_coord[epos] = gpos;
	      best_intron_midpt[epos] = m1[epos];
	    }
	}
    }

  *gleft = m1[est->len-1].left;
  *gright = m1[est->len-1].right;
  score = s1[est->len-1];

  if ( verbose ) { indent(); printf("mipdt score %d middle %d gleft %d gright %d est->len %d genome->len %d\n", score, middle-1, *gleft, *gright, est->len, genome->len ); }

  free(score1);
  free(score2);
  free(midpt1);
  free(midpt2);
  free(eseq);
  free(gseq);
  free(best_intron_score);
  free(best_intron_coord);

  return score;
}

void
blast_style_output( FILE *blast, SEQUENCE *genome, SEQUENCE *est, ge_alignment *ge, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, int gapped, int reverse  )
  
{
  int gsub, gpos, esub, epos, tsub, p;
  int matches=0, len=0, m;
  int total_matches=0, total_len=0;
  float percent;

  gsub = gpos = ge->gstart;
  esub = epos = ge->estart;

  if ( blast )
    {
      tsub = 0;
      for(p=0;p<ge->len;p++)
	if ( ge->align_path[p] <= INTRON )
	  {
	    write_MSP( blast, &matches, &len, &tsub, genome, gsub, gpos, est, esub, epos, reverse, gapped );
	    if ( gapped )
	      {
		if ( ge->align_path[p] == INTRON )
		  {
		      fprintf( blast, "?Intron  %5d %5.1f %5d %5d %-12s\n", -intron_penalty, 0.0, gpos+1, gpos+ge->align_path[p+1], genome->name ); 
		  }
		else /* proper intron */
		  {
		    if ( ge->align_path[p] == FORWARD_SPLICED_INTRON )
		      fprintf( blast, "+Intron  %5d %5.1f %5d %5d %-12s\n", -splice_penalty, 0.0, gpos+1, gpos+ge->align_path[p+1], genome->name ); 
		    else
		      fprintf( blast, "-Intron  %5d %5.1f %5d %5d %-12s\n", -splice_penalty, 0.0, gpos+1, gpos+ge->align_path[p+1], genome->name ); 

		  }
	      }

	    gpos += ge->align_path[++p];
	    esub = epos;
	    gsub = gpos;
	  }
	else if ( ge->align_path[p] == DIAGONAL )
	  {
	    m = lsimmat[(int) genome->s[gpos]][(int) est->s[epos]];
	    tsub += m;
	    if ( m > 0 )
	      {
		matches++;
		total_matches++;
	      }
	    len++;
	    total_len++;
	    gpos++;
	    epos++;
	  }
	else if ( ge->align_path[p] == DELETE_EST )
	  {
	    if ( gapped )
	      {
		tsub -= gap_penalty;
		epos++;
		len++;
		total_len++;
	      }
	    else
	      {
		write_MSP( blast, &matches, &len, &tsub, genome, gsub, gpos, est, esub, epos, reverse, gapped );
		epos++;
		esub = epos;
		gsub = gpos;
	      }
	  }
	else if ( ge->align_path[p] == DELETE_GENOME )
	  {
	    if ( gapped )
	      {
		tsub -= gap_penalty;
		gpos++;
		total_len++;
		len++;
	      }
	    else
	      {
		write_MSP( blast, &matches, &len, &tsub, genome, gsub, gpos, est, esub, epos, reverse, gapped );
		gpos++;
		esub = epos;
		gsub = gpos;
	      }
	  }
      write_MSP( blast, &matches, &len, &tsub, genome, gsub, gpos, est, esub, epos, reverse, gapped );

      if ( gapped )
	{
	  if ( total_len > 0 )
	    percent = (total_matches/(float)(total_len))*100.0;
	  else
	    percent = 0.0;

	  if ( reverse )
	    fprintf( blast, "\nSpan     %5d %5.1f %5d %5d %-12s %5d %5d %-12s  %s\n",  ge->score, percent, ge->gstop+1, ge->gstart+1, genome->name, est->len-ge->estop, est->len-ge->estart,  est->name, est->desc );
	  else
	    fprintf( blast, "\nSpan     %5d %5.1f %5d %5d %-12s %5d %5d %-12s  %s\n",  ge->score, percent, ge->gstart+1, ge->gstop+1, genome->name, ge->estart+1, ge->estop+1,  est->name, est->desc );
	}

    }
}
void
write_MSP( FILE *fp, int *matches, int *len, int *tsub, SEQUENCE *genome, int gsub, int gpos, SEQUENCE *est, int esub, int epos, int reverse, int gapped )
{
  float percent;

  if ( *len > 0 )
    percent = (*matches/(float)(*len))*100.0;
  else
    percent = 0.0;

  if ( percent > 0 )
    {
      if ( gapped )
	fprintf( fp, "Exon     " );
      else
	fprintf( fp, "Segment  " );
      if ( reverse )
	fprintf(fp, "%5d %5.1f %5d %5d %-12s %5d %5d %-12s  %s\n",  *tsub, percent, gpos, gsub+1, genome->name,  est->len-epos+1, est->len-esub,  est->name, est->desc );
      else
	fprintf(fp, "%5d %5.1f %5d %5d %-12s %5d %5d %-12s  %s\n",  *tsub, percent, gsub+1, gpos, genome->name, esub+1, epos, est->name, est->desc );
    }
  *matches = *len = *tsub = 0;
}

void
print_align( FILE *fp, SEQUENCE *genome, SEQUENCE *est, ge_alignment *ge, int width )
{
  int gpos, epos, pos, len, i, j, max, m;
  char *gbuf;
  char *ebuf;
  char *sbuf;
  int *gcoord, *ecoord;
  int namelen = strlen(genome->name) > strlen(est->name) ? strlen(genome->name): strlen(est->name)  ;
  char format[256];
  sprintf(format, "%%%ds %%6d ", namelen );
  if ( fp )
    {
      fprintf(fp, "\n");
      len = genome->len + est->len + 1;
      
      gbuf = (char*)calloc(len,sizeof(char));
      ebuf = (char*)calloc(len,sizeof(char));
      sbuf = (char*)calloc(len,sizeof(char));
      
      gcoord = (int*)calloc(len,sizeof(int));
      ecoord = (int*)calloc(len,sizeof(int));
      
      gpos = ge->gstart;
      epos = ge->estart;
      len = 0;
      for(pos=0;pos<ge->len;pos++)
	{
	  int way = ge->align_path[pos];
	  if ( way == DIAGONAL  ) /* diagonal */
	    {
	      gcoord[len] = gpos;
	      ecoord[len] = epos;
	      gbuf[len] = genome->s[gpos++];
	      ebuf[len] = est->s[epos++];
	      m = lsimmat[(int) gbuf[len]][(int) ebuf[len]];
	      sbuf[len] = ( m > 0 ? '|' : ' ' );
	      len++;
	    }
	  else if ( way == DELETE_EST )
	    {
	      gcoord[len] = gpos;
	      ecoord[len] = epos;
	      gbuf[len] = '-';
	      ebuf[len] = est->s[epos++];
	      sbuf[len] = ' ';
	      len++;
	    }
	  else if ( way == DELETE_GENOME )
	    {
	      gcoord[len] = gpos;
	      ecoord[len] = epos;
	      gbuf[len] = genome->s[gpos++];
	      ebuf[len] = '-';
	      sbuf[len] = ' ';
	      len++;
	    }
	  else if ( way <= INTRON )
	    {
	      /* want enough space to print the first 5 and last 5
		 bases of the intron, plus a string containing the
		 intron length */

	      int intron_width = ge->align_path[pos+1];
	      int half_width = intron_width > 10 ? 5 : intron_width/2;
	      int g = gpos-1;
	      char number[30];
	      int numlen;
	      sprintf(number," %d ", intron_width );
	      numlen = strlen(number);

	      for(j=len;j<len+half_width;j++)
		{
		  g++;
		  gcoord[j] = gpos-1;
		  ecoord[j] = epos-1;
		  gbuf[j] = tolower((int) genome->s[g]);
		  ebuf[j] = '.';
		  if ( way == FORWARD_SPLICED_INTRON )
		    sbuf[j] = '>';
		  else if ( way == REVERSE_SPLICED_INTRON )
		    sbuf[j] = '<';
		  else 
		    sbuf[j] = '?';
		}
	      len = j;
	      for(j=len;j<len+numlen;j++)
		{
		  gcoord[j] = gpos-1;
		  ecoord[j] = epos-1;
		  gbuf[j] = '.';
		  ebuf[j] = '.';
		  sbuf[j] = number[j-len];
		}
	      len = j;
	      g = gpos + intron_width - half_width-1;
	      for(j=len;j<len+half_width;j++)
		{
		  g++;
		  gcoord[j] = gpos-1;
		  ecoord[j] = epos-1;
		  gbuf[j] = tolower((int) genome->s[g]);
		  ebuf[j] = '.';
		  if ( way == FORWARD_SPLICED_INTRON )
		    sbuf[j] = '>';
		  else if ( way == REVERSE_SPLICED_INTRON )
		    sbuf[j] = '<';
		  else 
		    sbuf[j] = '?';
		}

	      gpos += ge->align_path[++pos];
	      len = j;
	    }
	}
      
      for(i=0;i<len;i+=width)
	{
	  max = ( i+width > len ? len : i+width );

	  fprintf(fp, format, genome->name, gcoord[i]+1 );
	  for(j=i;j<max;j++)
	    fputc( gbuf[j], fp );
	  fprintf(fp," %6d\n", gcoord[j-1]+1 );

	  for(j=0;j<namelen+8;j++)
	    fputc(' ',fp);
	  for(j=i;j<max;j++)
	    fputc( sbuf[j], fp );
	  fputc( '\n', fp );

	  fprintf(fp, format, est->name, ecoord[i]+1 );
	  for(j=i;j<max;j++)
	    fputc( ebuf[j], fp );
	  fprintf(fp," %6d\n\n", ecoord[j-1]+1 );
	}

      fprintf( fp, "\nAlignment Score: %d\n", ge->score );

      free(gbuf);
      free(ebuf);
      free(sbuf);
      free(gcoord);
      free(ecoord);
    }
}  

SEQUENCE *
find_splice_sites( SEQUENCE *genome, int forward )
{
  /* Finds all putative DONOR and ACCEPTOR splice sites 
     
     Returns a sequence object whose "dna" should be interpreted as an
     array indicating what kind (if any) of splice site can be found at
     each sequence position.
     
     DONOR    sites are NNGTNN last position in exon
     ^
     ACCEPTOR sites are NAGN last position in intron
     ^
     if forward==1 then search fot GT/AG
     else               search for CT/AC

     */
   
  SEQUENCE *sites = seqdup( genome );
  int pos;
  char *s = genome->s;

  for(pos=0;pos<sites->len;pos++)
    sites->s[pos] = NOT_A_SITE;

  if ( forward ) { /* gene is in forward direction -splice consensus is gt/ag */
    for(pos=1;pos<sites->len-2;pos++)
      {
	if ( tolower((int) s[pos]) == 'g' && tolower((int) s[pos+1]) == 't' ) /* donor */
	  sites->s[pos-1] |= DONOR; /* last position in exon */
	if ( tolower((int) s[pos]) == 'a' && tolower((int) s[pos+1]) == 'g' ) /* acceptor */
	  sites->s[pos+1] |= ACCEPTOR; /* last position in intron */
      }
    strcpy(sites->desc,"forward"); /* so that other functions know */
  }
  else { /* gene is on reverse strand so splice consensus looks like ct/ac */
    for(pos=1;pos<sites->len-2;pos++)
      {
	if ( tolower((int) s[pos]) == 'c' && tolower((int) s[pos+1]) == 't' ) /* donor */
	  sites->s[pos-1] |= DONOR; /* last position in exon */
	if ( tolower((int) s[pos]) == 'a' && tolower((int) s[pos+1]) == 'c' ) /* acceptor */
	  sites->s[pos+1] |= ACCEPTOR; /* last position in intron */
      }
    strcpy(sites->desc,"reverse"); /* so that other functions know */
  }

  return sites;
}

void
matinit(int match, int mismatch, int gap, int neutral, char padding_char)
{
  int c1, c2;
  
  for(c1=0;c1<256;c1++)
    for(c2=0;c2<256;c2++)
      {
	if ( c1 == c2 )
	  {
	    if ( c1 != '*' && c1 != 'n' &&  c1 != 'N' && c1 != '-' )
	      lsimmat[c1][c2] = match;
	    else
	      lsimmat[c1][c2] = 0;
	  }
	else
	  {
	    if ( c1 == padding_char || c2 == padding_char )
	      lsimmat[c1][c2] = lsimmat[c2][c1] = -gap;
	    else if( c1 == 'n' || c2 == 'n' || c1 == 'N' || c2 == 'N' ) 
	      lsimmat[c1][c2] = lsimmat[c2][c1] = neutral;
	    else
	      lsimmat[c1][c2] = lsimmat[c2][c1] = -mismatch;
	  }
      }

  for(c1=0;c1<256;c1++)
    {
      c2 = tolower(c1);
      lsimmat[c1][c2] = lsimmat[c1][c1];
      lsimmat[c2][c1] = lsimmat[c1][c1];
    }
}

void 
indent( )
{
  int n = indentation;

  while(n--)
    fputc(' ',stdout);
}

