/*  Last edited: Jan  4 20:48 1997 (badger) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
#include"seq_util.h"

#ifndef _LIN_ALIGN_H_
#define _LIN_ALIGN_H_

#ifndef MAIN
int lsimmat[256][256];
int verbose;
int indentation;
#else
extern int lsimmat[256][256];
extern int verbose;
extern int indentation;
#endif

#define MINUS_INFINITY -10000000

typedef enum { INTRON=0, DIAGONAL=1, DELETE_EST=2, DELETE_GENOME=3, FORWARD_SPLICED_INTRON=-1, REVERSE_SPLICED_INTRON=-2 } directions;
typedef enum { NOT_A_SITE=0, DONOR=2, ACCEPTOR=4 } donor_acceptor;

typedef struct 
{
  int gstart, estart;
  int gstop, estop;
  int score;
  int len;
  int *align_path;
}
ge_alignment;

typedef struct
{
  int left, right;
}
coords;

void 
indent( void );

void
print_align( FILE *fp, SEQUENCE *genome, SEQUENCE *est, ge_alignment *ge, int width );

void
blast_style_output( FILE *blast, SEQUENCE *genome, SEQUENCE *est, ge_alignment *ge, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, int gapped, int reverse  );

void
write_MSP( FILE *fp, int *matches, int *len, int *tsub, SEQUENCE *genome, int gsub, int gpos, SEQUENCE *est, int esub, int epos, int reverse, int gapped );

ge_alignment *
linear_space_est_to_genome( SEQUENCE *est, SEQUENCE *genome, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, SEQUENCE *splice_sites, float max_area );

ge_alignment *
recursive_est_to_genome( SEQUENCE *est, SEQUENCE *genome, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, SEQUENCE *splice_sites, float max_area, int init_path );

ge_alignment *
non_recursive_est_to_genome( SEQUENCE *est, SEQUENCE *genome, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, SEQUENCE *splice_sites, int backtrack, int needleman, int init_path );

int
midpt_est_to_genome( SEQUENCE *est, SEQUENCE *genome, int match, int mismatch, int gap_penalty, int intron_penalty, int splice_penalty, SEQUENCE *splice_sites, int middle, int *gleft, int *gright );

SEQUENCE *
find_splice_sites( SEQUENCE *genome, int direction );

void
matinit(int match, int mismatch, int gap, int neutral, char pad_char);

#endif
