/*  Last edited: Jun 22 11:59 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

/* #include <config.h> */

#ifndef _MYSCF_H_
#define _MYSCF_H_

/* 
#undef LITTLE_ENDIAN
#undef BIG_ENDIAN
#ifdef    WORDS_BIGENDIAN
#  define BIG_ENDIAN
#else
#  define LITTLE_ENDIAN
#endif
*/

#include <io_lib/scf.h>

#define MAX_TRACE_VAL_16 65535
#define MAX_TRACE_VAL_8    255

char *scf_subseq( Scf *scf, int start, int stop );

Scf *get_scf( char *filename );

Scf *complement_scf( Scf *scf );

#endif
