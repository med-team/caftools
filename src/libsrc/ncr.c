/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* NCR contains functions for computing logs of binomial coefficients
and factorials */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "ncr.h"

/* binomial coeff */

double lncr(int n, int r)
{
  if ( n < r || r < 0 || n <=0 )
    return 0.0;
  else
    return lfact(n) - lfact(r) - lfact(n-r);
}

/* trinomial coeff */

double lncrs(int n, int r, int s)
{
  if ( r < 0 || s < 0 || n-r-s < 0 )
    return 0.0;
  else
    return lfact(n)-lfact(r)-lfact(s)-lfact(n-r-s);
}

/* factorial */

#define MAX_FACT 100000
double lfact(int n)
{
  static double lf[MAX_FACT];
  static int start;
  
  if ( n <= 0 )
    return 0.0;
  else
    {
      if ( ! start )
	{
	  int i;
	  lf[0] = 0.0;
	  for(i=1;i<MAX_FACT;i++)
	    lf[i] = lf[i-1] + log((double)(i));
	  start=1;
	}
      if ( n >= MAX_FACT )
	{
	  fprintf(stderr,"error - factorial %d too big\n", n );
	  exit(1);
	}
      else
	return lf[n];
    }
}

double log_bin_prob(double p, int n, int r)
{
  double q = log(p+1.0e-20);
  double q1 = log(1.0e0-p+1.0e-20);
  
  return r*q + (n-r)*q1 + lncr(n,r);
}
