/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * Last edited: Sep 18 10:21 1996 (rmott)
 * $Id: padding.c 17347 2006-04-27 15:36:23Z jkb $
 * $Log$
 * Revision 1.11  2006/04/27 15:36:23  jkb
 * Fixed processing of Align_to_SCF records when the first one does not
 * start with "1" for the trace component (2nd two fields).
 *
 * Revision 1.10  2003/07/25 11:29:39  rmd
 * Fixed bug where pad_contig failed if the assembled_from lines were not in
 * the order that was expected.
 *
 * Revision 1.9  2003/07/25 10:47:30  dgm
 * Fixed comments within comments
 *
 * Revision 1.8  2002/03/04 17:00:41  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.7  1999/11/03 11:32:50  rmd
 * Removed verbose messages from caf_depad
 *
 * Revision 1.6  1999/04/01 09:24:00  rmd
 * Added caf_read_scf to read compressed SCF files directly rather than
 * use a temp file. Also protect stolen read tags when printing them out.
 *
 * Revision 1.5  1998/07/02 10:06:39  badger
 * (a) Fixed incorrect end conditions checks in quality/position interpolate functions
 * (b) Interpolation only worked correctly when interpolating one base. Now correct.
 * (c) BP_SIZE used instead of BQ_SIZE when interpolating quality. Caused odd zero
 * qual values to appear.
 *
 * Revision 1.4  1998/06/29  15:40:59  sd
 * Interpolate quality when padding
 *
 * Revision 1.3  1998/01/12  16:45:23  badger
 * Fixed 2 problems in caf_depad:
 * Reads ending in pads were not handled correctly
 * Reads ending with pads then a single base got truncated
 *
 * Revision 1.2  1997/07/22  11:25:42  badger
 * added base_position (saint louis)
 * fixed a few bugs.
 *
 * Revision 1.3  1997/07/18  20:53:04  sdear
 * insert_position is now -1, which means we should
 * interpolate values.
 *
 * Revision 1.2  1997/07/15  19:13:13  sdear
 * Support for BasePosition
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Revision 1.3  1996/10/03  17:42:19  rmott
 * *** empty log message ***
 *
 * Revision 1.2  1996/06/24  15:57:58  rmott
 * *** empty log message ***
 *
 * Revision 1.1  1996/06/24  15:57:07  rmott
 * Initial revision
 *
 */

/* PADDING.C - functions associated with padding and depadding CAF assemblies */

/* ************************************************************
 FUNCTIONS FOR PADDING 
   ************************************************************ */

#include <stdio.h>
#include <caf.h>
#include <padding.h>
#include <assinf.h>

int pad_cafAssembly( cafAssembly *CAF );

int pad_debugging=0;

int 
requirePadded( cafAssembly *CAF )
{
  int *pad_states = count_pad_states(CAF);

  if ( pad_states[unpadded_state] == 0 )
    {
      fprintf(stderr, "%s already padded\n", CAF->filename);
      return 1;
    }
  else
    {
      return pad_cafAssembly(CAF);
    }
}

int
pad_cafAssembly( cafAssembly *CAF )
{
/* pad an assembly 

   Returns +1 or -1 on success, 0 on failure

   Failure can be due to:

   no DNA for sequences assembled in contigs (isolated reads without dna pass)
   not all sequences in unpadded_state

   If assembly is already padded returns -1 &  prints a diagnostic
   Returns 1 if it actually does padding
*/

  CAFSEQ *contig, *seq;
  int i;
  int *pad_states = count_pad_states( CAF );

  if ( pad_states[padded_state] == arrayMax(CAF->seqs) ) /* all sequences already padded */
    { 
      fprintf(stderr, "WARNING: assembly is already padded\n");
      return -1;
    }
  else if ( pad_states[unpadded_state] == arrayMax(CAF->seqs) || ! (pad_states[padded_state] && pad_states[unpadded_state]) ) /* all sequences unpadded */
    { 

      for(i=0;i<arrayMax(CAF->seqs);i++)
	{
	  seq = arrp(CAF->seqs,i,CAFSEQ);
	  if ( (seq->type != is_assembly) && ! seq->dna )
	    {
	      fprintf(stderr, "WARNING: no DNA for %s\n", dictName(CAF->seqDict,seq->id));
/*	      return 0; */
	    }
	}

      for(i=0;i<arrayMax(CAF->seqs);i++)
	{
	  contig = arrp(CAF->seqs,i,CAFSEQ);
	  if ( contig->type == is_contig && pad_contig( contig, CAF ) <= unknown_state ) /* check for error */
	    {
	      fprintf(stderr, "ERROR: could not pad %s\n", dictName( CAF->seqDict, contig->id ));
	      return 0;
	    }
	}
      /* set pad_states correctly */
      
      for(i=0;i<arrayMax(CAF->seqs);i++)
	{
	  seq = arrp( CAF->seqs, i, CAFSEQ );
	  seq->pad_state = padded_state;
	}

      return 1;
    }
  else
    {
      fprintf(stderr, "WARNING: assembly is in a mixture of padding states\n");
      return 0;
    }
}

int
pad_contig ( CAFSEQ *contig, cafAssembly *CAF )

/* pad a contig

Return value is the original pad state (or an error code):

unknown_state           left unchanged, as input state was unknown
padded_state            already padded: left unchanged
unpadded_state          converted from unpadded to padded
-1                      an error occurred, eg due to an inconsistency between pad states for contig and readings

*/
{
  Array mapping=NULL, pads=NULL;
  Array rc_mapping=NULL, r_pads=NULL;
  ASSINF *a0, *a1;
  int i, j, reversed = FALSE;
  CAFSEQ *seq=NULL;
  char *unpadded_contig_dna, *unpadded_dna;
  int unpadded_contig_len, padded_contig_len;
  int unpadded_dna_len = 0;
  int S1 = 0, S2 = 0, R1 = 0, R2 = 0;
  int p, r, s, pos;
  Array newAssinf=NULL;

/*  fprintf(stderr, "\nContig %s\n",dictName(CAF->seqDict,contig->id)); */
 
  if ( contig->pad_state == padded_state )
    return padded_state;
  else if ( contig->pad_state == unknown_state )
    return unknown_state;
  else if ( contig->pad_state == unpadded_state ) {

    unpadded_contig_dna = stackText(contig->dna,0);
    unpadded_contig_len = strlen(unpadded_contig_dna);

    pads = arrayCreate(unpadded_contig_len + 1, int);
    *arrayp(pads,unpadded_contig_len,int) = 0;

    if ( contig->assinf ) {

      /* check for DNA */

      for(i=0;i<arrayMax(contig->assinf);i++) {

	a1 = arrp(contig->assinf,i,ASSINF);
	seq = arrp(CAF->seqs,a1->seq,CAFSEQ);

	if ( ! seq->dna ) {

	  fprintf(stderr,
		  "WARNING: no DNA for %s\n",
		  dictName(CAF->seqDict,a1->seq));
	  return -1;
	}
      }

      sort_assinf_by_seq_number(contig->assinf);
      
      newAssinf = arrayCreate(arrayMax(contig->assinf), ASSINF);

      a0 = NULL;
	  
      /* make a first pass through the contig to work out how many pads need
	 to be inserted after each base in the contig */
      
      for(j = 0; j < arrayMax(contig->assinf); j++) {

	a1 = arrp(contig->assinf,j,ASSINF);
	seq = arrp(CAF->seqs,a1->seq,CAFSEQ);

	if ( seq->pad_state != unpadded_state )
	  {
	    fprintf(stderr, "ERROR: Read %s in wrong pad state!\n",
		    dictName(CAF->seqDict,seq->id));
	    return -1;
	  }

	if ( a0 && ( a1->seq == a0->seq ) ) {

	  if ( a1->s1 > a0->s2 ) { /* forward read */

	    /* r is the difference in pads between contig
	       and read This is the number of pads which
	       have to be inserted into the contig */
	    
	    r = a1->r1 - a0->r2 - a1->s1 + a0->s2; 
	    pos = a0->s2;

	  }  else { /* reverse */

	    r = a1->r1 - a0->r2 - a0->s2 + a1->s1; 
	    pos = a1->s1;
	  }
	    
	  /* pads stores the max no of pads needed after pos in the contig */
	  p = array(pads,pos,int); /* fix */
	  if ( r > p )
	    *arrp(pads,pos,int) = r; 
	  
	  a0 = a1;

	} else { /* new reading */
	  a0 = arrp(contig->assinf,j,ASSINF);
	}
      }

      mapping = mapping_from_pads(pads,mapping); /* unpadded contig -> padded contig */
      padded_contig_len = arr(mapping,unpadded_contig_len,int)+arr(pads,unpadded_contig_len,int);
      pad_seq(contig, pads, padded_contig_len, FALSE); /* put pads in the contig sequence */
      adjust_tags(contig,mapping); /* adjust contig tags and base quality */
      adjust_quality(contig,mapping,padded_contig_len,0);
      
      /* Make a second pass through the contig to pad the readings */
      a0 = NULL;
      seq = NULL;

      for(j = 0; j < arrayMax(contig->assinf); j++) {

	a1 = arrp(contig->assinf,j,ASSINF);

	if ( a0 && a1->seq == a0->seq ) { /* same read as previous */
	  
	  if ( a1->s1 > a0->s2 ) { /* forward read */
	    
	    r = a1->r1 - a0->r2 - a1->s1 + a0->s2;
	    pos = a0->r2; /* position in reading */
	    reversed = FALSE;
	  } else { /* reverse */
	    
	    r = a1->r1 - a0->r2 - a0->s2 + a1->s1;
	    pos = unpadded_dna_len-a1->r1+1; /* position in reverse complement */
	    reversed = TRUE;
	  }
	  /* r_pads stores (initially) the difference between
	     the number of pads to insert in the read and the
	     contig. If the read is reverse-complemented then
	     r_pads is in rev-comp space */
	  
	  *arrp(r_pads,pos,int) = r; 
	  
	  S2 = a1->s2;
	  R2 = a1->r2;
	  
	  a0 = a1;

	} else { /* new read */
	  
	  a0 = arrp(contig->assinf,j,ASSINF);
	  
	  if ( seq ) { /* process the previous reading, if it exists */
	    
	    pad_reading( seq, CAF, reversed, R1, R2, S1, S2, r_pads, pads, rc_mapping, mapping, unpadded_dna_len, newAssinf );
	    /* if ( pad_debugging ) {
	      int nnn;
	      for(nnn=1;nnn<arrayMax(pads);nnn++) {
		fprintf(stderr,"%5d pads %5d mapping %5d\n", nnn, arr(pads,nnn,int), arr(mapping,nnn,int));
	      }
	    } */
	    seq = NULL;
	    reversed = FALSE;
	  }
		  
	  seq = arrp(CAF->seqs,a0->seq,CAFSEQ);
	  unpadded_dna = stackText(seq->dna,0);
	  unpadded_dna_len = strlen(unpadded_dna);
	  r_pads = arrayReCreate(r_pads,unpadded_dna_len+1,int);
	  *arrayp(r_pads,unpadded_dna_len,int) = 0;
	  rc_mapping = arrayReCreate(rc_mapping,unpadded_dna_len+1,int); /* mapping from unpadded read to unpadded contig */

	  S1 = a0->s1;
	  S2 = a0->s2;
	  R1 = a0->r1;
	  R2 = a0->r2;
	  if ( S1 > S2 )
	    reversed = TRUE;
	  else
	    reversed = FALSE;
	}
	if ( reversed )
	  /* the mapping of the unpadded rev comp read to unpadded contig */
	  for(r=a1->r1,s=a1->s1;r<=a1->r2;r++,s--)
	    *arrayp(rc_mapping,unpadded_dna_len-r+1,int) = s; 
	else
	  /* the mapping of the unpadded forward read to unpadded contig */
	  for(r=a1->r1,s=a1->s1;r<=a1->r2;r++,s++)
	    *arrayp(rc_mapping,r,int) = s; 
	
      }
      if ( seq ) /* process the last reading */
	pad_reading( seq, CAF, reversed, R1, R2, S1, S2, r_pads, pads, rc_mapping, mapping, unpadded_dna_len, newAssinf );
	  
    }
  }		      

  arrayDestroy(contig->assinf);
  contig->assinf = newAssinf;
  contig->pad_state = padded_state;
  return unpadded_state;

}

void
pad_seq( CAFSEQ *seq, Array pads, int len, int reverse )
/* replace the dna of a sequence by its padded version, destroying the old one */

{
  Stack new_dna = stackCreate( len+1 );
  char *s = stackText(new_dna,0);
  char *old = stackText(seq->dna,0);
  int j, k;

/*  if ( reverse )
    complement_seq(old);
*/
  if ( reverse )
    {
      int slen = strlen(old);
      old += slen-1;
      s += len;
      *s-- = 0;
      for(j=1;j<arrayMax(pads);j++)
	{
	  *s-- = *old--;
	  for(k=0;k<arr(pads,j,int);k++)
	    *s-- = padding_char;
	}
    }
  else
    {
      for(j=1;j<arrayMax(pads);j++)
	{
	  *s++ = *old++;
	  for(k=0;k<arr(pads,j,int);k++)
	    *s++ = padding_char;
	}
      *s = 0;
    }

  stackDestroy(seq->dna);

/*  if ( reverse )
    complement_seq(stackText(new_dna,0));
*/
  seq->dna = new_dna;
  seq->len = strlen(stackText(new_dna,0));
}

Array 
mapping_from_pads( Array pads, Array mapping )
/* generate the old2new mapping from the pads array 
assumes coords start at 1 */
{
  int j, p;

  mapping = arrayReCreate(mapping,arrayMax(pads)+1,int);
  *arrayp(mapping,1,int) = 1;
  *arrayp(mapping,arrayMax(pads)-1,int) = 0;
  for(j=2;j<arrayMax(pads);j++)
    {
      p = arr(pads,j-1,int);
      if ( p < 0 ) p = 0;
      *arrp(mapping,j,int) = p + arr(mapping,j-1,int) + 1;
/*      if ( pad_debugging )
	fprintf(stderr, "%5d mapping %5d\n", j, arr(mapping,j,int)); */
    }

  return mapping;
}

void
pad_scf( CAFSEQ *seq, Array mapping )

/* Generate align_to_scf data for the sequence seq, according to
mapping Assumes that mapping refers to the forward direction of the
read */

{
  static Array oldmapping =NULL;
  static Array newmapping =NULL;
  ASSINF *a;
  int i, j, k, k1;
  int r, s;

  if ( seq->assinf )
    {
      oldmapping = arrayReCreate(oldmapping,arrayMax(mapping),int);
      newmapping = arrayReCreate(newmapping,arr(mapping,arrayMax(mapping)-1,int),int);

      for(j=0;j<arrayMax(seq->assinf);j++) /* the old scf mapping */
	{
	  a = arrp(seq->assinf,j,ASSINF);
	  *arrayp(oldmapping,a->s2,int) = 0;
	  for(r=a->r1,s=a->s1;r<=a->r2;r++,s++) /* array */
	    *arrp(oldmapping,s,int) = r;
	}

      *arrayp(newmapping,arr(mapping,arrayMax(mapping)-1,int),int) = 0; 
      for(i=1;i<arrayMax(mapping);i++)
	{
	  k = arr(mapping,i,int); /* k is where base i is in the padded seq */
	  j = arr(oldmapping,i,int); /* j is where base i is in the scf */
	  *arrp(newmapping,k,int) = j; /* base k in the padded seq maps to j in the scf */
	}

/*      if ( pad_debugging )
	{
	  for(i=1;i<arrayMax(mapping);i++)
	    fprintf(stderr, "%5d newmap %5d\n", i, arr(newmapping,i,int));
	}
*/
      seq->assinf = arrayReCreate(seq->assinf,10,ASSINF);

      a = arrayp(seq->assinf,0,ASSINF);
      k1 = arr(newmapping,1,int);
      a->r1 = k1;
      a->s1 = 1;

      for(j=1;j<arrayMax(newmapping);j++)
	{
	  k = arr(newmapping,j,int);

	  if ( k )
	    {
	      if ( k == k1+1 || j==1 ) /* everything is hunky-dory */
		{
		  a->r2 = k;
		  a->s2 = j;
		}
	      else /* deletion */
		{
		  a = arrayp(seq->assinf,arrayMax(seq->assinf),ASSINF);

		  a->r1 = k;
		  a->s1 = j;
		  a->r2 = k;
		  a->s2 = j;
/*		  if ( pad_debugging )
		    fprintf(stderr, "delete %d %d\n", j, k ); */
		}
	    }
	  else /* insertion */
	    {
	      while(j < arrayMax(newmapping) && !(k = arr(newmapping,j,int))) 
		j++;

	      if ( k && j < arrayMax(newmapping) )
		{
		  a = arrayp(seq->assinf,arrayMax(seq->assinf),ASSINF);

		  a->r1 = k;
		  a->s1 = j;
		  a->r2 = k;
		  a->s2 = j;
		}
	    }
	  k1 = k;
	}
    }
}



void
reverse_map( Array mapping )
{
  int i, j, k, l;
  int max1, max2;
  
  max1 = arrayMax(mapping)-1;
  max2 = arr(mapping,max1,int);
  
  for(i=1,k=max1;i<=k;i++,k--)
    {
      j = arr(mapping,i,int);
      l = arr(mapping,k,int);
      *arrp(mapping,i,int) = max2-l+1;
      *arrp(mapping,k,int) = max2-j+1;
    }

}

void
pad_reading( CAFSEQ *seq, cafAssembly *CAF, int reversed, int R1, int R2, int S1, int S2, Array r_pads, Array pads, Array rc_mapping, Array mapping, int unpadded_len, Array newAssinf )
{
/* create a padded reading */

  int r, s, p, n;
  int i, ss;
  int padded_len;
  Array r_mapping=NULL;
  ASSINF *b;
  b = arrayp(newAssinf,arrayMax(newAssinf),ASSINF);
  b->seq = seq->id;
  b->s1 = arr(mapping,S1,int);
  b->s2 = arr(mapping,S2,int);

/*  pad_debugging = !strcmp( "22a13d10.s1", dictName(CAF->seqDict, seq->id));   */

/*  fprintf(stderr, "read %s\n",  dictName(CAF->seqDict, seq->id) ); */

  seq->pad_state = padded_state;
  if ( reversed )
    {
      r = unpadded_len-R1+1;
      R1 =unpadded_len-R2+1;
      R2 = r;
      r = S1;
      S1 = S2;
      S2 = r;
    }

  arrayp(rc_mapping,R2,int);
  arrayp(r_pads,R2,int);
  for(r=R1;r<=R2;r++)	/* create the mapping of the unpadded read to the unpadded contig */
    {
      s = arr(rc_mapping,r,int);
      p = arr(r_pads,r,int);
      *arrp(r_pads,r,int) = -p + arr(pads,s,int);
/*      if ( pad_debugging )
	fprintf(stderr, "%5d r_pads %5d\n", r, arr(r_pads,r,int)); */
    } 

/* fix the problem of pads attached to contig positions which do not map to read positions */

    for(r=R1,s=arr(rc_mapping,R1,int);r<R2;r++)
      {
	ss = arr(rc_mapping,r+1,int); /* ss is next contig position */
	if ( s && ss > s+1 ) /* if ss is not the successor to s, and s exists... */
	  {
	    for(i=s+1;i<ss;i++)
	      {
		*arrp(r_pads,r,int) += arr(pads,i,int); /* add the intervening pads */
	      }
	  }
	else if ( s && ! ss ) /* if ss maps to 0 ... */
	  {
	    int sss, j, k;

	    j = r+1;
	    sss = s+1;
/*	    if ( pad_debugging )
	      fprintf(stderr, "before r_pads=%d\n", arr(r_pads,r,int)); */
	    
	    while (  j < arrayMax(rc_mapping) && !(sss = arr(rc_mapping,j,int) ) )
	      {
		j++;
/*		if ( pad_debugging )
		  fprintf(stderr,"sss=%5d, pads=%5d\n", sss, arr(pads,sss,int)); */
	      }
	    for(k=s+1;k<sss;k++)
	      *arrp(r_pads,r,int) += arr(pads,k,int);

/*	    if ( pad_debugging )
	      fprintf(stderr, "after r_pads=%d\n", arr(r_pads,r,int)); */
	  }		
/*	if ( pad_debugging )
	  fprintf(stderr, "r=%5d s=%5d ss=%5d pads=%5d\n", r, s, ss, arr(r_pads,r,int)); */
	s = ss;
    }

  r_mapping = mapping_from_pads( r_pads, r_mapping );
  p = arr(r_pads,unpadded_len,int);
  if ( p < 0 ) p = 0;
  if ( reversed )
    padded_len = arr(r_mapping,unpadded_len,int); /* IS THIS RIGHT ? */  
  else
    padded_len = arr(r_mapping,unpadded_len,int)+p; /* IS THIS RIGHT ? */

/*  if ( pad_debugging )
    {
      int r0, s0, rc;
      fprintf(stderr, "p=%d %d\n", p, reversed);
      r0 = arr(r_mapping,R1,int);
      s0 = arr(mapping,arr(rc_mapping,R1,int),int);
      
      for(r=R1;r<=R2;r++)
	{
	  s = arr(rc_mapping,r,int);
	  
	  fprintf(stderr, "r: %5d rc: %5d rmap: %5d rlen: %5d smap: %5d slen: %5d\n", r, s,arr(r_mapping,r,int), arr(r_mapping,r,int)-r0+1, arr(mapping,s,int), arr(mapping,s,int)-s0+1);
	}
    }
*/
  pad_seq(seq, r_pads, padded_len, reversed ); /* put pads in the dna */

  b->r1 = arr(r_mapping,R1,int);
  b->r2 = arr(r_mapping,R2,int);

  if ( reversed )
    {
      n = padded_len-b->r1+1;
      b->r1 = padded_len-b->r2+1;
      b->r2 = n;

      reverse_map( r_mapping );
    }

  pad_scf( seq, r_mapping  ); /* sort out the alignment to SCF */

  adjust_tags(seq,r_mapping);
  adjust_quality(seq,r_mapping,padded_len,-1); /*use quality value 1 to disable interpolate*/
  adjust_position(seq,r_mapping,padded_len,-1);

}

/* ************************************************************
   FUNCTIONS for depadding 
   ************************************************************ */

int 
requireUnpadded( cafAssembly *CAF )
{
/* returns 1 on success, 0 on failure. If assembly is already padded
   it prints a diagnostic as well */

  int *pad_states = count_pad_states(CAF);

  if ( pad_states[padded_state] == 0 )
    {
      fprintf(stderr, "%s already unpadded\n", CAF->filename);
      return 1;
    }
  else
    {
      return caf_depad(CAF);
    }
}


int 
caf_depad( cafAssembly *CAF )
{
  DICT *seqDict;
  Array seqs;
  CAFSEQ *seq, *contig;
  int i, j, r_len, R_len, c_len, k, r1, r2, s1, s2, S1, S2, r, s, R, S;
  char *r_dna, *c_dna;
  Array assinf=NULL;
  Array c_map=NULL, r_map=NULL, cr_map=NULL;
  ASSINF *a, *b;
  int reversed, next;

/*  checkCAF( CAF, TRUE ); */

  seqs = CAF->seqs;
  seqDict = CAF->seqDict;
  
/* check that all contigs and reads linked to contigs have dna defined and are padded */

  for(i=0;i<arrayMax(seqs);i++)
    {
      contig = arrp( seqs, i, CAFSEQ );
      if ( contig->type == is_contig && contig->pad_state == padded_state )
	{
	  if ( ! contig->dna )
	    {
	      fprintf(stderr, "WARNING: no DNA for contig %s\n", dictName(CAF->seqDict,contig->id));
	      return 0;
	    }
	  if ( ! contig->assinf || ! arrayMax( contig->assinf ) )
	    {
	      fprintf(stderr,"WARNING: no reads attached to contig %s\n",dictName(CAF->seqDict,contig->id));
	    }
	  else
	    {
	      for(j=0;j<arrayMax(contig->assinf);j++)
		{
		  a = arrp(contig->assinf,j,ASSINF);
		  seq = arrp(seqs, a->seq, CAFSEQ);
		  if ( !seq->dna )
		    {
		      fprintf(stderr, "WARNING: no DNA for read %s in contig %s\n", dictName(CAF->seqDict,seq->id), dictName(CAF->seqDict,contig->id) );
		      return 0;
		    }
		  if ( seq->pad_state == unpadded_state )
		    {
		      fprintf(stderr, "WARNING: unpadded read %s in padded contig %s\n", dictName(CAF->seqDict,seq->id), dictName(CAF->seqDict,contig->id) );
		      return 0;
		    }
		  assinf_from_pads( seq, seqDict, dictName(seqDict,j) ); 
		}
	    }
	}
    }

  for(j=0;j<arrayMax(seqs);j++)
    {
      contig = arrp(seqs,j,CAFSEQ);
      if ( contig->type == is_contig)
	{
	  assinf = arrayReCreate(assinf,1000,CAFSEQ);

	  c_dna = stackText(contig->dna,0)-1; /* adjust offset so that coords start at 1 */
	  c_len = strlen(c_dna+1);

	  /* Get the mapping of padded contig coords to unpadded contig */

	  c_map = pad2unpad( c_dna, c_len, c_map );

	  cr_map = arrayReCreate(cr_map,c_len+1,int); /* contig[unpadded] -> read[unpadded] */
	      
	  /* now correct every read in the contig */

	  if ( contig->assinf && arrayMax(contig->assinf)  )
	    {
	      for(k=0;k<arrayMax(contig->assinf);k++)
		{
		  a = arrp(contig->assinf,k,ASSINF);
		  seq = arrp(seqs,a->seq,CAFSEQ);

		  /*	      pad_debugging = ! strcmp( dictName(CAF->seqDict,seq->id), "hh06d12.s2t"); */

		  r_dna = stackText(seq->dna,0)-1; /* coords start at 1 */
		  r_len = strlen(r_dna+1);
	      
		  if ( a->s1 <= a->s2 ) /* forward */
		    {
		      r1 = a->r1;
		      r2 = a->r2;
		      s1 = a->s1;
		      s2 = a->s2;
		      reversed = FALSE;
		    }
		  else
		    {
		      s1 = a->s2;
		      s2 = a->s1;
		      r1 = r_len-a->r2+1;
		      r2 = r_len-a->r1+1;
		      complement_seq(r_dna+1);
		      reversed = TRUE;
		    }
	      
		  r_map = pad2unpad( r_dna, r_len, r_map );
		  R_len = arr(r_map,r_len,int); /* length of unpadded read */

		  r = r_len;
		  while(!R_len && r > 0 ) /* fix problem of last base being a pad ... */
		    R_len = arr(r_map,--r,int);

		  /* the contig now has a complex mapping onto the read ... */
	      
		  S1 = arr(c_map,s1,int); /* [S1, S2] is the range in unpadded contig space */
		  S2 = arr(c_map,s2,int);
	      
		  if ( S1 == 0 ) /* need to search for the first proper (non-pad) base */
		    {
		      s = s1;
		      while( s1 <= s2 && ! S1 )
			{
			  s1++;
			  S1 = arr(c_map,s1,int);
			  /* fprintf(stderr, "trying %d %d\n", s1, S1); */
			}
		      r1 += (s1-s);
		      /* fprintf(stderr, "NOTE: adjusted S1 from %d to %d %d r1: %d %s\n", s, s1, S1, r1, dictName(CAF->seqDict,seq->id)); */
		    }
		  if ( S2 == 0 )
		    {
		      s = s2;
		      while( s1 <= s2 && ! S2 )
			{
			  s2--;
			  S2 = arr(c_map,s2,int);
			  /* fprintf(stderr, "trying %d %d\n", s2, S2); */
			}
		      /* fprintf(stderr, "NOTE: adjusted S2 from %d to %d %d %s\n", s, s2, S2, dictName(CAF->seqDict,seq->id)); */
		      r2 += (s2-s);
		    }
	      

		  /*		fprintf(stderr, "ERROR S1: %d s1: %d S2: %d s2: %d %s\n", S1, s1, S2, s2, dictName(CAF->seqDict,seq->id)); */
		  /* cr_map[S] is the unpadded read coord corresponding to
		     unpadded contig coord S, or 0 if it does not exist */

		  *arrayp(cr_map,S2,int) = 0;		  
		  for(s=S1;s<=S2;s++)
		    *arrp(cr_map,s,int) = 0;
	      
		  for(s=s1,r=r1;s<=s2;r++,s++)
		    {
		      S = arr(c_map,s,int); /* unpadded coord in contig */
		      R = arr(r_map,r,int); /* unpadded coord in read (can be 0)*/
		      if ( S != 0 )
			*arrp(cr_map,S,int) = R; 
		    }
	      
		  /* now go through cr_map */

		  s1 = S1;
		  while(s1<=S2)
		    {
		      /* move through pad */
		      while(s1 <= S2 && (r1=arr(cr_map,s1,int)) == 0)
			s1++;	
		      if (r1 == 0) break;  /* Found a pad on the end of the sequence */
		      b = arrayp(assinf,arrayMax(assinf),ASSINF); /* new assinf */
		      b->seq = seq->id;
		      r2 = r1;	/* start of matching interval */
		      s2 = s1;
		      
		      /* move through non-pad as long as cr_map increases by one */
/*		      while( ((next=arr(cr_map,s2+1,int)) != 0 ) && (next == r2+1) && (s2 < S2) ) */
		      while( (s2 < S2) && ((next=arr(cr_map,s2+1,int)) != 0 ) && (next == r2+1)  )
			{
			  s2++;	
			  r2 = next;
			}
		      
		      if ( reversed )
			{
			  b->r1 = R_len-r2+1;
			  b->r2 = R_len-r1+1;
			  b->s1 = s2;
			  b->s2 = s1;
			}
		      else
			{
			  b->r1 = r1;
			  b->r2 = r2;
			  b->s1 = s1;
			  b->s2 = s2;
			}
		      
		      s1 = s2+1;
		    }
	      
		  if ( reversed )
		    {
		      complement_seq(r_dna+1);
		      r_map = pad2unpad( r_dna, r_len, r_map );
		    }

		  /* sort out the mapping of the read onto scf ... */

		  depad_scf( seq, r_len, R_len, r_dna );

		  /* fix the sequence */
	      
		  depad_seq(r_dna+1); /* note change of coords */
	      

		  adjust_tags(seq,r_map);  
		  adjust_quality(seq,r_map,R_len,0);
		  adjust_position(seq,r_map,R_len,-1);
		  seq->pad_state = unpadded_state;
		  seq->len = strlen(r_dna+1);
		}
	      
	      arraySort(assinf, (int(*)(void *, void*))pad_cmp);

	      arrayDestroy(contig->assinf);
	      contig->assinf = arrayCopy(assinf);
	      
	    }

	  depad_seq(c_dna+1);
	  contig->len = strlen(c_dna+1);
	  adjust_tags(contig,c_map);  
	  
	  adjust_quality(contig,c_map,strlen(c_dna+1),0);
	  contig->pad_state = unpadded_state;
	}
    }

  /* depad isolated reads */

  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp( CAF->seqs, i, CAFSEQ );
      depad_isolated_reading( seq, CAF );
    }


  /* set pad_states  */
  
  for(i=0;i<arrayMax(CAF->seqs);i++)
    {
      seq = arrp( CAF->seqs, i, CAFSEQ );
      seq->pad_state = unpadded_state;
    }
  
  return 1; /* return 1 on success */
}

int 
pad_cmp( ASSINF *a, ASSINF *b )
{
  int n = a->seq-b->seq;

  if ( ! n )
    {
      n = a->r1-b->r1;
      if ( ! n )
	{
	  n = a->r2-b->r2;
	}
    }

  return n;
}

Array
pad2unpad(char *dna, int len, Array map)

/* get array mapping padded coord to unpadded */
{
  int n = 0;
  int m;

  map = arrayReCreate(map,len+1,int);
  *arrayp(map,len,int) = 0;
  
  for(m=1;m<=len;m++)
    if (dna[m] != padding_char  )
      *arrp(map,m,int) = ++n; /* contig_mapping[padded] = unpadded */
    else
      *arrp(map,m,int) = 0; /* pad */

  return map;
}

void
depad_scf( CAFSEQ *seq, int r_len, int R_len, char *r_dna )
{

  /* get the mapping of the padded reading onto the scf */

  static Array scf_map=NULL;
  static Array uscf_map=NULL;
  int m, n, r, s, l;
  ASSINF *b;

  scf_map = arrayReCreate(scf_map,r_len+1,int);
  *arrayp(scf_map,r_len,int) = 0;
  for(m=1;m<=r_len;m++)
    *arrp(scf_map,m,int) = 0;


  for(m=0;m<arrayMax(seq->assinf);m++)
    {
      b = arrp(seq->assinf,m,ASSINF);
      for(n=b->s1,l=b->r1;n<=b->s2;n++,l++)
	*arrp(scf_map,n,int) = l;
    }

/*  if ( pad_debugging)
    for(m=0;m<arrayMax(scf_map);m++)
      fprintf(stderr, "scf_map %5d %5d\n", m, arr(scf_map,m,int) );
*/
  /* and the mapping of the unpadded read onto the scf */

  uscf_map = arrayReCreate(uscf_map,R_len+1,int);
  *arrayp(uscf_map,R_len,int) = 0;

  for(m=1;m<=R_len;m++)
    *arrp(uscf_map,m,int) = 0;

  n = 0;
  for(m=1;m<=r_len;m++)
    if ( r_dna[m] != padding_char )
      *arrp(uscf_map,++n,int) = arr(scf_map,m,int);

/*  if ( pad_debugging)
    for(m=0;m<arrayMax(uscf_map);m++)
      fprintf(stderr, "uscf_map %5d %5d\n", m, arr(uscf_map,m,int) );
*/
  /* redo the align_to_SCF */

  seq->assinf = arrayReCreate(seq->assinf,10,ASSINF);

  r = 1;
  s = 1;
  while( s <= R_len )
    {
		  
      while( (r=arr(uscf_map,s,int)) == 0 && s <= R_len )
	s++;
      b = arrayp(seq->assinf,arrayMax(seq->assinf),ASSINF);
      b->s1 = s;
      b->r1 = r;

/*      while( ((n=arr(uscf_map,s+1,int)) != 0) && (n == r+1) && s <= R_len  ) */
      while(s < R_len && ((n=arr(uscf_map,s+1,int)) != 0) && (n == r+1)  )
	{
	  r = n;
	  s++;
	}
      b->s2 = s;
      b->r2 = r;
      s++;
    }
}



char*  
depad_seq( char *seq )
/* remove pads from seq */
{
  char *s = seq;
  char *t = seq;

  while(*s)
    {
      if ( *s != padding_char )
	*t++ = *s++;
      else
	s++;
    }
  *t = 0;
  
  return seq;
}

int 
nearest_coord( int coord, Array map )
{
  /*  Return the nearest mapping of coord.
      Assumes map returns 0 for non-mapped coords
      */
  
  int n = 0;
  
  if ( coord >= arrayMax(map) )
    coord = arrayMax(map)-1;
  if ( coord < 1 )
    coord = 1;

  while ( coord > 0 && ! (n = arr(map,coord,int)))
    coord --;
  
  return n;
}

  
Array 
inverse_map( Array map )
{

/* create an array giving the inverse of map.
Assumes coords are 1,2,3...
so that 0 means "no mapping"
*/

  Array imap=arrayCreate(arrayMax(map),int);
  int i, j;

  for(i=1;i<arrayMax(map);i++)
    {
      j = arr(map,i,int);
      if ( j > 0 )
	*arrayp(imap,j,int) = i;
    }

  return imap;
}

int 
depad_isolated_reading( CAFSEQ *seq, cafAssembly *CAF )
{
  char *r_dna;
  int r_len, R_len, r;
  Array r_map=NULL;

  if ( seq->type == is_read && seq->pad_state == padded_state && seq->dna )
    {
      r_dna = stackText(seq->dna,0)-1; /* coords start at 1 */
      r_len = strlen(r_dna+1);
      
      r_map = pad2unpad( r_dna, r_len, r_map );
      R_len = arr(r_map,r_len,int); /* length of unpadded read */
      
      r = r_len;
      while(!R_len && r > 0 ) /* fix problem of lest base being a pad ... */
	R_len = arr(r_map,--r,int);
      
      if ( seq->assinf ) 
	depad_scf( seq, r_len, R_len, r_dna );
      depad_seq(r_dna+1); /* note change of coords */
      adjust_tags(seq,r_map);  
      adjust_quality(seq,r_map,R_len,0);
      adjust_position(seq,r_map,R_len,0);
      seq->pad_state = unpadded_state;
      seq->len = strlen(r_dna+1);
      return 1;
    }
  return 0;
}


  
