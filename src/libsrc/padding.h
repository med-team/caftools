/*  Last edited: Jan  4 20:48 1997 (badger) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* PADDING.H - internal functions for padding/de-padding */

#ifndef _PADDING_H_
#define _PADDING_H_


#ifdef CPLUSPLUS
extern "C" { 
#endif

int caf_depad( cafAssembly *CAF );

int pad_contig ( CAFSEQ *contig, cafAssembly *CAF );

void pad_seq( CAFSEQ *seq, Array pads, int len, int reverse );

Array mapping_from_pads( Array pads, Array mapping );

void pad_scf( CAFSEQ *seq, Array mapping );

void reverse_map( Array mapping );
Array inverse_map( Array map );

void pad_reading( CAFSEQ *seq, cafAssembly *CAF, int reversed, int R1, int R2, int S1, int S2, Array r_pads, Array pads, Array rc_mapping, Array mapping, int unpadded_dna_len, Array newAssinf );

void depad_scf( CAFSEQ *seq, int r_len, int R_len, char *r_dna );

Array pad2unpad(char *dna, int len, Array map);

int pad_cmp( ASSINF *a, ASSINF *b );

char* depad_seq( char *seq );



#ifdef CPLUSPLUS
}
#endif


#endif

/******************* end of file *****************/

