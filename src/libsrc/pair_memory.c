/*  Last edited: Jun 13 17:08 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* stuff for handling pairs 

Need to store pairs [row, col] so that we can retrieve the nearest
pair less than a given input pair [Row, Col] st Col == col and Row > row

Needed for lin_align.c

*/

#include <stdio.h>
#include <stdlib.h>

#include <pair_memory.h>


#define LIMIT_RPAIR_SIZE 10000

static RPAIR *rpair=NULL;
static int rpairs=0;
static int rpair_size=0;
static int rpairs_sorted=0;
static int limit_rpair_size=LIMIT_RPAIR_SIZE;

int 
rpair_cmp(const void *a, const void *b )
{
  RPAIR *A = (RPAIR*)a;
  RPAIR *B = (RPAIR*)b;
  int n = A->col-B->col;

  if ( n == 0 )
    n = A->row-B->row;

  return n;
}

void 
rpair_init( int max_bytes )
{
  limit_rpair_size = max_bytes/sizeof(RPAIR);
  free_rpairs();
}

void free_rpairs(void)
{
  /*  fprintf(stderr,"FORGET: rpairs: %d\n", rpairs );*/
  if ( rpair ) free(rpair);
  rpair = NULL;
  rpair_size = 0;
  rpairs = 0;
  rpairs_sorted = 0;
}

int 
do_not_forget( int col, int row )
{

  if ( rpairs > limit_rpair_size )
    return 0; /* failure - ran out of memory */

  if ( rpairs >= rpair_size ) {
    rpair_size = ( rpairs == 0 ? 10000 : 2*rpairs );

    if ( rpair_size > limit_rpair_size ) /* enforce the limit */
      rpair_size = limit_rpair_size;

    rpair = (RPAIR*)realloc(rpair, sizeof(RPAIR)*rpair_size);
    if ( rpair == NULL ) {
      fprintf(stderr, "ERROR: Memory allocation failure in do_not_forget(). rpairs: %d rpair_size: %d\n", rpairs, rpair_size );
      return 0;
    }
  }

  rpair[rpairs].col = col;
  rpair[rpairs].row = row;

  rpairs++;
  rpairs_sorted = 0;

  return 1; /* success */
}

int 
remember( int col, int row )
{
  RPAIR rp;
  int left, right, middle, d;

  if ( ! rpairs_sorted ) {
    qsort( rpair, rpairs, sizeof(RPAIR), rpair_cmp );
    rpairs_sorted = 1;
  }

  rp.col = col;
  rp.row = row;

  left = 0;
  right = rpairs-1;
  
  if ( rpair_cmp( &rpair[left], &rp ) <= 0 && ( rpair_cmp(&rpair[right],&rp ) >= 0 ) )
    {
      while( right-left > 1 )
	{
	  middle = (left+right)/2;
	  d = rpair_cmp( &rpair[middle], &rp );
	  if ( d < 0 ) 
	    left = middle;
	  else if ( d >= 0 )
	    right = middle;
	}
      /*            fprintf(stderr, "col %d row %d found right: col %d row %d left: col %d row %d\n", col, row, rpair[right].col, rpair[right].row, rpair[left].col, rpair[left].row );  */
      if ( rpair_cmp( &rpair[right], &rp ) >= 0 && rpair[left].col == col && rpair[left].row < row ) /* success */
	return rpair[left].row;
    }

 /* error - this should never happen
 */
  fprintf(stderr, "ERROR in remember() left: %d (%d %d) right: %d (%d %d) col: %d row: %d\n", left, rpair[left].col, rpair[left].row, right, rpair[right].col, rpair[right].row, col, row);
  exit(1);


}
