/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

/* $Id: */

/* IMPLEMENTATION OF PRIORITY QUEUES. Taken from Aho, Hopcroft and Ullman */
/* uses heaps */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <priority_queue.h>

/* 

allocate memory for a priority queue for maxitems, using cmp() as the
comparison function 

returns a positer to the priority queue

*/

PQ *pq_alloc(int maxitems, int size, int (*cmp)(const void *, const void *))
{
  PQ *pq = (PQ*)calloc(1,sizeof(PQ));

  pq->maxitems = maxitems;
  pq->contents = (char**)calloc(maxitems,sizeof(char*))-1;
  pq->last = 0;
  pq->size = size;
  pq->cmp = cmp;

  return pq;
}

/* insert the item into the priority queue, unless the queue is full.
   returns item or NULL on failure */

char * pq_insert(char *item, PQ *pq)
{
  int i, j;
  char *temp;
  char **contents = pq->contents;
  int (*cmp)() = pq->cmp;

  if ( pq->last >= pq->maxitems )
    return NULL;
  else
    {
      pq->last++;
      pq->contents[i=pq->last] = item;
      while( i>1 && ( cmp(contents[i],contents[j=i/2]) < 0 ) )
	{
	  temp = contents[i];
	  contents[i] = contents[j];
	  contents[j] = temp;
	  i = j;
	}
    }

  return item;
}



/* inserts item into the queue, deleting the smallest element if necessary.
 If the item is smaller than the smallest item then is is NOT inserted

Note - the item is copied, but malloc is only called for while the queue
is not full. After that the memory is re-used

returns 1 on insertion and no replace, 2 on replace and 0 on failure */

int pq_replace(char * item, PQ *pq)
{
  char *temp;

  if ( pq->last < pq->maxitems )
    {
      temp = (char*)calloc(1,pq->size);
      memcpy(temp,item,pq->size);
      pq_insert(temp,pq);
      return 1;
    }
  else if ( pq->cmp( item, pq->contents[1] ) > 0 )
    {
      temp = pq_deletemin(pq);
      memcpy(temp,item,pq->size);
      pq_insert(temp,pq);
      return 1;
    }
  else
    return 0;
}

char *pq_getmin(PQ *pq)
{
  return pq->contents[1];
}


/* deletes the smallest member of the queue, and returns its address */

char *pq_deletemin(PQ *pq)
{
  int i, j;
  char *temp;
  static char *min_el;
  char **contents = pq->contents;
  int (*cmp)() = pq->cmp;

  if ( pq->last < 1 )
    return NULL;
  else
    {
      min_el = contents[1];
      contents[1] = contents[pq->last];
      pq->last--;
      i=1;
      while(i<=pq->last/2)
	{
	  if ( cmp( contents[2*i],contents[2*i+1]) < 0  || 2*i == pq->last )
	    j = 2*i;
	  else
	    j = 2*i+1;
	  if ( cmp( contents[i],contents[j]) > 0 )
	    {
	      temp = contents[i];
	      contents[i] = contents[j];
	      contents[j] = temp;
	      i = j;
	    }
	  else
	    {
	      return min_el;
	    }
	}
      return min_el;
    }
}


/* destroys the priority queue (but not the data) */

void pq_free(PQ *pq)
{
  free(pq->contents+1);
  free(pq);
}
  
