/*  Last edited: Jan  4 20:48 1997 (badger) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
 /* IMPLEMENTATION OF PRIORITY QUEUES. Taken from Aho, Hopcroft and Ullman */

typedef struct 
{
  char **contents;
  int last;
  int maxitems;
  int size;
  int (*cmp)();
} PQ;

/* public functions */

/* create a heap structure for a priority queue */
PQ *pq_alloc(int maxitems, int size, int (*cmp)(const void *, const void *));

/* insert an item into the heap - return NULL if heap is full */
char *pq_insert(char *item, PQ *pq);

/* delete and return the lowest member */
char *pq_deletemin(PQ *pq);

/* return the lowest member */
char *pq_getmin(PQ *pq);

/* insert and if necessary replace */
int pq_replace(char * item, PQ *pq);

/* destroy the priority queue */
void pq_free(PQ *pq);
