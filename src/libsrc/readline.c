/*  Last edited: Aug 15 16:20 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* functions for reading in lines (C) Richard Mott,, ICRF */

#include <stdio.h>
#include <ctype.h>

#include <readline.h>

void uncomment(char * string);


int read_line(FILE *file, char *string)
{	
  int	c;
  int	i=0;
  
  while ((c = getc(file)) != EOF)  {
    if (c == '\n') break;
    string[i++]   = c;
  }
  string[i] = '\0';
  if (c == EOF) return i ? i : EOF;
  return i;
}

int next_line(FILE *file)
{
  int	c;

  while ((c = getc(file)) != EOF) {
    if (c == '\n') return 1;
  }
  return 0;
}

int not_blank(char * string) /* checks whether string is full of white space */
{
  
  while (*string != 0)
    {
      if (!isspace((int) *string)) return 1;
      string++;
    }
  
  return 0;
}

int skip_comments( fp, string ) /* reads in successive lines, truncating
					comments and skipping blank lines */

char *string;
FILE *fp;
{
int n;

	*string = 0;
	while ( ( n = read_line( fp, string ) ) != EOF )
	{
		uncomment( string );
		if ( not_blank( string ) ) return n;
	}

	return n;

}

void uncomment(char * string) /* truncates string at the first ! */
{
    while ( *string != '!' /*&& *string != '#'*/ && *string != 0 ) string++;
    *string = 0;
}
