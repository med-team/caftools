/*  Last edited: Aug 15 16:20 2001 (rmd) */

#ifndef CAF_READLINE_H
#define CAF_READLINE_H

#include <stdio.h>

/* reads a line */

int read_line(FILE *file, char *string);

/* skips a line */

int next_line(FILE *file);

/* checks whether string is full of white space */

int not_blank(char * string);

/* reads in successive lines, truncating
   comments and skipping blank lines */

int skip_comments(FILE * fp, char * string);

#endif /* CAF_READLINE_H */
