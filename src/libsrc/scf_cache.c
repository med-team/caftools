/*  Last edited: Jun 13 12:59 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Sep 20 15:18 1996 (rmott) */

#include<stdio.h>
#include "regular.h"
#include "array.h"
#include "dict.h"
#include"myscf.h"
#include"cl.h"


int scf_cachesize=50;
DICT *scfDict=NULL;
Array scfArray=NULL;
Array last_used=NULL;
Array times_read=NULL;
int scf_event=1;
int cached=0;

void rescale16bit(Scf *scf);

Scf *get_scf( char *name )

/* given a scf file name, return the corresponding scf struct.
This uses an internal cache, of size scf_cachesize 

The environment variable RAWDATA should be set to a colon-separated
list of directories to search for the scf file

If RAWDATA is not set then only the current directory is searched

return value is NULL on failure
*/

{
  int id, i, n, min, oldest;
  Scf *scf=NULL;
  char buf[256];
  FILE *fp;

  scf_event++;

  if ( ! scfArray )
    {
      scfArray = arrayCreate(1000,Scf*);
      scfDict = dictCreate(1000);
      last_used = arrayCreate(1000,int);
      times_read = arrayCreate(1000,int);
    }

  if ( name && *name )
    {
      dictAdd(scfDict,name,&id);
      if ( ! (scf=array(scfArray,id,Scf*) ) ) /* test if the scf is already cached */
	{
	  if ( cached >= scf_cachesize ) /* cache is full */
	    {
	      /* find oldest cached member */
	      min = scf_event+1;
	      oldest = -1;
	      for(i=0;i<arrayMax(last_used);i++)
		{
		  n = arr(last_used,i,int);
		  if ( n > 0 && n < min && NULL != array(scfArray,i,Scf*) )
		    {
		      min = n;
		      oldest = i;
		    }
		}
	      /* trash oldest */
	      scf = arr(scfArray,oldest,Scf*);
	      scf_deallocate( scf );
/*	      printf("trashing %s\n", dictName(scfDict,oldest)); */
	      *arrp(scfArray,oldest,Scf*) = NULL;
	      *arrp(last_used,oldest,int) = -1;
	      cached--;
	    }

	  /* read in scf struct */

	  scf = NULL;

	  /* test if we can open scf file */
	  if ((fp = openfile_in_envpath( name, "r", "RAWDATA", buf )) != 0 )
	    {
	      fclose(fp);
	      if ( ! (scf = read_scf(buf))) /* test if we can read scf file */
		{
		  fprintf(stderr,"ERROR: cannot read SCF file %s\n", buf );
		}
	      else
		{
/*		  (*arrayp(times_read,id,int))++; */
		  if (scf->header.sample_size == 2) rescale16bit(scf); /* Rescale 16 bit traces for auto-edit */
		  *arrayp(last_used,id,int) = scf_event; /* update the event */
		  *arrayp(scfArray,id,Scf*) = scf;
		  cached++;
/*		  printf("reading %s %d\n", dictName(scfDict,id), arr(times_read,id,int)); */
		}
	    }
	  else
	    fprintf(stderr,"ERROR: cannot open SCF file %s\n", name );
	}
      else
	*arrp(last_used,id,int) = scf_event;
    }
  return scf;
}


/* change the cachesize. Can only be called before the cache has been initialised */

int set_cachesize( int newsize )
{
  int old = scf_cachesize;

  if ( newsize > 0 && ! scfArray )
    scf_cachesize = newsize;

/*  printf("set_cachesize: new: %d old: %d\n", scf_cachesize, old ); */

  return old;
}

void rescale16bit(Scf *scf)
{
  /* 8 bit scf traces are scaled so the biggest value is always 255
     so for now trace_edit reqires that 16 bit scf traces should
     also be scaled - even though it's probably not right */
  
  int sampleNum;
  int maxVal = 0;
  double scale;
  Samples2 *samples = scf->samples.samples2;
  for (sampleNum = 0; sampleNum < scf->header.samples; sampleNum++)
    {
      Samples2 *sample = &samples[sampleNum];
      if (sample->sample_A > maxVal) maxVal = sample->sample_A;
      if (sample->sample_C > maxVal) maxVal = sample->sample_C;
      if (sample->sample_G > maxVal) maxVal = sample->sample_G;
      if (sample->sample_T > maxVal) maxVal = sample->sample_T;
    }
  scale = (maxVal) ? (((double) MAX_TRACE_VAL_16) / (double) maxVal) : 1;
  for (sampleNum = 0; sampleNum < scf->header.samples; sampleNum++)
    {
      Samples2 *sample = &samples[sampleNum];
      sample->sample_A *= scale;
      sample->sample_C *= scale;
      sample->sample_G *= scale;
      sample->sample_T *= scale;
    }  
}
