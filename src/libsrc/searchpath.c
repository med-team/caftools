/*  Last edited: Jun 13 14:57 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Feb 29 11:13 1996 (rmott) */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/* some functions for opening a file in a file search path */
/* return value is the FILE pointer to the opened file, or NULL
   basename is the name of the file without directory
   mode is the file open mode cf fopen
   searchpath is the colon-separated search path
   fullname is the name of the file successfully opened, or NULL 
   env is an environment variable containing the search path
*/

FILE *openfile_in_searchpath(char *basename, char *mode, char *searchpath, char *fullname )
{
  FILE *fp=NULL;

  if ( searchpath ) /* if search path defined... */
    {
      char *s = strdup(searchpath);
      char *p = strtok(s,":");
      while ( p )
	{
	  sprintf(fullname,"%s/%s", p, basename );
	  if ((fp = fopen(fullname,mode)) != 0)
	    break;
	  p = strtok(NULL,":");
	}
      free(s);
    }

  if ( ! fp ) /* try to open in cwd */
    {
      strcpy( fullname, basename );
      fp = fopen(basename,mode);
    }

  if ( ! fp )
    *fullname = '\0';

  return fp;
}

/* open a file in a search path specified by the environment variable env */

FILE *openfile_in_envpath(char *basename, char *mode, char *env, char *fullname )
{
  return openfile_in_searchpath( basename, mode, getenv(env), fullname );
}

