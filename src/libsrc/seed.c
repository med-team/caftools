/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* GETSEED returns a seed for the random number generator. This is by
default determined by the system clock, or optionally by the
command-line option -s=65767 etc */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>

#include <cl.h>

int getseed( int *seed, int argc, char **argv )

{
    
    *seed = ((int)time(NULL))% 100000; 
    getint( "-seed", seed, argc, argv );

    /*    fprintf(stderr,"! seed = %d\n", *seed);  */

    return *seed;
}
