/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* $Id: */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include <seq_util.h>
#include <seq_dict.h>
#include <tree.h>
#include <cl.h>
#include <cmp.h>

#define PRIME 555557

void rewind_spec( char *spec ); /* from seq_util.c */
/* from sw.c */
int smith_waterman_score( SEQUENCE *seq1, SEQUENCE *seq2,
			  int match, int mismatch,
			  int gap, int neutral,
			  int offset, int window, int *coord1, int *coord2);

SEQ_DICT *create_dictionary(int argc, char **argv)
{
  int wordsize=12;
  int m;
  SEQUENCE *seq;
  char db[256];
  int *count;
  WORD_DICT *seq_dict;
  int i, j, n;
  int **hash_db;
  FILE *fp;
  char  outfile[256];
  DATABASE *DB;
  SEQ_DICT *seqdict;

  
  *db = '\0';
  getarg("-db=%s", db, argc,argv);
  
  if ( (fp=argfile("-create=%s","w", argc, argv, outfile)) && getarg("-db=%s", db, argc,argv) && (DB = open_database(db) ))
    { 
      printf("! creating sequence dictionary %s\n", outfile );
      
      getint("-word=%d",&wordsize,argc,argv);
      printf("! word size : %d\n", wordsize);

      seqdict = (SEQ_DICT*)calloc(1,sizeof(SEQ_DICT));

      seqdict->count = count = (int*)calloc(PRIME,sizeof(int));
      seqdict->db = open_database(db);
      seqdict->wordsize = wordsize;
      seqdict->prime = PRIME;
      seqdict->sequences = seqdict->db->sequences;
      seqdict->seq_name = (char**)calloc(seqdict->sequences,sizeof(char*));
      seqdict->seq_len = (int*)calloc(seqdict->sequences,sizeof(int));

      m = 0;
      while ((seq = next_seq( db )) != 0)
	{
	  if ( ! (m%100) ) fprintf(stderr, "%d\n", m );
	  
	  seq_dict = make_dictionary_for_sequence( seq, wordsize, NULL, 0, 1 );
	  qsort( seq_dict->hit, seq_dict->hits, sizeof(int), icmp);
	  j = 0;
	  for(i=0;i<seq_dict->hits;i++)
	    {
	      if ( seq_dict->hit[i] >= 0  )
		seq_dict->hit[j++] = seq_dict->hit[i];
	    }
	  seq_dict->hits = j;
	  j = 0;
	  for(i=1;i<seq_dict->hits;i++)
	    {
	      if ( seq_dict->hit[i] != seq_dict->hit[i-1] )
		seq_dict->hit[j++] = seq_dict->hit[i];
	    }
	  seq_dict->hits = j;
	  
	  for(i=0;i<seq_dict->hits;i++)
	    {
	      n = seq_dict->hit[i]%PRIME;
	      count[n]++;
	    }
	  free_seq(seq);
	  free(seq_dict->hit);
	  free(seq_dict);
	  m++;
	}
      
      seqdict->hash_db = hash_db = (int**)calloc(PRIME,sizeof(int*));
      for(n=0;n<PRIME;n++)
	{
	  if ( count[n] )
	    hash_db[n] = (int*)calloc(count[n],sizeof(int));
	  count[n] = 0;
	}
      
      rewind_spec(db);
      
      m = 0;
      while ((seq = next_seq( db )) != 0)
	{
	  if ( ! (m%100) ) fprintf(stderr, "%d\n", m );
	  
	  seq_dict = make_dictionary_for_sequence( seq, wordsize, NULL, 0, 1 );
	  qsort( seq_dict->hit, seq_dict->hits, sizeof(int), icmp);
	  j = 0;
	  for(i=0;i<seq_dict->hits;i++)
	    {
	      if ( seq_dict->hit[i] >= 0  )
		seq_dict->hit[j++] = seq_dict->hit[i];
	    }
	  seq_dict->hits = j;
	  j = 0;
	  for(i=1;i<seq_dict->hits;i++)
	    {
	      if ( seq_dict->hit[i] != seq_dict->hit[i-1] )
		seq_dict->hit[j++] = seq_dict->hit[i];
	    }
	  seq_dict->hits = j;
	  
	  for(i=0;i<seq_dict->hits;i++)
	    {
	      n = seq_dict->hit[i]%PRIME;
	      hash_db[n][count[n]++] = m;
	    }
	  seqdict->seq_len[m] = seq->len;
	  free_seq(seq);
	  free(seq_dict->hit);
	  free(seq_dict);
	  m++;
	}
      
/* write the text header */

      fprintf(fp, "%d\n", wordsize );
      fprintf(fp, "%d\n", PRIME );
      fprintf(fp, "%s\n", db );
      fprintf(fp, "%d\n", DB->sequences);

/* the sequence lengths */

      fwrite( seqdict->seq_len, sizeof(int), seqdict->sequences, fp );

/* the sequence names */

      rewind_spec(db);
      n = 0;
      while ((seq = next_seq( db )) != 0)
	{
	  seqdict->seq_name[n] = strdup(seq->name);
	  fprintf(fp,"%5d %s\n", n++, seq->name );
	  free_seq(seq);
	}
/* the hash table */

      for(n=0;n<PRIME;n++)
	if ( count[n] )
	  {
	    fwrite( &n, sizeof(int), 1, fp );
	    fwrite( &count[n], sizeof(int), 1, fp );
	    fwrite( hash_db[n], sizeof(int), count[n], fp );
	  }
      
      fclose(fp);
      return seqdict;
    }
  return NULL;
}

SEQ_DICT *read_dictionary(char *db, int argc, char **argv)
{
  
  FILE *fp;
  char dictfile[256], name[256], buf[256];
  SEQ_DICT *seqdict=NULL;
  int n, i;

  strcpy(buf,db);

  if (  (fp=openfile_in_seqpath( buf, "dict", "r", dictfile ) ) || (fp=openfile_in_seqpath_arg( "-dbdict=%s", NULL, "r", argc, argv, dictfile ) ))
    {
    
      seqdict = (SEQ_DICT*)calloc(1,sizeof(SEQ_DICT));

      if (  fscanf(fp, "%d\n%d\n%s\n%d\n", &seqdict->wordsize, &seqdict->prime, db, &seqdict->sequences ) != 4 )
	{
	  fprintf( stderr, "read error with dictionary file %s\n", dictfile );
	  exit(1);
	}

      printf("! opening dictionary %s\n", dictfile );
      
      seqdict->count = (int*)calloc(seqdict->prime, sizeof(int));
      seqdict->prob = (float*)calloc(seqdict->prime, sizeof(float));
      seqdict->hash_db = (int**)calloc(seqdict->prime, sizeof(int*));
      seqdict->seq_name = (char**)calloc(seqdict->sequences,sizeof(char*));
      seqdict->db = open_database(db);
      seqdict->seq_len = (int*)calloc(seqdict->sequences,sizeof(int));

      fread( seqdict->seq_len, sizeof(int), seqdict->sequences, fp );

      for(n=0;n<seqdict->sequences;n++)
	{
	  fscanf(fp, "%d %s\n", &i, name );
	  seqdict->seq_name[i] = (char*)strdup(name);
	}
      
      for(n=0;n<seqdict->prime;n++)
	{
	  if ( fread( &i, sizeof(int), 1, fp) )
	    {
	      fread( &seqdict->count[i], sizeof(int), 1, fp);
	      seqdict->hash_db[i] = (int*)calloc(seqdict->count[i],sizeof(int));
	      fread( seqdict->hash_db[i], sizeof(int), seqdict->count[i], fp );
	      seqdict->prob[i] = seqdict->count[i]/(seqdict->sequences+1.0e-10);
	    }
	  else
	    break;
	} 

      printf("! dictionary %s for db %s read in, %d sequences\n", dictfile, db, seqdict->sequences );
      
    }
  return seqdict;
}

/* compare query sequence to the seqquence dictionary seqdict.  If fp
is non-null then print top best hits out to fp, else return a pointer
to a match struct containing the sorted hits */

match_struct *compare_query(SEQUENCE *query, int best,
			    SEQ_DICT *seqdict, FILE *fp )
{ 
  WORD_DICT *query_dict, *comp_dict; 
  int j, i, n; 
  match_struct *match; 
  SEQUENCE *comp, *seq;
  int seq_no=0; 
  int wordsize = seqdict->wordsize; 
  int prime = seqdict->prime; 
  DATABASE *DB = seqdict->db; 
  int sequences = seqdict->sequences; 
  char **seq_name = seqdict->seq_name; 
  int *count = seqdict->count; 
  int **hash_db = seqdict->hash_db;
  float mean, std, mean_len;

  query_dict = make_dictionary_for_sequence( query, wordsize, NULL, 0, 1 );
  qsort( query_dict->hit, query_dict->hits, sizeof(int), icmp);
  match = (match_struct*)calloc(2*sequences,sizeof(match_struct));
 
  j = 0;
  for(i=0;i<query_dict->hits;i++)
    {
      if ( query_dict->hit[i] >= 0  )
	query_dict->hit[j++] = query_dict->hit[i];
    }
  query_dict->hits = j;
  j = 0;
  for(i=1;i<query_dict->hits;i++)
    {
      if ( query_dict->hit[i] != query_dict->hit[i-1] )
	query_dict->hit[j++] = query_dict->hit[i];
    }
  query_dict->hits = j;
  
  for(i=0;i<sequences;i++)
    {
      match[i].name = seq_name[i];
      match[i].strand = forward;
      match[i].score = 0;
      match[i].id = i;
    }
  
  for(i=0;i<query_dict->hits;i++)
    {
      n = query_dict->hit[i]%prime;
      for(j=0;j<count[n];j++)
	match[hash_db[n][j]].score--;
    }
  
  
  comp = seqdup( query );
  complement_seq( comp->s );
  
  comp_dict =  make_dictionary_for_sequence( comp, wordsize, NULL, 0, 1 );
  qsort( comp_dict->hit, comp_dict->hits, sizeof(int), icmp);
  
  j = 0;
  for(i=0;i<comp_dict->hits;i++)
    {
      if ( comp_dict->hit[i] >= 0  )
	comp_dict->hit[j++] = comp_dict->hit[i];
    }
  comp_dict->hits = j;
  j = 0;
  for(i=1;i<comp_dict->hits;i++)
    {
      if ( comp_dict->hit[i] != comp_dict->hit[i-1] )
	comp_dict->hit[j++] = comp_dict->hit[i];
    }
  comp_dict->hits = j;
  
  
  for(i=0;i<sequences;i++)
    {
      match[sequences+i].name = seq_name[i];
      match[sequences+i].strand = reverse;
      match[sequences+i].score = 0;
      match[sequences+i].id = i;
    }
  
  for(i=0;i<comp_dict->hits;i++)
    {
      n = comp_dict->hit[i]%prime;
      for(j=0;j<count[n];j++)
	match[sequences+hash_db[n][j]].score--;
    }

  mean_len = 0.0;
  for(i=0;i<sequences;i++)
    { 
      mean_len += seqdict->seq_len[i];
    }

  mean_len /= sequences;

/*  for(i=0;i<sequences;i++)
    {
      match[i].score *= (mean_len/(seqdict->seq_len[i]+1.0e-5));
      match[sequences+i].score *= (mean_len/(seqdict->seq_len[i]+1.0e-5));
    }
*/
  
  qsort(match,2*sequences,sizeof(match_struct),match_struct_cmp);

  mean = std = 0.0;
  for(i=0;i<2*sequences-best;i++)
    {
      mean += -match[i].score;
      std +=  match[i].score *  match[i].score;
    }
  mean /= i;
  std = 1.0e-5 + sqrt( (std - i*mean*mean)/(i-1) );


 /* for(i=0;i<2*sequences-best;i++)
    match[i].score = ( match[i].score + mean )/ std;
*/

  if ( fp )
    {
      int c1, c2, sw;

      fprintf(fp,"\n>>> %5d %-12s len %d\n", ++seq_no, query->name, query->len);
      if (DB )
	for(i=0;i<best;i++)
	  {
	    seq = read_sequence( match[i].name, DB );
	    if ( match[i].strand == reverse )
	      complement_seq(seq->s);
	    
	    /* FIXME: parameters to smith_waterman_score are almost certainly
	       wrong !! */
	    sw = smith_waterman_score( query, seq, 1, -3, -3, 0, 0, 0, &c1, &c2); 
	    fprintf(fp,"%5d %-15s %s %7.2f %7.2f sw: %5d %s\n", i+1, match[i].name, (match[i].strand == forward ? "+" : "-" ) , -match[i].score, (-match[i].score -mean)/std, sw, seq->desc );
	    free_seq(seq);
	  }
      else
	for(i=0;i<best;i++)
	  {
	    fprintf(fp,"%5d %-15s %7.2f\n", i+1, match[i].name, -match[i].score);
	  }
      fprintf(fp,"\n");
    }
  free_seq(comp);
  free_dict( comp_dict);
  free_dict( query_dict );

  if ( fp )
    free(match);
  else
    return match;

  return 0;
}

int match_struct_cmp(const void *ap, const void *bp)
{
  const match_struct *a = ap;
  const match_struct *b = bp;

  return fcmp(&a->score, &b->score);
}

