/*  Last edited: Jan  4 20:48 1997 (badger) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
enum { forward, reverse };

typedef struct
{
  DATABASE *db;
  int wordsize;
  int prime;
  int sequences;
  int **hash_db;
  int *count;
  float *prob;
  char **seq_name;
  int *seq_len;
}
SEQ_DICT;

SEQ_DICT *create_dictionary(int argc, char **argv);
SEQ_DICT *read_dictionary(char *db, int argc, char **argv);

typedef struct 
{
  float score;
  char *name;
  int id;
  int strand;
}
match_struct;


match_struct *compare_query(SEQUENCE *query, int best,
			    SEQ_DICT *seqdict, FILE *fp );

int match_struct_cmp(const void *ap, const void *bp);

