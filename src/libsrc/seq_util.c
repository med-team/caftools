/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* $Id: seq_util.c 13592 2004-11-25 16:54:23Z rmd $ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <cl.h>
#include <seq_util.h>
#include <cmp.h>
#include <readline.h>


static void add_database(DATABASE * db);
static int hash_name(char *string);

static int database_count = 0;
static DATABASE *databases[100];

FILE * openfile_in_seqpath_arg(char *format, char *ext,
			       char *mode, int argc, char **argv,
			       char *fullname)
{
  char buf[256];

  if ( getarg( format, buf, argc, argv ) )
    return openfile_in_seqpath( buf, ext, mode, fullname );
  else
    return NULL;
}

FILE * openfile_in_seqpath(char *basename, char *ext,
			   char *mode, char *fullname)
{
  char buf[256];

  strcpy(buf,basename);
  extension(buf,ext);

  return openfile_in_envpath( buf, mode, SEQPATH, fullname );
}
/*
char *seq_data_home(name)
char *name;
{
  char buf[256], *s;

  strcpy(buf,name);
  s = buf;
  while(*s)
    {
      *s = toupper(*s);
      s++;
    }

  if ( ( s = (char*)getenv(buf) ) || (s = (char*)getenv("SEQ_DATA_HOME") ) )
    return s;
  else
    return (char*)strdup("./");
}
*/

DATABASE * open_database(char *name)
{
  DATABASE *db;

  if ((db = which_database( name )) != 0)
    return db;

  if ((db = open_embl_database( name )) != 0)
    return db;
  else if ((db = open_nbrf_database( name )) != 0)
    return db ;
  else if ((db = open_fasta_database( name)) != 0)
    return db;
  else
    {
      fprintf(stderr, "ERROR could not open database %s\n", name );
      exit(1);
    }
}

DATABASE * open_embl_database(char *name)
{
  char buf[256];
  DATABASE *db;

  db = (DATABASE*)calloc(1,sizeof(DATABASE));
  db->database = (char*)strdup(name);
  db->datafile = openfile_in_seqpath(name, "dat", "r",buf);
  db->indexfile = openfile_in_seqpath(name, "index","r",buf);
  db->type = EMBL;

  if ( ! db->datafile || ! db->indexfile )
    {
      free( db );
      return NULL;
    }

  make_embl_index( db );
  
  fprintf(stderr,"\n! embl format database %s opened: %d sequences\n", name, db->sequences );

  add_database(db);

  return db;
}

char *
acc_nos( SEQUENCE *seq )
{
  if ( seq->database->type == EMBL )
    return embl_seq_comment( seq, "AC" );
  else
    return nbrf_seq_comment( seq, "ACCESSION" );
}

char * seq_comment(SEQUENCE *seq, char *key)
{
  if ( seq->database->type == EMBL )
    return embl_seq_comment( seq, key );
  else
    return nbrf_seq_comment( seq, key );
}

char * embl_seq_comment(SEQUENCE *seq, char *key)
{
  char line[256];
  unsigned long text_offset;
  unsigned long offset = get_offset( seq->name, seq->database, &text_offset );
  unsigned long end_pos;
  static char buf[MAX_BUF];

  *buf = 0;
  fseek(seq->database->datafile,offset,0);
  end_pos = find_next( seq->database->datafile, "//", line );
  fseek(seq->database->datafile,offset,0);

  while (1)
    {
      find_next( seq->database->datafile, key, line );
      if ( end_pos < ftell(seq->database->datafile ) )
	break;
      else
	{
	  if ( strlen(buf) + strlen(line) + 2 < MAX_BUF )
	    strcat( buf, clean_line(line)+strlen(key) );
	  else
	    break;
	}
    }

  return buf;
}

char * nbrf_seq_comment(SEQUENCE *seq, char *key)
{
#if 0
  char line[256];
  unsigned long text_offset;
  unsigned long end_pos;
#endif
  static char buf[MAX_BUF];

  *buf = 0;
#if 0
  /* This bit doesn't work, and I can't be bothered to fix it at the moment! */
  if ( seq->database->textfile )
    {
      fseek(seq->database->textfile,text_offset+4,0);
      end_pos = find_next( seq->database->textfile, ">>>>", line );
      fseek(seq->database->textfile,text_offset,0);

      while (1)
	{
	  find_next( seq->database->textfile, key, line );
	  if ( end_pos < ftell(seq->database->textfile ) )
	    break;
	  else
	    {
	      if ( strlen(buf) + strlen(line) + 2 < MAX_BUF )
		strcat( buf, clean_line(line)+strlen(key) );
	      else
		break;
	    }
	}
    }
#endif
  return buf;
}


DATABASE * open_fasta_database( char *name )
{
  char buf[256];
  DATABASE *db;

  db = (DATABASE*)calloc(1,sizeof(DATABASE));
  db->database = (char*)strdup(name);
  db->datafile = openfile_in_seqpath(name, "fasta", "r",buf);
  db->indexfile = openfile_in_seqpath(name, "index","r",buf);
  db->type = FASTA;

  if ( ! db->datafile || ! db->indexfile )
    {
      free( db );
      return NULL;
    }

  make_fasta_index( db );
  
  fprintf(stderr,"\n! fasta format database %s opened: %d sequences\n", name, db->sequences );

  add_database(db);

  return db;
}

DATABASE * open_nbrf_database( char *name )
{
  char buf[256];
  DATABASE *db;

  db = (DATABASE*)calloc(1,sizeof(DATABASE));
  db->database = (char*)strdup(name);
  db->datafile = openfile_in_seqpath(name, "seq", "r",buf);
  db->textfile = openfile_in_seqpath(name, "ref", "r",buf);
  db->indexfile = openfile_in_seqpath(name, "index", "r",buf);
  db->type = NBRF;

  if ( ! db->datafile || ! db->textfile || ! db->indexfile )
    {
      free( db );
      return NULL;
    }

  make_nbrf_index( db );
  
  fprintf(stderr,"\n! nbrf format database %s opened: %d sequences\n", name, db->sequences );

  add_database(db);

  return db;
}


DATABASE * which_database(char * database_name)
{
  int n;

  for(n = 0; n < database_count; n++)
    if ( ! strcmp( database_name, databases[n]->database ) )
      return databases[n];
  return NULL;
}

static void add_database(DATABASE * db)
{
  if ( database_count < 100 )
    databases[database_count++] = db;
  else
    {
      fprintf(stderr, "\nERROR: too many databases! \n");
      exit (1);
    }
}

void make_embl_index( DATABASE *db )
{
  int n;
  unsigned long offset;
  HASH_LIST *item;
  char name[256];

  db->index = (HASH_LIST**)calloc(PRIME_NUM,sizeof(HASH_LIST*));

  while( fscanf( db->indexfile, "%s %lu\n", name, &offset ) == 2 )
    {
      n = hash_name(name);
      item = (HASH_LIST*)calloc(1,sizeof(HASH_LIST));
      item->name = (char*)strdup(name);
      item->offset = offset;
      db->sequences++;

      if ( ! db->index[n] )
	db->index[n] = item;
      else
	{
	  item->next = db->index[n];
	  db->index[n] = item;
	}
    }

  rewind(db->indexfile);
}

void
make_fasta_index( DATABASE *db )


{
  int n;
  unsigned long offset;
  HASH_LIST *item;
  char name[256];

  db->index = (HASH_LIST**)calloc(PRIME_NUM,sizeof(HASH_LIST*));

  while( fscanf( db->indexfile, "%s %lu\n", name, &offset ) == 2 )
    {
      n = hash_name(name);
      item = (HASH_LIST*)calloc(1,sizeof(HASH_LIST));
      item->name = (char*)strdup(name);
      item->offset = offset;
      db->sequences++;

      if ( ! db->index[n] )
	db->index[n] = item;
      else
	{
	  item->next = db->index[n];
	  db->index[n] = item;
	}
    }

  rewind(db->indexfile);
}

void
make_nbrf_index( DATABASE *db )

{
  int n;
  unsigned long offset1, offset2;
  HASH_LIST *item;
  char name[256];

  db->index = (HASH_LIST**)calloc(PRIME_NUM,sizeof(HASH_LIST*));

  while( fscanf( db->indexfile, "%s %lu %lu\n", name, &offset1, &offset2 ) == 3 )
    {
      n = hash_name(name);
      item = (HASH_LIST*)calloc(1,sizeof(HASH_LIST));
      item->name = (char*)strdup(name);
      item->offset = offset1;
      item->text_offset = offset2;
      db->sequences++;

      if ( ! db->index[n] )
	db->index[n] = item;
      else
	{
	  item->next = db->index[n];
	  db->index[n] = item;
	}
    }

  rewind(db->indexfile);
}


SEQUENCE * read_seq(char *name)
{
  char *db, *sname;
  DATABASE *d;

  sname = (char*)strchr( name, ':' )+1;
  *(sname-1) = 0;
  db = name;

  d = open_database( db );
  return read_sequence( sname, d );

}

SEQUENCE *read_sequence(char *name, DATABASE *database)
{
  unsigned long text_offset;
  unsigned long offset = get_offset( name, database, &text_offset );

  if ( database->type == EMBL )
    return read_embl_sq( name, database, offset );
  else if ( database->type == NBRF )
    return read_nbrf_sq( name, database, offset);
  else if ( database->type == FASTA )
    return read_fasta_sq( name, database, offset);
  else
    return NULL;
}

unsigned long get_offset(char *name, DATABASE *database,
			 unsigned long *text_offset)
{
  int n;
  HASH_LIST *h;

  n = hash_name(name);
  h = database->index[n];

  *text_offset = 0;

  while( h )
    {
      if ( ! strcmp( name, h->name ) )
	{
	  *text_offset = h->text_offset;
	  return h->offset;
	}
      else
	h = h->next;
    }
  return 0;
}
  
SEQUENCE * read_embl_sq(char *name, DATABASE *database, unsigned long offset)
{
  char sname[256], line[512];
  int c;
  SEQUENCE *seq = (SEQUENCE*)calloc(1, sizeof(SEQUENCE));
  int len, n;

  fseek(database->datafile, offset, 0 );

  find_next( database->datafile, "ID", line );

  *sname = 0;
  sscanf( line, "ID  %s", sname );

  if ( strcmp( sname, name ) )
    {
      fprintf(stderr,"\n! ERROR on read_embl_sq: pointer offset for %s wrong! (gets %s instead)\n", name, sname );
	exit(1);
    }

  seq->name = (char*)strdup(name);
  seq->database = database;

  find_next( database->datafile, "DE", line );

  seq->desc = (char*)strdup(&line[5]);

  find_next( database->datafile, "SQ", line );

  sscanf( line, "SQ   Sequence %d", &len );

  seq->len = len;
  seq->s = (char*)calloc(len+1,sizeof(char));
  n = 0;

  while ( (c = getc(database->datafile)) != '/' && c != EOF )
    {
      if ( isalpha(c) && ! isspace(c) && n < len )
	seq->s[n++] = (char) tolower(c);
    }
  return seq;
}


/* read a fasta sequence in from a file. Can be used for multiple reads */
SEQUENCE * get_fasta_sq(FILE *fp)
{
  int c;
  SEQUENCE *seq=NULL;
  char name[256], desc[256];
  int i;
  static char *buffer=NULL;
  static int buflen=0;

  if ( buffer == NULL )
    {
      buflen = 100000;
      buffer = (char*)calloc(buflen,sizeof(char));
    }

  while( (c=getc(fp)) != '>' && c != EOF );

  if ( c == EOF )
    return NULL;
  else 
    {
      seq = (SEQUENCE*)calloc(1, sizeof(SEQUENCE));

      i = 0;
      while( ! isspace(c=getc(fp)) ) 
	if ( i < 254 )
	  name[i++] = c;


      name[i] = 0;
      seq->name = strdup(name);

      while( c != '\n' && c != EOF && isspace(c=getc(fp)) );

      i = 0;
      while( c != '\n' && c != EOF )
	{
	  if ( i < 254 ) 
	    desc[i++] = c;
	  c = getc(fp);
	}
      desc[i] = 0;
      seq->desc = strdup(desc);

      seq->len=0;
      while( (c=getc(fp)) != '>' && c != EOF )
	{
	  if ( isalpha(c)||(c=='-') )
	    {
	      if ( seq->len+1 >= buflen )
		{
		  buflen += 100000;
		  buffer = (char*)realloc(buffer, buflen*sizeof(char));
		}
	      buffer[seq->len++] = c;
	    }
	}

      buffer[seq->len] = 0;

      seq->s = (char*)strdup(buffer);
      
      buffer[0] = 0;

      ungetc(c,fp);
    }
  return seq;
}

SEQUENCE * read_fasta_sq(char *name, DATABASE *database, unsigned long offset)
{
  char sname[256], line[512];
  int c;
  SEQUENCE *seq = (SEQUENCE*)calloc(1, sizeof(SEQUENCE));
  int len, n;
  unsigned long current;

  fseek(database->datafile, offset, 0 );

  read_line(database->datafile,line);
  *sname = 0;
  sscanf( line, ">%s", sname );

  if ( strcmp( sname, name ) )
    {
      fprintf(stderr,"\n! ERROR on read_fasta_sq: pointer offset for %s wrong! (gets %s instead)\n", name, sname );
	exit(1);
    }

  seq->name = (char*)strdup(name);
  seq->database = database;

  seq->desc = (char*)strdup("");

  current = ftell(database->datafile);

  len=0;
  while((c=getc(database->datafile)) != '>' && c != EOF )
    len += ( ! isspace(c) && c != '\n' );
  seq->len = len;
  seq->s = (char*)calloc(len+1,sizeof(char));
  n = 0;

  fseek(database->datafile, current, 0 );

  while( (c=getc(database->datafile)) != '>' && c != EOF )
    if ( ! isspace(c) && c != '\n'  && n < len )
      seq->s[n++] = (char) tolower(c);

  seq->len = n;

  return seq;
}

SEQUENCE * read_nbrf_sq(char *name, DATABASE *database, unsigned long offset)
{
  char sname[256], line[512];
  int c;
  SEQUENCE *seq = (SEQUENCE*)calloc(1, sizeof(SEQUENCE));
  unsigned long current;
  int len;

  fseek(database->datafile, offset, 0 );

  *sname = 0;
  read_line(database->datafile,line);
  sscanf( line, ">>>>%s", sname );

  if ( strcmp( sname, name ) )
    {
      fprintf(stderr,"\n! ERROR on read_nbrf_sq: pointer offset for %s wrong! (gets %s instead)\n", name, sname );
	exit(1);
    }

  seq->name = (char*)strdup(name);
  seq->database = database;

  read_line(database->datafile,line);
  seq->desc = (char*)strdup(line);

  current = ftell(database->datafile);
  len = 0;

  while ( (c = getc(database->datafile)) != '>' && c != EOF )
    {
      if ( isalpha(c) && ! isspace(c)  )
	len++;
    }

  seq->len = len;
  seq->s = (char*)calloc(len+1,sizeof(char));
  fseek( database->datafile, current, 0 );
  len = 0;

  while ( (c = getc(database->datafile)) != '>' && c != EOF )
    {
      if ( isalpha(c) && ! isspace(c)  )
	seq->s[len++] = (char) tolower(c);
    }

  return seq;
}

unsigned long find_next(FILE *fp, char *text, char *line)
{
  while( read_line(fp,line) != EOF )
    {
      clean_line(line);
      if ( !strncmp( line, text, strlen(text) ) )
	return ftell(fp)-strlen(line);
    }
  return 0;
}


int hash_name(char * string)
{
  /* computes a integer hash value between 0 and PRIME_NUM-1 for the
     character string (case-insensitive)*/

  int n=0;

  while( *string )
    {
      n = (64*n + tolower((int) *string) ) % PRIME_NUM;
      string++;
    }

  return n;
}

void compile_embl_index(char * name )
{
  char buf[256];
  FILE *datafile, *indexfile;
  char sname[256];
  char c;
  unsigned long n, offset;
  int state=-1;

  datafile = openfile_in_seqpath( name, "dat", "r", buf);
  indexfile = openfile_in_seqpath( name, "index", "w", buf);

  fprintf( stderr, "compiling embl format index for database %s ...\n", name );

  state = -1;
  offset = 0;
  n = 0;
  while ( (c = fgetc( datafile ) ) != EOF )
    {
      if ( c == '\n' )
	{
	  state = 1;
	}
      else if ( (state == 1 || state == -1 ) && c == 'I' )
	{
	  state = 2;
	}
      else if ( state == 2 && c == 'D' )
	{
	  offset = ftell(datafile)-2;
/*	  while( isspace(c=fgetc(datafile)) );*/
	  fscanf( datafile, "%s", sname ); 
	  n++;
	  fprintf(indexfile, "%-10s %lu\n", sname, offset );
	  printf( "%-10s %lu\n", sname, offset );
	  state = 0;
	}
      else
	state = 0;
    }
  fclose(indexfile);
  fclose(datafile);

  fprintf(stderr, "... done. %lu sequences indexed\n", n );
}

void
compile_fasta_index( char *name )
{
  char buf[256];
  FILE *datafile, *indexfile;
  char sname[256];
  char c;
  unsigned long n, offset;
  int state=-1;

  datafile = openfile_in_seqpath( name, "fasta", "r", buf);
  indexfile = openfile_in_seqpath( name, "index", "w", buf);

  fprintf( stderr, "compiling fasta format index for database %s ...\n", name );

  state = -1;
  offset = 0;
  n = 0;
  while ( (c = fgetc( datafile ) ) != EOF )
    {
      if ( c == '\n' )
	{
	  state = 1;
	}
      else if ( (state == 1 || state == -1 ) && c == '>' )
	{
	  state = 2;
	}
      else
	state = 0;

      if ( state == 2 )
	{
	  offset = ftell(datafile)-1;
/*	  while( isspace(c=fgetc(datafile)) );*/
	  fscanf( datafile, "%s", sname ); 
	  n++;
	  fprintf(indexfile, "%-10s %lu\n", sname, offset );
	  printf( "%-10s %lu\n", sname, offset );
	  state = 0;
	}
    }
  fclose(indexfile);
  fclose(datafile);

  fprintf(stderr, "... done. %lu sequences indexed\n", n );
}

void
compile_nbrf_index( char *name )
{
  char buf[256];
  FILE *datafile, *textfile, *indexfile;
  char name1[256], name2[256];
  long c, d;
  unsigned long n;

  datafile = openfile_in_seqpath( name, "seq", "r",buf);
  textfile = openfile_in_seqpath( name, "ref", "r",buf);
  indexfile = openfile_in_seqpath( name, "index", "w",buf);

  fprintf( stderr, "compiling nbrf format index for database %s ...\n", name );

  n = 0;
  while (1)
    {
      c = seekto( datafile, ">>>>");
      d = seekto( textfile, ">>>>");

      if ( c == -1 || d == -1 )
	break;

      fscanf( datafile, ">>>>%s", name1 );
      fscanf( textfile, ">>>>%s", name2 );

      if ( strcmp( name1, name2 ) )
	{
	  fprintf( stderr, "\n! ERROR when indexing: different names %s %s\n", name1, name2 );
	  exit(1);
	}

      fprintf( indexfile, "%-10s %10lu %10lu\n", name1, c, d );
      n++;
    }
  fclose(indexfile);
  fclose(datafile);
  fclose(textfile);

  fprintf(stderr, "... done. %lu sequences indexed\n", n );
}

long seekto(FILE *fp, char *text)
{
  unsigned long p;
  char c, *s;

  while(1)
    {
      c = getc(fp);
      if ( c == EOF )
	return -1;
      else if ( c == *text )
	{
	  p = ftell( fp );
	  s = text;
	  while( *s )
	    {
	      if ( c != *s )
		break;
	      else if ( c == EOF )
		return -1;
	      else
		{
		  s++;
		  c = getc(fp);
		}
	    }
	  if (! *s )
	    {
	      p--;
	      fseek(fp,p,0);
	      return p;
	    }
	  else 
	    fseek(fp,p+1,0);
	}
    }
  /* return -1; */
}
	    
void free_seq(SEQUENCE *seq)
{
  if ( seq )
    {
      if ( seq->name && *(seq->name)) free( seq->name );
      if ( seq->s && *(seq->s) ) free( seq->s );
      if ( seq->desc && *(seq->desc) ) free( seq->desc );
      free( seq );
    }
}

FILE * which_file_of_sequences(char *filename)
{
  static char *filenames[100];
  static FILE *fp[100];
  static int files;
  int n;

  if ( *filename != '@' )
    return NULL;
  else
    filename++;

  for(n=0;n<files;n++)
    {
      if ( ! strcmp( filenames[n],filename) )
	{
	  if ( fp[n] )
	    return fp[n];
	  else return fp[n]=openfile(filename,"r");
	}
    }

  if ( files == 100 )
    {
      fprintf( stderr, "ERROR: too many files of sequences!\n");
      exit(1);
    }

  filenames[files] = (char*)strdup(filename);
  fp[files] = openfile(filename,"r");
  return fp[files++];
}

SEQUENCE * next_seq_from_list_file(FILE *list_file)
{
  char line[256], seq[256];
  SEQUENCE *sq = NULL;

  *line = 0;

  while ( ! sq &&  skip_comments( list_file, line ) != EOF )
    {
      sscanf( line, "%s", seq );
      sq = read_seq( seq );
    }

  return sq;
  
}

SEQUENCE * next_seq_from_database_spec( char *spec )
{
  char wild[256], line[256], name[256];
  DATABASE *db = is_sequence_spec( spec, wild );
  int len = strlen(wild);

  while( read_line( db->indexfile, line ) != EOF )
    {
      sscanf( line, "%s", name );
      if ( !strncmp( name, wild, len ) )
	return read_sequence( name, db );
    }

  return NULL;
}

DATABASE * is_sequence_spec(char *spec, char *wild)
{
  static char *old_spec=NULL;
  static char *old_wild;
  static DATABASE *old_db;
  char buf[256], *sname, *db, *s;

  if ( ! old_spec || strcmp( spec, old_spec ) )
    {
      old_spec = (char*)strdup(spec);
      strcpy(buf,spec);
      sname = (char*)strchr( buf, ':' );
      if ( ! sname )
	{
	  sname = (char*)strdup("");
	}
      else
	{
	  sname++;
	  *(sname-1) = 0;
	}

      if ((s = strchr( sname, '*')) != 0)
	*s = 0;
      db = buf;
      old_wild = (char*)strdup(sname);
      old_db = open_database(db);
    }

  strcpy(wild,old_wild);
  return old_db;
}

void rewind_spec( char *spec )
{
  char buf[256];
  DATABASE *db = is_sequence_spec( spec, buf );
  FILE *fp;

  if ( db )
    {
      rewind(db->indexfile);
      rewind(db->datafile);
    }
  else if ((fp = which_file_of_sequences( spec )) != 0)
    rewind(fp);
}

  
SEQUENCE *next_seq( char *spec_or_file )
{
  char wild[256];
  DATABASE *db = is_sequence_spec(spec_or_file,wild);
  FILE *fp;

  if ( db ) 
    return next_seq_from_database_spec(spec_or_file);
  else
    {
     if ( *spec_or_file == '@' && (fp = which_file_of_sequences( spec_or_file ) ) )
       return next_seq_from_list_file(fp);
   }
  return 0;
}

/*main(argc, argv)

int argc;
char **argv;
{
  DATABASE *db;
  SEQUENCE *seq;
  int n=2;
  char spec[256];

  while ( seq = next_seq_from_database_spec( argv[1] ) )
    {
      printf("seq: %s len: %d\n%s\n%s\n", seq->name, seq->len, seq->desc, seq->s );
      free_seq(seq);
    }
}*/

static char *complement_table=NULL;

char complement_base(char c)
{

  if ( !complement_table)
    {
      int x;
      complement_table = (char*)calloc(256,sizeof(char))+127;

      for(x=-127;x<128;x++)
	{
	  if ( x == 'a' )
	    complement_table[x] = 't';
	  else if ( x == 'c' )
	    complement_table[x] = 'g';
	  else if ( x == 'g' )
	    complement_table[x] = 'c';
	  else if ( x == 't' || x == 'u' )
	    complement_table[x] = 'a';
	  else if ( x == '[' )
	    complement_table[x] = ']';
	  else if ( x == ']' )
	    complement_table[x] = '[';
	  else if ( x == '*' )
	    complement_table[x] = '*';
	  else if ( x == '-' )
	    complement_table[x] = '-';
	  else
	    complement_table[x] = 'n';
	}
    }

  if ( isupper((int) c) )
    return toupper((int) complement_table[tolower((int) c)]);
  else
    return complement_table[tolower((int) c)];
}

char *complement_seq(char *seq)
{
  char *s = seq;
  char c, *t;

  while (*s)
    {
      *s = complement_base( *s);
	  
      s++;
    }

  t = seq;
  s--;

  while ( t < s )
    {
      c = *s;
      *s = *t;
      *t = c;
      t++;
      s--;
    }

  return seq;
}

SEQUENCE * subseq(SEQUENCE *seq, int start, int stop)

/* 
Creates a SEQUENCE containing the subsequence start to stop inclusive

If start > stop then returns the reverse complement

If start and stop are out of range they are truncated

 */

{
  SEQUENCE *subs = seqdup(seq);
  char *s;
  int i;
  int reverse;

  if ( start > stop )
    {
      reverse = 1;
      i = start;
      start = stop;
      stop = i;
    }
  else
    reverse = 0;

  if ( start < 0 ) start = 0;
  if ( start > seq->len-1 ) return NULL;
  if ( stop > seq->len-1 ) stop = seq->len-1;
    
  subs->len = stop-start+1;
  s = (char*)calloc(subs->len+1,sizeof(char));

  for(i=0;i<subs->len;i++)
    s[i] = seq->s[start+i];

  if ( reverse )
    complement_seq(s);

  free(subs->s);
  subs->s = s;

  return subs;
}


char * downcase(char *s)
{
  char *t = s;
  while (*s)
    {
      *s = tolower((int) *s);
      s++;
    }
  return t;
}

char * upcase(char *s)
{
  char *t = s;
  while (*s)
    {
      *s = toupper((int) *s);
      s++;
    }
  return t;
}

char * clean_line(char *s)
{
  char *t = s;

  while(*s)
    {
      if ( ! isprint((int) *s) )
	*s = ' ';
      s++;
    }
  return t;
}

/* convert a sequence into a regular expression. Allocates memory for the 
new string */

char * iubtoregexp(char *iubstring)
{
  int len=strlen(iubstring);
  char *s = iubstring;
  char *t, *r;

  while(*s)
    if ((t = iub_regexp(*s++)) != 0)
      len += strlen(t);

  r = (char*)calloc(len+3,sizeof(char));

  return iub2regexp( iubstring, r, len+1 );
  
}

/* convert a sequence with iub codes into a regular expression returns
either regexp or NULL if the length of the regexp is greater that
maxlen, which should be set to the max permitted size */

char * iub2regexp(char *iubstring, char *regexp, int maxlen)

{
  char *s, c;
  int len=0;

  maxlen--;
  while(*iubstring && len < maxlen)
    {
      if ((s = iub_regexp(c=*iubstring++)) != 0)
	{
	  strcat( regexp, s );
	  len += strlen(s);
	}
      else
	{
	  regexp[len++] = c;
	}
    }

  if ( len <= maxlen )
    {
      regexp[len]= 0;
      return regexp;
    }
  else
    return NULL;

}

/* return a pointer to the regexp corresponding to an iub code, or NULL otherwise  - note that only ambiguity codes return a non-null result*/

char * iub_regexp(char c)
{
  static char *iub[256];
  static int initialised;

  if ( ! initialised )
    {
      iub['r']= "[ag]";
      iub['y']= "[ctu]";
      iub['m']= "[ac]";
      iub['k']= "[gtu]";
      iub['s']= "[cg]";
      iub['w']= "[atu]";
      iub['h']= "[actu]";
      iub['b']= "[cgtu]";
      iub['v']= "[acg]";
      iub['d']= "[agtu]";
      iub['n']= "[acgtu]";
      initialised = 1;
    }

  return iub[tolower((int) c)];
}

#define MAXSEQLEN 100000


/* read a sequence from file */
SEQUENCE * get_seq_from_file(char *filename)
{
  FILE *fp;
  SEQUENCE *seq;
  char name[256];
  char desc[256];
  char buf[MAXSEQLEN+1];
  char *s, *t;
  int len;

  if ((fp=openfile( filename, "r" )) != 0)
    {
      seq = (SEQUENCE*)calloc(1, sizeof(SEQUENCE));
      read_line( fp, desc );
      sscanf( desc, "%s", name );
      seq->name = (char*)strdup(name);
      read_line( fp, desc );
      seq->desc = (char*)strdup(desc);
      len=fread(buf, sizeof(char), MAXSEQLEN, fp );
      buf[len] = 0;
      s = buf;
      t = buf;
      len = 0;
      while ( *s )
	{
	  if ( isalpha((int) *s) )
	    {
	      *t++ = tolower((int) *s);
	      len++;
	    }
	  s++;
	}
      *t = 0;
      seq->len = len;
      seq->s = (char*)strdup(buf);
      fclose(fp);
/*      fprintf(stderr,"read sequence %s len %d from file %s\n", name, len, filename ); */
      return seq;
    }
  return NULL;
}

SEQUENCE * seqdup(SEQUENCE *seq)
{
  SEQUENCE *dup;
  if ( seq )
    {
      dup = (SEQUENCE*)calloc(1, sizeof(SEQUENCE));
      *dup = *seq;
      if ( seq->name )
	dup->name = (char*)strdup(seq->name);
      else
	dup->name = NULL;
      if ( seq->desc )
	dup->desc = (char*)strdup(seq->desc);
      else
	dup->desc = NULL;
      if ( seq->s )
	dup->s = (char*)strdup(seq->s);
      else
	dup->s = NULL;


      return dup;
    }
  return NULL;
}

    
SEQUENCE * into_sequence(char *name, char *desc, char *s)
{
  SEQUENCE *seq = (SEQUENCE*)calloc(1,sizeof(SEQUENCE));
  seq->name = name;
  seq->desc = desc;
  seq->s = s;
  seq->len = strlen(s);
  return seq;
}

SEQUENCE * shuffle_seq(SEQUENCE *seq, int in_place)
{
  SEQUENCE *shuffled;

  if ( ! in_place )
    shuffled = seqdup( seq );
  else
    shuffled = seq;

  shuffle_s(shuffled->s);

  return shuffled;
}

/* in-place shuffle of a string */
char * shuffle_s(char *s)
{
  size_t n, m;
  size_t len = strlen(s);
  char tmp;

  for(n = 0; n < len; n++)
    {
      m = random() % len;
      tmp = s[m];
      s[m] = s[n];
      s[n] = tmp;
    }

  return s;
}
