/*  Last edited: Jun 13 13:03 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Aug 15 17:39 1995 (rmott) */
#define PRIME_NUM 10007
#define MAX_BUF 10000
#define SEQPATH "SEQ_DATA_HOME"

#ifndef _SEQ_UTIL_H_
#define _SEQ_UTIL_H_

typedef enum 
{ EMBL, NBRF, FASTA, SINGLE  } 
WHAT_KIND_OF_DATABASE;

typedef struct hash_list
{
  char *name;
  unsigned long offset, text_offset;
  struct hash_list *next;
}
HASH_LIST;

typedef struct
{
  char *database;
  WHAT_KIND_OF_DATABASE type;
  int sequences;
  unsigned long length;
  FILE *datafile;
  FILE *textfile;
  FILE *indexfile;
  HASH_LIST **index;
}
DATABASE;


typedef struct
{
  char *name;
  DATABASE *database;
  char *desc;
  int len;
  char *s;
}
SEQUENCE;


SEQUENCE * read_sequence(char *name, DATABASE *database);
/* SEQUENCE *read_sq(); */
SEQUENCE * read_seq(char *name);
SEQUENCE * read_embl_sq(char *name, DATABASE *database, unsigned long offset);
SEQUENCE * read_nbrf_sq(char *name, DATABASE *database, unsigned long offset);
SEQUENCE * read_fasta_sq(char *name, DATABASE *database, unsigned long offset);
SEQUENCE * next_seq_from_list_file(FILE *list_file);
SEQUENCE * next_seq_from_database_spec(char *spec);
SEQUENCE * next_seq(char *spec_or_file);
SEQUENCE * get_seq_from_file(char *filename);
SEQUENCE * seqdup(SEQUENCE *seq);
SEQUENCE * get_fasta_sq(FILE *fp);
SEQUENCE * shuffle_seq(SEQUENCE *seq, int in_place);
char * shuffle_s(char *s);

DATABASE * open_database(char *name);
DATABASE * which_database(char * database_name);
DATABASE * is_sequence_spec(char *spec, char *wild);
DATABASE * open_embl_database(char *name);
DATABASE * open_nbrf_database(char *name);
DATABASE * open_fasta_database(char *name);

FILE * which_file_of_sequences(char *filename);

unsigned long get_offset(char *name, DATABASE *database,
			 unsigned long *text_offset);
long seekto(FILE *fp, char *text);
unsigned long find_next(FILE *fp, char *text, char *line);
char * downcase(char *s);
char * upcase(char *s);
char * complement_seq(char *seq);
char * clean_line(char *s);
char * seq_comment(SEQUENCE *seq, char *key);
char * embl_seq_comment(SEQUENCE *seq, char *key);
char * nbrf_seq_comment(SEQUENCE *seq, char *key);
/* char * fasta_seq_comment(); */

/* static DATABASE *databases[100]; */
/* static int database_count;       */

char * iubtoregexp(char *iubstring);
char *iub2regexp(char *iubstring, char *regexp, int maxlen);
char *iub_regexp(char c);


FILE * openfile_in_seqpath(char *basename, char *ext,
			   char *mode, char *fullname);
FILE * openfile_in_seqpath_arg(char *format, char *ext,
			       char *mode, int argc, char **argv,
			       char *fullname);

SEQUENCE * into_sequence(char *name, char *desc, char *s);
SEQUENCE * subseq(SEQUENCE *seq, int start, int stop);
void free_seq(SEQUENCE *seq);

char complement_base(char c);


void make_embl_index( DATABASE *db );
void make_fasta_index( DATABASE *db );
void make_nbrf_index( DATABASE *db );


#endif
