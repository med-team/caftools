/*  Last edited: Jun 13 17:16 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <regex.h>
#include <seq_util.h>

void regerr(int errno)
{
  fprintf(stderr,"\n! ERROR in regexp, code %d\n",errno);
  exit(1);
}

char *next_regexp_match(char *instring, SEQUENCE *seq, int *len)
{
  static SEQUENCE *last_seq=NULL;
  static char *last_instring=NULL, *loc;
  static regex_t preg;
  char *instr_ = instring;
  regmatch_t match;
  char errbuf[256];
  int res;

  if ( instring != last_instring )
    {
      if (last_instring) regfree(&preg);
      
      /* For compatibility with the old regexp library that this
	 used to use, which terminated the regexp on '\n' */
      if ((loc = strchr(instring, '\n')) != NULL)
	{
	  instr_ = strdup(instring);
	  if (!instr_) {
	    perror(NULL);
	    exit(1);
	  }
	  instr_[loc - instring] = '\0';
	}
      res = regcomp(&preg, instr_, REG_NEWLINE);
      if (res != 0) {
	size_t sz = regerror(res, &preg, errbuf, sizeof(errbuf));
	fprintf(stderr, "\n! ERROR in regexp: %s%s\n",
		errbuf, sz < sizeof(errbuf) ? "" : "...");
	exit(1);
      }
      loc = seq->s;
      last_instring = instring;
      last_seq = seq;
      if (instr_ != instring)
	free(instr_);
    }

  if ( seq != last_seq )
    {
      loc = seq->s;
      last_seq = seq;
    }

  res = regexec(&preg, loc, 1, &match, REG_NOTEOL);
  if (res != REG_NOMATCH)
    {
      *len = match.rm_eo-match.rm_so;
      loc = seq->s + match.rm_so + 1;
      return seq->s + match.rm_so;
    }
  else
    {
      last_seq = NULL;
      return NULL;
    }
}

char *next_match(char *string, SEQUENCE *seq)
{
  return (char*)strstr( seq->s, string );
}
