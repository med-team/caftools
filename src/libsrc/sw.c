/*  Last edited: Jun 13 17:52 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Sep 10 18:03 1996 (rmott) */
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

#include <caf.h>
#include <seq_util.h>
#include <cl.h>
#include <cmp.h>

int sim_mat[256][256];
static int mat_init_ok=0;

int smith_waterman_score2( char *s1, char *s2, int len1, int len2,
			   int match, int mismatch, int gap, int neutral,
			   int offset, int window,
			   int *end_coord1, int *end_coord2);

int smith_waterman_score( SEQUENCE *seq1, SEQUENCE *seq2, int match, int mismatch, int gap, int neutral, int offset, int window, int *coord1, int *coord2 )
{
  int len1 = seq1->len;
  int len2 = seq2->len;
  char *s1 = seq1->s;
  char *s2 = seq2->s;

  alignStruct *as = smith_waterman_alignment2( s1, s2, len1, len2, match, mismatch, gap, neutral, offset, window );
  int score = smith_waterman_score2( s1, s2, len1, len2, match, mismatch, gap, neutral, offset, window, coord1, coord2);
  int ok = ( (score==as->score) && ( *coord1==as->end1) && (*coord2==as->end2) );
  printf("checking: score: %d %d coord1: %d %d coord2: %d %d  ok:%d\n", score, as->score, *coord1, as->end1, *coord2, as->end2, ok );
  return score;
}


int smith_waterman_score2( char *s1, char *s2, int len1, int len2, int match, int mismatch, int gap, int neutral, int offset, int window,  int *end_coord1, int *end_coord2 )

/* returns the smith-waterman score for the sequences s1 and s2, in a
band of half-width=window around diagonal=offset (=seq1-seq2) matches
score +match, mismatches +mismatch, gaps +gap (no gap start penalty)
*/
  
{
  int *A, *B, *a, *b, *c;
  int i1, i2;
  int max_score=0;
  int c1 = 0, c2 = 0;
  int m;
  int d, u, l;
  int start1, stop1;
  int start2, stop2;
  int x2;
  char *lower;
  char pad_char = '*'; /* NOTE fix this! */

  if ( ! mat_init_ok )
    mat_init(match,mismatch,gap,neutral,pad_char);

  if ( len1 > (m=strlen(s1)) )
    len1 = m;

  if ( len2 > (m=strlen(s2)) )
    len2 = m;

  A = (int*)calloc(len1+1,sizeof(int));
  B = (int*)calloc(len1+1,sizeof(int));

  lower = (char*)downcase(strdup(s1));
  
  a = A+1;
  b = B+1;

  if ( window < 0 )
    {
      start2 = 0;
      stop2 = len2-1;
    }
  else
    {
      start2 = -offset-window;
      if ( start2 < 0 )
	start2 = 0;
      if ( start2 >= len2 )
	start2 = len2-1;

      stop2 = len1-offset+window;
      if ( stop2 < 0 )
	stop2 = 0;
      if ( stop2 >= len2 )
	stop2 = len2-1;
    }

  for(i2=start2;i2<=stop2;i2++)
    {
      c = a;
      a = b;
      b = c;
      if ( window < 0 )
	{
	  start1 = 0;
	  stop1 = len1-1;
	}
      else
	{
	  start1 = i2+offset-window;
	  if ( start1 < 0 )
	    start1 = 0;
	  if ( start1 >= len1 )
	    start1 = len1-1;
	  stop1 = i2+offset+window;
	  if ( stop1 < 0 )
	    stop1 = 0;
	  if ( stop1 >= len1 )
	    stop1 = len1-1;
	}
      x2 = tolower((int) s2[i2]);

      for(i1=start1;i1<=stop1;i1++)
	{
	  d = a[i1-1] + sim_mat[(int) lower[i1]][x2];
	  u = a[i1] + gap;
	  l = b[i1-1] + gap;
	  if ( d > u )
	    {
	      if ( d > l )
		{
		  b[i1] = d;
		}
	      else
		b[i1] = l;
	    }
	  else if ( u > l )
	    b[i1] = u;
	  else
	    b[i1] = l;
	  if ( b[i1] < 0 )
	    b[i1] = 0;
	  if ( b[i1] > max_score )
	    {
	      max_score = b[i1];
	      c1 = i1;
	      c2 = i2;
	    }
	}
    }

  free(lower);
  free(A);
  free(B);

  *end_coord1 = c1;
  *end_coord2 = c2;

  return max_score;
}

alignStruct *
smith_waterman_alignment2( char *s1, char *s2, int len1, int len2, int match, int mismatch, int gap, int neutral, int offset, int window )
/* returns full sw alignment */
{
  int len;
  int **a;
  char **p;
  int *ibuf;
  char *cbuf;
  int i1, i2;
  int max_score=0;
  int c1 = 0, c2 = 0;
  int m, n;
  int d, u, l;
  int start1, stop1;
  int start2, stop2;
  int x2;
  alignStruct *as=NULL;
  int *AA;
  char *PP;
  Seg *seg;
  int gapping;
  int width, bufsize;
  char *lower;
  char pad_char = '*';

  if ( ! mat_init_ok )
    mat_init(match,mismatch,gap, neutral, pad_char);

  if ( len1 > (m=strlen(s1)) )
    len1 = m;

  if ( len2 > (m=strlen(s2)) )
    len2 = m;

  if ( window < 0 )
    {
      start2 = 0;
      stop2 = len2-1;
    }
  else
    {
      start2 = -offset-window;
      if ( start2 < 0 )
	start2 = 0;
      if ( start2 >= len2 )
	start2 = len2-1;

      stop2 = len1-offset+window;
      if ( stop2 < 0 )
	stop2 = 0;
      if ( stop2 >= len2 )
	stop2 = len2-1;
    }

  width = 2*window+5;
  bufsize = width*(stop2-start2+2);

  ibuf = (int*)calloc(bufsize,sizeof(int));
  cbuf = (char*)calloc(bufsize,sizeof(char));

  lower = (char*)downcase(strdup(s1));

  i2 = start2-1;
  if ( window < 0 )
    {
      start1 = 0;
      stop1 = len1-1;
    }
  else
    {
      start1 = i2+offset-window;
      if ( start1 < 0 )
	start1 = 0;
      if ( start1 >= len1 )
	start1 = len1-1;
      stop1 = i2+offset+window;
      if ( stop1 < 0 )
	stop1 = 0;
      if ( stop1 >= len1 )
	stop1 = len1-1;
    }
  
  a = (int**)calloc(stop2-start2+3,sizeof(int*))-start2+1;
  a[start2-1] = &ibuf[0]-start1+2;

  p = (char**)calloc(stop2-start2+3,sizeof(char*))-start2+1;
  p[start2-1] = &cbuf[0]-start1+2;

  for(m=1,i2=start2;i2<=stop2;m++,i2++)
    {
      if ( window < 0 )
	{
	  start1 = 0;
	  stop1 = len1-1;
	}
      else
	{
	  start1 = i2+offset-window;
	  if ( start1 < 0 )
	    start1 = 0;
	  if ( start1 >= len1 )
	    start1 = len1-1;
	  stop1 = i2+offset+window;
	  if ( stop1 < 0 )
	    stop1 = 0;
	  if ( stop1 >= len1 )
	    stop1 = len1-1;
	}
      a[i2] = &ibuf[m*width]-start1+2;
      p[i2] = &cbuf[m*width]-start1+2;

      x2 = tolower((int) s2[i2]);
      for(i1=start1;i1<=stop1;i1++)
	{
	  d = a[i2-1][i1-1] + sim_mat[(int) lower[i1]][x2];
	  u = a[i2-1][i1] + gap;
	  l = a[i2][i1-1] + gap;

	  AA = &a[i2][i1];
	  PP = &p[i2][i1];
	  if ( d >= u )
	    {
	      if ( d >= l )
		{
		  *AA = d;
		  *PP = path_diagonal;
		}
	      else
		{
		  *AA = l;
		  *PP = path_left;
		}
	    }
	  else if ( u > l )
	    {
	      *AA = u;
	      *PP = path_up;
	    }
	  else
	    {
	      *AA = l;
	      *PP = path_left;
	    }
	  if ( *AA <= 0 )
	    {
	      *AA = 0;
	      *PP = path_end;
	    }
	  else if ( *AA > max_score )
	    {
	      max_score = *AA;
	      c1 = i1;
	      c2 = i2;
	    }
	}
    }

  if ( max_score > 0 )
    {
      as = (alignStruct*)calloc(1,sizeof(alignStruct));
      as->score = max_score;
      as->end1 = c1;
      as->end2 = c2;
      len = 0;
      gapping = 0;
      while( (m=p[c2][c1]) != path_end )
	{
	  if ( m == path_diagonal )
	    {
	      c2--;
	      c1--;
	      len++;
	      gapping = 0;
	    }
	  else if ( m == path_left )
	    {
	      c1--;
	      if ( ! gapping )
		as->segs++;
	      gapping = 1;
	    }
	  else if ( m == path_up )
	    {
	      c2--;
	      if ( ! gapping )
		as->segs++;
	      gapping = 1;
	    }
	}
      as->start1 = c1+1;
      as->start2 = c2+1;
      
      if ( len )
	as->segs++;
      
      as->len = len;
      as->seg = (Seg*)calloc(as->segs,sizeof(Seg));
      seg = &as->seg[as->segs];
      gapping = 1;
      c1 = as->end1;
      c2 = as->end2;
      
      while( (m=p[c2][c1]) != path_end )
	{
	  if ( m == path_diagonal )
	    {
	      if ( gapping )
		{
		  gapping = 0;
		  seg--;
		  seg->end2 = c2;
		  seg->end1 = c1;
		}
	      seg->start2 = c2;
	      seg->start1 = c1;
	      n = sim_mat[tolower((int) s1[c1])][tolower((int) s2[c2])];
	      seg->score += n;
	      seg->matches += (n>0);
	      c2--;
	      c1--;
	    }
	  else if ( m == path_left )
	    {
	      c1--;
	      gapping = 1;
	    }
	  else if ( m == path_up )
	    {
	      c2--;
	      gapping = 1;
	    }
	}
    }
      
  free(a+start2-1);
  free(p+start2-1);
  free(ibuf);
  free(cbuf);
  free(lower);

  return as;
}

int 
equivalent_bases( int c1, int c2, int match, int mismatch, int gap )
{
  if ( c1 == c2 )
    {
      if ( c1 != '*' && c1 != 'n' && c1 != '-' )
	return match;
      else
	return 0;
    }
  else
    {
      if ( c1 == '*' || c2 == '*' )
	return gap;
      else if( c1 == 'n' || c1 == '-' || c2 == 'n' || c2 == '-' ) 
	return 0;
      else
	return mismatch;
    }
}

void
mat_init(int match, int mismatch, int gap, int neutral, char pad_char)
{
  int c1, c2;
  
  for(c1=0;c1<256;c1++)
    for(c2=0;c2<256;c2++)
      {
	if ( c1 == c2 )
	  {
	    if ( c1 != '*' && c1 != 'n' && c1 != '-' )
	      sim_mat[c1][c2] = match;
	    else
	      sim_mat[c1][c2] = 0;
	  }
	else
	  {
	    if ( c1 == pad_char || c2 == pad_char )
	      sim_mat[c1][c2] = sim_mat[c2][c1] = gap;
/*	    else if( c1 == 'n' || c1 == '-' || c2 == 'n' || c2 == '-' )  */
	    else if( c1 == 'n' || c2 == 'n'  ) 
	      sim_mat[c1][c2] = sim_mat[c2][c1] = neutral;
	    else
	      sim_mat[c1][c2] = sim_mat[c2][c1] = mismatch;
	  }
      }
  for(c1=0;c1<256;c1++)
    {
      c2 = tolower(c1);
      sim_mat[c1][c2] = sim_mat[c1][c1];
      sim_mat[c2][c1] = sim_mat[c1][c1];
    }

  mat_init_ok = 1;
}
  
void 
free_alignStruct( alignStruct *as )
{
  if ( as )
    {
      if (as->seg)
	free(as->seg);
      free(as);
    }
}

void 
print_alignment( FILE *fp, char *s1, char *s2, alignStruct *as, int width, char *label1, char *label2 )
{
  int i, /* lines, */ j1, j2, len=0;
  char *buf1, *b1;
  char *buf2, *b2;
  Seg *seg, *pseg;
  int start1, end1, start2, end2;
  char padding = '-';

  if ( fp && as && as->segs )
    {
      for(i=0;i<as->segs;i++)
	{
	  seg = &as->seg[i];
	  if ( seg->end2-seg->start2 != seg->end1-seg->start1)
	    {
	      fprintf(stderr,"ERROR segment mismatch %d %d   %d %d\n", seg->start1, seg->end1, seg->start2, seg->end2 );
	      exit(1);
	    }
	}

      seg = &as->seg[0];
      len = seg->end1-seg->start1+1;
      for(i=1;i<as->segs;i++)
	{
	  pseg = seg;
	  seg = &as->seg[i];
	  len += seg->end1-seg->start1+1;
	  len += seg->start1-pseg->end1-1 + seg->start2-pseg->end2-1;
	}

      len++;
      b1 = buf1 = (char*)calloc(len,sizeof(char));
      b2 = buf2 = (char*)calloc(len,sizeof(char));

      seg = &as->seg[0];
      len = seg->start2-seg->start1+1;
      for(j1=seg->start1,j2=seg->start2;j1<=seg->end1;j1++,j2++,b1++,b2++)
	{
	  *b1 = s1[j1];
	  *b2 = s2[j2];
	}

      for(i=1;i<as->segs;i++)
	{
	  pseg = seg;
	  seg = &as->seg[i];
	  if ( pseg->end1+1<seg->start1 )
	    for(j1=pseg->end1+1,j2=pseg->end2+1;j1<seg->start1;j1++,j2++,b1++,b2++)
	      {
		*b2 = padding;
		*b1 = s1[j1];
	      }
	  else
	    for(j2=pseg->end2+1,j1=pseg->end1+1;j2<seg->start2;j1++,j2++,b1++,b2++)
	      {
		*b1 = padding;
		*b2 = s2[j2];
	      }

	  
	  for(j1=seg->start1,j2=seg->start2;j1<=seg->end1;j1++,j2++,b1++,b2++)
	    {
	      *b1 = s1[j1];
	      *b2 = s2[j2];
	    }

	}
      /* lines = len/width + ( len%width > 0 ); */
      b1 = buf1;
      b2 = buf2;
      seg = &as->seg[0];
      start1 = end1 = seg->start1;
      start2 = end2 = seg->start2;
      while(*b1 && *b2)
	{
	  fprintf(fp,"%6d ", start1 );
	  for(j1=0;j1<width && *b1;j1++,b1++)
	    {
	      putc(*b1,fp);
	      if ( *b1 != padding )
		end1++;
	    }
	  fprintf(fp," %6d %s\n", end1-1, (label1 ? label1 : "") );

	  fprintf(fp,"%6d ", start2 );
	  for(j2=0;j2<width && *b2;j2++,b2++)
	    {
	      putc(*b2,fp);
	      if ( *b2 != padding )
		end2++;
	    }
	  fprintf(fp," %6d %s\n\n", end2-1, (label2 ? label2 : "") );
	  start1 = end1;
	  start2 = end2;

	}

      free(buf1);
      free(buf2);
    }
}

