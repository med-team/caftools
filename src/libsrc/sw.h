/*  Last edited: Jan  4 20:48 1997 (badger) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Nov  1 15:55 1995 (rmott) */

#ifndef _SW_H_
#define _SW_H_

#include"array.h"
#include"seq_util.h"

typedef struct
{
  int start1, start2, end1, end2, score, matches;
}
Seg;

typedef struct
{
  int score, len;
  int end1, end2;
  int start1, start2;
  int segs;
  Seg *seg;
}
alignStruct;

typedef struct 
{
  int contig;
  int read;
  int strand;
  int dscore;
  int score;
  int c1, c2;
  int r1, r2;
  int len1, len2;
  char *s1, *s2;
  int offset;
  alignStruct *as;
}
sw_struct;

typedef struct 
{
  int contig;
  char *s;
  char *padded;
  Array mapping;
  Array SW;
  Array reAlign;
}
Collation;

typedef enum { path_end, path_left, path_up, path_diagonal } path_directions;

alignStruct *smith_waterman_alignment2( char *s1, char *s2, int len1, int len2, int match, int mismatch, int gap, int neutral, int offset, int window );
int equivalent_bases( int c1, int c2, int match, int mismatch, int gap );
void mat_init(int match, int mismatch, int gap, int neutral, char padding_char );
void print_alignment( FILE *fp, char *s1, char *s2, alignStruct *as, int width, char *label1, char *label2 );

Array collateContigs( Array SW );
Array findPads( Array alignments );
char *paddedConsensus( char *consensus, Array pad, char padchar );
alignStruct *reAlign2( char *paddedConsensus, Array mapping, sw_struct *sw, int match, int mismatch, int gap, int window );
Array reAlign( Collation *coll, int match, int mismatch, int gap, int window );

alignStruct *bandedAlignment( SEQUENCE *seq1, SEQUENCE *seq2, int match, int mismatch, int gap, int neutral, int window );

#endif
