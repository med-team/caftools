/*  Last edited: Jun 13 18:12 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Sep 20 15:21 1996 (rmott) */
/* $Id: trace_align.c 20764 2007-05-30 10:31:59Z rmd $ */

#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <string.h>

#include <myscf.h>
#include <trace_align.h>
#include <bases.h>
#include <cl.h>
#include <seq_util.h>
#include <pair_memory.h>

#if defined(__sgi) || defined(LINUX) 
double nint(double x);
double nint(double x)
{
  return (x > 0)?floor(x + 0.5):floor(x - 0.5);
}
#endif

#ifdef DEBUG
#undef DEBUG 
#endif
void ps_intro( FILE *fp, int pages );


void
eval_hull( int samples, int **data, int **hull, int **left_hull, int **right_hull, int window );

void
global_convex_hull( int samples, int *data, int window, int *hull, int *left, int *right, int maximise );

ReScore *trace_align( Scf *scf,	/* scf struct to align */
		     int trace_start, /* start coord in trace */
		     int trace_end, /* end coord in trace */
		     char *seq,	/* sequence to align */
		     int seq_start, /* start coord in seq */
		     int seq_end, /* end coord in seq */
		     t_params *params, /* alignment params */
		     int rescore, /* rescore the alignment and return a pointer to a ReScore struct */
		     int *trace_coord, /* pre-allocated array returning the trace coordinates at which state transitions occur */
		     int *align_score, /* pre-allocated array returning the score for each aligned base */
		     int *total_score, /* the score for the alignment */
		     int *base_start, /* the start of the alignment in the seq */
		     int *base_end, /* the end of the alignment in the seq */
		     int *t_start, /* the start of the alignment in the trace */
		     int *t_end ) /* the end of the alignment in the trace */
  
  /* NOTE ON COORDINATE SYSTEM:
     Bases are numbered from position 0. 
     
     The active region of the sequence is [seq_start,seq_end]
     
     The arrays seq, trace_coord, w_score MUST appear to be allocated from 0
     to at least seq_end, since the data are written back assuming this
     (you can fake it by allocating a block P of size seq_end-seq_start+1
      and then passing in a pointer to P-seq_start)
      
      */
  
{
  /* preprocess the data */

  int **trace_weight, **local_min, **left_min, **right_min;
  int **sample_data;
  int samples;
  int bases;
  char *base;
  int *max_trace, *max_trace2, floor_level;
  Header *h = &scf->header;
  int i, c, n, b;
  ReScore *re_score=NULL;

  int min=params->minstate;	/* min no of trace coords spent in each state */
  int search_mode=params->mode;	/* either SMITH_WATERMAN or NEEDLEMAN */
  int samples_per_base=params->samples_per_base; /* window for banded alignement */
  float floor=params->floor;	/* number between 0 and 1 subtracted from traces */

  if( trace_start < 0 || trace_end < 0 || trace_end < trace_start || trace_end >= h->samples )
    {
      trace_start = 0;
      trace_end = h->samples-1;
    }
  samples = trace_end-trace_start+1;

  if( seq_start < 0 || seq_end < 0 || seq_end < seq_start  )
    {
      seq_start = 0;
      seq_end = h->bases-1;
    }
  bases = seq_end-seq_start+1;


  /*  printf ("rescore %d\n", rescore ); */

  /* arrays for local min info */

  trace_weight = alloc_sample_matrix(samples);
  local_min = alloc_sample_matrix(samples);
  left_min = alloc_sample_matrix(samples);
  right_min = alloc_sample_matrix(samples);

  /* copy the scf data into something easier to handle */

  sample_data = make_sample_data( scf, trace_start, samples );

  max_trace = sample_data[base_n]; /* note aliasing */
  max_trace2 = (int*)calloc(samples,sizeof(int));


  base = (char*)calloc(bases+2,sizeof(char))+1;

  /* create numeric codes for bases. 
     NOTE anything not a,c,g,t is coded as n */

  base[-1] = base_n;
  for(i=0;i<bases;i++)
    {
      base[i] = i_base(seq[seq_start+i]);
      if ( base[i] > base_n )
	base[i] = base_n;
    }

  /* compute the envelope of traces. Note that max_trace = sample_data[base_n] */

  trace_envelope( samples, sample_data, max_trace, max_trace2 );

  
  /* subtract 90% (or whatever floor is set to) of the max trace from each
     trace to make the weights mostly negative, for Smith-Waterman style alignment */

  for(c=0;c<samples;c++)
    {
      
      floor_level = floor*max_trace[c]; 
      
      for(i=base_a;i<=base_t;i++)
	trace_weight[i][c] =  (int)sample_data[i][c] - floor_level;
      
      trace_weight[base_n][c] = 0;
    } 

  /* create the data on local minima, shoulders etc */

  eval_local_minima( samples, sample_data, local_min, left_min, right_min );

  /*  eval_hull( samples, sample_data, local_min, left_min, right_min, 10 );  */
	  
  /* do the alignment */

  if ( params->gap_penalty <= 0 )
    {
      trace_align2( samples, trace_weight, local_min, bases, base, min, search_mode, samples_per_base, &trace_coord[seq_start], &align_score[seq_start], total_score, base_start, base_end, t_start, t_end );
    }
  else				/* gapped alignment */
    {
      gapped_trace_align( samples, trace_weight, local_min, bases, base, min, samples_per_base, params->gap_penalty, &trace_coord[seq_start], &align_score[seq_start], total_score, base_start, base_end, t_start, t_end );
    }

  /*  printf("! score: %d base_start %d base_end %d\n", *total_score, *base_start, *base_end ); */

  *base_start += seq_start;
  *base_end += seq_start;

  if ( rescore )		/* create and fill a ReScore structure describing the alignment in detail */
    {
      int bb;

      /* rescore the alignment */
      re_score = allocReScore( *base_start, *base_end );

      /*      for(c=0;c<samples;c++)
	      printf("%5d %d\n", c, local_min[base_n][c] ); */
      
      /* compute the max-possible scorings */

      for(b=*base_start;b<*base_end;b++)
	{
	  /*      printf("%5d %c\n", b, seq[b]); */
	  bb = b - seq_start;
	  n = base[bb];
	  for(c=trace_coord[b]+1;c<=trace_coord[b+1];c++)
	    {
	      re_score->max[b] += max_trace[c];	/* max possible score */
	      re_score->score[b] += sample_data[n][c]; /* -max_trace2[c];  score for aligned base - height of second-max*/
	    }
	  re_score->right[b] = left_min[base_n][trace_coord[b+1]]; /* right-hand side of peak */
	  re_score->left[b] = right_min[base_n][trace_coord[b]]; /* left-hand side of peak */
	  re_score->width[b] = trace_coord[b+1]-trace_coord[b]; /* width of aligned base */
	  for(c=trace_coord[b]+2;c<trace_coord[b+1]-1;c++) /* note we ignore internal local minima if they are 2 pixels from the edge */
	    {
	      re_score->internal[b] += local_min[base_n][c]; /* internal derivative info - the sum of all local min values inside for the maximum base - possibly CHANGE THIS!!!! */
	    }
	  /*	  if ( trace_coord[b] > 0 )
		  {
		  printf("%5d %c ",b , seq[b] );
		  re_score->internal[b] = convex_hull( sample_data[base_n], trace_coord[b]+2 , trace_coord[b+1]-2 );
		  } */

	}
    }

  /* translate the trace coordinates back into the input space */

  for(i=*base_start;i<=*base_end;i++)
    trace_coord[i] += trace_start;

  *t_start += trace_start;
  *t_end += trace_start;

  free_sample_matrix( sample_data );
  free_sample_matrix( left_min );
  free_sample_matrix( right_min );
  free_sample_matrix( local_min );
  free_sample_matrix( trace_weight );

  free(base-1);
  free(max_trace2);

  return re_score;
}

void
eval_local_minima( int samples, int **data, int **info, int **left_side, int **right_side )
{
  
  /* Determine and score local "minima" 
     
     Criterion for a local minimum is:
     
     (a) true local minimum:   f'[c-1] < 0 && f'[c+1] > 0
     
     (b) left shoulder:        f'[c] >= 0 && f'[c-1] > f'[c] && f'[c+1] > f'[c]
     
     (c) right shoulder        f'[c] <= 0 && f'[c-1] < f'[c] && f'[c+1] < f'[c]
     
     score at coord c is 
     
     
     max { |f'(x) - f'(y)| }
     x <= c
     y >= c
     c-x < window
     y-c < window
     
     where window is 5 samples
     
     */


  int i, c, n, x, dc, dleft, dright, max;
  int *d = (int*)calloc(samples,sizeof(int));
  int window = 5;

  for(i=base_a;i<=base_n;i++)
    for(c=0;c<samples;c++)
      info[i][c] = left_side[i][c] = right_side[i][c] = 0;

  for(i=base_a;i<=base_n;i++)
    {
      /* calculate the derivative */

      for(c=1;c<samples;c++)
	d[c] = data[i][c]-data[i][c-1];

      for(c=window;c<samples-window;c++)
	{
	  dc = dleft = dright = 0;
	  if (
	      ( d[c-1] < 0 && d[c+1] > 0 ) || /* local minimum */
	      ( (d[c] >= 0) && (d[c-1] >= d[c]) && (d[c+1] >= d[c] ) ) || /* left shoulder */
	      ( (d[c] <= 0) && (d[c-1] <= d[c]) && (d[c+1] <= d[c] ) ) /* right shoulder */
	      )		
	    { 

	      /* estimate the derivative at c. 
		 This can be d[c] or 0 if at a local minimum */

	      if ( d[c-1] < 0 && d[c+1] > 0 )
		dc = 0;
	      else
		dc = d[c];


	      /* left side */
	      /* move through zero derivative region flanking c, if it exists */
	      
	      x = c-1;
	      while( (d[x] == 0) && (x > c-window ) )
		x--;
	      dleft = d[x];
	      
	      /* move left along trace so long as:
		 
		 (a) sign of derivative does not change
		 (b) absolute value of derivative increases
		 (c) inside window 
		 
		 */

	      while( (x > c-window) && (dleft*d[x] > 0 ) && (abs(d[x]) >= abs(d[x+1]) ) )
		x--;
		    
	      left_side[i][c] = abs(d[x+1]-dc);

	      /* ditto for right side */
	      /* move through zero derivative region flanking c, if it exists */
	      
	      x = c+1;
	      while( (d[x] == 0) && (x < c+window ) )
		x++;
	      dright = d[x];
	      
	      while( (x < c+window) && (dright*d[x] > 0 ) && (abs(d[x]) >= abs(d[x-1]) ) )
		x++;
		    
	      right_side[i][c] = abs(d[x-1]-dc);

	      if (dleft > 0 && dright < 0 ) /* error ! we have found a flat-topped local max, so set to 0 */
		left_side[i][c] = right_side[i][c] = 0;

	      if ( left_side[i][c] > 0 && right_side[i][c] > 0 )
		info[i][c] = left_side[i][c] + right_side[i][c];
	    }
	}	
      /* reduce  runs of non-zero values to their max */

      for(c=window;c<samples-window;c++)
        {
	  if ( info[i][c] > 0 )
	    {
	      x = c;
	      max = 0;
	      while( x < samples && info[i][x] > 0 ) 
		{
		  if ( info[i][x] > max )
		    max = info[i][x];
		  x++;
		}
	      x = c;
	      n = 0;
	      while( x < samples && info[i][x] > 0 ) 
		{
		  if ( info[i][x] == max )
		    {
		      if ( n > 0 )
			info[i][x] = left_side[i][x] = right_side[i][x] = 0;
		      n++;
		    }
		  else if ( info[i][x] < max ) 
		    info[i][x] = left_side[i][x] = right_side[i][x] = 0;
		  x++;
		}
	    }
	}
    }

  /*  for(c=1600;c<1700;c++)
      printf("%5d %5d   %5d %5d %5d\n", c, data[base_g][c], info[base_g][c], left_side[base_g][c], right_side[base_g][c] );
      */

  /* the data for base_n 
     
     for(c=window;c<samples-window;c++)
     {
     info[base_n][c] = 0;
     for(i=base_a;i<=base_t;i++)
     if ( info[base_n][c] < info[i][c] )
     info[base_n][c] = info[i][c];
     }
     */

  free(d);
}

void trace_align2( int samples, 
		  int **trace_weight, 
		  int **local_min,
		  int bases, char *base,
		  int minimum,
		  int search_mode,
		  int samples_per_base,
		  int *trace_coord,
		  int *align_score,
		  int *total_score,
		  int *base_start,
		  int *base_end,
		  int *trace_start,
		  int *trace_end )
{
  /*
     
     samples = size of trace_X arrays
     trace_weight[A,C,G,T][i] = array of weights for each base at trace coordinate i;
     transition[i] = weight for making a transition at trace coordinate i;
     
     bases = size of base array (# of bases)
     base = array of bases called [     indexed as 0,1,2,3,(4=N) ]
     
     minimum = bound on allowed length of time before changing state.
     search_mode = SMITH_WATERMAN best local similarity
     
     search_mode = NEEDLEMAN global similarity, in the sense that it
     stretches from the end of the sequence (not the trace)
     
     trace_coord = array giving coords for transitions (pre-allocated, of size bases+1) 
     w_score = array giving score for each base (pre-allocated)
     
     samples_per_base is the expected max no. of samples per base, in order to do a banded alignment
     Set this to 0 for a full alignment
     
     backtracking is performed via a path[b][c] matrix which is set_bit-packed (1 bit per cell)
     this reduces memory by a factor of 8
     note paths can only go left or diagonal not up.
     */

  unsigned char **path, *P;
  int b, c, m, i;
  int *score1, *score2, *s1, *s2, *s3;
  int change_state, no_change, max_c = 0, max_b = 0, max_score=INT_MIN;
  int score, last, lastc, tscore, tlast;
  int *w;
  int *wc, **cum=NULL;
  int samples8;
  int bit[8] = {1, 2, 4, 8, 16, 32, 64, 128 }; /* for bit packing */
  int pat;
  int previous_state, current_state;
  int *loc_min;
  int win_start, win_end;
  float scale;
  int cells = 0;
  int window_band;

  /* the window band half-width */

  window_band  = bases*samples_per_base;

#ifdef DEBUG
  /* store the scores to check backtracking */  
  int **temp;
  int tdiff, t_last;
#endif

  path = (unsigned char**)calloc(bases+1,sizeof(unsigned char*));
  path++;

  samples8 = 2+samples/8;	/* for bit packing */

  for(b=-1;b<bases;b++)
    {
      path[b] = (unsigned char*)calloc(samples8,sizeof(unsigned char));
      path[b]++;
    }


  score1 = (int*)calloc(samples+1,sizeof(int));
  score2 = (int*)calloc(samples+1,sizeof(int));

  s1 = score1+1;
  s2 = score2+1;

#ifdef DEBUG

  temp = (int**)calloc(bases+2,sizeof(int*))+1;
  for(b=-1;b<bases;b++)
    temp[b] = (int*)calloc(samples+2,sizeof(int))+1;
#endif

  cum = (int**)calloc(5,sizeof(int*));
  for(i=0;i<5;i++)
    cum[i] = (int*)calloc(samples+1,sizeof(int))+1;
  
  cum[base_a][0] = trace_weight[base_a][0];
  cum[base_c][0] = trace_weight[base_c][0];
  cum[base_g][0] = trace_weight[base_g][0];
  cum[base_t][0] = trace_weight[base_t][0];
  cum[base_n][0] = trace_weight[base_n][0];
  
  for(c=1;c<samples;c++)
    {
      cum[base_a][c] = cum[base_a][c-1] + trace_weight[base_a][c];
      cum[base_c][c] = cum[base_c][c-1] + trace_weight[base_c][c];
      cum[base_g][c] = cum[base_g][c-1] + trace_weight[base_g][c];
      cum[base_t][c] = cum[base_t][c-1] + trace_weight[base_t][c];
      cum[base_n][c] = cum[base_n][c-1] + trace_weight[base_n][c];
    }

  scale = (samples/(bases+1.0e-5));

  previous_state = base_n;
  for(b=0;b<bases;b++)
    {
      s3 = s1;
      s1 = s2;
      s2 = s3;
      P = path[b];

      current_state = base[b];
      w = trace_weight[(int) base[b]];
      wc = cum[previous_state];


      if ( previous_state == current_state ) /* the state transition penalty */
	loc_min = local_min[(int) base[b]];
      else
	loc_min = local_min[base_n];


      if ( window_band > 0 )
	{
	  win_start = b*scale - window_band;
	  if ( win_start < 0 )
	    win_start = 0;
	  win_end = b*scale + window_band;
	  if ( win_end > samples-1)
	    win_end = samples-1;
	}
      else
	{
	  win_start = 0;
	  win_end = samples-1;
	}

      if ( search_mode == SMITH_WATERMAN )
	{

	  for(c=win_start;c<minimum;c++)
	    {
	      no_change = s1[c-1] + w[c] - loc_min[c];
	      change_state = s2[0] + w[c] + wc[c-1] + loc_min[c]  ;

	      if ( (no_change > change_state) && (no_change > 0 ) ) /* left */
		{
		  s1[c] = no_change;
		  if ( s1[c] > max_score )
		    {
		      max_score = s1[c];
		      max_c = c;
		      max_b = b;
		    }
		}
	      else if ( change_state < 0 ) 
		{
		  s1[c] = 0;
		}
	      else		/* diagonal */
		{
		  P[c/8] |= bit[c%8]; /* bit packing */
		  s1[c] = change_state;
		  if ( s1[c] > max_score )
		    {
		      max_score = s1[c];
		      max_c = c;
		      max_b = b;
		    }
		}

	    }
	  
	  if ( win_start < minimum )
	    win_start = minimum;

	  for(c=win_start;c<=win_end;c++)
	    {
	      cells++;
	      no_change = s1[c-1] + w[c] - loc_min[c];
	      if ( no_change < 0 )
		no_change = 0;
	      change_state = s2[c-minimum] + w[c] + wc[c-1]-wc[c-minimum]  + loc_min[c] ;
	      if ( change_state < 0 )
		change_state = 0;
	      if (  (no_change > change_state)  ) /* left */
		{
		  s1[c] = no_change;
		  if ( s1[c] > max_score )
		    {
		      max_score = s1[c];
		      max_c = c;
		      max_b = b;
		    }
		}
	      else if ( change_state == 0 ) 
		{
		  s1[c] = 0;
		}
	      else		/* diagonal */
		{
		  P[c/8] |= bit[c%8]; /* bit packing */
		  s1[c] = change_state;
		  if ( s1[c] > max_score )
		    {
		      max_score = s1[c];
		      max_c = c;
		      max_b = b;
		    }
		}
	    }
	}
      else
	{
	  for(c=0;c<minimum;c++)
	    {
	      no_change = s1[c-1] - loc_min[c];
	      change_state = s2[0] + wc[c-1]  + loc_min[c] ;

	      if ( change_state < no_change )
		{
		  s1[c] = no_change + w[c];
		}
	      else
		{
		  P[c/8] |= bit[c%8];
		  s1[c] = change_state + w[c];
		}

	    }
	  
	  for(c=minimum;c<samples;c++)
	    {
	      no_change = s1[c-1]  -loc_min[c];
	      change_state = s2[c-minimum] + wc[c-1]-wc[c-minimum] + loc_min[c];

	      if ( change_state < no_change )
		{
		  s1[c] = no_change + w[c];
		}
	      else
		{
		  P[c/8] |= bit[c%8];
		  s1[c] = change_state + w[c];
		}
 
	    }
	}  
      previous_state = current_state;

#ifdef DEBUG
      for(c=-1;c<samples;c++)
	temp[b][c] = s1[c];
#endif
    }

  if ( search_mode != SMITH_WATERMAN )
    {
      max_score = s1[0];
      max_c = 0;
      max_b = bases-1;
      for(c=0;c<samples;c++)
	{
	  if ( s1[c] > max_score )
	    {
	      max_score = s1[c];
	      max_c = c;
	    }
	}
    }

  /*  printf("!max score: %d minimum %d max_c: %d max_b: %d bases: %d mode:%d cells: %d window_band: %d\n", max_score, minimum, max_c, max_b, bases, search_mode, cells, window_band );    */

  /* back-tracking phase */

  *total_score = max_score;
  *base_end = max_b;
  *trace_end = max_c;

  for(b=0;b<bases;b++)
    trace_coord[b] = 0;

  trace_coord[max_b] = max_c;
  b = max_b;
  c = max_c;
  score = 0;
  tscore = 0;
  last = 0;
  tlast = 0;
  lastc = c;


#ifdef DEBUG
  t_last = max_score;
#endif

  while( b >= 0 && c >= 0 && ( search_mode == NEEDLEMAN || score <= max_score ) )
    {
      pat = bit[c%8] & path[b][c/8];

      if ( pat > 0 )
	{
#ifdef DEBUG
	  tdiff = t_last-temp[b][c];
	  t_last = temp[b][c];
	  printf("%5d %5d %5d %c %5d %5d %5d    %5d %5d %c %d\n", b, c, lastc-c, c_base(base[b]), score+tscore, tscore-tlast, score-last, temp[b][c], tdiff, ( (tdiff == score-last + tscore-tlast ) ? ' ': '*'), trace_weight[base[b]][c] );    
#endif

	  align_score[b] = score-last;
	  last = score;
	  tlast = tscore;
	  lastc = c;


	  if ( base[b]==base[b-1] )
	    score +=  local_min[(int) base[b]][c];
	  else
	    score +=  local_min[base_n][c];

	  score += trace_weight[(int) base[b]][c]; 

	  trace_coord[b] = c;

	  if ( ! minimum )
	    {
	      b--;
	      c--;
	    }
	  else
	    {
	      b--;
	      if ( minimum <= c )
		m = minimum;
	      else
		m = c;
	      if ( b >= 0 &&  base[b] <= base_n )
		score += cum[(int) base[b]][c-1]-cum[(int) base[b]][c-m];
	      else if ( b >= 0 )
		score += cum[base_n][c-1]-cum[base_n][c-m];
	      c-=m;
	    }
	}
      else if ( pat == 0 )
	{
	  score += trace_weight[(int) base[b]][c]; 
	  c--;
	}
      /*      else
	      break; */
    }
  /*  printf("%5d %5d %5d %c %5d %5d \n", b, c, lastc-c,  c_base(base[b]), score, score-last);    */

  if ( b < 0 )
    b = 0;
  *base_start = b;
  if ( c < 0 )
    c = 0;
  *trace_start = c;

	
     
  free(score1);
  free(score2);

  for(b=-1;b<bases;b++)
    {
      free(path[b]-1);
    }

  free(path-1);

  for(i=0;i<5;i++)
    free(cum[i]-1);
  free(cum);
}


void 
gapped_trace_align( int samples, 
			int **trace_weight, 
			int **local_min,
			int bases, char *base,
			int minimum,
			int window_band,
			int gap_penalty,
			int *trace_coord,
			int *align_score,
			int *total_score,
			int *base_start,
			int *base_end,
			int *trace_start,
			int *trace_end )
{
  /*
     
     samples = size of trace_X arrays
     trace_weight[A,C,G,T][i] = array of weights for each base at trace coordinate i;
     transition[i] = weight for making a transition at trace coordinate i;
     
     bases = size of base array (# of bases)
     base = array of bases called [     indexed as 0,1,2,3,(4=N) ]
     
     minimum = bound on allowed length of time before changing state.
     search_mode = SMITH_WATERMAN best local similarity
     
     trace_coord = array giving coords for transitions (pre-allocated, of size bases+1) 
     align_score = array giving score for each base (pre-allocated)
     
     
     backtracking is performed via a path[b][c] matrix which is set_bit-packed (2 bits per cell)
     this reduces memory by a factor of 4
     note paths can go left or diagonal or up.
     */

  unsigned char **path, *P;
  int b, c, m = 0, i;
  int *score1, *score2, *s1, *s2, *s3;
  int change_state, no_change, max_c = 0, max_b = 0, max_score=INT_MIN;
  int score, last, lastc, in_gap;
  int *w;
  int *wc, **cum=NULL;
  int samples8;
  int dbit[4] = { 1, 4, 16, 64 }; /* for bit packing */
  int vbit[4] = { 2, 8, 32, 128 };
  int hbit[4] = { 3, 12, 48, 192 };
  int previous_state, current_state, vertical_gap;
  int *loc_min;
  int *best_col, *best_base;	/* for vertical gaps */

  path = (unsigned char**)calloc(bases+1,sizeof(unsigned char*));
  path++;

  samples8 = 2+samples/4;	/* for bit packing */

  for(b=-1;b<bases;b++)
    {
      path[b] = (unsigned char*)calloc(samples8,sizeof(unsigned char));
      path[b]++;
    }


  score1 = (int*)calloc(samples+1,sizeof(int));
  score2 = (int*)calloc(samples+1,sizeof(int));

  s1 = score1+1;
  s2 = score2+1;

  best_col = (int*)calloc(samples+1,sizeof(int));
  best_base = (int*)calloc(samples+1,sizeof(int));

  cum = (int**)calloc(5,sizeof(int*));
  for(i=0;i<5;i++)
    {
      cum[i] = (int*)calloc(samples+1,sizeof(int))+1;
      cum[i][0] = trace_weight[i][0];
    }

  for(c=1;c<samples;c++)
    {
      for(i=0;i<5;i++)
	cum[i][c] = cum[i][c-1] + trace_weight[i][c];
    }

  previous_state = base_n;
  for(b=0;b<bases;b++)
    {
      s3 = s1;
      s1 = s2;
      s2 = s3;
      P = path[b];

      current_state = base[b];
      w = trace_weight[(int) base[b]];
      wc = cum[previous_state];


      if ( previous_state == current_state )
	loc_min = local_min[(int) base[b]];
      else
	loc_min = local_min[(int) base_n];

      for(c=0;c<minimum;c++)
	{
	  no_change = s1[c-1] + w[c] - loc_min[c];
	  change_state = s2[0] + w[c] + wc[c-1]  + loc_min[c];
	  vertical_gap = best_col[c] - gap_penalty + loc_min[c];
	  if ( vertical_gap > no_change && vertical_gap > change_state )
	    {
	      s1[c] = vertical_gap;
	      P[c/4] |= vbit[c%4];
	      if ( s1[c] > max_score )
		{
		  max_score = s1[c];
		  max_c = c;
		  max_b = b;
		}
	    }
	  else if ( (no_change > change_state) && (no_change > 0 ) ) /* left */
	    {
	      s1[c] = no_change;
	      P[c/4] |= hbit[c%4];
	      if ( s1[c] > max_score )
		{
		  max_score = s1[c];
		  max_c = c;
		  max_b = b;
		}
	    }
	  else if ( change_state < 0 ) /* terminate */
	    {
	      s1[c] = 0;
	    }
	  else			/* diagonal */
	    {
	      P[c/4] |= dbit[c%4]; /* 2-bit packing */
	      s1[c] = change_state;
	      if ( s1[c] > max_score )
		{
		  max_score = s1[c];
		  max_c = c;
		  max_b = b;
		}
	    }

	  if ( best_col[c] < s1[c] )
	    {
	      do_not_forget( c, b ); 
	      best_col[c] = s1[c];
	      best_base[c] = b;
	    }
	}
      
      for(c=minimum;c<samples;c++)
	{
	  no_change = s1[c-1] + w[c] - loc_min[c];
	  if ( no_change < 0 )
	    no_change = 0;
	  change_state = s2[c-minimum] + w[c] + wc[c-1]-wc[c-minimum]  + loc_min[c];
	  if ( change_state < 0 )
	    change_state = 0;

	  vertical_gap = best_col[c-minimum] - gap_penalty + w[c] + wc[c-1]-wc[c-minimum] +loc_min[c];

	  if ( vertical_gap > no_change && vertical_gap > change_state )
	    {
	      s1[c] = vertical_gap;
	      P[c/4] |= vbit[c%4];
	      if ( s1[c] > max_score )
		{
		  max_score = s1[c];
		  max_c = c;
		  max_b = b;
		}
	    }
	  else if (  (no_change > change_state)  ) /* left */
	    {
	      s1[c] = no_change;
	      P[c/4] |= hbit[c%4];
	      if ( s1[c] > max_score )
		{
		  max_score = s1[c];
		  max_c = c;
		  max_b = b;
		}
	    }
	  else if ( change_state == 0 ) 
	    {
	      s1[c] = 0;
	    }
	  else			/* diagonal */
	    {
	      P[c/4] |= dbit[c%4]; /* bit packing */
	      s1[c] = change_state;
	      if ( s1[c] > max_score )
		{
		  max_score = s1[c];
		  max_c = c;
		  max_b = b;
		}
	    }

	  if ( best_col[c] < s1[c] )
	    {
	      do_not_forget( c, b ); 
	      best_col[c] = s1[c];
	      best_base[c] = b;
	    }
	}
      previous_state = current_state;
    }

  
  printf("!max score: %d minimum %d max_c: %d max_b: %d bases: %d\n", max_score, minimum, max_c, max_b, bases);  

  *total_score = max_score;
  *base_end = max_b;
  *trace_end = max_c;

  for(b=0;b<bases;b++)
    trace_coord[b] = -1;

  trace_coord[max_b] = max_c;

  b = max_b;
  c = max_c;
  score = 0;
  last = 0;
  lastc = c;
  vertical_gap = 0;
  in_gap = 0;

  while( b >= 0 && c >= 0 ) 
    {
      if ( (path[b][c/4] & hbit[c%4])  == vbit[c%4] ) /* vertical*/
	{
	  int new_b;

	  align_score[b] = 0;
	  trace_coord[b] = c;
	  last = score;
	  lastc = c;
	  new_b = remember( c, b );
	  while( b >= new_b )
	    trace_coord[b--] = c;

	  b = new_b;
	  if ( ! in_gap )
	    vertical_gap = -gap_penalty + cum[(int) base[b]][c-1]-cum[(int) base[b]][c-m] ;
	  in_gap = 1;
	}
      else if ( (path[b][c/4] & hbit[c%4]) == dbit[c%4] ) /* diagonal */
	{
	  score += trace_weight[(int) base[b]][c] + vertical_gap; 
	  vertical_gap = 0;
	  in_gap = 0;
	  align_score[b] = score-last;
	  last = score;
	  lastc = c;
	  
	  if ( base[b] == base[b-1] ) 
	    score += local_min[base_n][c];
	  else
	    score += local_min[(int) base[b]][c];

	  vertical_gap = 0;
	  trace_coord[b] = c;
	  if ( ! minimum )
	    {
	      b--;
	      c--;
	    }
	  else
	    {
	      b--;
	      if ( minimum <= c )
		m = minimum;
	      else
		m = c;
	      if ( b >= 0 )
		score += cum[(int) base[b]][c-1]-cum[(int) base[b]][c-m];
	      
	      c-=m;
	    }
	}
      else if ( (path[b][c/4] & hbit[c%4]) == hbit[c%4] ) /* horizontal */
	{
	  score += trace_weight[(int) base[b]][c] + vertical_gap  ; 
	  if ( base[b] == base[b-1] ) 
	    score -= local_min[(int) base[b]][c];
	  else
	    score -= local_min[base_n][c];

	  vertical_gap = 0;
	  in_gap = 0;
	  c--;
	}
      else			/* stop */
	{
	  break;
	}
    }

  printf("backtrack score %d\n", score );

  *base_start = b;
  *trace_start = c;

  free(score1);
  free(score2);

  for(b=-1;b<bases;b++)
    {
      free(path[b]-1);
    }

  free(path-1);

  for(i=0;i<5;i++)
    free(cum[i]-1);
  free(cum);

  free_rpairs();		/* the pair memory */
}

void 
ps_print_alignment( 
		   FILE *fp, 
		   Scf *scf, 
		   int trace_start, 
		   int trace_end, 
		   int base_count, 
		   char *base, 
		   int base_start, 
		   int base_end, 
		   int *trace_coord, 
		   char *title )
{

  int pixels_per_line = 500;
  int lines_per_page = 4;
  int pages;
  int page;
  int line;
  int base_incr=10;
  int page_height = 800;
  int height = page_height/lines_per_page-20;
  int h = height-50;
  int trace_position = trace_start;
  Samples1 *s1;
  Samples2 *s2;
  int i, x;
  float scale=(scf->header.sample_size == 2) ? (h / 65536.0) : (h/256.0);
  int *trace_coord2=NULL;
  float p;

  if (trace_coord)
    {
      trace_coord2 = (int*)calloc(trace_end+1,sizeof(int));
      for(i=base_start;i<=base_end;i++)
	{
	  trace_coord2[trace_coord[i]] = 1;
	}
    }

  p = (trace_end-trace_start)/(1.0+pixels_per_line*lines_per_page);
  if ( (float)((int)p) < p )
    pages = p+1;
  else
    pages =p;

  ps_intro(fp,pages);

  for(page=1;page<=pages;page++)
    {
      fprintf(fp, "/Helvetica-Bold findfont 14 scalefont setfont 0 setgray\n");
      fprintf(fp, "%%%%Page: %d %d\n%%%%BeginPageSetup\n%%%%EndPageSetup\nnewpath\n",page,page);
      fprintf(fp, "%5d %5d translate\n", 70, page_height-height);
      fprintf(fp, "%5d %5d m (%s     page %d/%d) sh\n", 0, 150, (title ? title : ""), page, pages );
      fprintf(fp, "/Helvetica-Bold findfont 10 scalefont setfont\n");

      for(line=0;line<lines_per_page;line++)
	{
	  fprintf(fp,"\n%% line %d\n", line+1);
	  if ( scf->header.sample_size == 2 )	/* 16-bit data */
	    {
	      s2 = &scf->samples.samples2[trace_position];
	      fprintf(fp,"gs adenine 0 %g m\n", s2->sample_A*scale );
	      for(i=0;
		  i<pixels_per_line && trace_position+i <= trace_end; 
		  i++, s2++)
		fprintf(fp, "%d %g l\n", i, s2->sample_A*scale);
	      fprintf(fp,"s gr\n");

	      s2 = &scf->samples.samples2[trace_position];
	      fprintf(fp,"gs cytosine 0 %g m\n",  s2->sample_C*scale );
	      for(i=0;
		  i<pixels_per_line && trace_position+i <= trace_end;
		  i++, s2++)
		fprintf(fp, "%d %g l\n", i, s2->sample_C*scale);
	      fprintf(fp,"s gr\n");

	      s2 = &scf->samples.samples2[trace_position];
	      fprintf(fp,"gs guanine 0 %g m\n", s2->sample_G*scale );
	      for(i=0;
		  i<pixels_per_line && trace_position+i <= trace_end;
		  i++, s2++)
		fprintf(fp, "%d %g l\n", i, s2->sample_G*scale);
	      fprintf(fp,"s gr\n");

	      s2 = &scf->samples.samples2[trace_position];
	      fprintf(fp,"gs thyamine 0 %g m\n",  s2->sample_T*scale );
	      for(i=0;
		  i<pixels_per_line && trace_position+i <= trace_end;
		  i++,s2++)
		fprintf(fp, "%d %g l\n", i, s2->sample_T*scale);
	      fprintf(fp,"s gr\n");
	    }
	  else /* 8 bit data */
	    {
	      s1 = &scf->samples.samples1[trace_position];
	      fprintf(fp,"gs adenine 0 %g m\n", s1->sample_A*scale );
	      for(i=0;
		  i<pixels_per_line && trace_position+i <= trace_end; 
		  i++, s1++)
		fprintf(fp, "%d %g l\n", i, s1->sample_A*scale);
	      fprintf(fp,"s gr\n");

	      s1 = &scf->samples.samples1[trace_position];
	      fprintf(fp,"gs cytosine 0 %g m\n",  s1->sample_C*scale );
	      for(i=0;
		  i<pixels_per_line && trace_position+i <= trace_end;
		  i++, s1++)
		fprintf(fp, "%d %g l\n", i, s1->sample_C*scale);
	      fprintf(fp,"s gr\n");

	      s1 = &scf->samples.samples1[trace_position];
	      fprintf(fp,"gs guanine 0 %g m\n", s1->sample_G*scale );
	      for(i=0;
		  i<pixels_per_line && trace_position+i <= trace_end;
		  i++, s1++)
		fprintf(fp, "%d %g l\n", i, s1->sample_G*scale);
	      fprintf(fp,"s gr\n");

	      s1 = &scf->samples.samples1[trace_position];
	      fprintf(fp,"gs thyamine 0 %g m\n",  s1->sample_T*scale );
	      for(i=0;
		  i<pixels_per_line && trace_position+i <= trace_end;
		  i++,s1++)
		fprintf(fp, "%d %g l\n", i, s1->sample_T*scale);
	      fprintf(fp,"s gr\n");
	    }

	  if ( trace_coord )
	    {
	      /* s1 = &scf->samples.samples1[trace_position]; */
	      fprintf(fp,"gs unknown\n");
	      for(i=0;i<pixels_per_line && trace_position+i <= trace_end;i++/*,s1++*/)
		if( trace_coord2[i+trace_position] )
		  fprintf(fp, "%5d 0 m %5d %d l s\n", i, i, h);
	    }

	  if ( base && trace_coord  )
	    {
	      for(i=base_start;i<=base_end;i++)
		{
		  if ( trace_coord[i] && trace_coord[i+1] >= trace_position && trace_coord[i+1] < trace_position+pixels_per_line  )
		    {
		      if ( trace_coord[i+1] > trace_coord[i] )
			x = trace_coord[i+1]-trace_coord[i];
		      else
			x = 10;
		      fprintf( fp, "%d (%c) wid  %d add %d m (%c) sh\n", x, base[i], trace_coord[i]-trace_position, h, base[i] );
		      if ( !(i%base_incr) )
			{
			  x = trace_coord[i]-trace_position;
			  fprintf( fp, "gs black %d %d m (%d) sh gr\n", x, h-12,i );
			  fprintf( fp, "gs black %d %d m (%d) sh gr\n", x, -12,trace_coord[i] );
			}
		    }
		}
	    }

	  fprintf(fp,"0 %d translate\n", -height );
	  trace_position += pixels_per_line;
	}
      fprintf(fp,"\nshowpage\n");
    }
  fprintf(fp,"%%%%Trailer\n%%%%EOF\n");

  if ( trace_coord2 ) 
    free(trace_coord2);
}

void 
ps_print_multiple_alignment( 
			    FILE *fp, 
			    Scf *scf, 
			    int trace_start, 
			    int trace_end, 
			    int base_count, 
			    char *base, 
			    int base_start, 
			    int base_end,
			    int centre,
			    int *trace_coord, 
			    char *title,
			    char *linetitle,
			    int *newpage )
{
  int pixels_per_line = 400;
  int lines_per_page = 8;
  static int pages=0;
  int page=0;
  static int line=0;
  int base_incr=10;
  int page_height = 800;
  int height = page_height/lines_per_page-20;
  int h = height-50;
  int trace_position = trace_start;
  Samples1 *s1;
  Samples2 *s2;
  int i, x;
  float scale=(scf->header.sample_size == 2) ? (h / 65536.0) : (h/256.0);
  int *trace_coord2=NULL;
  float p;

  if (trace_coord)
    {
      trace_coord2 = (int*)calloc(trace_end+1,sizeof(int));
      for(i=base_start;i<=base_end;i++)
	{
	  trace_coord2[trace_coord[i]+trace_start] = 1;
	}
    }

  p = (trace_end-trace_start)/(1.0+pixels_per_line*lines_per_page);

  if ( pages == 0 )
    ps_intro(fp,0);

  if ( *newpage == 1 )
    {
      *newpage = 0;

      if ( page > 0 )
	fprintf(fp, "\nshowpage\n");

      page++;
      pages++;

      fprintf(fp, "/Helvetica-Bold findfont 14 scalefont setfont 0 setgray\n");
      fprintf(fp, "%%%%Page: %d %d\n%%%%BeginPageSetup\n%%%%EndPageSetup\nnewpath\n",page,page);
      fprintf(fp, "%5d %5d translate\n", 70, page_height-height);
      fprintf(fp, "%5d %5d m (%s     page %d) sh\n", 0, 150, (title ? title : ""), page);
      fprintf(fp, "/Helvetica-Bold findfont 10 scalefont setfont\n");

      line = 0;
    }

  trace_position = centre - 0.5*pixels_per_line;
  
  fprintf(fp,"\n%% line %d\n", line+1);
  if ( scf->header.sample_size == 2 )	/* 16-bit data */
    {
      s2 = &scf->samples.samples2[trace_position];
      fprintf(fp,"gs adenine 0 %g m\n", s2->sample_A*scale );
      for(i=0;
	  i<pixels_per_line && trace_position+i <= trace_end; 
	  i++, s2++)
	fprintf(fp, "%d %g l\n", i, s2->sample_A*scale);
      fprintf(fp,"s gr\n");
      
      s2 = &scf->samples.samples2[trace_position];
      fprintf(fp,"gs cytosine 0 %g m\n",  s2->sample_C*scale );
      for(i=0;
	  i<pixels_per_line && trace_position+i <= trace_end;
	  i++, s2++)
	fprintf(fp, "%d %g l\n", i, s2->sample_C*scale);
      fprintf(fp,"s gr\n");
      
      s2 = &scf->samples.samples2[trace_position];
      fprintf(fp,"gs guanine 0 %g m\n", s2->sample_G*scale );
      for(i=0;
	  i<pixels_per_line && trace_position+i <= trace_end;
	  i++, s2++)
	fprintf(fp, "%d %g l\n", i, s2->sample_G*scale);
      fprintf(fp,"s gr\n");
      
      s2 = &scf->samples.samples2[trace_position];
      fprintf(fp,"gs thyamine 0 %g m\n",  s2->sample_T*scale );
      for(i=0;
	  i<pixels_per_line && trace_position+i <= trace_end;
	  i++,s2++)
	fprintf(fp, "%d %g l\n", i, s2->sample_T*scale);
      fprintf(fp,"s gr\n");
    }
  else /* 8 bit data */
    {
      s1 = &scf->samples.samples1[trace_position];
      fprintf(fp,"gs adenine 0 %g m\n", s1->sample_A*scale );
      for(i=0;
	  i<pixels_per_line && trace_position+i <= trace_end; 
	  i++, s1++)
	fprintf(fp, "%d %g l\n", i, s1->sample_A*scale);
      fprintf(fp,"s gr\n");
      
      s1 = &scf->samples.samples1[trace_position];
      fprintf(fp,"gs cytosine 0 %g m\n",  s1->sample_C*scale );
      for(i=0;
	  i<pixels_per_line && trace_position+i <= trace_end;
	  i++, s1++)
	fprintf(fp, "%d %g l\n", i, s1->sample_C*scale);
      fprintf(fp,"s gr\n");
      
      s1 = &scf->samples.samples1[trace_position];
      fprintf(fp,"gs guanine 0 %g m\n", s1->sample_G*scale );
      for(i=0;
	  i<pixels_per_line && trace_position+i <= trace_end;
	  i++, s1++)
	fprintf(fp, "%d %g l\n", i, s1->sample_G*scale);
      fprintf(fp,"s gr\n");
      
      s1 = &scf->samples.samples1[trace_position];
      fprintf(fp,"gs thyamine 0 %g m\n",  s1->sample_T*scale );
      for(i=0;
	  i<pixels_per_line && trace_position+i <= trace_end;
	  i++,s1++)
	fprintf(fp, "%d %g l\n", i, s1->sample_T*scale);
      fprintf(fp,"s gr\n");
    }
  
  if ( trace_coord )
    {
      /* s1 = &scf->samples.samples1[trace_position]; */
      fprintf(fp,"gs unknown\n");
      for(i=0;i<pixels_per_line && trace_position+i <= trace_end;i++/*,s1++ */)
	if( trace_coord2[i+trace_position] )
	  fprintf(fp, "%5d 0 m %5d %d l s\n", i, i, h);
    }
  
  if ( base && trace_coord  )
    {
      for(i=base_start;i<=base_end;i++)
	{
	  if ( trace_coord[i] && trace_coord[i+1]+trace_start >= trace_position && trace_coord[i+1]+trace_start < trace_position+pixels_per_line  )
	    {
	      if ( trace_coord[i+1] > trace_coord[i] )
		x = trace_coord[i+1]-trace_coord[i];
	      else
		x = 10;
	      fprintf( fp, "%d (%c) wid  %d add %d m (%c) sh\n", x, base[i], trace_coord[i]+trace_start-trace_position, h, base[i] );
	      if ( !(i%base_incr) )
		{
		  x = trace_coord[i]+trace_start-trace_position;
		  fprintf( fp, "gs black %d %d m (%d) sh gr\n", x, h-12,i );
		  fprintf( fp, "gs black %d %d m (%d) sh gr\n", x, -12,trace_coord[i]+trace_start );
		}
	    }
	}
    }
  
  fprintf(fp,"0 %d translate\n", -height );

  if ( trace_coord2 ) 
    free(trace_coord2);
}

void 
ps_intro( FILE *fp, int pages )
{

  if ( pages )
    fprintf( fp, "%%!PS-Adobe-3.0\n%%%%Pages: %d\n%%%%EndComments\n%%%%BeginProlog\n", pages);
  else
    fprintf( fp, "%%!PS-Adobe-3.0\n%%%%Pages: (atend)\n%%%%EndComments\n%%%%BeginProlog\n");

  fprintf( fp, "/adenine  { 0 1 0 setrgbcolor } def\n");
  fprintf( fp, "/cytosine { 0 0 1 setrgbcolor } def\n");
  fprintf( fp, "/guanine  { 0 0 0 setrgbcolor } def\n");
  fprintf( fp, "/thyamine { 1 0 0 setrgbcolor } def\n");
  fprintf( fp, "/unknown  { 0.5 0.5 0.5 setrgbcolor } def\n");
  fprintf( fp, "/black    { 0 0 0 setrgbcolor } def\n");
  fprintf( fp, "/gs { gsave } def\n");
  fprintf( fp, "/gr { grestore } def\n");
  fprintf( fp, "/s  { stroke } def\n");
  fprintf( fp, "/sh { show } def\n");
  fprintf( fp, "/m { moveto } def\n");
  fprintf( fp, "/l { lineto } def\n");
  fprintf( fp, "/wid { stringwidth pop /w exch def w sub 2 div } def\n");
  fprintf( fp, "%%%%EndProlog\n%%%%BeginSetup\n");
  fprintf( fp, "%%%%EndSetup\n");
}

ReScore *
allocReScore( int base_start, int base_end)
{
  int len;
  ReScore *re_score;

  re_score = (ReScore*)calloc(1,sizeof(ReScore));
  re_score->b_start = base_start;
  re_score->b_end = base_end;

  len = base_end-base_start+1;

  re_score->score =    (int*)calloc(len,sizeof(int))-base_start;
  re_score->max =      (int*)calloc(len,sizeof(int))-base_start;
  re_score->left =     (int*)calloc(len,sizeof(int))-base_start;
  re_score->right =    (int*)calloc(len,sizeof(int))-base_start;
  re_score->internal = (int*)calloc(len,sizeof(int))-base_start;
  re_score->width =    (int*)calloc(len,sizeof(int))-base_start;

  return re_score;
}

void
free_ReScore( ReScore *reScore )
{
  if ( reScore )
    {
      free( reScore->internal+reScore->b_start );
      free( reScore->score+   reScore->b_start);
      free( reScore->max+     reScore->b_start );
      free( reScore->left+    reScore->b_start );
      free( reScore->right+   reScore->b_start );
      free( reScore->width+   reScore->b_start );
      free( reScore );
    }
}



TRACE_ALIGN_DATA *
trace_align_create( int len )
{
  TRACE_ALIGN_DATA *t = (TRACE_ALIGN_DATA*)calloc(1,sizeof(TRACE_ALIGN_DATA));

  t->coord = (int*)calloc(len,sizeof(int));
  t->score = (int*)calloc(len,sizeof(int));

  return t;
}

void
trace_align_free( TRACE_ALIGN_DATA *t )
{
  if ( t )
    {
      free(t->coord);
      free(t->score);
      free(t);
    }
}


int 
trace_quality_clip( ReScore *reScore, int *left, int *right, int threshold )
{
  int *cum, *min;
  int i, m, n;
  int total;
  int bstart = reScore->b_start, bend = reScore->b_end;

  cum = (int*)calloc((bend+1),sizeof(int)); 
  min = (int*)calloc((bend+1),sizeof(int)); 


  for(i=bstart;i<bend;i++)
    {
      if ( reScore->left[i] < reScore->right[i] )
	cum[i] = ( reScore->left[i] < reScore->right[i] ? reScore->left[i] : reScore->right[i] ) - threshold;
      if ( i > bstart )
	cum[i] += cum[i-1];
    }
  total = cum[bend-1];

  min[bend] = bend;
  for(i=bend-1;i>=bstart;i--)
    {
      n = total - cum[i];
      m = total - cum[min[i+1]];

      if ( n < m )
	min[i] = i;
      else
	min[i] = min[i+1];
    }

  m = 100000;
  for(i=bstart+1;i<bend;i++)
    {
      n = cum[i-1] + total - cum[min[i]];
      if ( n < m )
	{
	  m = n;
	  *left = i;
	  *right = min[i];
	}
    }

  free(cum);
  free(min);

  return *right-*left+1;
}

void
get_trace_align_params( t_params *params, int argc, char **argv )
{
  char mode[256];

  getint("-window",&params->samples_per_base,argc,argv);
  getint("-min",&params->minstate,argc,argv);
  getfloat("-floor",&params->floor,argc,argv);
  getfloat("-t_weight",&params->deriv_weight,argc,argv);
  getint("-trace_gap", &params->gap_penalty,argc,argv);

  strcpy(mode,"smith_waterman");
  if ( getarg("-mode", mode, argc,argv ) )
    legal_string( mode, align_modes, ALIGN_MODES, &params->mode );

}

int 
convex_hull( int *data, int start, int stop )
  
  /* returns the difference between the convex hull of the data and the sum of the data values */
  
{
  int i, j;
  int h, W, width = stop-start+1;
  double increment, y;
  int *hull;
  int *d;
  int diff=0, d1, n;
    
  printf("in hull %d %d ", start, stop );

  /* remove concave flanks */

  d1 = data[start+1]-data[start];
  i = start+1;
  while( i < stop && data[i+1]-data[i] >= d1 )
    {
      d1 = data[i+1]-data[i];
      i++;
    }

  start = i-1;

  d1 = data[stop]-data[stop-1];
  i = stop-1;
  while( i > start && data[i]-data[i-1] <= d1 )
    {
      d1 = data[i]-data[i-1];
      i--;
    }
  
  stop = i+1;
  
  printf("in hull %d %d\n", start, stop );
  
  if ( width >= 3 )
    {
      hull = (int*)calloc( width, sizeof(int));

      d = &data[start];
      for(h=0;h<width;h++)
	hull[h] = d[h];

      for(i=0;i<width;i++)
	{
	  for(j=i+2;j<width;j++)
	    {
	      W = j-i;
	      y = d[i];
	      increment = (d[j]-d[i])/(double)W;
	      for(h=i+1;h<i+W;h++)
		{
		  y += increment;
		  n = (int)nint(y);
		  if ( n > hull[h] )
		    hull[h] = n;
		}
	    }
	  for(h=0;h<width;h++)
	    {
	      printf("%5d %5d\n", hull[h], d[h] );
	      diff += hull[h]-d[h];
	    }
	  free(hull);
	}
    }
  return diff;
}
void
eval_hull( int samples, int **data, int **hull, int **left_hull, int **right_hull, int window )
{

  int base;

  for(base=base_a;base<=base_n;base++)
    global_convex_hull( samples, data[base], window, hull[base], left_hull[base], right_hull[base], 1 );

  { 
    int c;
    for(c=0;c<samples;c++)
      {
	printf("%5d ", c );
	for(base=base_a;base<=base_n;base++)
	  printf("   %3d %3d %3d %3d", data[base][c], hull[base][c], left_hull[base][c], right_hull[base][c] );
	printf("\n");
      }
  }
}

void
global_convex_hull( int samples, int *data, int window, int *hull, int *left, int *right, int maximise )
{
  /* 
     Fills hull[] with the pseudo-convex hull of data[h], by drawing chords
     between data points up to "window" data-points apart.  If "maximise" is true then
     hull[h] is 0 unless hull[h]-data[h] is a local max. These h
     points are likely state transitions and can be used as local_min[].
     */
  
  
  int h, k, n, maxima=0, m;
  double w, y, increment;
  int *tmp = (int*)calloc(samples,sizeof(int));
  
  for(h=0;h<samples;h++)
    hull[h] = 0;
  

  for(h=0;h<samples;h++)
    if ( data[h-1] <= data[h] && data[h+1] <= data[h] && ( data[h] != data[h-1] || data[h] != data[h+1] ) )
      tmp[maxima++] = h;

  for(n=1;n<maxima;n++)
    {
      w = (double) ( tmp[n]-tmp[n-1] );
      increment = (data[tmp[n]] - data[tmp[n-1]])/w;
      y = data[tmp[n-1]];
      for(k=tmp[n-1]+1;k<tmp[n];k++)
	{
	  y += increment;
	  m = (int)nint(y);
	  printf("n %5d k %5d %5d y %g %g %d %d\n", n, k, tmp[n-1], y, increment, m, hull[k] );
	  if ( hull[k] < m )
	    {
	      hull[k] = m;
	      left[k] = data[tmp[n-1]]-data[k];
	      right[k] = data[tmp[n]]-data[k];
	    }
	}
    }
  /*
     while( window > 2 )
     {
     w = (double)window;
     for(h=0;h<samples-window;h++)
     {
     k = h+window;
     increment = (data[k]-data[h])/w;
     y = data[h];
     for(k=h+1;k<h+window;k++)
     {
     y += increment;
     n = nint(y);
     if ( hull[k] < n )
     {
     hull[k] = n;
     left[k] = data[h]-data[k];
     right[k] = data[h+window]-data[k];
     }
     }
     }
     window--;
     }
     */
  
  if ( maximise )
    {
      
      for(h=0;h<samples;h++)
	tmp[h] = 0;

      for(h=0;h<samples;h++)
	hull[h] -= data[h];
      
      for(h=1;h<samples-1;h++)
	{
	  if ( hull[h] == 0 && hull[h-1] == 0 )
	    tmp[h] = 0;
	  else if ( hull[h-1] <= hull[h] && hull[h+1] <= hull[h] && tmp[h-1] == 0)
	    tmp[h] = hull[h];
	}
      for(h=0;h<samples;h++)
	{
	  hull[h] = tmp[h];
	  if ( hull[h] == 0 )
	    right[h] = left[h] = 0;
	}

    }
  free(tmp);
}

int **
alloc_sample_matrix( int samples )
{
  int i;
  int **matrix = (int**)calloc(base_n+1,sizeof(int*));

  for(i=base_a;i<=base_n;i++)
    matrix[i] = (int*)calloc(samples,sizeof(int));

  return matrix;
}

void 
free_sample_matrix( int **matrix )
{
  int i;
  if ( matrix )
    {
      for(i=base_a;i<=base_n;i++)
	free(matrix[i]);
      free(matrix);
    }
}

int **
make_sample_data( Scf *scf, int trace_start, int samples )
{
  Header h = scf->header;
  int **sample_data =  alloc_sample_matrix(samples);
  Samples1 *s1;
  Samples2 *s2;
  int c;

  if ( h.sample_size == 2 )	/* 16-bit data */
    {
      s2 = &scf->samples.samples2[trace_start];
      for(c=0;c<samples;c++,s2++)
	{
	  sample_data[base_a][c] = (int)s2->sample_A;
	  sample_data[base_c][c] = (int)s2->sample_C;
	  sample_data[base_g][c] = (int)s2->sample_G;
	  sample_data[base_t][c] = (int)s2->sample_T;
	}
    }
  else				/* 8-bit */
    {
      s1 = &scf->samples.samples1[trace_start];
      for(c=0;c<samples;c++,s1++)
	{
	  sample_data[base_a][c] = (int)s1->sample_A;
	  sample_data[base_c][c] = (int)s1->sample_C;
	  sample_data[base_g][c] = (int)s1->sample_G;
	  sample_data[base_t][c] = (int)s1->sample_T;
	}
    }

  return sample_data;
}

void
trace_envelope( int samples, int **sample_data, int *max_trace, int *max_trace2 )
{
/* max_trace[c] returns the max trace (ie enveloe), and max_trace2 the second-max */

  int c, i;

  for(c=0;c<samples;c++)
    {
      max_trace[c] = -1;
      max_trace2[c] = -2;
      for(i=base_a;i<=base_t;i++)
	{
	  if ( max_trace[c] < sample_data[i][c] )
	    {
	      max_trace2[c] = max_trace[c];
	      max_trace[c] = sample_data[i][c];
	    }
	  else if ( max_trace2[c] < sample_data[i][c] )
	    {
	      max_trace2[c] = sample_data[i][c];
	    }
	}
    }
}
