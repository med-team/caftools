/*  Last edited: Jun 22 12:55 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/* trace alignment support */

#ifndef _TRACE_ALIGN_H_
#define _TRACE_ALIGN_H_

#include <io_lib/scf.h>
#include <bases.h>


enum trace_align_modes { SMITH_WATERMAN, NEEDLEMAN, ALIGN_MODES };

typedef struct /* parameters controlling alignment */
{
int mode;               /* SMITH_WATERMAN or NEEDLEMAN */
int minstate;           /* min time spent in a state */
int samples_per_base;   /* max expected no of samples per base, for banded alignment (<= 0 turns it off ) */
float deriv_weight;     /* weight to multiple derivative info by */
float floor;            /* 0 < floor < 1 , subtracted from traces to make random alignments negative */
int gap_penalty;        /* gap penalty. Must be > 0 . If -ve then ungapped alignment used */
} t_params;

typedef struct /* Re-score arrays */
{
  int b_start, b_end; /* start, end sequence coords */
  int *score, *max;   /* weight score and max possible for each aligned base (arrays start at 0 correspond to base_start)*/
  int *left, *right;  /* left and right heights for each aligned base */
  int *internal;      /* internal derivative data */
  int *width;         /* width in pixels of each aligned base */
}
ReScore;


typedef struct
{
  int base_start, base_end;
  int trace_start, trace_end;
  int *coord, *score;
  ReScore *rs;
}
TRACE_ALIGN_DATA;

#ifdef MAIN
char *align_modes[ALIGN_MODES] = { "smith_waterman", "needleman" };
#else
extern char *align_modes[ALIGN_MODES];
#endif

void trace_align2( int samples, 
		  int **trace_weight, 
		  int **local_min,
		  int bases, char *base,
		  int minimum,
		  int search_mode,
		  int window_band,
		  int *trace_coord,
		  int *align_score,
		  int *total_score,
		  int *base_start,
		  int *base_end,
		  int *trace_start,
		  int *trace_end );



void gapped_trace_align( int samples, 
			int **trace_weight, 
			int **local_min,
			int bases, char *base,
			int minimum,
			int window_band,
			int gap_penalty,
			int *trace_coord,
			int *align_score,
			int *total_score,
			int *base_start,
			int *base_end,
			int *trace_start,
			int *trace_end );

ReScore *trace_align( Scf *scf,             /* scf struct to align */
		 int trace_start,       /* start coord in trace */
		 int trace_end,         /* end coord in trace */
		 char *seq,             /* sequence to align */
		 int seq_start,         /* start coord in seq */
		 int seq_end,           /* end coord in seq */
		 t_params *params,      /* alignment params */
		 int rescore,
		 int *trace_coord,
		 int *align_score,
		 int *total_score,      /* the score for the alignment */
		 int *base_start,       /* the start of the alignment in the seq */
		 int *base_end,         /* the end of the alignment in the seq */
		 int *t_start,          /* the start of the alignment in the trace */
		 int *t_end );           /* the end of the alignment in the trace */

void ps_print_alignment( FILE *fp, Scf *scf, int trace_start, int trace_end, int base_count, char *base, int base_start, int base_end, int *transition_coord, char *title );

void ps_print_multiple_alignment( 
				 FILE *fp, 
				 Scf *scf, 
				 int trace_start, 
				 int trace_end, 
				 int base_count, 
				 char *base, 
				 int base_start, 
				 int base_end,
				 int centre,
				 int *trace_coord, 
				 char *title,
				 char *linetitle,
				 int *newpage );

#endif

void free_ReScore( ReScore *reScore );
ReScore *allocReScore( int base_start, int base_end);

void get_trace_align_params( t_params *params, int argc, char **argv );

void
trace_envelope( int samples, int **sample_data, int *max_trace, int *max_trace2  );

int **
make_sample_data( Scf *scf, int trace_start, int samples );

void 
free_sample_matrix( int **matrix );

int **
alloc_sample_matrix( int samples );


void
eval_local_minima( int samples, int **data, int **info, int **left_side, int **right_side );

