/*  Last edited: Jan  4 20:48 1997 (badger) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Oct 18 11:46 1995 (rmott) */
/* magic numbers for trace editing */

/* thresholds for deciding a replacement edit is no good */

/* trace quality at a given base position is defined by three numbers:

   q  ratio of peak height of max base to height of nect heighest base

   r  ratio of consensus base height to max base 

   s  average height difference between peak and local minimum

*/

/* 
Note that 16 bit traces are scaled to the range 0 to 65535, while
8 bit traces are scaled to the range 0 to 255.
*/

#define SCF_16BIT_BODGE_FACTOR 256

#define PEAK_WINDOW    0.25

#define BAD_Q_RATIO      1.5
#define BAD_R_RATIO      0.5
#define BAD_S_THRESH    10.0

#define GOOD_Q_RATIO     3.0
#define GOOD_R_RATIO     0.5
/* #define GOOD_S_THRESH   10.0 */
#define GOOD_S_THRESH  2560.0

#define P_CUSUM_THRESH 3.0
#define N_CUSUM_THRESH 2.0

/* threshold for voting rule */

#define VOTE_THRESH    0.6

/* thresholds for deletions */

/* #define GOOD_PEAK_THRESH 30 */
#define GOOD_PEAK_THRESH 7680

/* add this quantity to traces to prevent problems with low values */

#define TRACE_BOTTOM  30
#define TRACE_FLOOR_8   10
#define TRACE_FLOOR_16 2560

#define MAX_Q_VALUE 10

#define WINDOW_SIZE    3
