/* $Id: trace_quality.c 15154 2005-06-16 11:24:39Z rmd $ */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
#include <stdio.h>
#include <string.h>
#include <caf.h>
#include <myscf.h>
#include <cl.h>
#include <trace_edit.h>
#include <trace_quality.h>

/* #define DEBUG 1     */

int trace_quality8(Scf *scf, int coord, traceQuality *tq, int base);
int trace_quality16(Scf *scf, int coord, traceQuality *tq, int base);
int peak_count1_8(Scf *scf, int start, int stop, int base, int min_width);
int peak_count1_16(Scf *scf, int start, int stop, int base, int min_width);

/* Trace-editing functions for the autoeditor */

void 
trace_edit(contigAlignment *Align, cafAssembly *Ass )
{
  
  /* go through an assembly and use trace information to determine whether to make an edit */
  
  int j, i;
  int negations,confirmations;
  traceQuality *tq, TQ;
  alignData *ad, *ad1, *ad2;
  Array status = Align->status;
  int min = Align->min, max=Align->max;
  alignStatus *S;
  CAFSEQ *contig = Align->contig;
  Array aligned = Align->aligned;
  Array *B, *B1, *B2, *B3, *B4;
  ASSINF *a;
  int tcoord;
  int strand;
  float p_cusum, n_cusum, worst;
  int reverse_ok, forward_ok, terminator_ok;
  double p_forward, n_forward, p_reverse, n_reverse, p_terminator, n_terminator;
  int min_width=2;

  tq = &TQ;

  if ( min < Ass->from )
    min = Ass->from;
  if ( Ass->to > 0 && max > Ass->to )
    max = Ass->to;

  /* go through each contig position */

  for(j=min+1;j<=max;j++)
    {
      S = arrayp(status,j,alignStatus);
      
/* if using traces then try to make more edits */

      if ( proper_edit(S->status) )
	{

/* replacements */

#ifdef DEBUG 
	  if ( S->status != ok )
	    fprintf(stderr,"%5d %-20s %s %d %c\n", j, status_text[S->status], Consensus_text[S->consensus], S->depth, c_base(S->best) );
#endif
	  /* examine all edits (including the strong ones */
	  if ( /* S->consensus != Strong && */ S->status == replace && S->depth > 1 && proper_base(S->best)  && S->clear  )
	    {
	      /* confirmations = no of reads which positively confirm the edit
		 negations     = no of reads which stronly contradict the edit 

		 */

	      p_forward = n_forward = p_reverse = n_reverse = p_terminator = n_terminator = 0.0;

	      confirmations = negations = 0;
	      n_cusum = p_cusum =  worst = 0.0;

	      B = arrp(aligned,j,Array);
	      for(i=0;i<arrayMax(*B); i++)
		{
		  ad = arrp( *B, i, alignData ); /* the alignData for the i'th aligned read at contig coord j */
		  if ( ad && ad->id != -1 && ! ad->bad )
		    {
		      if ( S->consensus != Strong || i_base(ad->base) != S->best ) 
			/* don't bother to examine confirmations of strong edits */
			{
			  a = arrp( contig->assinf, ad->ass_index, ASSINF );
			  /* get the trace cood corresponding to the read coord (this can be messy of the read has a pad !) */
			  if ( get_trace_base_coordinate( Ass->seqs, a, j, least_upper_bound, &tcoord, &strand ) )
			    {
			      if ( get_trace_quality(Ass,ad->id,strand,tcoord,tq,S->best) ) 
				/* evaluate the trace quality at this position */
				{ 
#ifdef DEBUG
				  fprintf(stderr,"%-20s s: %.2f q: %.2f r: %.2f term: %d %d %d a->id:%d a->seq:%d pos:%d %c\n", dictName(Ass->seqDict,ad->id), tq->s, tq->q, tq->r, is_terminated(ad->strand), tcoord, strand, ad->id, a->seq, ad->pos, ad->base );
#endif
				  if ( tq->s > GOOD_S_THRESH )
				    {
				      if ( tq->r < 0.99  )
					{
					  negations++;
					  n_cusum += tq->q;
					  if ( tq->q > worst )
					    worst = tq->q; /* worst is the most damning evidence against the edit */
					  
					  if ( is_terminated(ad->strand) )
					    n_terminator += tq->q;
					  else if ( is_forward(ad->strand) )
					    n_forward += tq->q;
					  else if ( is_reverse(ad->strand) )
					    n_reverse += tq->q;
					}
				      else 
					{
					  confirmations++;
					  p_cusum += tq->q;
					  if ( is_terminated(ad->strand) )
					    p_terminator += tq->q;
					  else if ( is_forward(ad->strand) )
					    p_forward += tq->q;
					  else if ( is_reverse(ad->strand) )
					    p_reverse += tq->q;
					}
				    }
				}
			    }
			}
		    }
		}

	      p_cusum =  p_cusum/(confirmations+1.0e-10);
	      n_cusum = n_cusum/(negations+1.0e-10);
	      
	      forward_ok = ( p_forward > n_forward );
	      reverse_ok = ( p_reverse > n_reverse );
	      terminator_ok = (  p_terminator > n_terminator );
	      
	      /* criterion for promoting an edit to Strong:
		 (a) worst base call < 3.0
		 (b) either average q for consensus reads > 1+ average q for others OR
		 at least 2 of reverse_ok, forward_ok and terminator_ok are true */
	      
	      if ( S->consensus != Strong && worst < 3.0 && 
		  ( (p_cusum > n_cusum + 1) || 
		   ( reverse_ok + forward_ok + terminator_ok > 1 )))
		{
#ifdef DEBUG 
		  fprintf(stderr,"upgrading p_cusum: %f n_cusum: %f forward: %g %g  reverse: %g %g ok: %d %d %d worst: %g\n", p_cusum, n_cusum, p_forward, n_forward , p_reverse, n_reverse, forward_ok, reverse_ok, terminator_ok, worst );
#endif
		  S->consensus = Strong;
		}
	      else if ( S->consensus == Strong && worst > 3.0 )
		{
#ifdef DEBUG 
		  fprintf(stderr,"downgrading strong replacement p_cusum: %g n_cusum: %g confirmations: %d negations %d worst: %g\n",
			 p_cusum, n_cusum, confirmations, negations, worst); 
#endif
		  S->conflict = TRUE; /* set conflict flag so that a tag gets written */
		  S->consensus = Medium;
		}
	      
#ifdef DEBUG 
	      else if ( S->consensus != Strong )
		fprintf (stderr,"leaving unchanged replace %s p_cusum: %f n_cusum: %f\n", Consensus_text[S->consensus], p_cusum, n_cusum ); 
#endif
	    }

/* replacement  edits masquerading as unclears  */

	  if ( S->status == unclear  &&  S->clear &&  S->base_calls[base_i] == 0 )
	    {
	      float votes[nucleotides];
	      float sum;
	      float max_vote = 0;
	      int max_vote_base = 0;

/*	      if ( S->depth == 0 )
		printf("%d replace2 zero depth\n", j); */
	      for_any_base(i)
		votes[i] = 1.0e-5;
	      B = arrp(aligned,j,Array);
	      for(i=0;i<arrayMax(*B);i++)
		{
		  ad = arrp( *B, i, alignData );
		  if ( ad && ad->id != -1 && ! ad->bad )
		    {
		      a = arrp( contig->assinf, ad->ass_index, ASSINF );
		      if ( (i_base(ad->base) != base_i) && get_trace_base_coordinate( Ass->seqs, a, j, least_upper_bound, &tcoord, &strand ) )
			accumulate_votes( Ass, ad->id, strand, tcoord, votes );
		    }
		}
	      
	      sum = 1.0e-10;
	      for_any_base(i)
		sum += votes[i];
	      for_any_base(i)
		votes[i] /= sum;

	      for_each_base(i)
		{
		  if (votes[i] > max_vote)
		    {
		      max_vote = votes[i];
		      max_vote_base = i;
		    }
		}
	      if ( max_vote > VOTE_THRESH )
		{
		  S->status = replace;
		  S->consensus = Strong;
		  S->best = max_vote_base;
#ifdef DEBUG 
		  fprintf(stderr,"%d  upgrading unclear %g\n", j, votes[S->best] ); 
#endif
		}
#ifdef DEBUG 
	      fprintf(stderr,"%d  leaving unclear %g\n", j, votes[S->best] ); 
#endif
	    }


	  /* deletions */

	  if ( S->status == deletion && ( S->depth > 1 && j > min && j < max  && S->base_calls[base_i] < S->depth ))
	    {
	      int y, d;
	      confirmations = negations = 0;
	      n_cusum = p_cusum = 0.0;
	      
	      B = arrp(aligned,j,Array);
	      B1 = arrp(aligned,j-1,Array);
	      B2 = arrp(aligned,j+1,Array);
	      for(i=0;i<arrayMax(*B); i++)
		{
		  ad = arrp( *B, i, alignData );
		  if ( ad && ad->id != -1 && ! ad->bad )
		    {
		      a = arrp( contig->assinf, ad->ass_index, ASSINF );
		      if ( proper_base(i_base(ad->base)) && get_trace_base_coordinate( Ass->seqs, a, j, least_upper_bound, &tcoord, &strand ) )
			{
			  if ((i < arrayMax(*B1)) && (i < arrayMax(*B2)))
			    {
			      ad1 = arrp(*B1,i,alignData); /* left  base */
			      ad2 = arrp(*B2,i,alignData); /* right base */
			      y = 0;
			      if ( ad1->id != -1 && ad2->id != -1 )
				{
				  if ( ad->base != ad1->base && ad->base != ad2->base ) /* isolated deletion */
				    {
				      d = isolated_peak( Ass, ad->id, strand, tcoord, i_base(ad->base) );
#ifdef DEBUG
				      fprintf(stderr,"%5d isolated deletion %d %s \n", j, d, Consensus_text[S->consensus] ); 
#endif
				      if (d > GOOD_PEAK_THRESH) /* good quality peak so not ok to edit */
					{
					  negations++;
#ifdef DEBUG
					  fprintf(stderr,"downgrading ");
#endif
					}
				      else
					{
					  confirmations++;
#ifdef DEBUG
					  fprintf(stderr,"upgrading ");
#endif
					}
#ifdef DEBUG
				      fprintf(stderr,"\n"); 
#endif
				    } 
				  else if ( ad1->base == ad->base && ad2->base == ad->base ) /* run of three bases, with the error on the middle base */
				    {
				      d=peak_count( Ass, ad->id, strand, tcoord, -1, i_base(ad->base),3,min_width);
#ifdef DEBUG 
				      fprintf(stderr,"%5d %s triple centre %d\n",j, dictName(Ass->seqDict,ad->id), d); 
#endif
				      if ( d < 3 )
					confirmations++;
				      else
					negations++;
				    }
				  else if ( ad1->base == ad->base || (i_base(ad1->base) == base_n && ad2->base != ad->base) ) 
				    {
				      B3 = arrp(aligned, j - 2, Array);
				      if ( i < arrayMax(*B3) && ad1->base == ad->base && arrp(*B3, i, alignData)->base == ad->base ) 
					{      /* run of three with error on left */
					  d=peak_count( Ass, ad->id, strand, tcoord, -2, i_base(ad->base),3,min_width);
#ifdef DEBUG 
					  fprintf(stderr,"%5d %s triple left %d\n",j, dictName(Ass->seqDict,ad->id), d); 
#endif
					  if ( d < 3 )
					    confirmations++;
					  else
					    negations++;
					}
				      else 
					{
					  d = peak_count( Ass, ad->id,strand, tcoord, -1, i_base(ad->base),2,min_width );
#ifdef DEBUG
					  fprintf(stderr,"%5d %s double left %d\n", j, dictName(Ass->seqDict,ad->id), d );
#endif
					  if ( d < 2 ) /* run of two */
					    confirmations++;
					  else
					    negations++;
					}
				    }
				  else if (  ad2->base == ad->base || (i_base(ad2->base) == base_n && ad1->base != ad->base) )
				    {
				      B4 = arrp(aligned, j + 2, Array);
				      if ( i < arrayMax(*B4) && ad2->base == ad->base && arrp(*B4, i, alignData)->base == ad->base ) 
					{
					  d=peak_count( Ass, ad->id, strand, tcoord, 1, i_base(ad->base), 3,min_width); /* run of three with error on right */
#ifdef DEBUG 
					  fprintf(stderr,"%5d %s triple right %d\n",j,dictName(Ass->seqDict,ad->id),d);
#endif
					  if ( d < 3 )
					    confirmations++;
					  else
					    negations++;
					}
				      else 
					{
					  d = peak_count( Ass, ad->id, strand, tcoord, 1, i_base(ad->base),2,min_width );
#ifdef DEBUG
					  fprintf(stderr,"%5d %s double right %d\n", j, dictName(Ass->seqDict,ad->id), d );
#endif
					  if ( d < 2 ) /* run of two */
					    confirmations++;
					  else
					    negations++;
					}
				    }
				}
#ifdef DEBUG 
			      fprintf(stderr,"%5d %c %c %-15s %2d %5d %d %d\n", j, c_base(S->best), ad->base, dictName(Ass->seqDict,ad->id),strand, tcoord, confirmations, negations);   
#endif
			    }
			}
		    }
		}
	      if ( confirmations && ! negations && S->base_calls[base_i] >= S->depth-1 ) 
		{
#ifdef DEBUG 
		  if ( S->consensus != Strong )
		    fprintf(stderr,"%d upgrading\n", j );
#endif
		  S->consensus = Strong;
		}
	      else if ( S->consensus == Strong && negations ) 
		{
		  if ( S->consensus == Strong )
		    {
#ifdef DEBUG 
		      fprintf(stderr,"%5d downgrading\n", j); 
#endif
		      S->consensus = Medium;
		    }
		}
	    }
	}
    }
}




int 
get_trace_quality( cafAssembly *Ass, int read, int strand, int coord, traceQuality *tq, int base )
{
/* evaluate the trace quality tq struct for a read at a particular position */

  CAFSEQ *seq = arrp(Ass->seqs,read,CAFSEQ);
  Scf *scf;

  if ((scf = find_scf( Ass, seq )) != 0)
    {
      if ( is_reverse(strand) )
	{
	  coord = scf->header.bases - coord+1;
	  complement_scf( scf ); /* NOTE: complement_scf sets an internal flag so that repeated calls have no effect */
	}
      return trace_quality( scf, coord, tq, base );
    }
  return 0;
}

int 
trace_quality( Scf *scf, int coord, traceQuality *tq, int base )
{
  if ( scf->header.sample_size == 2 )
    return trace_quality16(scf, coord, tq, base); /* 16 bit data */
  return trace_quality8(scf, coord, tq, base); /* 8 bit data */
}

int 
trace_quality8( Scf *scf, int coord, traceQuality *tq, int base )

/* return the trace quality for a read at a given coord for a window of bases either side 

returns TRUE if the window is in range and FALSE otherwise

The struct tq returns the trace quality info, with fields:

q = peak height ratio for the central base
s = peak separation ratio for the central base
r = peak height ratio for the preferred base 
*/

{
  int index, centre, new_centre;
  Header *h = &scf->header;
  Bases *B = scf->bases;
  Samples1 *samples1 = scf->samples.samples1;
  Samples1 *s1;
  int p1, p2;
  int x_A, x_C, x_G, x_T;
  int p_A, p_C, p_G, p_T;
  int left_window, right_window;
  int left_min, right_min;
  int max;
  int global_max;
  double x;

  tq->q = tq->s = tq->r = global_max = 0;
  coord--;

  if ( coord > 0 && coord < h->bases-1 ) /* knock off first and last bases */
    {
      centre = B[coord].peak_index;
      
      left_window = centre-(centre-(int)B[coord-1].peak_index)*PEAK_WINDOW;
      right_window = centre+((int)B[coord+1].peak_index-centre)*PEAK_WINDOW;

      p1 = p2 = 0.0; /* p1 is the max trace at the base called position, p2 the second max */

      p_A = p_C = p_G = p_T = 0;
      x_A = x_C = x_G = x_T = centre;

      for(index=left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak for each base in window */
	{
	  if ( p_A < (int)s1->sample_A )
	    {
	      p_A = (int)s1->sample_A;
	      x_A = index;
	    }
	  if ( p_C < (int)s1->sample_C )
	    {
	      p_C = (int)s1->sample_C;
	      x_C = index;
	    }
	  if ( p_G < (int)s1->sample_G )
	    {
	      p_G = (int)s1->sample_G;
	      x_G = index;
	    }
	  if ( p_T < (int)s1->sample_T )
	    {
	      p_T = (int)s1->sample_T;
	      x_T = index;
	    }
/*	  printf("%d %d %d %d  %d %d %d %d\n", p_A, p_C, p_G, p_T, (int)s1->sample_A,(int)s1->sample_C,(int)s1->sample_G,(int)s1->sample_T); */
	}

      /* check for local max - ie not on boundary. If so then reset back to ABI position */
      
      if ( x_A == left_window || x_A == right_window )
	{
	  x_A = centre;
	  p_A = samples1[centre].sample_A;
	}
      if ( x_C == left_window || x_C == right_window )
	{
	  x_C = centre;
	  p_C = samples1[centre].sample_C;
	}
      if ( x_G == left_window || x_G == right_window )
	{
	  x_G = centre;
	  p_G = samples1[centre].sample_G;
	}
      if ( x_T == left_window || x_T == right_window )
	{
	  x_T = centre;
	  p_T = samples1[centre].sample_T;
	}
      
      /* add a small const to avoid distortions due to low peaks */
      
      p_A += TRACE_FLOOR_8;
      p_C += TRACE_FLOOR_8;
      p_G += TRACE_FLOOR_8;
      p_T += TRACE_FLOOR_8;
      
      /* get best and second-best peaks */
      
      if ( p_A > p_C )
	{
	  p1 = p_A;
	  p2 = p_C;
	  new_centre = x_A;
	}
      else
	{
	  p1 = p_C;
	  p2 = p_A;
	  new_centre = x_C;
	}
      if ( p1 <= p_G )
	{
	  p2 = p1;
	  p1 = p_G;
	  new_centre = x_G;
	}
      else if ( p2 < p_G )
	p2 = p_G;
      
      if ( p1 <= p_T )
	{
	  p2 = p1;
	  p1 = p_T;
	  new_centre = x_T;
	}
      else if ( p2 < p_T )
	p2 = p_T;
    
      x = p1; 
      global_max = p1-TRACE_FLOOR_8; /* the uncorrected max height */

      tq->q = x/p2; /* ratio of peak heights */
	
	if ( tq->q > MAX_Q_VALUE /*TRACE_FLOOR_8*/ )
	  tq->q = MAX_Q_VALUE /*TRACE_FLOOR_8*/;
      
      /* ratio of height of consensus base to max */
      
      if ( base == base_a )
	tq->r = p_A/x;
      else if ( base == base_c )
	tq->r = p_C/x;
      else if ( base == base_g )
	tq->r = p_G/x;
      else if ( base == base_t )
	tq->r = p_T/x;
      else
	tq->r = 0.0;
      
      /* now compute a measure of how well-defined a peak is */
      
      left_window = B[coord-1].peak_index;
      right_window = B[coord+1].peak_index;
      left_min = right_min = 100000;

      for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak for each base in window */
	{
	  if ( (int)s1->sample_A > (int)s1->sample_C )
	    max = (int)s1->sample_A;
	  else
	    max = (int)s1->sample_C;
	  
	  if ( max < (int)s1->sample_G )
	    max = (int)s1->sample_G;
	  
	  if ( max < (int)s1->sample_T )
	    max = (int)s1->sample_T;

	  /* max is the max of the 4 traces at a given position, ie gives the envelope of the traces*/

	  /* keep record of the left and right minima on the envelope */

	  if ( index <= new_centre && left_min > max )
	    left_min = max;
	  if ( index >= new_centre && right_min > max )
	    right_min = max; 
	}

      /* tq->s is the peak-height -local background */

      if ( right_min > left_min )
	tq->s = global_max - right_min;
      else
	tq->s = global_max - left_min;

      tq->s *= SCF_16BIT_BODGE_FACTOR;  /* Try to make 8 and 16 bit SCF files
					   return the same value for tq->s */

#ifdef DEBUG
      fprintf(stderr,"tq->r = %f tq->q = %f tq->s = %f p1:%d p2:%d %d %d %d %d base; %d %c\n", tq->r, tq->q, tq->s, p1, p2, p_A, p_C, p_G, p_T, base, c_base(base) );  
#endif
      return TRUE;
    }
  return FALSE;
}

int 
trace_quality16( Scf *scf, int coord, traceQuality *tq, int base )

/* return the trace quality for a read at a given coord for a window of bases either side 

returns TRUE if the window is in range and FALSE otherwise

The struct tq returns the trace quality info, with fields:

q = peak height ratio for the central base
s = peak separation ratio for the central base
r = peak height ratio for the preferred base 
*/

{
  int index, centre, new_centre;
  Header *h = &scf->header;
  Bases *B = scf->bases;
  Samples2 *samples2 = scf->samples.samples2;
  Samples2 *s2;
  int p1, p2;
  int x_A, x_C, x_G, x_T;
  int p_A, p_C, p_G, p_T;
  int left_window, right_window;
  int left_min, right_min;
  int max;
  int global_max;
  double x;

  tq->q = tq->s = tq->r = global_max = 0;
  coord--;

  if ( coord > 0 && coord < h->bases-1 ) /* knock off first and last bases */
    {
      centre = B[coord].peak_index;
      
      left_window = centre-(centre-(int)B[coord-1].peak_index)*PEAK_WINDOW;
      right_window = centre+((int)B[coord+1].peak_index-centre)*PEAK_WINDOW;

      p1 = p2 = 0.0; /* p1 is the max trace at the base called position, p2 the second max */

      p_A = p_C = p_G = p_T = 0;
      x_A = x_C = x_G = x_T = centre;

      for(index=left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak for each base in window */
	{
	  if ( p_A < (int)s2->sample_A )
	    {
	      p_A = (int)s2->sample_A;
	      x_A = index;
	    }
	  if ( p_C < (int)s2->sample_C )
	    {
	      p_C = (int)s2->sample_C;
	      x_C = index;
	    }
	  if ( p_G < (int)s2->sample_G )
	    {
	      p_G = (int)s2->sample_G;
	      x_G = index;
	    }
	  if ( p_T < (int)s2->sample_T )
	    {
	      p_T = (int)s2->sample_T;
	      x_T = index;
	    }
/*	  printf("%d %d %d %d  %d %d %d %d\n", p_A, p_C, p_G, p_T, (int)s2->sample_A,(int)s2->sample_C,(int)s2->sample_G,(int)s2->sample_T); */
	}

      /* check for local max - ie not on boundary. If so then reset back to ABI position */
      
      if ( x_A == left_window || x_A == right_window )
	{
	  x_A = centre;
	  p_A = samples2[centre].sample_A;
	}
      if ( x_C == left_window || x_C == right_window )
	{
	  x_C = centre;
	  p_C = samples2[centre].sample_C;
	}
      if ( x_G == left_window || x_G == right_window )
	{
	  x_G = centre;
	  p_G = samples2[centre].sample_G;
	}
      if ( x_T == left_window || x_T == right_window )
	{
	  x_T = centre;
	  p_T = samples2[centre].sample_T;
	}
      
      /* add a small const to avoid distortions due to low peaks */
      
      p_A += TRACE_FLOOR_16;
      p_C += TRACE_FLOOR_16;
      p_G += TRACE_FLOOR_16;
      p_T += TRACE_FLOOR_16;
      
      /* get best and second-best peaks */
      
      if ( p_A > p_C )
	{
	  p1 = p_A;
	  p2 = p_C;
	  new_centre = x_A;
	}
      else
	{
	  p1 = p_C;
	  p2 = p_A;
	  new_centre = x_C;
	}
      if ( p1 <= p_G )
	{
	  p2 = p1;
	  p1 = p_G;
	  new_centre = x_G;
	}
      else if ( p2 < p_G )
	p2 = p_G;
      
      if ( p1 <= p_T )
	{
	  p2 = p1;
	  p1 = p_T;
	  new_centre = x_T;
	}
      else if ( p2 < p_T )
	p2 = p_T;
    
      x = p1; 
      global_max = p1-TRACE_FLOOR_16; /* the uncorrected max height */

      tq->q = x/p2; /* ratio of peak heights */
	
	if ( tq->q > MAX_Q_VALUE /*TRACE_FLOOR_16*/ )
	  tq->q = MAX_Q_VALUE /*TRACE_FLOOR_16*/;
      
      /* ratio of height of consensus base to max */
      
      if ( base == base_a )
	tq->r = p_A/x;
      else if ( base == base_c )
	tq->r = p_C/x;
      else if ( base == base_g )
	tq->r = p_G/x;
      else if ( base == base_t )
	tq->r = p_T/x;
      else
	tq->r = 0.0;
      
      /* now compute a measure of how well-defined a peak is */
      
      left_window = B[coord-1].peak_index;
      right_window = B[coord+1].peak_index;
      left_min = right_min = 100000;

      for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak for each base in window */
	{
	  if ( (int)s2->sample_A > (int)s2->sample_C )
	    max = (int)s2->sample_A;
	  else
	    max = (int)s2->sample_C;
	  
	  if ( max < (int)s2->sample_G )
	    max = (int)s2->sample_G;
	  
	  if ( max < (int)s2->sample_T )
	    max = (int)s2->sample_T;

	  /* max is the max of the 4 traces at a given position, ie gives the envelope of the traces*/

	  /* keep record of the left and right minima on the envelope */

	  if ( index <= new_centre && left_min > max )
	    left_min = max;
	  if ( index >= new_centre && right_min > max )
	    right_min = max; 
	}

      /* tq->s is the peak-height -local background */

      if ( right_min > left_min )
	tq->s = global_max - right_min;
      else
	tq->s = global_max -left_min;

#ifdef DEBUG
      fprintf(stderr,"tq->r = %f tq->q = %f tq->s = %f p1:%d p2:%d %d %d %d %d base; %d %c\n", tq->r, tq->q, tq->s, p1, p2, p_A, p_C, p_G, p_T, base, c_base(base) );  
#endif
      return TRUE;
    }
  return FALSE;
}

int 
possible_overcall( cafAssembly *Ass, int read, int strand, int coord, int direction, int base)

/* returns the difference in height between the min value of a trace
between two bases and the lower of the two endpoints. Thus if a trace
dips below the endpoints the function returns -ve.

We can use this to determine if there is an overcall eg when AA has
been called in place of A and where the trace is a simple one-humped
curve */

{ 
  CAFSEQ *seq = arrp(Ass->seqs,read,CAFSEQ); 
  Scf *scf; 
  int i, j, k, min=1000000; 
  int s, si, sj;
  Header *h;
  Bases *B;
  Samples1 *samples1;
  Samples2 *samples2;

  if ((scf = find_scf( Ass, seq )) != 0)
    {
      h = &scf->header;
      B = scf->bases;
      samples1 = scf->samples.samples1;
      samples2 = scf->samples.samples2;

      if ( is_reverse(strand) )
	{
	  coord = h->bases - coord+1;
	  complement_scf( scf );
	}

      coord--;
      if ( direction > 0 )
	{
	  i = B[coord].peak_index;
	  j = B[coord+direction].peak_index;
	}
      else
	{
	  j = B[coord].peak_index;
	  i = B[coord+direction].peak_index;
	}

      if ( h->sample_size == 1 ) /* 8 bit data */
	{
	  if ( base == base_a )
	    {
	      for(k=i;k<=j;k++)
		{
		  s = samples1[k].sample_A;
		  if ( min >  s )
		    min = s;
		}
	      si = samples1[i].sample_A;
	      sj = samples1[j].sample_A;
	      min -= ( si < sj ? si : sj );
	    }
	  if ( base == base_c )
	    {
	      for(k=i;k<=j;k++)
		{
		  s = samples1[k].sample_C;
		  if ( min >  s )
		    min = s;
		}
	      si = samples1[i].sample_C;
	      sj = samples1[j].sample_C;
	      min -= ( si < sj ? si : sj );
	    }
	  else if ( base == base_g )
	    {
	      for(k=i;k<=j;k++)
		{
		  s = samples1[k].sample_G;
		  if ( min >  s )
		    min = s;
		}
	      si = samples1[i].sample_G;
	      sj = samples1[j].sample_G;
	      min -= ( si < sj ? si : sj );
	    }
	  else if ( base == base_t )
	    {
	      for(k=i;k<=j;k++)
		{
		  s = samples1[k].sample_T;
		  if ( min >  s )
		    min = s;
		}
	      si = samples1[i].sample_T;
	      sj = samples1[j].sample_T;
	      min -= ( si < sj ? si : sj );
	    }
	  else
	    min = -1;
#ifdef DEBUG	      
	  fprintf(stderr,"%5d  min: %d i:%d si: %d j: %d sj:%d\n", coord, min, i, si, j, sj );
#endif
	}
      else
	{  /* 16 bit data */
	  if ( base == base_a )
	    {
	      for(k=i;k<=j;k++)
		{
		  s = samples2[k].sample_A;
		  if ( min >  s )
		    min = s;
		}
	      si = samples2[i].sample_A;
	      sj = samples2[j].sample_A;
	      min -= ( si < sj ? si : sj );
	    }
	  if ( base == base_c )
	    {
	      for(k=i;k<=j;k++)
		{
		  s = samples2[k].sample_C;
		  if ( min >  s )
		    min = s;
		}
	      si = samples2[i].sample_C;
	      sj = samples2[j].sample_C;
	      min -= ( si < sj ? si : sj );
	    }
	  else if ( base == base_g )
	    {
	      for(k=i;k<=j;k++)
		{
		  s = samples2[k].sample_G;
		  if ( min >  s )
		    min = s;
		}
	      si = samples2[i].sample_G;
	      sj = samples2[j].sample_G;
	      min -= ( si < sj ? si : sj );
	    }
	  else if ( base == base_t )
	    {
	      for(k=i;k<=j;k++)
		{
		  s = samples2[k].sample_T;
		  if ( min >  s )
		    min = s;
		}
	      si = samples2[i].sample_T;
	      sj = samples2[j].sample_T;
	      min -= ( si < sj ? si : sj );
	    }
	  else
	    min = -1;
#ifdef DEBUG	      
	  fprintf(stderr,"%5d  min: %d i:%d si: %d j: %d sj:%d\n", coord, min, i, si, j, sj );
#endif
	}
      return min;
    }
  else
    return -1; /* error condition this return would imply there has not been an overcall*/
}

int 
peak_count( cafAssembly *Ass, int read, int strand, int coord, int direction, int base, int range, int min_width)
{

/* count the number of local maxima (peaks) for read in direction from coord for base in range */

  CAFSEQ *seq = arrp(Ass->seqs,read,CAFSEQ);
  Scf *scf;
  int start, stop;
  Bases *B;
  
  if ((scf = find_scf( Ass, seq )) != 0)
    {
      if ( is_reverse(strand) )
	{
	  coord = scf->header.bases - coord+1;
	  complement_scf( scf );
	}

      B =scf->bases;
      coord--;
      range--;
      if ( direction < 0 )
	coord += direction;
      if ( coord <= 0 )
	return 0;

      start = B[coord].peak_index-2;
      stop = B[coord+range].peak_index+2; /* +/- 2 pixels to capture end peaks */

#ifdef DEBUG	  
      fprintf(stderr,"%c %c  %d %d  i:%d j:%d\n", B[coord].base, B[coord+range].base,coord,coord+range, start, stop ); 
#endif
      return peak_count1( scf, start, stop, base, min_width ); 
    }
  return 0;
}

int 
peak_count1( Scf *scf, int start, int stop, int base, int min_width )
{
  if ( scf->header.sample_size == 2 )
    return peak_count1_16(scf, start, stop, base, min_width); /* 16 bit data */
  return peak_count1_8(scf, start, stop, base, min_width); /* 8 bit data */  
}

int 
peak_count1_8( Scf *scf, int start, int stop, int base, int min_width )

/* count the number of peaks between start and stop (in trace coords) for a given base.
A peak is a point in the trace with the property that in a window of +-
min_width the trace never exceeds the value at the point. This rules
out shoulders unless they wider than min_width, but allows flat-topped peaks 

Note that min_width only has an effect if it is bigger than 1 (sensible value is 2) */

{ 
  int peaks=0; 
  int i, j; 
  int last_was_max=0; 
  int is_peak; 
  Samples1 *samples1 = scf->samples.samples1; 
  int s1, s2, s3;
  int ok;

  if ( base == base_a )
    for(i=start;i<=stop;i++)
      {
	s1 = samples1[i-1].sample_A;
	s2 = samples1[i].sample_A;	
	s3 = samples1[i+1].sample_A;
	is_peak = ( s1 <= s2 && s3 <= s2 );
	if ( is_peak && ! last_was_max )
	  {
	    ok = 1;
	    for(j=i-min_width;j<=i+min_width;j++)
	      if ( (int)samples1[j].sample_A > s2 )
		{
		  ok = 0;
		  break;
		}
	    if ( ok )
	      peaks += is_peak;
	  }
	last_was_max = is_peak;
      }
  else if ( base == base_c )
    for(i=start;i<=stop;i++)
      {
	s1 = (int)samples1[i-1].sample_C;
	s2 = (int)samples1[i].sample_C;
	s3 = (int)samples1[i+1].sample_C;
	is_peak = ( s1 <= s2 && s3 <= s2 );
	if ( is_peak && ! last_was_max )
	  {
	    ok = 1;
	    for(j=i-min_width;j<=i+min_width;j++)
	      if ( (int)samples1[j].sample_C > s2 )
		{
		  ok = 0;
		  break;
		}
	    if ( ok )
	      peaks += is_peak;
	  }
	last_was_max = is_peak;
      }
  else if ( base == base_g )
    for(i=start;i<=stop;i++)
      {
	s1 = (int)samples1[i-1].sample_G;
	s2 = (int)samples1[i].sample_G;
	s3 = (int)samples1[i+1].sample_G;
	is_peak = ( s1 <= s2 && s3 <= s2 );
	if ( is_peak && ! last_was_max )
	  {
	    ok = 1;
	    for(j=i-min_width;j<=i+min_width;j++)
	      if ( (int)samples1[j].sample_G > s2 )
		{
		  ok = 0;
		  break;
		}
	    if ( ok )
	      peaks += is_peak;
	  }
	last_was_max = is_peak;
      }
  else if ( base == base_t )
    for(i=start;i<=stop;i++)
      {
	s1 = (int)samples1[i-1].sample_T;
	s2 = (int)samples1[i].sample_T;
	s3 = (int)samples1[i+1].sample_T;
	is_peak = ( s1 <= s2 && s3 <= s2 );
	if ( is_peak && ! last_was_max )
	  {
	    ok = 1;
	    for(j=i-min_width;j<=i+min_width;j++)
	      if ( (int)samples1[j].sample_T > s2 )
		{
		  ok = 0;
		  break;
		}
	    if ( ok )
	      peaks += is_peak;
	  }
	last_was_max = is_peak;
      }
      
  return peaks;
}

int 
peak_count1_16( Scf *scf, int start, int stop, int base, int min_width )

/* count the number of peaks between start and stop (in trace coords) for a given base.
A peak is a point in the trace with the property that in a window of +-
min_width the trace never exceeds the value at the point. This rules
out shoulders unless they wider than min_width, but allows flat-topped peaks 

Note that min_width only has an effect if it is bigger than 1 (sensible value is 2) */

{ 
  int peaks=0; 
  int i, j; 
  int last_was_max=0; 
  int is_peak; 
  Samples2 *samples2 = scf->samples.samples2; 
  int s1, s2, s3;
  int ok;

  if ( base == base_a )
    for(i=start;i<=stop;i++)
      {
	s1 = samples2[i-1].sample_A;
	s2 = samples2[i].sample_A;	
	s3 = samples2[i+1].sample_A;
	is_peak = ( s1 <= s2 && s3 <= s2 );
	if ( is_peak && ! last_was_max )
	  {
	    ok = 1;
	    for(j=i-min_width;j<=i+min_width;j++)
	      if ( (int)samples2[j].sample_A > s2 )
		{
		  ok = 0;
		  break;
		}
	    if ( ok )
	      peaks += is_peak;
	  }
	last_was_max = is_peak;
      }
  else if ( base == base_c )
    for(i=start;i<=stop;i++)
      {
	s1 = (int)samples2[i-1].sample_C;
	s2 = (int)samples2[i].sample_C;
	s3 = (int)samples2[i+1].sample_C;
	is_peak = ( s1 <= s2 && s3 <= s2 );
	if ( is_peak && ! last_was_max )
	  {
	    ok = 1;
	    for(j=i-min_width;j<=i+min_width;j++)
	      if ( (int)samples2[j].sample_C > s2 )
		{
		  ok = 0;
		  break;
		}
	    if ( ok )
	      peaks += is_peak;
	  }
	last_was_max = is_peak;
      }
  else if ( base == base_g )
    for(i=start;i<=stop;i++)
      {
	s1 = (int)samples2[i-1].sample_G;
	s2 = (int)samples2[i].sample_G;
	s3 = (int)samples2[i+1].sample_G;
	is_peak = ( s1 <= s2 && s3 <= s2 );
	if ( is_peak && ! last_was_max )
	  {
	    ok = 1;
	    for(j=i-min_width;j<=i+min_width;j++)
	      if ( (int)samples2[j].sample_G > s2 )
		{
		  ok = 0;
		  break;
		}
	    if ( ok )
	      peaks += is_peak;
	  }
	last_was_max = is_peak;
      }
  else if ( base == base_t )
    for(i=start;i<=stop;i++)
      {
	s1 = (int)samples2[i-1].sample_T;
	s2 = (int)samples2[i].sample_T;
	s3 = (int)samples2[i+1].sample_T;
	is_peak = ( s1 <= s2 && s3 <= s2 );
	if ( is_peak && ! last_was_max )
	  {
	    ok = 1;
	    for(j=i-min_width;j<=i+min_width;j++)
	      if ( (int)samples2[j].sample_T > s2 )
		{
		  ok = 0;
		  break;
		}
	    if ( ok )
	      peaks += is_peak;
	  }
	last_was_max = is_peak;
      }
      
  return peaks;
}

void 
accumulate_votes( cafAssembly *Ass, int read, int strand, int tcoord, float *votes )
/* used to determine the consensus base in cases where the ABI calls conflict */
{
  CAFSEQ *seq = arrp(Ass->seqs,read,CAFSEQ);
  Scf *scf;
  int i, j;
  Bases *B;
  Samples1 *samples1;
  Samples2 *samples2;
  float s = 0;
  float tmp[nucleotides];

  if ((scf = find_scf( Ass, seq )) != 0)
    {

      for(i=0;i<nucleotides;i++)
	tmp[i] = 0.0;
      if ( is_reverse(strand) )
	{
	  tcoord = scf->header.bases - tcoord+1;
	  complement_scf( scf );
	}
      
      tcoord--;

      B =scf->bases;
      i = B[tcoord].peak_index;
      if ( scf->header.sample_size == 2 )
	{ /* 16 bit */
	  samples2 = scf->samples.samples2;
	  
	  for(j=i-2;j<=i+2;j++)
	    {
	      if ( (float)samples2[j].sample_A > votes[base_a] )
		tmp[base_a] = samples2[j].sample_A;
	      if ( (float)samples2[j].sample_C > votes[base_c] )
		tmp[base_c] = samples2[j].sample_C;
	      if ( (float)samples2[j].sample_G > votes[base_g] )
		tmp[base_g] = samples2[j].sample_G;
	      if ( (float)samples2[j].sample_T > votes[base_t] )
		tmp[base_t] = samples2[j].sample_T;
	    }
	}
      else
	{ /* 8 bit */
	  samples1 = scf->samples.samples1;
	  
	  for(j=i-2;j<=i+2;j++)
	    {
	      if ( (float)samples1[j].sample_A > votes[base_a] )
		tmp[base_a] = samples1[j].sample_A;
	      if ( (float)samples1[j].sample_C > votes[base_c] )
		tmp[base_c] = samples1[j].sample_C;
	      if ( (float)samples1[j].sample_G > votes[base_g] )
		tmp[base_g] = samples1[j].sample_G;
	      if ( (float)samples1[j].sample_T > votes[base_t] )
		tmp[base_t] = samples1[j].sample_T;
	    }
	}
      s = 1 + tmp[base_a] + tmp[base_c] + tmp[base_g] + tmp[base_t];
	  
      tmp[base_a] /= s;
      tmp[base_c] /= s;
      tmp[base_g] /= s;
      tmp[base_t] /= s;

      votes[base_a] += tmp[base_a];
      votes[base_c] += tmp[base_c];
      votes[base_g] += tmp[base_g];
      votes[base_t] += tmp[base_t];
    }
}

int 
get_trace_coordinate_from_ad( cafAssembly *Ass, alignData *ad, int approximation_type, int *tcoord, int *strand )
{
/*  gets the trace coordinate corresponding to an alignData struct */

  int read;
  CAFSEQ *seq;
  ASSINF *a;
  int closest = -1;
  int j, xcoord;

  read = ad->id;
  if ( read < 0 )
    return FALSE;

  if ( is_forward(ad->strand) )
    *strand = forward_strand;
  else if ( is_reverse(ad->strand) )
    *strand = reverse_strand;
  else
    return FALSE;

  seq = arrp(Ass->seqs, ad->id,CAFSEQ);
  if ( ! seq->SCF_File || ! seq->assinf)
    return FALSE;

  xcoord = ad->pos;

  for(j=0;j<arrayMax(seq->assinf);j++)
    {
      a = arrp( seq->assinf, j, ASSINF ); 
      
      if ( a->s1 <= xcoord && a->s2 >= xcoord )
	{
	  *tcoord = xcoord - a->s1 + a->r1;
	  return 1;
	}
      else if ( approximation_type == least_upper_bound && a->s1 >= xcoord )
	{
	  if ( closest == -1 || closest > a->s1 )
	    {
	      closest = a->s1;
	      *tcoord = a->r1;
	    }
	}
      else if ( approximation_type == greatest_lower_bound && a->s2 <= xcoord )
	{
	  if ( closest == -1 || closest < a->s2 )
	    {
	      closest = a->s2;
	      *tcoord = a->s2 - a->s1 + a->r1;
	    }
	}
    }
  if ( closest == -1 )
    return FALSE;
  else 
    return approximation_type;
  
}

int 
get_trace_base_coordinate( Array seqs, ASSINF *b, int coord, int approximation_type, int *tcoord, int *strand )
{
/* 
 finds the coordinate on the trace of b->seq at coordinate coord 
 coord is coordinate in contig space 
 tcoord returns the base cooordinate on the trace 
 if it can't find tccord then it tries to find the closest approximataion from above or below, as given by approximation_type  
 strand is -1 if the trace coordinate is the reverse complement , 1 otherwise 

 return value is 
 exact (1) on success (ie there is a corresponding coord on the trace),
 least_upper_bound (2) or 
 greatest_lower_bound(3) on partial success - ie found nearest approximation
 0 if out of range 
*/

  CAFSEQ *seq;
  ASSINF *a;
  int j;
  int xcoord;
  int closest = -1;

/*  printf("trace_base %d %d %d %d\n", coord, approximation_type, b->read, arrayMax(seqs) ); */

  if ( b && b->seq < arrayMax(seqs) )
    { 
      seq = arrp( seqs, b->seq, CAFSEQ );

/*NOTE this is commented out which may introduce bugs */
/*      if ( ! seq->SCF_File ) 
	return 0; */

      if ( b->s1 < b->s2 ) /* forward strand */
	{
	  *strand = forward_strand;
	  if ( b->s1 <= coord && b->s2 >= coord && seq->assinf )
	    { 
	      xcoord  = coord - b->s1 + b->r1; /* the coord on the strand */

	      for(j=0;j<arrayMax(seq->assinf);j++)
		{
		  a = arrp( seq->assinf, j, ASSINF ); 

		  if ( a->s1 <= xcoord && a->s2 >= xcoord )
		    {
		      *tcoord = xcoord - a->s1 + a->r1;
		      return 1;
		    }
		  else if ( approximation_type == least_upper_bound && a->s1 >= xcoord )
		    {
		      if ( closest == -1 || closest > a->s1 )
			{
			  closest = a->s1;
			  *tcoord = a->r1;
			}
		    }
		  else if ( approximation_type == greatest_lower_bound && a->s2 <= xcoord )
		    {
		      if ( closest == -1 || closest < a->s2 )
			{
			  closest = a->s2;
			  *tcoord = a->s2 - a->s1 + a->r1;
			}
		    }
		}
	    }
	}
      else /* reverse strand */
	{
	  *strand = reverse_strand;
	  if ( b->s2 <= coord && b->s1 >= coord && seq->assinf ) 
	    { 
	      xcoord = b->s1 -coord + b->r1;
/*	      printf(" c+ %d ",xcoord); */
	      for(j=0;j<arrayMax(seq->assinf);j++)
		{
		  a = arrp( seq->assinf, j, ASSINF ); 
/*		  printf(" d%d %d %d", j, a->s1, a->s2 );  */
		  if ( a->s1 <= xcoord && a->s2 >= xcoord )
		    {
		      *tcoord = xcoord - a->s1 + a->r1;
		      /*		  *tcoord = bases- *tcoord + 1; */
/*		      printf( "trace %5d file %-15s coord %5d  rev: %2d  xcoord %5d tcoord %5d\n", b->read, seq->SCF_File, coord, *strand, xcoord, *tcoord   );   */
		      return 1;
		    }
		  else if ( approximation_type == least_upper_bound && a->s1 >= xcoord )
		    {
		      if ( closest == -1 || closest > a->s1 )
			{
			  closest = a->s1;
			  *tcoord = a->r1;
			}
		    }
		  else if ( approximation_type == greatest_lower_bound && a->s2 <= xcoord )
		    {
		      if ( closest == -1 || closest < a->s2 )
			{
			  closest = a->s2;
			  *tcoord = a->s2 - a->s1 + a->r1;
			}
		    }
		}
	    }
	}
    }
/*  printf("\n");  */
  if ( closest == -1 )
    return 0;
  else 
    return approximation_type;
}

int 
isolated_peak( cafAssembly *Ass, int read, int strand, int coord, char base )
/* used to determine whether an isolated deletion is ok */
{
  CAFSEQ *seq = arrp(Ass->seqs,read,CAFSEQ);
  Scf *scf;
  Bases *B;
  Header *h;
  Samples1 *samples1, *s1;
  Samples2 *samples2, *s2;
  int left_window = 0, right_window = 0;
  int left_min, right_min;
  int max;
  int centre = 0;
  int index;
  
  if ((scf = find_scf( Ass, seq )) != 0)
    {
      h = &scf->header;
      
      if ( is_reverse(strand))
	{
	  coord = scf->header.bases - coord+1;
	  complement_scf( scf );
	}
      
      coord--;
      
      B =scf->bases;
      
      if ( coord > 0 && coord < scf->header.bases-1 )
	{
	  if ( base == base_i )
	    {
	    }
	  else
	    {
	      left_window = (B[coord-1].peak_index+B[coord].peak_index)/2;
	      right_window =( B[coord+1].peak_index+B[coord].peak_index)/2;
	      centre = B[coord].peak_index;
	    }
	  left_min = right_min = 100000;
	  max = 0;
	  
	  if ( h->sample_size != 1 )
	    { /* 16 bit */
	      samples2 = scf->samples.samples2;
	      
	      if ( base == base_a )
		{
		  for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak */
		    {
		      if ( max < (int)s2->sample_A )
			{
			  if ( (int)samples2[index-1].sample_A <= (int)s2->sample_A && (int)samples2[index+1].sample_A <= (int)s2->sample_A )
			    {
			      max = (int)s2->sample_A;
			      centre = index;
			    }
			}
		    }
		  left_window = B[coord-1].peak_index;
		  right_window = B[coord+1].peak_index;
		  for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak */
		    {
		      if ( index < centre && (int)s2->sample_A < left_min )
			left_min = (int)s2->sample_A;
		      if ( index > centre && (int)s2->sample_A < right_min )
			right_min = (int)s2->sample_A;
		    }
		}
	      else if ( base == base_c )
		{
		  for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak */
		    {
		      if ( max < (int)s2->sample_C )
			{
			  if ( (int)samples2[index-1].sample_C <= (int)s2->sample_C && (int)samples2[index+1].sample_C <= (int)s2->sample_C )
			    {
			      max = (int)s2->sample_C;
			      centre = index;
			    }
			}
		    }
		  left_window = B[coord-1].peak_index;
		  right_window = B[coord+1].peak_index;
		  for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak */
		    {
		      if ( index < centre && (int)s2->sample_C < left_min )
			left_min = (int)s2->sample_C;
		      if ( index > centre && (int)s2->sample_C < right_min )
			right_min = (int)s2->sample_C;
		    }
		}
	      else if ( base == base_g )
		{
		  for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak */
		    {
		      if ( max < (int)s2->sample_G )
			{
			  if ( (int)samples2[index-1].sample_G <= (int)s2->sample_G && (int)samples2[index+1].sample_G <= (int)s2->sample_G )
			    {
			      max = (int)s2->sample_G;
			      centre = index;
			    }
			}
		    }
		  left_window = B[coord-1].peak_index;
		  right_window = B[coord+1].peak_index;
		  for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak */
		    {
		      if ( index < centre && (int)s2->sample_G < left_min )
			left_min = (int)s2->sample_G;
		      if ( index > centre && (int)s2->sample_G < right_min )
			right_min = (int)s2->sample_G;
		    }
		}
	      else if ( base == base_t )
		{
		  for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak */
		    {
		      if ( max < (int)s2->sample_T )
			{
			  if ( (int)samples2[index-1].sample_T <= (int)s2->sample_T && (int)samples2[index+1].sample_T <= (int)s2->sample_T )
			    {
			      max = (int)s2->sample_T;
			      centre = index;
			    }
			}
		    }
		  left_window = B[coord-1].peak_index;
		  right_window = B[coord+1].peak_index;
		  for(index = left_window,s2=&samples2[left_window];index<=right_window;index++,s2++) /* find max peak */
		    {
		      if ( index < centre && (int)s2->sample_T < left_min )
			left_min = (int)s2->sample_T;
		      if ( index > centre && (int)s2->sample_T < right_min )
			right_min = (int)s2->sample_T;
		    }
		}
	      else
		return -1;

	      if ( right_min > left_min )
		return (max - right_min);
	      else
		return (max - left_min);
	    }
	  else
	    { /* 8 bit */
	      samples1 = scf->samples.samples1;
	      
	      if ( base == base_a )
		{
		  for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak */
		    {
		      if ( max < (int)s1->sample_A )
			{
			  if ( (int)samples1[index-1].sample_A <= (int)s1->sample_A && (int)samples1[index+1].sample_A <= (int)s1->sample_A )
			    {
			      max = (int)s1->sample_A;
			      centre = index;
			    }
			}
		    }
		  left_window = B[coord-1].peak_index;
		  right_window = B[coord+1].peak_index;
		  for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak */
		    {
		      if ( index < centre && (int)s1->sample_A < left_min )
			left_min = (int)s1->sample_A;
		      if ( index > centre && (int)s1->sample_A < right_min )
			right_min = (int)s1->sample_A;
		    }
		}
	      else if ( base == base_c )
		{
		  for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak */
		    {
		      if ( max < (int)s1->sample_C )
			{
			  if ( (int)samples1[index-1].sample_C <= (int)s1->sample_C && (int)samples1[index+1].sample_C <= (int)s1->sample_C )
			    {
			      max = (int)s1->sample_C;
			      centre = index;
			    }
			}
		    }
		  left_window = B[coord-1].peak_index;
		  right_window = B[coord+1].peak_index;
		  for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak */
		    {
		      if ( index < centre && (int)s1->sample_C < left_min )
			left_min = (int)s1->sample_C;
		      if ( index > centre && (int)s1->sample_C < right_min )
			right_min = (int)s1->sample_C;
		    }
		}
	      else if ( base == base_g )
		{
		  for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak */
		    {
		      if ( max < (int)s1->sample_G )
			{
			  if ( (int)samples1[index-1].sample_G <= (int)s1->sample_G && (int)samples1[index+1].sample_G <= (int)s1->sample_G )
			    {
			      max = (int)s1->sample_G;
			      centre = index;
			    }
			}
		    }
		  left_window = B[coord-1].peak_index;
		  right_window = B[coord+1].peak_index;
		  for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak */
		    {
		      if ( index < centre && (int)s1->sample_G < left_min )
			left_min = (int)s1->sample_G;
		      if ( index > centre && (int)s1->sample_G < right_min )
			right_min = (int)s1->sample_G;
		    }
		}
	      else if ( base == base_t )
		{
		  for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak */
		    {
		      if ( max < (int)s1->sample_T )
			{
			  if ( (int)samples1[index-1].sample_T <= (int)s1->sample_T && (int)samples1[index+1].sample_T <= (int)s1->sample_T )
			    {
			      max = (int)s1->sample_T;
			      centre = index;
			    }
			}
		    }
		  left_window = B[coord-1].peak_index;
		  right_window = B[coord+1].peak_index;
		  for(index = left_window,s1=&samples1[left_window];index<=right_window;index++,s1++) /* find max peak */
		    {
		      if ( index < centre && (int)s1->sample_T < left_min )
			left_min = (int)s1->sample_T;
		      if ( index > centre && (int)s1->sample_T < right_min )
			right_min = (int)s1->sample_T;
		    }
		}
	      else
		return -1;

	      if ( right_min > left_min )
		return (max - right_min) * SCF_16BIT_BODGE_FACTOR;
	      else
		return (max - left_min) * SCF_16BIT_BODGE_FACTOR;
	    }
	}
    }
  return -1;
}

Scf *
find_scf( cafAssembly *Ass, CAFSEQ *seq )

/* tries to find the scf file corresponding to a sequence If the
SCF_File is set then it tries to use that. On failure it tries to use
the read name. If it succeeds then seq->SCF_File becomes instantiated
 */

{
  Scf *scf=NULL;

  if ( seq )
    {
      if ( ! ( seq->SCF_File && (scf = get_scf( seq->SCF_File ) ) ) )
	{
	  if ((scf = get_scf( dictName(Ass->seqDict,seq->id) )) != 0)
	    {
	      seq->SCF_File = (char*)messalloc(1+strlen(dictName(Ass->seqDict,seq->id) ) );
	      strcpy(seq->SCF_File,dictName(Ass->seqDict,seq->id) );
	    }
	}
    }

  return scf;
}

/* 
 * $Log$
 * Revision 1.5  2005/06/16 11:24:39  rmd
 * Moved log.
 *
 * Revision 1.4  2002/03/04 17:00:44  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.3  1998/04/28  14:05:03  rmd
 * Added support for 16 bit SCF files
 *
 * Revision 1.2  1998/01/12  16:48:29  badger
 * Fixed two array access problems in trace_edit
 *
 * Revision 1.1  1997/07/09  16:22:19  badger
 * Initial revision
 *
 * Revision 1.4  1996/10/03  17:45:01  rmott
 * *** empty log message ***
 *
 * Revision 1.3  1996/02/21  11:32:03  rmott
 * CAF version
 * */
