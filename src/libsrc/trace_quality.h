/*  Last edited: Jun 25 11:37 2001 (rmd) */
/* 
   go through an assembly and use trace information to determine whether
   to make an edit
*/

void trace_edit(contigAlignment *Align, cafAssembly *Ass );

/*
  evaluate the trace quality tq struct for a read at a particular position
*/

int  get_trace_quality(cafAssembly *Ass, int read, int strand, int coord,
		       traceQuality *tq, int base);


/* return the trace quality for a read at a given coord for a window of
   bases either side 

   returns TRUE if the window is in range and FALSE otherwise

   The struct tq returns the trace quality info, with fields:

   q = peak height ratio for the central base
   s = peak separation ratio for the central base
   r = peak height ratio for the preferred base 
*/

int trace_quality( Scf *scf, int coord, traceQuality *tq, int base );


/* returns the difference in height between the min value of a trace
   between two bases and the lower of the two endpoints. Thus if a trace
   dips below the endpoints the function returns -ve.
   
   We can use this to determine if there is an overcall eg when AA has
   been called in place of A and where the trace is a simple one-humped
   curve */

int possible_overcall(cafAssembly *Ass, int read, int strand, int coord,
		      int direction, int base);

/* count the number of local maxima (peaks) for read in direction
   from coord for base in range */

int peak_count(cafAssembly *Ass, int read, int strand, int coord,
	       int direction, int base, int range, int min_width);

int peak_count1( Scf *scf, int start, int stop, int base, int min_width );

/* used to determine the consensus base in cases where the ABI calls
   conflict */

void accumulate_votes( cafAssembly *Ass, int read, int strand, int tcoord,
		       float *votes );

/*  gets the trace coordinate corresponding to an alignData struct */

int get_trace_coordinate_from_ad( cafAssembly *Ass, alignData *ad,
				  int approximation_type, int *tcoord,
				  int *strand );

/* 
 finds the coordinate on the trace of b->seq at coordinate coord 
 coord is coordinate in contig space 
 tcoord returns the base cooordinate on the trace 
 if it can't find tccord then it tries to find the closest approximataion
 from above or below, as given by approximation_type  
 strand is -1 if the trace coordinate is the reverse complement , 1 otherwise 

 return value is 
 exact (1) on success (ie there is a corresponding coord on the trace),
 least_upper_bound (2) or 
 greatest_lower_bound(3) on partial success - ie found nearest approximation
 0 if out of range 
*/
int get_trace_base_coordinate( Array seqs, ASSINF *b, int coord,
			       int approximation_type, int *tcoord,
			       int *strand );

/* used to determine whether an isolated deletion is ok */

int isolated_peak( cafAssembly *Ass, int read, int strand, int coord,
		   char base );

/* tries to find the scf file corresponding to a sequence If the
   SCF_File is set then it tries to use that. On failure it tries to use
   the read name. If it succeeds then seq->SCF_File becomes instantiated
*/

Scf *find_scf( cafAssembly *Ass, CAFSEQ *seq );

