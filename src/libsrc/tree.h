/*  Last edited: Jun 13 18:19 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Jul  7 10:44 1995 (rmott) */

#ifndef _TREE_H_
#define _TREE_H_

#include <seq_util.h> /* for SEQUENCE */

typedef struct
{
  int *hit;
  int hits;
  int folded;
  int sorted;
} WORD_DICT;

typedef struct
{
  int size;
  int *init;
  int len;
  int *next;
}
WORD_LIST;

typedef struct
{
  int start;
  int stop;
  int *h;
  float p;
}
DICT_HIST;

typedef struct 
{
  int wordsize;
  int tablesize;
  int actualsize;
  short **wordlist;
  int *counts;
  int *id;
}
TRANSPOSE;

int translate(char c);
/* SEQUENCE *generate_sequence(); */
TRANSPOSE  *transpose_dictionaries(WORD_DICT **dicts, int seqs, int wordsize);

WORD_DICT *make_dictionary_for_sequence( SEQUENCE *seq, int wordsize, int *exclude, int fold, int sort );

void free_dict(WORD_DICT *d);
int legal_word( char *s, int wordsize );
void free_dict_hist( DICT_HIST *dh );
DICT_HIST *create_dict_histogram( WORD_LIST *wl1, WORD_LIST *wl2 );
DICT_HIST *create_dict_histogram2( WORD_DICT *d, WORD_LIST *wl2, int thresh );
void free_word_list( WORD_LIST *wl );
WORD_LIST *create_word_list( WORD_DICT *dict, int size );

char * int2word(int w, int wordsize);

#endif
