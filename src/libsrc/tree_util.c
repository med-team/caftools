/*  Last edited: Jun 13 17:13 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

/* $Id: tree_util.c 13587 2004-11-25 13:48:46Z rmd $ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <seq_util.h>
#include <tree.h>
#include <cmp.h>
#include <cl.h>

int icomplement( int w, int wordsize, int fold);

void free_dicts(WORD_DICT ** dictionaries, int tablesize)
{
  int n;

  for(n = 0; n < tablesize; n++)
    {
      free(dictionaries[n]->hit);
    }

  free(dictionaries);
}

void free_dict( WORD_DICT *d )
{
  free(d->hit);
  free(d);
}

int 
legal_word( char *s, int wordsize )
{
  int len = 0;
  int m = 1, t;
  int w = 0;

  while( len < wordsize && *s )
    { 
      if ( (t = translate(*s)) >= 0 )
	{
	  w += t*m;
	  m *= 4;
	  len++;
	  s++;
	}
      else
	return -1;
    }
  return w;
}

char *
get_word( char *s, int wordsize, int *w )

{
  int len = 0;
  int m = 1, t;
  *w = 0;

  while( len < wordsize && *s )
    { 
      if ( (t = translate(*s)) >= 0 )
	{
	  *w += t*m;
	  m *= 4;
	  len++;
	}
      else 
	{
	  m = 1;
	  *w = 0;
	  len = 0;
	}
      s++;
    }
  
/*  if ( ! *s )
    *w = -1; */

  return s;
}

char * int2word(int w, int wordsize)
{
  if ( w < 0 )
    return NULL;
  else
    {
      char *s = (char*)calloc(wordsize+1,sizeof(char));
      int u, n;
      for(n=0;n<wordsize;n++)
	{
	  u = w-4*(w/4);
	  if ( u == 0 )
	    s[n] = 'a';
	  else if ( u == 1 )
	    s[n] = 'c';
	  else if ( u == 2 )
	    s[n] = 'g';
	  else if ( u == 3 )
	    s[n] = 't';
	  else 
	    s[n] = 'n';
	  w = w/4;
	}
      s[wordsize] = '\0';
      return s;
    }
}

WORD_DICT *
make_dictionary_for_sequence( SEQUENCE *seq, int wordsize, int *exclude, int fold, int sort )

/* makes a word dictionary from a sequence.

wordsize is the length of the words, so the dictionay is of size 4^wordsize
exclude is an array of ints representing words to be excluded
fold is a boolean indicating whether to represent both strands
sort is a boolean indicating whther to return the words sorted and with duplicates removed

*/
{
  int n, w, m, t, wc=0;
  WORD_DICT *dict;
  char *s = seq->s;

  m = 1;
  for(n=1;n<wordsize;n++)
    m *= 4;

  dict = (WORD_DICT*)malloc(sizeof(WORD_DICT));
  dict->folded = fold;
  dict->sorted = sort;

  if ( sort )
    {
      int *temp = (int*)calloc(seq->len,sizeof(int));

      s = get_word( s, wordsize, &w );
      
      if ( *s )
	if ( exclude && ! exclude[w] )
	  temp[wc++] = icomplement(w,wordsize,fold);
      
      while(*s)
	{
	  if ( ( t = translate(*s++) ) >= 0 )
	    {
	      temp[wc++] = icomplement(w =  w/4 + t*m, wordsize,fold);
	    }
	  else 
	    {
	      if ((s = get_word( s, wordsize, &w )) != 0)
		temp[wc++]= icomplement(w,wordsize,fold);
	    }
	}
      qsort(temp, wc, sizeof(int), icmp);
      
      m=0;
      if ( temp[0] >= 0 )
	m++;
      for(n=1;n<wc;n++)
	if( temp[n] != temp[n-1] && temp[n] >= 0)
	  temp[m++] = temp[n];

      dict->hit = (int*)calloc(m,sizeof(int));

      for(n=0;n<m;n++)
	dict->hit[n] = temp[n];
      dict->hits = m;

      free(temp);

    }
  else
    {
      int w1 = wordsize-1;

      dict->hit = (int*)calloc(seq->len-w1,sizeof(int));
      w = -1;

      for(wc=0,s=seq->s;wc<seq->len-w1;wc++,s++)
	{
	  if ( w >= 0 )
	    {
	      if ( (t = translate(s[w1]) ) >= 0 )
		w =  w/4 + t*m;
	      else 
		w = -1;
	    }
	  else
	    w = legal_word(s, wordsize );

	  dict->hit[wc] = icomplement(w,wordsize,fold);
/*	  printf("%5d %5d\n", wc, dict->hit[wc]);*/
	}
      dict->hits = wc;
    }

  return dict;
}

int translate(char c)
  {
    c = tolower((int) c);

    if ( c == 'a' )
      return 0;
    else if ( c == 'c' )
      return 1;
    else if ( c == 'g' )
      return 2;
    else if ( c == 't' || c == 'u' )
      return 3;
    else
      return -1;
  }

#if 0
/* Removed due to use of rand3 */
SEQUENCE *
generate_sequence( len, id, name, seed )

int len, id, *seed;
char *name;
{
  SEQUENCE *seq = (SEQUENCE*)malloc(sizeof(SEQUENCE));
  float x;
  char buf[256];

  seq->len = len;
  seq->s = (char*)calloc(len+1,sizeof(char));
  sprintf( buf, "%s_%d", name, id );
  seq->name = (char*)strdup(buf);

  seq->s[len] = NULL;

  while(len--)
    {
      x = rand3(seed);
      if ( x < 0.25 )
	seq->s[len] = 'a';
      else if ( x < 0.50 )
	seq->s[len] = 'c';
      else if ( x < 0.75 )
	seq->s[len] = 'g';
      else
	seq->s[len] = 't';
    }

  return seq;
}
#endif

TRANSPOSE * transpose_dictionaries(WORD_DICT **dicts, int seqs, int wordsize )
{
  int tablesize=1, w, n, m, *temp;
  short **wordlist;
  int *pointers;
  int *counts, *id;
  TRANSPOSE *t;

  for(n=0;n<wordsize;n++)
    tablesize *= 4;

  wordlist = (short**)calloc( tablesize, sizeof(short*));
  pointers = (int*)calloc( seqs, sizeof(int));
  counts = (int*)calloc( tablesize, sizeof(int));
  id = (int*)calloc( tablesize, sizeof(int));


  for(n=0;n<seqs;n++)
    pointers[n] = 0;

  temp = (int*)calloc(seqs,sizeof(int));

  for(w=0;w<tablesize;w++)
    { 
      m = 0;
      for(n=0;n<seqs;n++)
	{
	  if( dicts[n]->hit[pointers[n]] == w )
	    {
	      temp[m++] = n;
	      if ( pointers[n] < dicts[n]->hits )
		pointers[n]++;
	    }
	}
      wordlist[w] = (short*)calloc(m, sizeof(short));
      counts[w] = m;
      while(m--)
	wordlist[w][m] = temp[m];
    }

  free(temp);

  t = (TRANSPOSE*)malloc(sizeof(TRANSPOSE));
  t->wordlist = wordlist;
  t->counts = counts;
  t->wordsize = wordsize;
  t->tablesize = tablesize;
  t->id = id;
  return t;
}

void write_dictionaries(int argc, char **argv, WORD_DICT **dicts,
			int seqs, int wordsize)
{
  FILE *fp;
  char filename[256];

  if ((fp = argfile("-write_dicts=%s", "w", argc, argv, filename )) != 0)
    {
      printf("! writing dictionaries to file %s\n", filename );
      putw( seqs, fp );
      putw( wordsize, fp );

/* unfinished */

    }
}
	

int 
icomplement( int w, int wordsize, int fold )

{
  static int *comp;

  if ( fold &&  ! comp )
    {
      int i, j;
      char *s;
      int w4=1;

      for(i=1;i<=wordsize;i++)
	w4 *= 4;
      comp = (int*)calloc(w4,sizeof(int));
      for(i=0;i<w4;i++)
	{
	  s = int2word(i, wordsize);
/*	  printf("%s %8d ", s, i );*/
	  complement_seq(s);
	  get_word( s, wordsize, &j );
	  comp[i] = ( i < j ? i : j );
/*	  printf("%s %8d %8d\n", s, j, comp[i] );*/
	  free(s);
	}
    }

  if ( fold && w >= 0 )
    return comp[w];
  else
    return w;
}
	  

	  


		
WORD_LIST *create_word_list( WORD_DICT *dict, int size )
{
  WORD_LIST *wl = (WORD_LIST*)calloc(1,sizeof(WORD_LIST));
  int i, w;
  int *last;

  wl->size = size;
  wl->init = (int*)calloc(size,sizeof(int));
  wl->len = dict->hits;
  wl->next = (int*)calloc(dict->hits,sizeof(int));

  last = (int*)calloc(size,sizeof(int));

  for(i=0;i<size;i++)
    wl->init[i] = -1;

  for(i=0;i<dict->hits;i++)
    wl->next[i] = -1;

  for(i=0;i<dict->hits;i++)
    {
      w = dict->hit[i];
      if ( w >= 0 )
	{
	  if ( wl->init[w] == -1 )
	    {
	      wl->init[w] = i;
	      last[w] = i;
	    }
	  else
	    {
	      wl->next[last[w]] = i;
	      last[w] = i;
	    }
	}
    }

  free(last);

  return wl;
}

void free_word_list( WORD_LIST *wl )
{
  free(wl->init);
  free(wl->next);
  free(wl);
}

DICT_HIST *create_dict_histogram2( WORD_DICT *wd1, WORD_LIST *wl2, int thresh )

/* create a word histogram from a WORD_DICT and a WORD_LIST 
(so this is suitable for screening a db sequence vs a preprocessed query).
If thresh > 0 the function does a fast check that the total number of
distinct matching words is > thresh. If not then the histogram is not
created and NULL returned */

{
  int i1, i2, w, n=0;
  DICT_HIST *dh;

  if ( thresh > 0 )
    {
      int matches=0;
      for(i1=0;i1<wd1->hits;i1++)
	{
	  w = wd1->hit[i1];
	  if ( w >= 0 )
	    matches += (wl2->init[w]>=0);
	}
      if ( matches <= thresh )
	return NULL;
    }

  dh = (DICT_HIST*)calloc(1,sizeof(DICT_HIST));

  dh->start = -wl2->len;
  dh->stop = wd1->hits;
  dh->h = (int*)calloc(dh->stop-dh->start+1,sizeof(int))-dh->start;

  for(i1=0;i1<wd1->hits;i1++)
    {
      w = wd1->hit[i1];
      if ( w >= 0 )
	{
	  i2 = wl2->init[w];
	  while ( i2 >= 0 )	  
	    {
	      dh->h[i1-i2]++;
	      i2 = wl2->next[i2];
	      n++;
	    }
	}
      dh->p += n;
    }
  return dh;
}

void free_dict_hist( DICT_HIST *dh )
{
  free(dh->h+dh->start);
  free(dh);
}

DICT_HIST *create_dict_histogram( WORD_LIST *wl1, WORD_LIST *wl2 )
{
  int i1, i2, w, n=0;
  DICT_HIST *dh = (DICT_HIST*)calloc(1,sizeof(DICT_HIST));

  dh->start = -wl2->len;
  dh->stop = wl1->len;
  dh->h = (int*)calloc(dh->stop-dh->start+1,sizeof(int))-dh->start;
  for(w=0;w<wl1->size;w++)
    {
      i1 = wl1->init[w];
      while( i1 >= 0 )
	{
	  i2 = wl2->init[w];
	  while ( i2 >= 0 )	  
	    {
	      dh->h[i1-i2]++;
	      i2 = wl2->next[i2];
	      n++;
	    }
	  i1 = wl1->next[i1];
	}
      dh->p += n;
    }
  return dh;
}





