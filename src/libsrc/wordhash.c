/*  Last edited: Jun 13 18:17 2001 (rmd) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <wordhash.h>
#include <tree.h>

int skip_comments(FILE * fp, char *string ); /* from readline.c */

/* functions to read in a hash table of words */


int * 
read_ignore_hash_table( FILE *fp, int *wordsize )

/* read in a frquency table of common words and creat a hash table */
{
  char line[256];
  char word[245];
  int *table = (int*)calloc(TABLESIZE,sizeof(int));
  int collisions = 0;
  int entries = 0;
  int w;
  int Wordsize;

  while( skip_comments( fp, line ) != EOF )
    {
      if ( sscanf( line, "%s", word ) == 1 )
	{
	  Wordsize = strlen(word);
	  if ( *wordsize == -1 )
	    *wordsize = Wordsize;

	  if ( *wordsize != Wordsize )
	    {
	      fprintf(stderr,"ERROR - inconsistent wordsizes %d %d %s\n", Wordsize, *wordsize, word );
	      exit(1);
	    }

	  w = legal_word( word, *wordsize );
	  collisions += enter_word( w, *wordsize, table );
	  if ( ! findEntry( w, table )  )
	    {
	      fprintf(stderr, "ERROR, hash failed to relocate %d\n", w );
	    }
	  entries++;
	}
    }

  fprintf(stderr, "hash table entries: %d collisions: %d size: %d\n", entries, collisions, TABLESIZE );
  return table;
}

int 
enter_word( int w, int wordsize, int *table)
{
  int  n, N;
  int collision=0;
  
  w++;
  n = HASH(w);

/*   printf("enter %20d %20d\n", w, n );  */

  if ( table[n] == 0 )
    table[n] = w;
  else
    {
      collision=1;
      N = (n+1)%TABLESIZE;
      while( (N != n) && table[N] != 0 && table[N] != w)
	N = (N+1)%TABLESIZE;
      if ( N == n ) 
	{
	  fprintf(stderr, "ERROR - hash table is full\n");
	  exit(1);
	}
      else if ( table[N] == 0 )
	table[N] = w;
    }
  return collision;
}

int 
findEntry( int w, int *table )
{
  
  int n;

  w++;
  n = HASH(w);

/*  fprintf(stderr, "find %15d %5d %15d\n", w, n, table[n] ); */

  if ( table[n] == 0 ) return 0;
  else if ( table[n] == w ) return 1;
  else
    {
      int N = (n+1)%TABLESIZE;

      while( (table[N] != 0) && (table[N] != w) && (n != N ) )
	N = (N+1)%TABLESIZE;

      if ( table[N] == w )
	return 1;
      else
	return 0;
    }
}
