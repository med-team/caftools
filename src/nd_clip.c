/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 *  Last edited: Sep 23 15:18 1996 (rmott)
 * $Id: nd_clip.c 11038 2003-07-25 14:54:57Z dgm $
 * $Log$
 * Revision 1.4  2003/07/25 14:54:57  dgm
 * Updated comments to remove nested comments.
 *
 * Revision 1.3  2002/05/31 11:12:43  mng
 * Added caf2ace
 *
 * Revision 1.2  2002/03/04 17:00:20  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 * Revision 1.1  1997/04/03  13:44:09  badger
 * Initial revision
 *
 * Revision 1.1  1996/10/03  17:22:54  rmott
 * Initial revision
 *
 */

#define MAIN

#include<stdio.h>
#include<string.h>
#include<cl.h>
#include<caf.h>
#include"cafseq.h"
/* #include"scf.h" */



int 
main(int argc, char **argv)
{
  char CAFfile[256];
  char clip_type[256];
  cafAssembly *CAF;
  int n, i, clip_id=0;
  int fail=1;     /* delete failures */
  int warn=0;     /* print warnings */
  int svector=0;  /* clip sequencing vector */
  int cvector=0;  /* clip cloning vector */
  char **tags;
  int ntags=0;
  Array ids;
  FILE *logfp=NULL;
  char logfile[256], outfile[256];

  strcpy(clip_type,"");

  getarg("-clip",clip_type,argc,argv);
  tags = split_on_separator(clip_type,',',&ntags); /* get the comma-separated list of tags */
  getboolean("-svector",&svector,argc,argv);
  getboolean("-cvector",&cvector,argc,argv);
  getboolean("-fail",&fail,argc,argv);
  getboolean("-warn",&warn,argc,argv);
  logfp = argfile("-logfile","w",argc,argv,logfile);
  CAF = readCAF( argc, argv, CAFfile );
  getarg("-out", outfile,argc,argv);
  gethelp(argc,argv);

  if ( CAF )
    {

      ids = arrayCreate(ntags,int);
      
      for(i=0;i<ntags;i++)
	{
	  if ( dictFind(CAF->tagTypeDict,tags[i],&clip_id) )
	    *arrayp(ids,arrayMax(ids),int) = clip_id;
	}

      if ( requireUnpadded(CAF) )
	{
	  n = do_clip_assembly( CAF, ids, cvector, svector, fail, warn, logfp );
	  fprintf(stderr," %d clip operations\n", n );
	}
      else
	{
	  fprintf(stderr, "ERROR: d_clip: Could not depad assembly %s \n", CAFfile);
	  exit(1);
	}

      sanitiseCAF(CAF);
      writeCAF( CAF, CAFfile, argc, argv );
      if ( logfp )
	fclose(logfp);
      return 0;
    }
  return 1;
}
