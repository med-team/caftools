/*  Last edited: Mar  6 12:30 2002 (mng) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Oct  4 15:31 1996 (rmott) */
/* $Id: nd_edit.c 15163 2005-06-16 12:38:26Z rmd $ */

#define MAIN

#include<stdio.h>
#include<ctype.h>
#include"cl.h"
#include"caf.h"

int
main(int argc, char **argv)
{
  CAFSEQ *seq;
  FILE *fp;
  int i, j;
  ASSINF *a;
  cafAssembly *CAF;
  char acefile[256];
  char outfile[256];
  int verbose = 0;
  Array edited_seqs;
  int skip = TRUE;
  int show_case = TRUE;

  getboolean("-verbose",&verbose,argc,argv);
  getboolean("-skip",&skip,argc,argv); /* do not output missaligned reads */
  getboolean("-show_case",&show_case,argc,argv); /* show where edits have been made by case in contigs */
  strcpy(acefile,"stdin");
  CAF = readCAF( argc, argv, acefile );
  (fp = argfile("-out","w",argc,argv, outfile) ) || (fp=stdout) ;
  gethelp(argc,argv);

  if ( CAF )
    {
      edited_seqs = arrayCreate(arrayMax(CAF->seqs),EDITED_CAFSEQ*);
      arrayp(edited_seqs,arrayMax(CAF->seqs)-1,EDITED_CAFSEQ*);

      for(i=0;i<arrayMax(CAF->seqs);i++)
	{
	  seq = arrp( CAF->seqs, i, CAFSEQ );
	  if ( seq->type == is_contig && seq->assinf )
	    {
	      if (verbose)
		printf("\n\n ***** %s ***** \n\n", dictName(CAF->seqDict, i ) );
	      for(j=0;j < arrayMax(seq->assinf); j++)
		{
		  a = arrp(seq->assinf, j, ASSINF) ;
		  a->s = ass_subseq( CAF->seqs, a );
		}
	    }
	  if ( seq->type == is_read && seq->assinf )
	    {
	      if (verbose)
		printf("%s bad: %d skip: %d\n", dictName(CAF->seqDict, i ), seq->bad, skip );
	      if ( skip == FALSE || seq->bad == FALSE )
		*arrp(edited_seqs,i,EDITED_CAFSEQ*) = edit_read( CAF, seq );
	    }
	}

      for(i=0;i<arrayMax(CAF->seqs);i++)
	{
	  seq = arrp( CAF->seqs, i, CAFSEQ );
	  if ( seq->type == is_contig )
	    *arrp(edited_seqs,i,EDITED_CAFSEQ*) = edit_contig( CAF, seq, edited_seqs, skip, show_case );
	}

      write_edited( CAF, edited_seqs, skip, fp );
      fclose(fp);

      return 0;
    }
  return 1;
}


EDITED_CAFSEQ *
edit_read( cafAssembly *Ass, CAFSEQ *seq )
{
  Array new_dna;
  char *dna;
  int i, j, k = 0, len, newlen;
  edit_struct *edit;
  Array new2old;
  Array old2new;
  Array old2lower;
  Array new2lower;
  ASSINF *a;
  int k1, read = 0;
  Array new_assinf;
  ASSINF entry;
  EDITED_CAFSEQ *edited_seq=NULL;
  int debugging;

  if ( seq->type == is_read && seq->bad == FALSE )
    {
      dna = stackText(seq->dna,0);
      len = strlen(dna);
      edited_seq = (EDITED_CAFSEQ*)messalloc(sizeof(EDITED_CAFSEQ));

      if ( seq->assinf )
	{
	  /*debugging = !strcmp(dictName(Ass->seqDict, seq->id ),"fa69o19.s1");*/
	  debugging = FALSE;
	  if ( debugging ) 
	    fprintf(stderr,"read %s %d\n", dictName(Ass->seqDict, seq->id ),len);   

	  old2lower = arrayCreate( len + 50 , I_SIZE );
	  edited_seq->new2old = new2old = arrayCreate( len + 50 , I_SIZE );
	  edited_seq->new2lower = new2lower = arrayCreate( len + 50 , I_SIZE );
	  edited_seq->old2new = old2new = arrayCreate( len + 50 , I_SIZE );
	  edited_seq->new_assinf = new_assinf = arrayCreate( arrayMax(seq->assinf)+5, ASSINF );
	  edited_seq->new_dna = new_dna = arrayCreate( len+50, char );
	  edited_seq->seq = seq;
	  edited_seq->id = seq->id;

	  *arrayp(old2lower,len-1,I_SIZE) = -1;
	  for(i=0;i<len;i++)
	    *arrp(old2lower,i,I_SIZE) = -1;

	  for(i=0;i<arrayMax(seq->assinf);i++)
	    {
	      a = arrp(seq->assinf, i, ASSINF );
	      read = a->seq;
	      if ( debugging )
		fprintf(stderr,"%5d %5d %5d %5d\n", a->r1, a->r2, a->s1, a->s2);  
	      arrayp(old2lower,a->s2,I_SIZE);
	      for(j=a->s1,k=a->r1;j<=a->s2;j++,k++)
		*arrp(old2lower,j,I_SIZE) = k;
	    }

	  *arrayp(old2new,len-1,I_SIZE) = 1;
	  for(i=1;i<=len;i++)
	    *arrp(old2new, i, I_SIZE ) = 1;

	  if ( seq->edit )
	    {
	      for(i=0;i<arrayMax(seq->edit);i++)
		{
		  edit = arrp(seq->edit,i,edit_struct);
		  edit->old = dna[edit->pos-1];
		  if ( edit->consensus == Strong )
		    {
		      if ( edit->status == deletion )
			{
/*			  printf("deletion at %d\n", edit->pos );  */
			  *arrayp(old2new,edit->pos,I_SIZE) = 0;
			}
		      else if ( edit->status == insert || edit->status == replace )
			dna[edit->pos-1] = edit->New;
		      
		      edit->done = TRUE;
		    }
		}
	    }
	  for(i=2;i<=len;i++)
	    *arrp(old2new,i,I_SIZE) += arr(old2new,i-1,I_SIZE); /* old2new[i] = where i maps to in new sequence (old2new) */
	  newlen = arr(old2new,len,I_SIZE);
	  
	  if ( debugging )
	    {
	      fprintf(stderr, "newlen = %d\n", newlen );
	      for(i=1;i<=len;i++)
		fprintf(stderr, "%5d -> %5d\n", i, arr(old2new,i,I_SIZE));
	    }
	      
	  for(i=len;i>0;i--)
	    {
	      k = arr(old2new,i,I_SIZE);
	      *arrayp(new2old,k,I_SIZE) = i;
	    }

	  if ( seq->edit )
	    {
	      for(i=0;i<arrayMax(seq->edit);i++)
		{
		  edit = arrp(seq->edit,i,edit_struct);
		  for(j=edit->pos;j>0;j++)
		    if ( (k=arr(old2new,j,I_SIZE)) > 0 )
		      break;
		  if ( k < 1 )
		    for(j=edit->pos;j<arrayMax(old2new);j++)
		      if ( (k=arr(old2new,j,I_SIZE)) > 0 )
			break;
		  if ( edit->status == deletion && edit->done == TRUE && dna[edit->pos-1] == padding_char )
		    {
		      edit->pos = 0;
		    }
		  else
		    edit->pos = k;
		}
	    }


	  arrayp(new2lower,len,I_SIZE);
	  for(i=1;i<=len;i++)
	    {
	      *arrp(new2lower,i,I_SIZE) = -1;
	      k = arr(new2old,i,I_SIZE);
	      if ( k >= 1 )
		{
		  j = arr(old2lower,k,I_SIZE);
		  if ( j >= 1 )
		    *arrp(new2lower,i,I_SIZE) = j;
		}
	    }

	  
	  if ( debugging )
	    {
	      fprintf(stderr, "newlen = %d\n", newlen );
	      for(i=1;i<=len;i++)
		fprintf(stderr, "%5d ---> %5d\n", i, arr(new2lower,i,I_SIZE));
	    }
	      

	  k1 = k = arr(new2lower,1,I_SIZE);
	  entry.r1 = entry.s1 = 1;

	  for(i=2;i<=len /* && k1<=len */;i++)
	    {
	      k1 = arr(new2lower,i,I_SIZE);
	      if ( k1 != -1 && entry.r1 == -1 )
		{
		  entry.r1 = k1;
		  entry.s1 = i;
		}
	      if ( debugging )
		fprintf(stderr,"k=%d k1=%d i=%d\n", k, k1, i );
	      if (( k1 == -1 )|| (k1 - k != 1 ) )
		{
		  entry.r2 = k;
		  entry.s2 = i-1;
		  if ( entry.r1 > 0 && entry.r2 > 0)
		    {
		      a = arrayp(new_assinf,arrayMax(new_assinf),ASSINF);
		      a->r1 = entry.r1;
		      a->r2 = entry.r2;
		      a->s1 = entry.s1;
		      a->s2 = entry.s2;
		      a->seq = read;
		    }
		  if ( k1 != -1 )
		    {
		      entry.r1 = k1;
		      entry.s1 = i;
		    }
		  else
		      entry.r1 = -1;
		}
	      k = k1;
	    }

	  if ( debugging )
	    fprintf(stderr,"k=%d k1=%d i=%d\n", k, k1, i );
	  entry.r2 = k;
	  entry.s2 = i-1;
	  if ( entry.r1 > 0 && entry.r2 > 0)
	    {
	      a = arrayp(new_assinf,arrayMax(new_assinf),ASSINF);
	      a->r1 = entry.r1;
	      a->r2 = entry.r2;
	      a->s1 = entry.s1;
	      a->s2 = entry.s2;
	      a->seq = read;
	    }
	
	  *arrayp(new_dna,newlen,char) = NULL;
	  for(i=0;i<newlen;i++)
	    {
	      k = arr(new2old,i+1,I_SIZE);
	      *arrp(new_dna,i,char) = dna[k-1];
	    }

	  adjust_tags1( seq,old2new);

	  adjust_quality1(seq,old2new,newlen,0);
	  adjust_position1(seq,old2new,newlen,-1);
	  arrayDestroy(old2lower); 
/*	  arrayDestroy(old2new); */
	}
    }

  return edited_seq;
}


EDITED_CAFSEQ *
edit_contig( cafAssembly *Ass, CAFSEQ *contig, Array edited_seqs, int skip, int show_case )
{
  Array new_dna;
  char *dna;
  int i, j, k, len, newlen;
  edit_struct *edit;
  Array new2old;
  Array old2new;
  Array old2lower;
  Array new2lower;
  ASSINF *a, *b;
  Array new_assinf;
  EDITED_CAFSEQ *edited_contig=NULL, *edited_read;
  int read;
  CAFSEQ *seq2;

  if ( contig->type == is_contig )
    {
      dna = stackText(contig->dna,0);
      len = strlen(dna);
      edited_contig = (EDITED_CAFSEQ*)messalloc(sizeof(EDITED_CAFSEQ));

      if ( show_case )
	{
	  for(i=0;i<len;i++)
	    dna[i] = toupper((int) dna[i]);
	}

      if ( contig->assinf )
	{

	  old2lower = arrayCreate( len + 50 , int );
	  edited_contig->new2old = new2old = arrayCreate( len + 50 , int );
	  edited_contig->new2lower = new2lower = arrayCreate( len + 50 , int );
	  edited_contig->old2new = old2new = arrayCreate( len + 50 , int );
	  edited_contig->new_assinf = new_assinf = arrayCreate( arrayMax(contig->assinf)+5, ASSINF );
	  edited_contig->new_dna = new_dna = arrayCreate( len+50, char );
	  edited_contig->seq = contig;
	  edited_contig->id = contig->id;

	  *arrayp(old2new,len,int) = 1;
	  for(i=1;i<=len;i++)
	    *arrp(old2new, i, int ) = 1;

	  if ( contig->edit )
	    {
	      for(i=0;i<arrayMax(contig->edit);i++)
		{
		  edit = arrp(contig->edit,i,edit_struct);
		  edit->old = dna[edit->pos-1];
		  if ( edit->consensus == Strong )
		    {
		      if ( edit->status == deletion )
			{
			  *arrayp(old2new,edit->pos,int) = 0;
			}
		      else if ( edit->status == insert || edit->status == replace )
			{
			  dna[edit->pos-1] = edit->New;
			  if ( show_case )
			    dna[edit->pos-1] = toupper((int) dna[edit->pos-1]);
			}
		      edit->done = TRUE;
		    }
		  else if ( show_case )
		    {
		      dna[edit->pos-1] = tolower((int) dna[edit->pos-1]);
		    }
		}
	    }

	  for(i=2;i<=len;i++)
	    *arrp(old2new,i,int) += arr(old2new,i-1,int); /* old2new[i] = where i maps to in new sequence (old2new) */
		  

	  newlen = arr(old2new,len,int);

	  for(i=len;i>0;i--)
	    {
	      k = arr(old2new,i,int);
	      *arrayp(new2old,k,int) = i;
	    }

	  if ( contig->edit )
	    {
	      for(i=0;i<arrayMax(contig->edit);i++)
		{
		  edit = arrp(contig->edit,i,edit_struct);
/*		  if ( edit->consensus == strong_Consensus ) */
		    {
		      k = 0;
		      for(j=edit->pos;j>0;j++)
			if ( (k=arr(old2new,j,int)) > 0 )
			  break;
		      if ( k < 1 )
			for(j=edit->pos;j<arrayMax(old2new);j++)
			  if ( (k=arr(old2new,j,int)) > 0 )
			    break;
		      edit->pos = k;
		    }
		}
	    }

	  for(i=0;i<arrayMax(contig->assinf);i++)
	    {
	      a = arrp(contig->assinf, i, ASSINF );
	      read = a->seq;
	      seq2 = arrp(Ass->seqs,a->seq,CAFSEQ);
	      if ( skip == FALSE || seq2->bad == FALSE )
		{
		  b = arrayp(new_assinf,arrayMax(new_assinf),ASSINF);
		  if (NULL != (edited_read = arr(edited_seqs,a->seq,EDITED_CAFSEQ*)))
		    {
		      b->r1 = arr(edited_read->old2new,a->r1,I_SIZE);
		      b->r2 = arr(edited_read->old2new,a->r2,I_SIZE);
		    }
		  else
		    {
		      b->r1 = a->r1;
		      b->r2 = a->r2;
		    }
		  b->s1 = arr(old2new,a->s1,int);
		  b->s2 = arr(old2new,a->s2,int);
		  b->seq = a->seq;
		}
	    }

	  *arrayp(new_dna,newlen,char) = NULL;
	  for(i=0;i<newlen;i++)
	    {
	      k = arr(new2old,i+1,int);
	      *arrp(new_dna,i,char) = dna[k-1];
	    }

	  adjust_tags(contig,old2new);
	  adjust_quality(contig,old2new,newlen, 0);
	  arrayDestroy(old2lower); 
	}
    }
  return edited_contig;
}

void 
write_edited( cafAssembly *Ass, Array edited_seqs, int skip, FILE *fp )
{
  int i, j;
  CAFSEQ *seq, *seq2;
  ASSINF *a;
  EDITED_CAFSEQ *eseq;
  edit_struct *edit;
  char *dna;
  int done, uncl, ediT;
  char text[256];

  dictAdd(Ass->tagTypeDict,"DONE",&done);
  dictAdd(Ass->tagTypeDict,"EDIT",&ediT);
  dictAdd(Ass->tagTypeDict,"UNCL",&uncl);

  for(i=0;i<arrayMax(edited_seqs);i++)
    {
      eseq = arr(edited_seqs,i,EDITED_CAFSEQ*);
      seq = arrp(Ass->seqs,i,CAFSEQ);
      if ( seq->dna && (skip == FALSE || seq->bad == FALSE) )
	{
	  /* copy the edited sequence over the old one */

	  if ( eseq && eseq->new_dna)
	    {
	      seq->dna = stackReCreate(seq->dna,arrayMax(eseq->new_dna)+1);
	      dna = stackText(seq->dna,0);
	      for(j=0;j<arrayMax(eseq->new_dna);j++)
		dna[j] = arr(eseq->new_dna,j,char);
	      dna[arrayMax(eseq->new_dna)] = NULL;
	    }

	  /* fixup the assembled from info */

	  if ( eseq && eseq->new_assinf )
	    seq->assinf = eseq->new_assinf;

	  for(j=0;j<arrayMax(seq->assinf);j++)
	    {
	      a = arrp(seq->assinf,j,ASSINF);
	      seq2 = arrp(Ass->seqs,a->seq,CAFSEQ);
	      if ( skip == TRUE && seq2->bad == TRUE )
		a->seq = -1; /* deletion reference to this read - writeCafSeq will now ignore it */
	    }

	  /* write edit tags as real tags */

	  if ( seq->edit ) 
	    {
	      for(j=0;j<arrayMax(seq->edit);j++)
		{
		  edit = arrp(seq->edit,j,edit_struct);
		  *text = NULL;
		  if ( edit->done && edit->pos ) 
		    {

		      if ( edit->status == replace )
			sprintf(text, "AUTO-EDIT: replaced %c by %c at %d (%s, %s, %s)", 
				edit->old, edit->New, edit->pos, strand_text[edit->stranded], 
				compound_text[is_compound(edit->compound)], Consensus_text[edit->consensus] );
		      else if ( edit->status == deletion )
			sprintf(text, "AUTO-EDIT: deleted %c at %d  (%s, %s, %s)",
				edit->old, edit->pos, strand_text[edit->stranded], 
				compound_text[is_compound(edit->compound)], Consensus_text[edit->consensus] );
		      else if ( edit->status == insert )
			sprintf( text, "AUTO-EDIT: inserted %c at %d  (%s, %s, %s)",
				edit->New, edit->pos, strand_text[edit->stranded], 
				compound_text[is_compound(edit->compound)], Consensus_text[edit->consensus] );
		      newTag(seq,Ass,"DONE", edit->pos, edit->pos, text );
		    }
		  else if ( ! edit->done && edit->pos && edit->status == unclear )
		    {
		      sprintf( text, "AUTO-EDIT: unclear edit at %d (%s, %s %s)", edit->pos,  strand_text[edit->stranded], compound_text[edit->compound], Consensus_text[edit->consensus]);
		      newTag(seq,Ass,"UNCL", edit->pos, edit->pos, text );
		    }
		  else if ( ! edit->done && edit->pos )
		    {
		      if ( edit->status == replace )
			sprintf( text, "AUTO-EDIT: Propose replace %c by %c at %d (%s, %s, %s)", 
				edit->old, edit->New, edit->pos, strand_text[edit->stranded], 
				compound_text[is_compound(edit->compound)], Consensus_text[edit->consensus] );
		      else if ( edit->status == deletion )
			sprintf(text, "AUTO-EDIT: Propose delete %c at %d on (%s, %s, %s)",
				edit->old, edit->pos, strand_text[edit->stranded], 
				compound_text[is_compound(edit->compound)], Consensus_text[edit->consensus] );
		      else if ( edit->status == insert )
			sprintf(text, "AUTO-EDIT: Propose insert %c at %d (%s, %s, %s)",
				edit->New, edit->pos, strand_text[edit->stranded], 
				compound_text[is_compound(edit->compound)], Consensus_text[edit->consensus] );
		      newTag(seq,Ass,"EDIT", edit->pos, edit->pos, text );
		    }
		}
	    }
	  /* now write out the sequence */
	  seq->edit = NULL;
	  writeCafSeq( Ass, fp, seq );

	}
    }
}

/* 
 *  $Log$
 *  Revision 1.6  2005/06/16 12:38:26  rmd
 *  And even more warnings...
 *
 *  Revision 1.5  2005/06/16 12:35:40  rmd
 *  Fixed yet more compiler warnings.
 *
 * Revision 1.4  2005/06/16 12:34:48  rmd
 * Fixed some compiler warnings.
 * Moved log.
 *
 * Revision 1.3  2002/05/31 11:12:43  mng
 * Added caf2ace
 *
 * Revision 1.2  1997/07/22 11:21:15  badger
 * added base_position (work in saint louis)
 *
 * Revision 1.4  1997/07/18  20:50:39  sdear
 * Now use -1 as insert_position for adjust_position1
 *
 * Revision 1.3  1997/07/15  20:57:30  sdear
 * Minor bugs during debugging mode.
 *
 * Revision 1.2  1997/07/15  19:13:52  sdear
 * Support for BasePosition
 *
 * Revision 1.1  1997/04/03  13:44:09  badger
 * Initial revision
 *
 * Revision 1.1  1996/10/03  14:43:03  rmott
 * Initial revision
 *
 * Revision 1.1  1996/10/03  14:43:03  rmott
 * Initial revision
 * */
