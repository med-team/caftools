/* $Id: ne_clip.c 20764 2007-05-30 10:31:59Z rmd $ */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

#define MAIN

#include<stdio.h>
#include<string.h>
#include"cl.h"
#include"caf.h"
#include"io_lib/scf.h"
#include"cafseq.h"  //void sanitiseCAF(cafAssembly *CAF);

int main(int argc, char **argv)
{
  DICT *seqDict;
  Array seqs;
  CAFSEQ *seq;
  int i, j;
  ASSINF *a;
  cafAssembly *CAF;
  contigAlignment *Alignment;
  char CAFfile[256];
  char outfile[256];
  float badthresh=0.0; /* note threshold set to 0 */
  int s1_only=0;
  FILE *log_fp;
  char logfile[256];
  int depth=4;
  int max_run=5;
  char debug[256];
  int match=1, mismatch=-20, gap=-20, neutral=-2; /* very strong penalties */
  float st_louis = -1.0;
  int original=FALSE;
  int extend=TRUE;
  int use_phrap_consensus=FALSE;

  *debug = NULL;
  getfloat("-bad",&badthresh,argc,argv); /* percent threshold for excluding badly aligned or poor reads */
  getarg("-debug",debug,argc,argv);
  log_fp = argfile("-logfile","w",argc,argv,logfile); /* log file for dumping clipping info */
  getint("-match",&match,argc,argv);
  getint("-mismatch",&mismatch,argc,argv);
  getint("-gap",&gap,argc,argv);
  getint("-neutral",&neutral,argc,argv);

  getboolean("-original",&original,argc,argv); /* use original (input) consensus */
  getboolean("-extend",&extend,argc,argv); /* don't extend out again */

  if ( mismatch > 0 )
    mismatch = -mismatch;
  if ( gap > 0 )
    gap = -gap;
  if ( neutral > 0 )
    neutral = -neutral;

  CAF = readCAF( argc, argv, CAFfile );

  getarg("-out", outfile,argc,argv);
  getboolean("-phrap", &use_phrap_consensus, argc, argv );
  gethelp(argc,argv);

  if ( CAF )
    {
      requirePadded(CAF);

      seqs = CAF->seqs;
      seqDict = CAF->seqDict;

      CAF->from = CAF->to = -1;

      for(i=0;i<arrayMax(seqs);i++)
	{
	  seq = arrp( seqs, i, CAFSEQ );
/*	  printf("%s\n", dictName(seqDict,i));*/
	  assinf_from_pads( seq, seqDict, dictName(seqDict,i) ); 
	}
      for(i=0;i<arrayMax(seqs);i++)
	{
	  seq = arrp( seqs, i, CAFSEQ );
	  if ( seq->type == is_contig && seq->assinf )
	    {
	      if ( seq->assinf && arrayMax(seq->assinf) > 1 )
		{
		  /* extract the sequences for each read */
		  
		  for(j=0;j < arrayMax(seq->assinf); j++)
		    {
		      a = arrayp(seq->assinf, j, ASSINF) ;
		      a->s = ass_subseq( seqs, a );
		    }

		  /* create the contigAlignment struct */

		  Alignment = getContigAlignment( CAF, seq, s1_only, badthresh );

		  
		  propose_edits_for_contig( Alignment, CAF, depth, max_run, FALSE, st_louis, use_phrap_consensus );

		  /* clip */

		  propose_clip_alignment( Alignment, CAF, extend, log_fp, match, mismatch, gap, neutral, debug, original );

		  handleDestroy(Alignment->handle);
/*		  destroyContigAlignment( Alignment);    */
		}
	    }
	}

      
      sanitiseCAF(CAF);
      writeCAF( CAF, CAFfile, argc, argv);

      if ( log_fp )
	fclose(log_fp);
    }
return 0;
}

