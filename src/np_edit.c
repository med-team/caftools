/*  Last edited: Mar  5 15:47 2002 (mng) */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */
/*  Last edited: Oct  4 15:00 1996 (rmott) */
/* $Id: np_edit.c 15169 2005-06-16 13:00:16Z rmd $ */

/* NP_EDIT

Proposes edits for a sequence assembly. The proposed edits are written
out to be made by nd_edit 

Author: Richard Mott

*/

#include<stdio.h>
#include<string.h>
#include"cl.h"
#include"caf.h"
#include"myscf.h"

int set_cachesize( int newsize );

int
main(int argc, char **argv)
{
  DICT *seqDict;
  Array seqs;
  CAFSEQ *seq;
  int i, j;
  ASSINF *a;
  cafAssembly *CAF;
  contigAlignment *Alignment;
  int use_scf=0;
  char CAFfile[256];
  char outfile[256];
  float badthresh=85.0;
  int verbose = 0, s1_only=0;
  FILE *log_fp;
  char logfile[256];
  int depth=4;
  int max_run=4;
  int max_stack=3;
  int dump_edits=0;
  int do_all_edits=0;
  int scf_cachesize=0;
  float stlouis = -1;
  int base_quality=FALSE;
  int closeHoles = TRUE;
  int use_phrap_consensus=FALSE;
  STORE_HANDLE alignmentHandle;

/*********************************************************************
  Parse the command-line
  *********************************************************************/

  CAFfile[0] = 0;
  logfile[0] = 0;
  getboolean("-closeholes", &closeHoles, argc, argv); /* Close holes left by deleting all reads */
  getboolean("-scf",&use_scf,argc,argv); /* use traces (SCF files) */
  getboolean("-verbose",&verbose,argc,argv); /* voluminous output */
  getfloat("-bad",&badthresh,argc,argv); /* percent threshold for excluding badly aligned or poor reads */
  getint("-depth", &depth,argc,argv); /* minimum depth for an edit */
  getint("-max_run", &max_run,argc,argv); /* maximum cluster size for compound edits */
  getint("-max_stack", &max_run,argc,argv); /* maximum cluster size for compound edits */
  getboolean("-dump",&dump_edits,argc,argv); /* print out edits to logfile too */
  getint("-scfcache", &scf_cachesize,argc,argv); /* no of scf structs held in memory simultaneously */
  getfloat("-stlouis", &stlouis,argc,argv); /* threshold for st louis style autoediting */
  log_fp = argfile("-logfile","w",argc,argv,logfile); /* log file for dumping trace editing info */
  CAF = readCAF( argc, argv, CAFfile ); /* Read in the CAF */
  strcpy(outfile,"stdout");
  getarg("-out", outfile,argc,argv);
  getboolean("-phrap", &use_phrap_consensus, argc, argv );
  gethelp(argc,argv);

/* set the number of SCF traces that are cached. This should be
   greater than the maximum depth of coverage to avoid disk thrashing */

  set_cachesize(scf_cachesize); 

  if ( CAF )
    {
      requirePadded(CAF); /* make sure assembly is padded */
      seqs = CAF->seqs;
      seqDict = CAF->seqDict;

      /* get start and end points. This is only used for debugging,
         NOT for production */

      CAF->from = CAF->to = -1;
      getint("-from=%d", &CAF->from, argc,argv);
      getint("-to=%d", &CAF->to, argc,argv);

      /* Fix bad CAF files which don't have align_to_SCF data. Figure
         this out from the pads, assuming the read has not been
         edited. This is a relic since all CAF files now have this
         information */

      for(i=0;i<arrayMax(seqs);i++)
	{
	  seq = arrp( seqs, i, CAFSEQ );
	  assinf_from_pads( seq, seqDict, dictName(seqDict,i) ); 
	}

      /* Now auto-edit each contig */

      for(i=0;i<arrayMax(seqs);i++)
	{
	  seq = arrp( seqs, i, CAFSEQ );
	  if ( seq->type == is_contig && seq->assinf ) /* is a contig and has some reads */
	    {
	      if (verbose)
		{
		  fprintf(stderr,"\n\n ***** %s ***** \n\n", dictName(seqDict, i ) );
		  for(j=0;j < arrayMax(seq->assinf); j++)
		    {
		      a = arrayp(seq->assinf, j, ASSINF) ;
		      fprintf (stderr, "Assembled_from %s %d %d %d %d\n", 
			       dictName (seqDict, a->seq), a->s1, a->s2, a->r1, a->r2) ;
		    } 
		}

	      /* fix negative Assembled_from coordinates. Caused by
                 duff CAF files. Again, should not be needed anymore */

	      for(j=0;j < arrayMax(seq->assinf); j++)
		{
		  a = arrayp(seq->assinf, j, ASSINF) ;
		  if ( a->s1 < 1 || a->s2 < 1 )
		    {
		      if ( a->s1 < a->s2 )
			{
			  if ( a->s1 <= 0 )
			    {
			      fprintf(stderr,"//fixing Assembled_from %s %d %d %d %d\n", 
			       dictName (seqDict, a->seq), a->s1, a->s2, a->r1, a->r2) ; 
			      a->r1 = a->r1 + a->s1 -1;
			      a->s1 = 1;
			    }
			}
		      else
			{
			  if ( a->s2 <= 0 )
			    {
			      fprintf(stderr,"// fixing Assembled_from %s %d %d %d %d\n", 
				     dictName (seqDict, a->seq), a->s1, a->s2, a->r1, a->r2) ; 
			      a->r1 = a->r1 - a->s2 +1;
			      a->s2 = 1;
			    }
			}			      
		    } 
		}
	      /* only process contigs with more than one read */

	      if ( seq->assinf && arrayMax(seq->assinf) > 1 )
		{
		  /* extract the aligned sequences for each read, and
                     rev comp if needed */

		  for(j=0;j < arrayMax(seq->assinf); j++)
		    {
		      a = arrayp(seq->assinf, j, ASSINF) ;
		      a->s = ass_subseq( seqs, a );
		    }

/* create the contigAlignment struct */

		  Alignment = getContigAlignment( CAF, seq, s1_only, badthresh );

/* fill holes created by deleting reads */
		  if (closeHoles)
		    close_holes( Alignment, CAF, log_fp );

/* evaluate the edits required using consensus rules and optionally traces */

		  propose_edits_for_contig( Alignment, CAF, depth, max_run, use_scf, stlouis, use_phrap_consensus );

		  create_edit_tags_for_contig( CAF, Alignment, do_all_edits );

		  if ( verbose && log_fp )
		    print_contig_alignment( log_fp, Alignment );  
		  if ( log_fp )
		    print_contig_statistics(log_fp, CAF, Alignment, badthresh, depth, 0 ); 

		  compare_edits_to_consensus( CAF, Alignment, log_fp, use_scf, dump_edits );

/* write tags describing regions containing compound (neighbouring) edits */

		  write_compound_tags( CAF, Alignment, max_run );

/* write tags describing regions containing stacked edits */

		  write_stack_tags( CAF, Alignment, max_stack );

/* write tags describing potential polymorphisms and other discrepancies */

		  write_discrepancy_tags( CAF, Alignment );

/* This code is turned off */

		  if ( base_quality)
		    make_base_quality( CAF, Alignment, seq );

		  if ( log_fp )
		    print_contig_statistics(log_fp, CAF, Alignment, badthresh, depth, 1 ); 

/* free-up the contigAlignment and its associated data */
		  alignmentHandle = Alignment->handle; /* Prevent bizarre memory error */
		  handleDestroy(alignmentHandle);
		}

	    }
	}

      if ( verbose )
	fprintf(stderr,"writing output to %s\n", outfile );

/* Write tags describing potentially misassembled reads */

      write_miss_tags( CAF );

/* And finally write the CAF database */

      writeCAF( CAF, CAFfile, argc, argv );

      return 0;
    }
  return 1;
} 

/*
 * $Log$
 * Revision 1.6  2005/06/16 13:00:16  rmd
 * Added prototype for set_cachesize.
 *
 * Revision 1.5  2005/06/16 11:43:43  rmd
 * Moved log.
 *
 * Revision 1.4  2002/05/31 11:12:43  mng
 * Added caf2ace
 *
 * Revision 1.3  1998/01/12 16:35:16  badger
 * Fixed reading freed memory problems
 *
 * Revision 1.2  1997/09/04  16:45:11  badger
 * Add use_phrap_consensus
 *
 * Revision 1.1  1997/04/03  13:44:09  badger
 * Initial revision
 *
 * Revision 1.2  1996/10/04  13:59:02  rmott
 * *** empty log message ***
 *
 * Revision 1.1  1996/10/03  14:44:27  rmott
 * Initial revision
 *
 * Revision 1.1  1996/10/03  14:44:27  rmott
 * Initial revision
 * */
