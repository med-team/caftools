/*  Last edited: Aug 16 10:15 2001 (rmd) */
/*
 * File: os.h
 *
 * Author: 
 *         MRC Laboratory of Molecular Biology
 *	   Hills Road
 *	   Cambridge CB2 2QH
 *	   United Kingdom
 *
 * Description: operating system specific type definitions
 *
 */

#ifndef _OS_H_
#define _OS_H_

#include <limits.h>
#include <inttypes.h>

/*
 *-----------------------------------------------------------------------------
 * Typedefs for data sizes. Note there's umpteen versions of typedefs here
 * due to old code being supported. The ones that should be used everywhere
 * are {u,}int[124].
 *-----------------------------------------------------------------------------
 */

/*
 * One byte integers
 */ 
/*
typedef unsigned char	uint1;
typedef signed char	uint1;
*/
typedef unsigned char	int1;

/*
 * Two byte integers
 */
typedef signed short	int2;
typedef unsigned short	uint2;

/*
 * Four byte integers
 */
typedef signed int	int4;
typedef unsigned int	uint4;


/*
 * Backwards compatibility
 */
typedef signed char	int_1;
typedef unsigned char	uint_1;
typedef signed short	int_2;
typedef unsigned short	uint_2;
typedef signed int	int_4;
typedef unsigned int	uint_4;


/*
 *-----------------------------------------------------------------------------
 * Some handy definitions.
 *-----------------------------------------------------------------------------
 */

#define MAXINT4 (INT_MAX)
#define MAXINT2 (SHRT_MAX)

/* FIXME - this shouldn't be needed now */
#ifndef MAXINT
#    define MAXINT (INT_MAX)
#endif

/*
 *=============================================================================
 * Anything below here should not be changed.
 *=============================================================================
 */

#define False 0
#define True 1

#define iswap_int8(x) \
    (((x & 0x00000000000000ff) << 56) + \
     ((x & 0x000000000000ff00) << 40) + \
     ((x & 0x0000000000ff0000) << 24) + \
     ((x & 0x00000000ff000000) <<  8) + \
     ((x & 0x000000ff00000000) >>  8) + \
     ((x & 0x0000ff0000000000) >> 24) + \
     ((x & 0x00ff000000000000) >> 40) + \
     ((x & 0xff00000000000000) >> 56))

#define iswap_int4(x) \
    (((x & 0x000000ff) << 24) + \
     ((x & 0x0000ff00) <<  8) + \
     ((x & 0x00ff0000) >>  8) + \
     ((x & 0xff000000) >> 24))

#define iswap_int2(x) \
    (((x & 0x00ff) << 8) + \
     ((x & 0xff00) >> 8))

#define swap_int8(src, dst) ((dst) = iswap_int8(src))
#define swap_int4(src, dst) ((dst) = iswap_int4(src))
#define swap_int2(src, dst) ((dst) = iswap_int2(src))

/*
 * Macros to specify that data read in is of a particular endianness.
 * The macros here swap to the appropriate order for the particular machine
 * running the macro and return the new answer. These may also be used when
 * writing to a file to specify that we wish to write in (eg) big endian
 * format.
 *
 * This leads to efficient code as most of the time these macros are
 * trivial.
 */
#ifdef WORDS_BIGENDIAN

#define be_int4(x) (x)
#define be_int2(x) (x)
#define be_int1(x) (x)

#define le_int4(x) iswap_int4((x))
#define le_int2(x) iswap_int2((x))
#define le_int1(x) (x)

/* in case anything uses it... */
#undef LITTLE_ENDIAN
#ifndef BIG_ENDIAN
#define BIG_ENDIAN
#endif

#else /* WORDS_BIGENDIAN */

#define be_int4(x) iswap_int4((x))
#define be_int2(x) iswap_int2((x))
#define be_int1(x) (x)

#define le_int4(x) (x)
#define le_int2(x) (x)
#define le_int1(x) (x)

/* in case anything uses it... */
#undef BIG_ENDIAN
#ifndef LITTLE_ENDIAN
#define LITTLE_ENDIAN
#endif

#endif /* WORDS_BIGENDIAN */

#ifndef BIG_ENDIAN
#ifndef LITTLE_ENDIAN
#error Must define BIG_ENDIAN or LITTLE_ENDIAN in Makefile
#endif
#endif

#ifdef BIG_ENDIAN
#ifdef LITTLE_ENDIAN
#error Must only define one of BIG_ENDIAN and LITTLE_ENDIAN in Makefile
#endif
#endif

#endif /*_OS_H_*/
