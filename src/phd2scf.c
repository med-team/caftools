/*  Last edited: Mar  6 12:37 2002 (mng) */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <io_lib/Read.h>

int main(int argc, char **argv);
int read_line(char **phd_file, char **trace_file_in, char **scf_file_out);
static void memory_barf(size_t sz);
static void file_open_barf(char *name, char *access);
static void file_close_barf(char* verb, char *name);
void process_one_trace(char *phd_file, char *trace_file_in, char *scf_file_out);
static void realloc_trace_memory(Read *trace, size_t new_size);
static char * read_phd_line(FILE *fp);


char *prog;

int main(int argc, char **argv)
{
  char *p;
  char *phd_file     = 0;
  char *trace_file_in  = 0;
  char *scf_file_out = 0;

  prog = argv[0];
  while ((p = strstr(prog, "/")) != 0) {
    prog = p + 1;
  }

  if (argc == 4) {
    phd_file      = argv[1];
    trace_file_in = argv[2];
    scf_file_out  = argv[3];

    process_one_trace(phd_file, trace_file_in, scf_file_out);
  } else if (argc == 1) {
    
    while (!read_line(&phd_file, &trace_file_in, &scf_file_out)) {

      process_one_trace(phd_file, trace_file_in, scf_file_out);
    }

  } else {
    fprintf(stderr, "%s: Usage: %s phd_file trace_file_in scf_file_out\n",
	    prog, prog);
    return 3;
  }

  return 0;
}

int read_line(char **phd_file, char **trace_file_in, char **scf_file_out)
{
  static char *linebuf = 0;
  static int  buflen   = 0;
  int c        = 0;
  char *bufptr = 0;
  int size     = 0;
  int nonspace = 0;

  if (!linebuf) {
    buflen = 256;
    linebuf = (char *) malloc(buflen * sizeof(char));
    if (!linebuf) memory_barf(buflen * sizeof(char));
  }

  do {
    bufptr = linebuf;
    size   = buflen;

    while ((c = getchar()) != EOF) {
      
      if (c == '\n') break;
      if (!isspace(c)) nonspace = 1;

      *bufptr++ = (char) c;
      
      if (!--size) {
	linebuf = (char *) realloc(linebuf, buflen * 2 * sizeof(char));
	if (!linebuf) memory_barf(buflen * 2 * sizeof(char));
	bufptr = linebuf + buflen;
	size   = buflen;
	buflen *= 2;
      }
    }
    *bufptr = 0;
  } while (!nonspace && !feof(stdin));

  /* Find phd_file, trace_file_in and scf_file_out in line */

  bufptr = linebuf;
  
  /* skip leading whitespace */
  while (*bufptr && isspace((int) *bufptr)) { bufptr++; }

  /* Remember where phd_file starts */
  *phd_file      = bufptr;

  /* skip over phd file name */
  while (*bufptr && !isspace((int) *bufptr)) { bufptr++; }
  
  /* Change 1st whitespace char to 0 to terminate the phd_file string */
  if (*bufptr) *bufptr++ = 0;

  /* skip any remaining whitespace */
  while (*bufptr && isspace((int) *bufptr)) { bufptr++; }

  /* Remember where trace_file_in starts */
  *trace_file_in = bufptr;

  /* Skip over trace_file_in name */
  while (*bufptr && !isspace((int) *bufptr)) { bufptr++; }
  
  /* Change 1st whitespace char to 0 to terminate the trace_file_in string */
  if (*bufptr) *bufptr++ = 0;

  /* skip any remaining whitespace */
  while (*bufptr && isspace((int) *bufptr)) { bufptr++; }

  /* Remember where scf_file_out starts */
  *scf_file_out = bufptr;
  
  /* Skip over scf_file_out name */
  while (*bufptr && !isspace((int) *bufptr)) { bufptr++; }
  
  /* Change 1st whitespace char to 0 to terminate the scf_file_out string */
  if (*bufptr) *bufptr++ = 0;

  
  if (nonspace && (!**phd_file || !**trace_file_in || !**scf_file_out)) {
    fprintf(stderr, "%s: Input file format error. Format should be:\n", prog);
    fprintf(stderr, "phd_dir trace_file_in scf_file_out\n");
    exit(2);
  }
  
  return feof(stdin);
}

static void memory_barf(size_t sz)
{
  double szmb = (float) sz / 1048576.0;
  char *errmsg = strerror(errno);
  if (!errmsg) errmsg = "";
  fprintf(stderr, "%s: Couldn't allocate %.3f Mb %s\n",
	  prog, szmb, errmsg);
  exit(1);
}

static void file_open_barf(char *name, char *access)
{
  char *errmsg = strerror(errno);
  if (!errmsg) errmsg = "";

  fprintf(stderr, "%s: Couldn't open %s for %s %s\n",
	  prog, name, access, errmsg);
  exit(1);
}

static void file_close_barf(char* verb, char *name)
{
  char *errmsg = strerror(errno);
  if (!errmsg) errmsg = "";

  fprintf(stderr, "%s: Error %s %s %s\n",
	  prog, verb, name, errmsg);
  exit(1);
}

void process_one_trace(char *phd_file, char *trace_file_in, char *scf_file_out)
{
  FILE *fp_phd;
  FILE *fp_trace_in;
  FILE *fp_scf_out;
  Read *trace;
  int dna_found = 0;
  char *line = 0;
  int max_nbases;
  char base;
  int  qual;
  int  pos;

  fprintf(stderr, "%s %s %s\n", phd_file, trace_file_in, scf_file_out);
  
  fp_phd = fopen(phd_file, "r");
  if (!fp_phd) file_open_barf(phd_file, "reading");

  fp_trace_in = fopen(trace_file_in, "rb");
  if (!fp_trace_in) file_open_barf(trace_file_in, "reading");

  trace = fread_reading(fp_trace_in, trace_file_in, 0);
  if (!trace) {
    fprintf(stderr, "%s: Error reading trace file %s\n", prog, trace_file_in);
    exit(2);
  }

  if (fclose(fp_trace_in)) file_close_barf("reading", trace_file_in);

  /* Read the phd file */

  max_nbases = trace->NBases;
  trace->NBases = 0;

  /* Deal with input traces that have only a very small number of basecalls
     to start with - so we don't need to call realloc loads of times.
     Also allocates memory if there were no basecalls to start with.
  */
  if (max_nbases < 256) {
    realloc_trace_memory(trace, 256);
    max_nbases = 256;
  }

  while (!feof(fp_phd) && !dna_found) {
    line = read_phd_line(fp_phd);

    if (!strncmp(line, "BEGIN_DNA", 9)) dna_found = 1;
  }

  if (!dna_found) {
    fprintf(stderr, "%s: No BEGIN_DNA found in phd file %s\n", prog, phd_file);
    exit(2);
  }

  while (!feof(fp_phd) && dna_found) {
    line = read_phd_line(fp_phd);
    if (!line) file_close_barf("reading", phd_file);

    if (!strncmp(line, "END_DNA", 7)) {
      dna_found = 0;
      break;
    }

    if (sscanf(line, "%c%d%d", &base, &qual, &pos) != 3) {
      fprintf(stderr, "%s: Bad DNA line in phd file %s\n", prog, phd_file);
      exit(2);
    }

    trace->base[trace->NBases]    = (char) toupper((int) base);
    trace->basePos[trace->NBases] = pos;

    /* Qualities are a bit more complex... */
    switch (base)
      {
      case 'a':
      case 'A':
	trace->prob_A[trace->NBases] = qual;
	trace->prob_C[trace->NBases] = 0;
	trace->prob_G[trace->NBases] = 0;
	trace->prob_T[trace->NBases] = 0;
	break;
      case 'c':
      case 'C':
	trace->prob_A[trace->NBases] = 0;
	trace->prob_C[trace->NBases] = qual;
	trace->prob_G[trace->NBases] = 0;
	trace->prob_T[trace->NBases] = 0;
	break;
      case 'g':
      case 'G':
	trace->prob_A[trace->NBases] = 0;
	trace->prob_C[trace->NBases] = 0;
	trace->prob_G[trace->NBases] = qual;
	trace->prob_T[trace->NBases] = 0;
	break;
      case 't':
      case 'T':
	trace->prob_A[trace->NBases] = 0;
	trace->prob_C[trace->NBases] = 0;
	trace->prob_G[trace->NBases] = 0;
	trace->prob_T[trace->NBases] = qual;
	break;
      default:
	trace->prob_A[trace->NBases] = qual;
	trace->prob_C[trace->NBases] = qual;
	trace->prob_G[trace->NBases] = qual;
	trace->prob_T[trace->NBases] = qual;
	break;
      }

    /* If sequence has got too big, allocate more memory for them */

    if (++trace->NBases >= max_nbases) {
      realloc_trace_memory(trace, max_nbases * 2);
      max_nbases *= 2;
    }
  }

  if (dna_found) {
    fprintf(stderr, "%s: No END_DNA found in phd file %s\n", prog, phd_file);
    exit(2);
  }
  if (fclose(fp_phd)) file_close_barf("reading", phd_file);

  /* Write out new trace file */

  fp_scf_out = fopen(scf_file_out, "wb");
  if (!fp_scf_out) file_open_barf(scf_file_out, "writing");

  if (fwrite_reading(fp_scf_out, trace, TT_SCF))
    file_close_barf("writing", scf_file_out);

  if (fclose(fp_scf_out)) file_close_barf("writing", scf_file_out);

  /* Free up memory used by trace */

  read_deallocate(trace);
}

static void realloc_trace_memory(Read *trace, size_t new_size)
{
  trace->base = realloc(trace->base,       new_size * sizeof(char));
  if (!trace->base) memory_barf(new_size * sizeof(char));

  trace->basePos = realloc(trace->basePos, new_size * sizeof(short));
  if (!trace->basePos) memory_barf(new_size * sizeof(short));

  trace->prob_A = realloc(trace->prob_A,   new_size * sizeof(char));
  if (!trace->prob_A) memory_barf(new_size * sizeof(char));

  trace->prob_C = realloc(trace->prob_C,   new_size * sizeof(char));
  if (!trace->prob_C) memory_barf(new_size * sizeof(char));

  trace->prob_G = realloc(trace->prob_G,   new_size * sizeof(char));
  if (!trace->prob_G) memory_barf(new_size * sizeof(char));

  trace->prob_T = realloc(trace->prob_T,   new_size * sizeof(char));
  if (!trace->prob_T) memory_barf(new_size * sizeof(char));  
}

static char * read_phd_line(FILE *fp)
{
  static char line[256];
  static char discard[256];

  if (!fgets(line, 256, fp)) return 0;

  if (!strstr(line, "\n")) {
    /* Discard rest of line */

    while (!feof(fp) && !ferror(fp)) {
      if (fgets(discard, 256, fp) && strstr(discard, "\n")) break;
    }
  }

  return line;
}

