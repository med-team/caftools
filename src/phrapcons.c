/* $Id: phrapcons.c 15158 2005-06-16 11:40:54Z rmd $ */

/*
 * tag the contig consensus over regions where the phrap quality is
 * below a threshold
 */

#define MAIN 1
#define DEBUG 0

#include<stdio.h>
#include<ctype.h>
#include"cl.h"
#include"caf.h"

void
recompute_base_quality( CAFSEQ *contig, cafAssembly *CAF );

int
main(int argc, char **argv)
{
  CAFSEQ *contig, *seq;
  int thresh=30;
  cafAssembly *CAF;
  char caffile[256];
  int i, j, k, n=0;
  int bq=0,ns=0;
  int recompute=FALSE;

  getint("-thresh", &thresh, argc, argv );
  getboolean("-recompute",&recompute,argc,argv);

  fprintf(stderr, "new phrapcons\n");
  if (NULL != (CAF = readCAF(argc, argv, caffile)))
    {
      requirePadded(CAF);

      for(i=0;i<arrayMax(CAF->seqs);i++)
	{
	  seq = arrp(CAF->seqs,i,CAFSEQ);
	  if ( is_read == seq->type )
	    {
	      ns++;
	      if ( seq->base_quality )
		bq++;
	    }
	}
      if ( 2*bq > ns ) /* at least 50% of reads must have base quality info */
	{
	  for(i=0;i<arrayMax(CAF->seqs);i++)
	    {
	      contig = arrp(CAF->seqs,i,CAFSEQ);
	      if ( is_contig == contig->type && ( contig->base_quality || recompute ) )
		{
		  int maxj;

		  if ( recompute )
		    recompute_base_quality( contig, CAF );

		  maxj = arrayMax(contig->base_quality);
		  j = 0;
		  while( j < maxj )
		    {
		      k = j;
		      while (j < maxj && arr(contig->base_quality, j, signed char) < thresh)
			j++;
		      if (j > k)
			{
			  newTag( contig, CAF, "LOW", k,j-1,"Low Phrap Consensus Quality");
			  n++;
			}
		      else
			j++;
		    }
		}
	    }
	  fprintf(stderr, "phrapcons made %d new tags at threshold %d\n", n, thresh );
	}
      else
	{
	  fprintf(stderr, "WARNING: phrapcons did not tag the consensus. reads with base quality data: %d / %d\n", bq, ns );
	}

      writeCAF(CAF,caffile,argc,argv);
      return 0;
    }
  return 1;
}

void
recompute_base_quality( CAFSEQ *contig, cafAssembly *CAF )
{
  /* recompute the base quality for a contig

     The rule is:
     
     quality at each coordinate =
     
     max( 
     { max unterminated forward read + max unterminated reverse read } , 
     { max terminated read + max unterminated read } 
     ) 
     
     - max of reads which disagree with phrap consensus 
     
     */

  char *contig_dna = stackText(contig->dna,0)-1;
  char *read_dna;
  int i, j, k, strand, len;
  char r, c;
  ASSINF *a;
  Array quality;
  Array forward, reverse, terminator, non_terminator, against;
  BQ_SIZE p, q, old, *bq;
  CAFSEQ *seq;
  int s1, s2;
  int missing = 0;

  if ( contig->assinf )
    {
      fprintf(stderr, "recompute %s\n", dictName(CAF->seqDict,contig->id ) );
      len = ( contig->base_quality ? arrayMax(contig->base_quality) : strlen(contig_dna)+2 );

      forward = arrayCreate(len,BQ_SIZE);
      reverse = arrayCreate(len,BQ_SIZE);
      terminator = arrayCreate(len,BQ_SIZE);
      non_terminator = arrayCreate(len,BQ_SIZE);
      against = arrayCreate(len,BQ_SIZE);

      for(k=0;k<arrayMax(contig->assinf);k++)
	{
	  a = arrp(contig->assinf,k,ASSINF);
	  seq = arrp(CAF->seqs,a->seq,CAFSEQ);
	  if ( seq->base_quality && seq->dna )
	    {
	      read_dna = stackText(seq->dna,0)-1;

	      if ( a->s1 <= a->s2 ) /* forward */
		{
		  s1 = a->s1;
		  s2 = a->s2;
		  strand = +1;
		  j = a->r1;
		}
	      else                  /* reverse */
		{
		  s1 = a->s2;
		  s2 = a->s1;
		  strand = -1;
		  j = a->r2;
		}

	      for(i=s1;i<=s2;i++,j+=strand)
		{
		  c = contig_dna[i];
		  r = read_dna[j];

		  if ( strand == -1 )
		    r = complement_base(r);

		  q = arr(seq->base_quality,j-1,BQ_SIZE);

		  if ( tolower((int) c) == tolower((int) r) )
		    {
		      if ( seq->is_terminator == TRUE )
			{
			  bq = arrayp(terminator,i-1,BQ_SIZE ); 
			  if ( *bq < q )
			    *bq = q;
			}
		      else
			{
			  bq = arrayp(non_terminator,i-1,BQ_SIZE );
			  if ( *bq < q )
			    *bq = q;
			  
			  if ( strand == -1 )
			    {
			      bq = arrayp(reverse,i-1,BQ_SIZE );
			      if ( *bq < q )
				*bq = q;
			    }
			  else
			    {
			      bq = arrayp(forward,i-1,BQ_SIZE );
			      if ( *bq < q )
			      *bq = q;
			    }
			}
		    }
		  else
		    {
		      bq = arrayp(against,i-1,BQ_SIZE );
		      if ( *bq < q )
			*bq = q;
		    }
		}
	    }
	  else
	    missing++;
	}
      
      if ( missing < 0.10*arrayMax(contig->assinf ) )
	{
	  len = strlen(contig_dna+1);
	  quality = arrayCreate(len,BQ_SIZE);
	  
	  
	  for(i=0;i<len;i++)
	    {
	      p = array(forward,i,BQ_SIZE) + array(reverse,i,BQ_SIZE) - array(against,i,BQ_SIZE);
	      q = array(terminator,i,BQ_SIZE) + array(non_terminator,i,BQ_SIZE) - array(against,i,BQ_SIZE);

	      if ( p < q ) p = q;

	      if ( p > 99 ) p = 99;
	      if ( p < 0 ) p = 0;

	      *arrayp(quality,i,BQ_SIZE) = p;
	      if ( DEBUG )
		{
		  old = array(contig->base_quality,i,BQ_SIZE);
		  fprintf(stderr, "%5d %c %5d %5d  %5d    %3d %3d  %3d %3d  %5d\n", i, contig_dna[i+1], old, p, old-p, array(forward,i,BQ_SIZE), array(reverse,i,BQ_SIZE), array(terminator,i,BQ_SIZE), array(non_terminator,i,BQ_SIZE), array(against,i,BQ_SIZE) );
		}
	    }
	  
	  if ( contig->base_quality )
	    arrayDestroy(contig->base_quality);
	  contig->base_quality = quality;
	}
      else
	fprintf(stderr, "WARNING: %s has %d/%d missing base quality\n", dictName(CAF->seqDict,contig->id), missing, arrayMax(contig->assinf));
      arrayDestroy(forward);
      arrayDestroy(reverse);
      arrayDestroy(terminator);
      arrayDestroy(non_terminator);
      arrayDestroy(against);
    }
}
	  
/*
 * $Log$
 * Revision 1.4  2005/06/16 11:40:54  rmd
 * Moved log.  Fixed compiler warnings.
 *
 * Revision 1.3  2003/07/25 14:54:57  dgm
 * Updated comments to remove nested comments.
 *
 * Revision 1.2  2002/05/31 11:12:44  mng
 * Added caf2ace
 *
 * Revision 1.1  1999/01/05 14:28:09  rmd
 * Added phrapcons
 *
 * Revision 1.1  1996/07/04  09:59:12  rmott
 * Initial revision
 *
 */
