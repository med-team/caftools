/* $Id: scf2phd.c 20764 2007-05-30 10:31:59Z rmd $ */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <ctype.h>

#include <io_lib/Read.h>
#include <io_lib/scf.h>

#define CHUNK_SIZE 1024

typedef struct phredpar_entry {
  char *key;
  char *chem;
  char *dye;
  char *machine;
} phredpar_entry;

typedef struct str_chunk {
  char *chunk;
  struct str_chunk *next;
  size_t used;
} str_chunk;

typedef struct phredpar_data {
  str_chunk *strings;
  phredpar_entry *chem_list;
  size_t cl_size;
  size_t cl_used;
} phredpar_data;

int main(int argc, char **argv);
static void file_open_barf(char *name, char *access);
static void file_close_barf(char* verb, char *name);
static int process_fofn(char *fofn, phredpar_data *chem_list_data);
static int process_file(FILE* fp_scf, char* scf_file,
			FILE* fp_phd, char* phd_file,
			phredpar_data *chem_list);
static int read_phred_parameter_file(char *file, phredpar_data *data);
static int init_phredpar_data(phredpar_data *data);
static void free_phredpar_data(phredpar_data *data);
static int ppe_cmp(const void *aa, const void *bb);
static int ppe_search(const void *aa, const void *bb);
static void get_chem_info(Read *read, char **chem, char **dye,
			  phredpar_data *chem_list);
char *prog;

int main(int argc, char **argv)
{
  phredpar_data chem_list_data = { NULL, NULL, 0, 0 };
  char *p;
  char *phd_file   = 0;
  char *scf_file   = 0;
  char *fofn       = 0;
  char *param_file = 0;
  FILE *fp_phd     = 0;
  FILE *fp_scf     = 0;
  /* int i; */

  prog = argv[0];
  while ((p = strstr(prog, "/")) != 0) {
    prog = p + 1;
  }

  if (argc == 3) {

    if (0 == strcmp(argv[1], "-fofn")) {

      fofn = argv[2];

    } else {

      scf_file = argv[1];
      phd_file = argv[2];

      fp_scf = fopen(scf_file, "rb");
      if (!fp_scf) file_open_barf(scf_file, "reading");

      fp_phd = fopen(phd_file, "w");
      if (!fp_phd) file_open_barf(phd_file, "writing");

    }

  } else if (argc == 2) {

    scf_file = argv[1];
    phd_file = "stdout";
    fp_scf = stdin;
    fp_phd = stdout;

  } else {
    fprintf(stderr, "Bad parameters\n");
    return 1;
  }

  if (NULL != (param_file = getenv("PHRED_PARAMETER_FILE"))) {
    fprintf(stderr, "Reading %s...\n", param_file);
    read_phred_parameter_file(param_file, &chem_list_data);
  }

#if 0
  if (chem_list_data.cl_used) {
    for (i = 0; i < chem_list_data.cl_used; i++) {
      fprintf(stderr, ">> \"%s\"\t%s\t%s\t%s\n",
	      chem_list_data.chem_list[i].key,
	      chem_list_data.chem_list[i].chem,
	      chem_list_data.chem_list[i].dye,
	      chem_list_data.chem_list[i].machine);
    }
  }
#endif

  if (NULL != fofn) {
    return process_fofn(fofn, &chem_list_data);
  }

  if (process_file(fp_scf, scf_file, fp_phd, phd_file, &chem_list_data)) {
    fprintf(stderr, "Error reading scf file '%s'\n", scf_file);
    return 1;
  }

  return 0;
}

static void file_open_barf(char *name, char *access)
{
  char *errmsg = strerror(errno);
  if (!errmsg) errmsg = "";

  fprintf(stderr, "%s: Couldn't open %s for %s %s\n",
	  prog, name, access, errmsg);
  exit(1);
}

static void file_close_barf(char* verb, char *name)
{
  char *errmsg = strerror(errno);
  if (!errmsg) errmsg = "";

  fprintf(stderr, "%s: Error %s %s %s\n",
	  prog, verb, name, errmsg);
  exit(1);
}

static int process_fofn(char *fofn, phredpar_data *chem_list_data) {
  char line[1024];
  char phd[1024 + 6];
  char *e;
  FILE *f;
  FILE *fp_scf;
  FILE *fp_phd;
  int res;

  if (0 == strcmp(fofn, "-")) {
    f = stdin;
  } else {
    f = fopen(fofn, "r");
    if (NULL == f) file_open_barf(fofn, "reading");
  }

  while (NULL != fgets(line, 1024, f)) {
    e = strstr(line, "\n");
    if (NULL != e) *e = 0;

    fp_scf = fopen(line, "rb");
    if (NULL == fp_scf) file_open_barf(line, "reading");

    strcpy(phd, line);
    strcat(phd, ".phd.1");

    fp_phd = fopen(phd, "w");
    if (NULL == fp_phd) file_open_barf(phd, "writing");

    res = process_file(fp_scf, line, fp_phd, phd, chem_list_data);

    fclose(fp_scf);
    fclose(fp_phd);

    if (0 != res) {
      fprintf(stderr, "Error processing %s\n", line);
      return 1;
    }
  }

  if (0 != strcmp(fofn, "-")) {
    fclose(f);
  }

  return 0;
}

static int process_file(FILE *fp_scf, char *scf_file,
			FILE *fp_phd, char *phd_file,
			phredpar_data *chem_list)
{
  /* Scf*  scf  = 0; */
  char *chem = NULL;
  char *dye  = NULL;
  Read *read = NULL;
  int   i    = 0;
  time_t t   = 0;

  /* scf = fread_scf(fp_scf);
  if (!scf) return 1;

  read = scf2read(scf);
  scf_deallocate(scf);*/

  read = fread_reading(fp_scf, scf_file, TT_SCF);

  if (!read) return 1;

  get_chem_info(read, &chem, &dye, chem_list);

  t = time(NULL);

  fprintf(fp_phd, "BEGIN_SEQUENCE %s\n\n", read->trace_name);

  fprintf(fp_phd, "BEGIN_COMMENT\n\n");

  fprintf(fp_phd, "CHROMAT_FILE: %s\n", scf_file);
  fprintf(fp_phd, "ABI_THUMBPRINT: 0\n");
  fprintf(fp_phd, "QUALITY_LEVELS: 99\n");
  fprintf(fp_phd, "TIME: %s", ctime(&t));
  fprintf(fp_phd, "TRACE_ARRAY_MIN_INDEX: 0\n");
  fprintf(fp_phd, "TRACE_ARRAY_MAX_INDEX: %d\n",
	  read->NPoints > 0 ? read->NPoints - 1 : 0);
  fprintf(fp_phd, "CHEM: %s\n", chem);
  fprintf(fp_phd, "DYE: %s\n", dye);

  fprintf(fp_phd, "\nEND_COMMENT\n\n");
  
  fprintf(fp_phd, "BEGIN_DNA\n");
  
  for (i = 0; i < read->NBases; i++) {
    char base = read->base[i];
    int  qual = 0;

    switch (base) {
    case 'A':
    case 'a':
      qual = (int) read->prob_A[i]; break;
    case 'C':
    case 'c':
      qual = (int) read->prob_C[i]; break;
    case 'G':
    case 'g':
      qual = (int) read->prob_G[i]; break;
    case 'T':
    case 't':
      qual = (int) read->prob_T[i]; break;
    default:
      qual = (int) read->prob_A[i]; break;
    }
    fprintf(fp_phd, "%c %d %d\n",
	    tolower((int) base), qual, (int) read->basePos[i]);
  }
  fprintf(fp_phd, "END_DNA\n\n");
  fprintf(fp_phd, "END_SEQUENCE\n\n");

  read_deallocate(read);

  return 0;
}


#define SKIP_SPACE(p) while (0 != *(p) && isspace((int) *(p))) (p)++
#define SKIP_NON_SPACE(p) while (0 != *(p) && !isspace((int) *(p))) (p)++

static int read_phred_parameter_file(char *file, phredpar_data *data) {
  char line[1024];
  FILE *fp_phredpar;
  size_t l_key;
  size_t l_chem;
  size_t l_dye;
  size_t l_machine;
  size_t l;
  str_chunk *new_str;
  phredpar_entry *new_list;
  char *p;
  char *key = NULL;
  char *chem = NULL;
  char *dye = NULL;
  char *machine = NULL;
  int res;

  if (0 != (res = init_phredpar_data(data))) {
    free_phredpar_data(data);
    return res;
  }
  
  fp_phredpar = fopen(file, "r");
  if (NULL == fp_phredpar) file_open_barf(file, "reading");

  while (NULL != fgets(line, 1024, fp_phredpar)) {
     p = line;
    SKIP_SPACE(p);

    if (0 != strncmp(p, "begin", 5)) continue;
    p += 5;
    SKIP_SPACE(p);

    if (0 == strncmp(p, "chem_list", 9)) break;
  }
  
  while (NULL != fgets(line, 1024, fp_phredpar)) {
    p = line;
    SKIP_SPACE(p);

    if (*p == '#') continue;
    
    if (0 == strncmp(p, "end", 3)) break;

    if (*p != '"') continue;

    key = ++p;
    while (0 != *p && '"' != *p) p++;
    if (0 == *p) continue;
    l_key = p - key;
    *p++ = 0;

    SKIP_SPACE(p);
    chem = p;
    SKIP_NON_SPACE(p);
    if (0 == *p) continue;
    l_chem = p - chem;
    *p++ = 0;
    
    SKIP_SPACE(p);
    dye = p;
    SKIP_NON_SPACE(p);
    if (0 == *p) continue;
    l_dye = p - dye;
    *p++ = 0;

    SKIP_SPACE(p);
    machine = p;
    SKIP_NON_SPACE(p);
    l_machine = p - machine;
    *p++ = 0;

    l = l_key + l_chem + l_dye + l_machine + 4;

    if (CHUNK_SIZE - data->strings->used < l) {
      new_str = (str_chunk *) calloc(1, sizeof(str_chunk));
      if (NULL == new_str) {
	free_phredpar_data(data);
	return 1;
      }

      new_str->chunk = malloc((CHUNK_SIZE > l ? CHUNK_SIZE : l) * sizeof(char));
      if (NULL == new_str->chunk) {
	free(new_str);
	free_phredpar_data(data);
	return 1;
      }

      new_str->next = data->strings;
      data->strings = new_str;
    }
    
    if (data->cl_used == data->cl_size) {
      data->cl_size *= 2;
      new_list = (phredpar_entry *) realloc(data->chem_list,
					    data->cl_size * sizeof(phredpar_entry));
      if (NULL == new_list) {
	free_phredpar_data(data);
	return 1;
      }

      data->chem_list = new_list;
    }

    strncpy(data->strings->chunk + data->strings->used, key, l_key);
    data->chem_list[data->cl_used].key     = data->strings->chunk + data->strings->used;
    data->strings->used += l_key + 1;
    strncpy(data->strings->chunk + data->strings->used, chem,    l_chem);
    data->chem_list[data->cl_used].chem    = data->strings->chunk + data->strings->used;
    data->strings->used += l_chem + 1;
    strncpy(data->strings->chunk + data->strings->used, dye,     l_dye);
    data->chem_list[data->cl_used].dye     = data->strings->chunk + data->strings->used;
    data->strings->used += l_dye + 1;
    strncpy(data->strings->chunk + data->strings->used, machine, l_machine);
    data->chem_list[data->cl_used].machine = data->strings->chunk + data->strings->used;
    data->strings->used += l_machine + 1;
    data->cl_used++;
  }

  fclose(fp_phredpar);
  
  qsort(data->chem_list, data->cl_used, sizeof(phredpar_entry), ppe_cmp);

  return 0;
}

static int init_phredpar_data(phredpar_data *data) {
  data->strings = NULL;
  data->chem_list = NULL;

  data->strings = (str_chunk *) calloc(1, sizeof(str_chunk));
  if (NULL == data->strings) return 1;
  data->strings->chunk = malloc(CHUNK_SIZE * sizeof(char));
  if (NULL == data->strings->chunk) {
    free(data->strings);
    return 1;
  }

  data->chem_list = (phredpar_entry *) malloc(10 * sizeof(phredpar_entry));
  if (NULL == data->chem_list) return 1;
  data->cl_used = 0;
  data->cl_size = 10;

  return 0;
}

static void free_phredpar_data(phredpar_data *data) {

  str_chunk *n;

  if (NULL == data) return;

  while (NULL != data->strings) {
    n = data->strings->next;
    free(data->strings);
    data->strings = n;
  }

  if (NULL != data->chem_list) {
    free(data->chem_list);
    data->chem_list = NULL;
    data->cl_used = 0;
    data->cl_size = 0;
  }
}

static int ppe_cmp(const void *aa, const void *bb) {
  phredpar_entry *a = (phredpar_entry *) aa;
  phredpar_entry *b = (phredpar_entry *) bb;

  return strcmp(a->key, b->key);
}

static int ppe_search(const void *aa, const void *bb) {
  char *a = (char *) aa;
  phredpar_entry *b = (phredpar_entry *) bb;

  return strcmp(a, b->key);
}

static void get_chem_info(Read *read, char **chem, char **dye,
			  phredpar_data *chem_list) {

  phredpar_entry *entry;
  char *c, *dyep;
  char s;

  *chem = "unknown";
  *dye  = "unknown";

  if (NULL == read->info) return;
  
  c = strstr(read->info, "DYEP=");
  if (NULL == c) return;

  c += 5;
  while (0 != *c && '\n' != *c && isspace((int) *c)) c++;

  dyep = c;
  while (0 != *c && '\n' != *c) c++;

  s = *c;
  *c = 0;

  entry = bsearch(dyep, chem_list->chem_list, chem_list->cl_used,
		  sizeof(phredpar_entry), ppe_search);

  *c = s;

  if (NULL == entry) return;

  *chem = entry->chem;
  *dye  = entry->dye;
}
