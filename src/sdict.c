/* $Id: sdict.c 15136 2005-06-16 09:03:12Z rmd $ */
/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 */

/* (C) Richard Mott, Sanger Centre 1995-1997 */

#include<stdio.h>
#include<ctype.h>
#include<math.h>
#include<string.h>
#include<malloc.h>
#include<errno.h>
#include<stdlib.h>
#include <unistd.h>
#include"cl.h"
#include"seq_util.h"
#include"tree.h"
#include"ncr.h"
#include"cmp.h"
#include"wordhash.h"

#define DBMAX 100000
#define IO_BUFFSIZE 1000

/* sdict will screen a database of sequences and file all pairs of sequences 
that share a common 15mer (the word size can be changed). It is 
designed as a pre-screen for doing Smith-Waterman comparisons on the resulting pairs. */

typedef struct
{
  int seq_id, word_id;
}
HP;

typedef struct
{
  int seq_id1, seq_id2, count;
}
HT;

int hp_cmp( HP *a, HP *b);
int ht_cmp( HT *a, HT *b);

int *exclude_oligos();

void dump_and_merge( char *, HT *, int , char * );

int
main( int argc, char **argv )
{
  int wordsize=15, seqs=50;
  int n, i, j, k, m;
  size_t s1, s2;
  WORD_DICT *dictionary;
  SEQUENCE *seq;
  char db[256], bufferfile[270], bufferfile2[550], bufferfile3[550], outfile[256], triple_file[270];
  HP *pbuffer, p1, p2, *list;
  HT *tbuffer, triple;
  FILE  *bfp, *obfp, *bfp1, *bfp2, *outfp, *fasta;
  int *exclude=NULL;
  int pbuf_size=0, tbuf_size=0, a, b, *count, *wcount;
  int tmax_buf, pmax_buf;
  float fmax_buf=60.0;
  int written=0, wmax=0;
  int files, list_index, threshold, pthresh=1;
  char **name;
  int list_size_threshold=-1;
  FILE *freqfp;
  char freqfile[256];
  FILE *ignorefp;
  char ignorefile[256];
  int *ignore;
  int ignored=0;
  int total=0;
  char tmpdir[256], mergefile[270];
  
/* command line parsing and initialisation */

  getint("-word", &wordsize, argc, argv ); /* wordsize for hashing */

  if ( wordsize > 15 ) {
    fprintf(stderr, "ERROR: wordsize %d is too big (max 15)\n", wordsize);
    exit(1);
  }
  fprintf(stderr, " wordsize: %d\n",wordsize);

  getfloat("-maxbuf",&fmax_buf,argc,argv); /* buf size in mbytes */
  tmax_buf = (int)(fmax_buf*1000000/sizeof(HT)); /* size in bytes */
  getint("-word_thresh", &list_size_threshold,argc,argv); /* min frequency of a word for printing out if required */
  getint("-pthresh", &pthresh,argc,argv); /* min number of words two seq need in common to be printed */

/* output file of frequent words */

  freqfile[0] = 0;
  if ( ! (freqfp = argfile("-freq","w",argc,argv,freqfile) ) )
    freqfp = NULL;

/* output file of pairs of sequences */

  outfile[0] = 0;
  if ( ! (outfp=argfile("-pairs", "w", argc, argv, outfile ) ) )
    {
      strcpy(outfile, "pairs");
      outfp = openfile(outfile,"w");
    }

/* input file of fasta seqs */

  fasta = argfile_force("-fasta","r",argc,argv,db);

/* tmp dir */

  strcpy(tmpdir, "./");
  getarg("-tmpdir", tmpdir, argc, argv );
  

  sprintf(bufferfile, "%s/buffer.temp", tmpdir );
  bfp = openfile(bufferfile,"w");


/* buffer memory allocation */

  tbuffer = NULL;
  while ( NULL == (tbuffer = (HT*)calloc(tmax_buf+10,sizeof(HT)) ) )
    tmax_buf /= 2;

  if ( ! tbuffer )
    {
      fprintf(stderr,"could not allocate buffer size %ld\n", tmax_buf*sizeof(HT));
      exit(1);
    }
  else
    fprintf(stderr," buffer size %ld\n", tmax_buf*sizeof(HT));

  pmax_buf = (tmax_buf*sizeof(HT))/sizeof(HP);
  pbuffer = (HP*)tbuffer; /* share buffer for pairs and triples */

  seqs = 1;

/* read in the optional table of words to ignore */

  ignorefile[0] = 0;
  if ( (ignorefp = argfile("-ignore","r",argc,argv,ignorefile)) )
    ignore = read_ignore_hash_table( ignorefp, &wordsize );
  else
    ignore = NULL;

  gethelp(argc,argv);

/* read in the sequences */

  if (  fasta )
    {
      name = (char**)calloc(DBMAX,sizeof(char*));

      while ( (seq = get_fasta_sq(fasta)) )
	{
	  name[seqs] = strdup(seq->name);
	  if ( ! ( seqs%1000) )   
	    {
	      fprintf(stderr,"%5d %-10s %5d\n", seqs, seq->name, seq->len );
	      fprintf(stderr,"%50.50s\n", seq->s );
	    }

	  /* make the word dictionary */

	  dictionary = (WORD_DICT*)make_dictionary_for_sequence( seq, wordsize, exclude, 0, 0 );

	  /* sort and remove duplicates */

	  qsort(dictionary->hit,dictionary->hits,sizeof(int),icmp);

	  m = 1;
	  for(n=1;n<dictionary->hits;n++)
	    if ( dictionary->hit[n] != dictionary->hit[n-1] )
	      dictionary->hit[++m] = dictionary->hit[n];

	  dictionary->hits = m+1;

	  /* remove common words */

	  if ( ignore )  
	    {
	      for(n=0;n<dictionary->hits;n++)
		{
		  if ( findEntry( dictionary->hit[n], ignore ) )
		    {
		      dictionary->hit[n] = -1;
		      ignored++;
		    }
		}
	      total += dictionary->hits;
	    }

	  /* create a list of pairs [ seqno, word_id ] in buffer */

	  for(n=0;n<dictionary->hits;n++)
	    {
	      if ( pbuf_size > pmax_buf )
		{
		  fwrite(pbuffer,sizeof(HP),pbuf_size,bfp);
		  written += pbuf_size;
		  pbuf_size = 0;
		}
	      if ( dictionary->hit[n] >= 0 )
		{
		  pbuffer[pbuf_size].seq_id = seqs;
		  pbuffer[pbuf_size].word_id = dictionary->hit[n];
		  pbuf_size++;
		}
	    }
	  free_dict(dictionary);

	  /* repeat for the complement of the sequence */

	  complement_seq(seq->s);

	  dictionary = (WORD_DICT*)make_dictionary_for_sequence( seq, wordsize, exclude, 0, 0 );

	  qsort(dictionary->hit,dictionary->hits,sizeof(int),icmp);
	  m = 1;
	  for(n=1;n<dictionary->hits;n++)
	    if ( dictionary->hit[n] != dictionary->hit[n-1] )
	      dictionary->hit[++m] = dictionary->hit[n];

	  dictionary->hits = m+1;

	  if ( ignore )  /* remove common words */
	    {
	      for(n=0;n<dictionary->hits;n++)
		if ( findEntry( dictionary->hit[n], ignore ) )
		  {
		    dictionary->hit[n] = -1;
		    ignored++;
		  }
	      total += dictionary->hits;
	    }

	  for(n=0;n<dictionary->hits;n++)
	    {
	      if ( pbuf_size > pmax_buf ) /* flush the buffer to disk */
		{
		  fwrite(pbuffer,sizeof(HP),pbuf_size,bfp);
		  written += pbuf_size;
		  pbuf_size = 0;
		}

	      if ( dictionary->hit[n] >= 0 )
		{
		  pbuffer[pbuf_size].seq_id = -seqs;
		  pbuffer[pbuf_size].word_id = dictionary->hit[n];
		  pbuf_size++;
		}
	    }
	  free_dict(dictionary);

	  free_seq(seq);
	  seqs++;
	}

      seqs--;
      fprintf(stderr,"Total seqs: %d\n", seqs );

/* flush the pair buffer */

      if ( pbuf_size > 0 )
	{
	  fwrite(pbuffer,sizeof(HP),pbuf_size,bfp);
	  written += pbuf_size;
	  pbuf_size = 0;
	}

      fclose(bfp);

      fprintf(stderr,"%d pairs written\n", written);

/* get the threshold for word frequencies, if not defined on the command-line */

      if ( list_size_threshold == -1 )
	{
	  list_size_threshold=0.01*seqs;
	  if ( list_size_threshold < 20 )
	    list_size_threshold=20;
	}
      fprintf(stderr,"list_size_threshold %d\n", list_size_threshold );

      if ( ignore )
	fprintf(stderr,"total words: %d, ignored: %d %.2f%%\n", total, ignored, (100.0*ignored)/(total+1.0) );

      fprintf(stderr,"resorting and splitting buffer file %s\n", bufferfile);

      bfp = fopen(bufferfile,"r");
      
      n=0;
      do
	{
	  pbuf_size=fread(pbuffer,sizeof(HP),pmax_buf,bfp);
	  if ( pbuf_size > 0 )
	    {
	      qsort(pbuffer,pbuf_size,sizeof(HP),(void *)hp_cmp);
	      sprintf(bufferfile2,"%s/%s.%d",tmpdir, bufferfile,n);
	      obfp = fopen(bufferfile2,"w");
	      fwrite(pbuffer,sizeof(HP),pbuf_size,obfp);
	      fclose(obfp);
	      fprintf(stderr,"%s %d\n",bufferfile2,pbuf_size);
	      n++;
	    }
	}
      while(pbuf_size>0);
      
      files=n;
      fprintf(stderr,"merging files\n");
      
      while(files > 1 )
	{
	  m = files;
	  for(i=0;i<files/2;i++)
	    {
	      sprintf(bufferfile2,"%s/%s.%d",tmpdir,bufferfile,i);
	      bfp1 = fopen(bufferfile2,"r");
	      j = files-1-i;
	      
	      sprintf(bufferfile3,"%s/%s.%d",tmpdir,bufferfile,j);
	      bfp2 = fopen(bufferfile3,"r");
	      
	      fprintf(stderr,"merging %s %s to %s\n", bufferfile2, bufferfile3, bufferfile2);
	      
	      sprintf(mergefile,"%s/merged", tmpdir );
	      obfp = fopen(mergefile,"w");

	      s1=fread(&p1,sizeof(HP),1,bfp1);
	      s2=fread(&p2,sizeof(HP),1,bfp2); 

	      while( s1>0 || s2>0 )
		{
		  n = p1.word_id-p2.word_id;
		  if ( n < 0 )
		    {
		      fwrite(&p1,sizeof(HP),1,obfp); 
		      s1=fread(&p1,sizeof(HP),1,bfp1); 
		    }
		  else if ( n > 0 )
		    {
		      fwrite(&p2,sizeof(HP),1,obfp); 
		      s2=fread(&p2,sizeof(HP),1,bfp2);
		    }
		  else
		    {
		      fwrite(&p2,sizeof(HP),1,obfp); 
		      s2=fread(&p2,sizeof(HP),1,bfp1);

		      fwrite(&p1,sizeof(HP),1,obfp); 
		      s1=fread(&p1,sizeof(HP),1,bfp2);
		    }
		}
	      
	      fclose(bfp1);
	      fclose(bfp2);
	      fclose(obfp);
	      unlink(bufferfile2);
	      unlink(bufferfile3);
	      rename("merged",bufferfile2);
	      m--;
	    }
	  files = m;
	}
      
      fprintf(stderr,"extracting sequence pairs\n");
      sprintf(bufferfile2,"%s.%d",bufferfile,0);
      bfp = fopen(bufferfile2,"r");

      sprintf(triple_file,"%s/merged.tmp",tmpdir);
      fprintf(stderr,"using %s as overflow\n", triple_file);
      
      fclose(fopen(triple_file,"w")); /* create the file */

      list_index = 0;
      tbuf_size = 0;
      m = 0;
      list = (HP*)calloc(DBMAX,sizeof(HP));
      count = (int*)calloc(DBMAX,sizeof(int));
      wcount = (int*)calloc(2*DBMAX,sizeof(int));
      
      threshold = 0.9*tmax_buf;

      /* read in the HP pairs, sorted by word */

      while((s1=fread(&list[list_index],sizeof(HP),1,bfp))>0)  
	{
	  if ( list_index == 0 || (n=(list[list_index].word_id == list[list_index-1].word_id ))) /* same word as previous */
	    {
	      list_index++; /* list_index is the count of the number of times the word is listed */
	    }
	  else if ( list_index > 1 && ! n ) /* different word, so process the preceeding list */
	    {
	      wcount[list_index]++; /* wcount[N] count the number of words that occur in exactly N sequences */
	      if ( list_index > wmax )
		wmax = list_index;
	      

/* 		fprintf(stderr,"%-20s %8d %10d   %d %u\n", int2word(list[0].id2, wordsize), list_index, list[0].id2, list_size_threshold, freqfp ); */

	      if ( freqfp && ( list_index > list_size_threshold ) ) /* optionally print the word and its frequency */
		{
		  int error;
/*		  fprintf(stderr,"here %-20s %8d %10d   %d %u\n", int2word(list[0].id2, wordsize), list_index, list[0].id2, list_size_threshold, freqfp ); */
		  error = fprintf(freqfp,"%-20s %8d %10d\n", int2word(list[0].word_id, wordsize), list_index, list[0].word_id );
		  if ( error < 0 )
		    {
		      fprintf(stderr, "error %d errno %d ", error, errno );
		      perror(" sdict ");
		    }
		}
	      /* create a list of all the triples [seq1, seq2, word] */

	      for(i=0;i<list_index;i++) 
		for(j=0;j<i;j++)
		  {
		    if ( list[i].seq_id > 0 || list[j].seq_id > 0 ) /* at least one read must be forwards */
		      {
			if ( list[i].seq_id < list[j].seq_id ) /* store the sequence ids sorted */
			  {
			    a = list[i].seq_id;
			    b = list[j].seq_id;
			  }
			else
			  {
			    b = list[i].seq_id;
			    a = list[j].seq_id;
			  }
			
			tbuffer[tbuf_size].seq_id1 = a;
			tbuffer[tbuf_size].seq_id2 = b;
			tbuffer[tbuf_size].count = 1;
			tbuf_size++;
			m++;
			
			if ( tbuf_size > threshold ) /* buffer is nearly full so sort and remove duplicates  */
			  {
			    dump_and_merge( triple_file, tbuffer, tbuf_size, tmpdir );
			    tbuf_size = 0; /* empty the buffer */
			  }
		      }
		}
	      list[0] = list[list_index]; /* copy the new word to the head of the list */
	      list_index = 1; /* WAS ZERO!!*/
	    }
	  else if ( list_index == 1 && ! n ) /* list of length 1 - a unique word */
	    {
	      wcount[1]++; /* keep a count of unique words */
	      list[0] = list[list_index];
	      list_index = 1;
	    }
	}

      /* process the last block of data. No need to test if it differs */
      
      if ( list_index > 1  ) /* different word, so process the preceeding list */
	{
	  wcount[list_index]++;	/* wcount[list_index] count the number of words that occur in exactly list_index sequences */
	  if ( list_index > wmax )
	    wmax = list_index;
	  
	  if ( freqfp && ( list_index+1 > list_size_threshold ) ) /* optionally print the word and its frequency */
	    fprintf(freqfp,"%-20s %8d %10d\n", int2word(list[0].word_id, wordsize), list_index+1, list[0].word_id );
	  
	  for(i=0;i<list_index;i++) /* create a list of all the triples [seq1, seq2, word] */
	    for(j=0;j<i;j++)
	      {
		if ( list[i].seq_id > 0 || list[j].seq_id > 0 ) /* at least one read must be forwards */
		  {
		    if ( list[i].seq_id < list[j].seq_id ) /* store the sequence ids sorted */
		      {
			a = list[i].seq_id;
			b = list[j].seq_id;
		      }
		    else
		      {
			b = list[i].seq_id;
			a = list[j].seq_id;
		      }
		    
		    tbuffer[tbuf_size].seq_id1 = a;
		    tbuffer[tbuf_size].seq_id2 = b;
		    tbuffer[tbuf_size].count = 1;
		    tbuf_size++;
		    m++;
		    
		    if ( tbuf_size > threshold ) /* buffer is nearly full so sort and remove duplicates  */
		      {
			dump_and_merge( triple_file, tbuffer, tbuf_size, tmpdir );
			tbuf_size = 0; /* empty the buffer */
		      }
		  }
	      }
	}
      else if ( list_index == 1 ) /* list of length 1 - a unique word */
	{
	  wcount[1]++;		/* keep a count of unique words */
	}
      

      if ( freqfp )
	{
	  fprintf(stderr, "writing word frequencies to %s.\n", freqfile );
	  fflush(freqfp);
	  fclose(freqfp);
	  if ( ! outfp ) 
	    exit(0);
	}

      fprintf(stderr,"pairs: %d\n", m );


      /* flush the tbuffer */
      dump_and_merge( triple_file, tbuffer, tbuf_size, tmpdir );
      tbuf_size = 0;

      fprintf(stderr,"new size %d\n", tbuf_size);
      written = 0;

      /* read in from the temporary file */

      bfp = fopen( triple_file, "r" );
      while( fread( &triple, sizeof(HT), 1, bfp ) > 0 )
	{
	  if ( triple.count > pthresh )
	    {
	      if ( triple.seq_id2 < 0 )
		fprintf(outfp, "-1 %-20s ",  name[-triple.seq_id2] );
	      else
		fprintf(outfp, "+1 %-20s ",  name[triple.seq_id2] );

	      if ( triple.seq_id1 < 0 )
		fprintf(outfp, "-1 %-20s ",  name[-triple.seq_id1] );
	      else
		fprintf(outfp, "+1 %-20s ",  name[triple.seq_id1] );
	      fprintf(outfp," %5d\n", triple.count );
	      written++;
	    }
	  count[triple.seq_id1]++;
	  count[triple.seq_id2]++;
	}
      
      fprintf(stderr,"%d pairs exceed threshold %d\n", written, pthresh);
      
      fprintf(stderr,"distribution of word hits:\n");
      n = 0;
      m = 0;
      for(i=0;i<=wmax;i++)
	{
	  n += wcount[i];
	}
      j = 0;
      for(i=0;i<=wmax;i++)
	{
	  j += wcount[i];
	  k = wcount[i]*(i*(i-1))/2;
	  m += k;
	  fprintf(stderr,"%5d %5d %5d %10d %8.4f\n", i, wcount[i], k, m, j/(n+1.0e-10));
	}
      
      unlink(bufferfile);
      unlink(bufferfile2);
    }
  if ( outfp )
    {
      fflush(outfp);
      fclose(outfp);
    }
return 0;
}

int 
hp_cmp( HP *a, HP *b)
{
  int n = a->word_id- b->word_id;
  if ( ! n )
    n = a->seq_id - b->seq_id;
  return n;
}

int 
ht_cmp( HT *a, HT *b)
{
  int n = a->seq_id2- b->seq_id2;
  if ( ! n )
    n = a->seq_id1 - b->seq_id1;
  return n;
}


void dump_and_merge( char *infile, HT *tbuffer, int tbuf_size, char *tmpdir )
{
  FILE *input;
  FILE *output;
  int i, j, n, read_ok;
  char tmpfile[256];
  HT triple;
  int written = 0;

/* sort and flatten */

  qsort(tbuffer,tbuf_size,sizeof(HT),(void *)ht_cmp);
  j = 0;
  for(i=1;i<tbuf_size;i++)
    {
      if ( ht_cmp( &tbuffer[i], &tbuffer[i-1] ) )
	tbuffer[++j] = tbuffer[i];
      else
	tbuffer[j].count+=tbuffer[i].count;
    }
  tbuf_size = j+1;
  fprintf(stderr,"new size %d\n", tbuf_size);
  
  input = fopen(infile,"r"); /* open the existing file (which may be empty) */
  strcpy(tmpfile,tempnam(tmpdir,"sdict")); /* temp filename */
  output = fopen(tmpfile,"w"); /* new file for merged data */
		  
  i = 0;

  read_ok = ( fread( &triple, sizeof(HT), 1, input ) > 0); 

  fprintf(stderr,"dumping: tbuf_size %d\n", tbuf_size);

  while( read_ok  && i < tbuf_size )
    {
      if ( !( n = ht_cmp( &triple, &tbuffer[i] ) ) )
	{
	  triple.count += tbuffer[i].count;
	  fwrite(&triple,sizeof(HT),1,output);
	  written++;
	  i++;

	  read_ok = ( fread( &triple, sizeof(HT), 1, input ) > 0); 
	}
      else if ( n > 0 )
	{
	  fwrite(&tbuffer[i],sizeof(HT),1,output);
	  written++;
	  i++;
	}
      else
	{
	  fwrite(&triple,sizeof(HT),1,output);
	  written++;

	  read_ok = ( fread( &triple, sizeof(HT), 1, input ) > 0); 
	}
    }
  if ( i < tbuf_size )
    {
      written += tbuf_size-i;
      fwrite( &tbuffer[i],sizeof(HT),tbuf_size-i,output);
    }
  else 
    {
      while ( read_ok )
	{
	  fwrite(&triple,sizeof(HT),1,output);
	  written++;

	  read_ok = ( fread( &triple, sizeof(HT), 1, input ) > 0); 
	}
    }
  
  fclose(input);
  fclose(output);

  fprintf(stderr,"%d triples written\n", written );

  unlink(infile); /* delete the old file */
  rename(tmpfile,infile);   /* swap the dump file names */
}


