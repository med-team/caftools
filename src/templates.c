/*
 * This software has been created by Genome Research Limited (GRL). 
 * GRL hereby grants permission to use, copy, modify and distribute 
 * this software and its documentation for non-commercial purposes 
 * without fee at the user's own risk on the basis set out below.
 * GRL neither undertakes nor accepts any duty whether contractual or 
 * otherwise in connection with the software, its use or the use of 
 * any derivative, and makes no representations or warranties, express
 * or implied, concerning the software, its suitability, fitness for
 * a particular purpose or non-infringement.
 * In no event shall the authors of the software or GRL be 
 * responsible or liable for any loss or damage whatsoever arising in
 * any way directly or indirectly out of the use of this software or 
 *  its derivatives, even if advised of the possibility of such damage.
 * Our software can be freely distributed under the conditions set 
 * out above, and must contain this copyright notice.
 *
 * $Id: templates.c 20877 2007-06-08 08:59:00Z rmd $
 * $Log$
 * Revision 1.3  2003/07/25 14:54:57  dgm
 * Updated comments to remove nested comments.
 *
 * Revision 1.2  2003/07/25 11:16:33  rmd
 * Changes for new version of autoconf.
 * Stop cafcat exploding if you try to write a contig with no assembled_from
 * Added -depad and -template_info options to templates.
 *
 * Revision 1.1  2002/03/04 17:00:20  rmd
 * Converted to use autoconf and automake.
 * Added acedb array package in src/acedb to make whole thing easier to install.
 * Quite a few changes to remove compiler warnings.
 *
 */

#include <stdio.h>
#include <caf.h>
#include <cl.h>

#define min(A,B) ((A)<(B)?(A):(B))
#define max(A,B) ((A)>(B)?(A):(B))

typedef struct {
  int read_id;
  int contig_start;
  int contig_end;
  int read_start;
  int read_end;
  int dirn;
} READ_POS_INFO;

static int rp_compare(const void *a, const void *b);
static void write_templates(cafAssembly * caf, int template_info);


int main(int argc, char **argv)
{
  cafAssembly *CAF;                    /* CAF file data       */
  FILE *fp = 0;                        /* CAF file to read in */
  char caffile[1024] = {0};            /* CAF file to read    */
  int  depad = 0;                      /* Depad caf file      */
  int  template_info = 0;              /* Print library and insert size */
  strcpy(caffile, "stdin");

  /* process options */

  getboolean("-depad",         &depad,         argc, argv);
  getboolean("-template_info", &template_info, argc, argv);
  fp = argfile("-caf", "r", argc, argv, caffile);
  gethelp(argc, argv);

  /* read in the caf file */

  if (!fp) {
    fp = stdin;
    strcpy(caffile, "stdin");
  }

  CAF = readCafAssembly(fp, caffile, NULL);
  
  /* pad it */
  if (depad) {
    requireUnpadded(CAF);
  } else {
    requirePadded(CAF);
  }

  /* produce output */

  write_templates(CAF, template_info);

  return 0;
}

static void write_templates(cafAssembly * caf, int template_info)
{
  Array   seqs;            /* Array of CAFSEQ objects in caf */
  CAFSEQ *contig;          /* Current contig CAFSEQ object */
  CAFSEQ *read;            /* Current read CAFSEQ object */
  ASSINF *af;  /* Assembled_from for current contig */
  Array   read_positions;  /* Where reads are in a contig */
  Array   reads_seen;      /* Reads in the current contig */
  READ_POS_INFO *rp;       /* READ_POS_INFO struct for current read */
  int     num_reads;       /* Number of reads in contig */
  int     read_num;        /* Index of read in read_positions */
  int     i, j;

  seqs = caf->seqs;

  reads_seen     = arrayCreate(arrayMax(seqs) + 1, int);
  read_positions = arrayCreate(arrayMax(seqs) + 1, READ_POS_INFO);

  for (i = 0; i < arrayMax(seqs); i++) {
    contig = arrp(seqs, i, CAFSEQ);

    /* Ignore non-contigs */
    if (contig->type != is_contig) continue;

    if (!contig->assinf) {
      fprintf(stderr,
	      "WARNING: no reads attached to contig %s\n",
	      dictName(caf->seqDict, i));
      continue;
    }

    num_reads = 0;
    for (j = 0; j < arrayMax(contig->assinf); j++) {
      af = arrp(contig->assinf, j, ASSINF);

      if (!arr(reads_seen, af->seq, int)) {
	arr(reads_seen, af->seq, int) = ++num_reads;

	read_num = num_reads - 1;

	rp = arrp(read_positions, read_num, READ_POS_INFO);

	rp->read_id = af->seq;
	rp->contig_start = min(af->s1, af->s2) - 1;
	rp->contig_end   = max(af->s1, af->s2);
	rp->read_start   = af->r1 - 1;
	rp->read_end     = af->r2;
	rp->dirn         = af->s2 - af->s1;

      } else {

	read_num = arr(reads_seen, af->seq, int) - 1;

	rp = arrp(read_positions, read_num, READ_POS_INFO);
	if (!rp->dirn) rp->dirn = af->s2 - af->s1;
	rp->contig_start = min(rp->contig_start, min(af->s1, af->s2) - 1);
	rp->contig_end   = max(rp->contig_end,   max(af->s1, af->s2));
	rp->read_start   = min(rp->read_start,   af->r1 - 1);
	rp->read_end     = max(rp->read_end,     af->r2);
      }
    }

    /* Sort into output order */
    qsort(arrp(read_positions, 0, READ_POS_INFO), num_reads,
	  sizeof(READ_POS_INFO), rp_compare);

    /* Print out results */
    
    for (j = 0; j < num_reads; j++) {
      rp = arrp(read_positions, j, READ_POS_INFO);

      read = arrp(seqs, rp->read_id, CAFSEQ);

      printf("%-15s %-15s %-15s %-15s %5d %5d %5d %5d %-15s %-15s",
	     dictName(caf->seqDict, i),
	     dictName(caf->seqDict, rp->read_id),
	     dictName(caf->templateDict, read->template_id),
	     (read->strand == forward_strand ? "forward" : "reverse"),
	     (rp->dirn > 0 ? rp->contig_start + 1 : rp->contig_end),
	     (rp->dirn > 0 ? rp->contig_end       : rp->contig_start + 1),
	     rp->read_start + 1,
	     rp->read_end,
	     (rp->dirn > 0 ? "positive" : "negative"),
	     dye_strings[read->dye]);

      if (template_info) {

	printf(" %-10s %5d %5d\n",
	       dictName(caf->ligationDict, read->ligation_no),
	       read->insert_size1,
	       read->insert_size2);

      } else {

	printf("\n");

      }
    }
  }
}

static int rp_compare(const void *a, const void *b)
{
  return (((const READ_POS_INFO *) a)->contig_start
	  - ((const READ_POS_INFO *) b)->contig_start);
}
